<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Videos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['video_url', 'poem_name', 'cat_type','item_id','status'];

    public function getBooks()
    {
        return $this->belongsTo(JayanthiEvents::Class,'item_id', 'id');
    }

     public static function removeVideos($id)
    {
        $file = Videos::findOrFail($id);

      
        Videos::destroy($id);

        return true;
    }

    

}
