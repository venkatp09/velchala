<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;
use App\Models\Photos;
class JayanthiEvents extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['event_name', 'event_url','img','event_date','meta_title','meta_keywords','meta_description','event_location','event_des'];

    public function getVideos()
    {
        return $this->hasMany(Videos::Class,'item_id', 'id');
    }
    public function getPhotos()
    {
        return $this->hasMany(Photos::Class,'event_name', 'id');
    }
    public function getUser()
    {
        return $this->belongsTo(User::Class, 'blog_user_id');
    }

    public static function removeEvent($id)
    {
        $file = JayanthiEvents::findOrFail($id);
         
        if ($file->img != '') {
            File::delete('theme/uploads/events/' . $file->img);
        }
        
        Photos::where("event_name",$id)->where("cat_type",'3')->delete();
        JayanthiEvents::destroy($id);

        return true;
    }

    public static function imageUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('theme/uploads/events/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


        $extension = $inputname->getClientOriginalName();
        $fileName = time() . $extension;
        $inputname->move($uploadPath, $fileName);

        return $fileName;
    }

   

}
