<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'oitem_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'oitem_order_id',
        'oitem_delivery_date',
        'oitem_delivery_time',
        'oitem_message',
        'oitem_nameongift',
        'oitem_product_id',

        'oitem_item_sku_id',
        'oitem_item_sku_vendor_price',
        'oitem_product_sku_site_price',
        'oitem_item_sku_vdc_commission_type',
        'oitem_item_sku_vdc_commission_value',
        'oitem_item_sku_vdc_final_price',
        'oitem_item_sku_store_discount_type',
        'oitem_item_sku_store_discount_value',
        'oitem_item_sku_store_price',

        'oitem_product_name',
        'oitem_qty',
        'oitem_product_price',
        'oitem_discount_price',
        'oitem_discount_type',
        'oitem_sub_total',
        'oitem_note',
        'oitem_type',
        'oitem_status',
        'oitem_delivery_charge',
        'oitem_product_options',
        'oitem_payment_status'];

    public function getProduct()
    {
        return $this->belongsTo(Products::Class, 'oitem_product_id');
    }

    public function getOrder()
    {
        return $this->belongsTo(Orders::Class, 'oitem_order_id');
    }
}
