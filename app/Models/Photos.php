<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;

class Photos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photos';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['status', 'cat_type','event_name','img','id'];

    public function getBooksPhotos()
    {
        return $this->belongsTo(JayanthiEvents::Class,'event_name', 'id');
    }
     public static function removePhots($id)
    {
        $file = Photos::findOrFail($id);

        if ($file->img != '') {
            File::delete('theme/uploads/photos/' . $file->img);
        }
        Photos::destroy($id);

        return true;
    }
}
