<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Models\Videos;

class Products extends Model
{
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    public static $adminEmail = 'info@velchala.com';
//    public static $adminEmail = 'gogikarsairam@gmail.com';


    protected $fillable = [
        'publication_name',
        'img',
        'price',
        'stock',

        'pages',
        'author',

        'publisher_id',
        'sku_code',
        'dis_price',

        'publication_slug',

        'language',
        'description',
        'published_date',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'publications_status',
        'pdf_file',
        
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'publications';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'b_id';


    public function getCategory()
    {
        return $this->belongsTo(Categories::Class, 'publisher_id', 'id');
    }


   


    public static function createProduct($request)
    {

        $returnarray = array(
            'status' => 'start',
            'values' => '',

        );
        $requestData = $request->all();
         if ($requestData['b_id'] != '') {

                $products = Products::findOrFail($requestData['b_id']);
                }           
        if ($requestData['b_id'] == '') {
            
            $validatedData = $request->validate([

                'publication_name' => 'required|unique:publications',
                'publisher_id' => 'required',
                'published_date' => 'required',
                'img' => 'required',
                'price'=> "required|integer",
                
                
            ], [
                'publication_name.required' => 'Name is required',
                'publication_name.unique' => 'This product name is already exist',
                'publisher_id.required' => 'Name is required',
                'stock.required' => 'Name is required',
                'img.required' => 'Book Profile Pic Upload Required',
                'price'=> "Price should be integer",
                'dis_price.required' => 'Should Be less than to Price',
            ]);
            
           
            if($requestData['img']->getClientOriginalName()!=""){
            $extension = str_replace(" ","-",$requestData['img']->getClientOriginalName());
            $fileName = time() . $extension;
            $uploadPath = 'theme/uploads/publications/';
            $requestData['img']->move($uploadPath, $fileName);
            $requestData['img']=$fileName;
            }else{
             $requestData['img']=$requestData['old_img'];

            }

            if($requestData['pdf_file']->getClientOriginalName()!=""){
            $extension = str_replace(" ","-",$requestData['pdf_file']->getClientOriginalName());
            $fileName = time() . $extension;
            $uploadPath = 'theme/uploads/publications/';
            $requestData['pdf_file']->move($uploadPath, $fileName);
            $requestData['pdf_file']=$fileName;
            }else{
             $requestData['pdf_file']=$requestData['old_pdf_file'];

            }
           
            
            $requestData['publication_slug'] = str_slug($requestData['publication_name']);
            $requestData['publications_status'] = 0;
            $productData = Products::create($requestData);
            $product_id = $productData->b_id;
             if(isset($requestData['book_videos'])){
            $book_videosCount = count($requestData['book_videos']);
            for($i=0;$book_videosCount>$i;$i++){
               
                 DB::table('video')->insert(
                    array(
                   
                    'video_url'    =>   $requestData['book_videos'][$i],
                    'poem_name'   =>    $requestData['book_videos_name'][$i],
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   2,
                    'item_id'      =>   $product_id
                    )
                );
            }
             }

            $productUpdate['sku_code'] = 'VCL-' . $product_id;
            $productInfo = Products::where('b_id', $product_id)->update($productUpdate);
            $returnarray['status'] = 'created';
            $returnarray['values'] = $productData;

        } else {
            
            
            $requestData['publication_slug'] = str_slug($requestData['publication_name']);
            if($request->has('img')){
                
            $extension = str_replace(" ","-",$requestData['img']->getClientOriginalName());
            $fileName = time() . $extension;
            $uploadPath = 'theme/uploads/publications/';
            $requestData['img']->move($uploadPath, $fileName);
            $requestData['img']=$fileName;
            }else{
             $requestData['img']=$requestData['old_img'];
             unset($requestData['old_img']);
            }
            if($request->has('pdf_file')){
            $extension = str_replace(" ","-",$requestData['pdf_file']->getClientOriginalName());
            $fileName = time() . $extension;
            $uploadPath = 'theme/uploads/publications/';
            $requestData['pdf_file']->move($uploadPath, $fileName);
            $requestData['pdf_file']=$fileName;
            }else{
             $requestData['pdf_file']=$requestData['old_pdf_file'];

            }
            $product_id = $requestData['b_id'];
            if($requestData['stock']=="out_of_stock"){

                $requestData['publications_status'] = 0;
                
            }
            $products->update($requestData);
            if(isset($requestData['book_videos'])){
            $book_videosCount = count($requestData['book_videos']);
             Videos::where('item_id', $product_id)->delete();
           
            for($i=0;$book_videosCount>$i;$i++){

               
                 DB::table('video')->insert(
                    array(
                   
                    'video_url'    =>   $requestData['book_videos'][$i],
                    'poem_name'   =>    $requestData['book_videos_name'][$i],
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   2,
                    'item_id'      =>  $product_id
                    )
                );
            }
            }
           
            $returnarray['status'] = 'updated';
            $returnarray['values'] = $product_id;
        }


        return $returnarray;
    }


   


}
