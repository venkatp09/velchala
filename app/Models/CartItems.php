<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;

class CartItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart_items';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['publ_id', 'cart_id','quanity','price'];

    public function getBooks()
    {
        return $this->belongsTo(Products::Class,'b_id', 'publ_id');
    }
    
}
