<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Contactus extends Model
{

    public static $adminEmail = 'info@velchala.com';


    protected $table = 'contactus';

    protected $primaryKey = 'cu_id';


    protected $fillable = [
        
        'first_name',
        'email',
        'phone_number',
        'subject',
        'message',
        'login_user_id',
        'book_stockist',
        'city_name'
    ];


    public static function contactUsEmails($conatct_id)
    {

        $contactData = Contactus::where('cu_id', $conatct_id)->first();


//        AdminEmail

        $data['emailData'] = $contactData;

        Mail::send('emails_templates.pages.contactus_for_user', $data, function ($message) use ($contactData) {

            $message->from(self::$adminEmail, 'velchala.com');
            $message->to($contactData->email);
            $message->subject("Thank you for contacting us!");
        });

//        userEmail

        Mail::send('emails_templates.pages.contactus_for_admin', $data, function ($message) use ($contactData) {

            $message->from($contactData->email, $contactData->first_name );
             $message->to('venkatp06@gmail.com');
            $message->to(self::$adminEmail);
            //Add a subject
            $message->subject("Contact Form Message From " . $contactData->first_name);
        });


//        dd($contactData);

    }

}
