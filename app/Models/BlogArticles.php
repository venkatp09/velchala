<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;
class BlogArticles extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blog_articles';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['blog_article_name', 'blog_article_pdf','blog_article_url','blog_article_desc','meta_title','meta_keywords','meta_description','blog_status'];

    public static function imageUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('theme/uploads/blogs_articles/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


    $extension = str_replace(" ","",$inputname->getClientOriginalName());
    $fileName = time() . $extension;
    $inputname->move($uploadPath, $fileName);

        return $fileName;
    }
    public static function removeArticles($id)
    {
        $file = BlogArticles::findOrFail($id);

        if ($file->blog_article_pdf != '') {
            File::delete('theme/uploads/blogs_articles/' . $file->blog_article_pdf);
        }
        BlogArticles::destroy($id);

        return true;
    }
}
