<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;

class Pincodes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pincodes';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pin_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pincode','address','price','status'];

    public static function removePincodes($id)
    {
        $file = Pincodes::findOrFail($id);
        Pincodes::destroy($id);
        return true;
    }

    
}
