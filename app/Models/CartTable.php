<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;

class CartTable extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'total_price'];

    public function getCartItems()
    {
        return $this->belongsTo(CartItems::Class,'cart_id', 'id');
    }
    
}
