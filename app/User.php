<?php

namespace App;

use App\Models\VendorAccount;
use App\Notifications\EmailVerificationNotification;
use App\Notifications\MailResetPasswordNotification;
use App\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\File;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'gender', 'dob', 'role', 'status', 'image', 'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function vendorAccount()
    {
        return $this->belongsTo(VendorAccount::Class, 'id', 'va_vendor_id');
    }



    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new EmailVerificationNotification());
    }


    public static function uploadProfilePic($inputName, $oldImage = null)
    {
        $uploadPath = public_path('/theme/uploads/user_profile/');
        $thumbPath = public_path('/theme/uploads/user_profile/');

        if (!file_exists($thumbPath)) {
            mkdir($thumbPath, 0777, true);
        }
        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        $extension = $inputName->getClientOriginalName();

        $fileName = time() . $extension;


        $thumb_img = \Intervention\Image\Facades\Image::make($inputName->getRealPath())->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
        });
        $thumb_img->save($thumbPath . '/' . $fileName, 60);

        $inputName->move($uploadPath, $fileName);

        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
                File::delete($thumbPath  . $oldImage);
            }
        }


        return $fileName;
    }

}
