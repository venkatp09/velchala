<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;


use App\Models\Categories;
use App\Models\Products;
use App\Models\Ratings;
use App\Models\SendEnquiry;

use App\Models\Magazines;
use App\Models\Subscriptions;
use App\Models\Photos;
use App\Models\Poems;
use App\Models\Videos;
use App\Models\JayanthiEvents;
use App\Models\BlogNews;
use App\Models\BlogArticles;
use App\Models\CartItems;
use App\Models\CartTable;

//use \Darryldecode\Cart\Cart;

use App\Models\Blogs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Mail;
use Cart;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('IsVendor');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $req)
    {


        $data = array();
        $data['active_menu'] = 'home';
        if($req->segment("1")=="telugu"){

             $req->session()->put('lang',"telugu");
             
        }
        elseif($req->segment("1")=="english"){

             Session::forget('lang');
        }
//        ::with('getProducts.productImages')

        $data['publications'] = Products::where('publications_status',1)->where('home_page',1)->get();
           
         $data['photos'] = DB::table('photos')
                           // ->join('events', 'events.id', '=', 'photos.event_name')
                ->select('*')->orderBy('id',"desc")->limit('12')->get();

        $data['sub_active_menu'] = 'home';
        
        $data['wishlistProducts'] = getWishlistProducts();
        if( $req->session()->get('lang')=="telugu"){
        
        
        $data['homePoemsTelugu']=array(
        array("
        రోజా! <br>
        మేమెంతో  ఆశ్చర్యపోతాం నిను చూచి<br>
        నీవేల ఉండగలుగుతున్నావా అంట ప్రసన్నంగా<br>
        ఆ అనేకానేక వారాల మధ్యా పొరల మధ్యా<br>
        నీ చుట్టూరా కావలి కాస్తున్న ఆ ముళ్ల మధ్యా<br>
        ఎలా ఉండగలుగుతుంది నీ గర్భగుడి అంత ప్రసన్నంగా వాటిలో యని"),

        array("
        బ్రతుకుకర్థం తెలియాలంటే<br>
        బ్రతుకు లోతుల్లోకి తొంగి చూడు.<br>
        నీవు జీవించిన జీవితపు పుటలు,<br>
        నీకే కాస్త నిదానంగ నిలచి తిరిగేసి చూడు."),

        array("
        నేనెపుడూ ఆలోచిస్తాను ఆలోచించని వాళ్ల గురించి,<br>
        నేనాలోచించీ ఆలోచించీ అలసిపోతాను,<br>
        వారూ ఆలోచించక కూడా అలసినట్టగడపడతారు నాకు.<br>
        చాలానాళ్ళకు తెలిసింది నాకు ఆలోచించకపోతే కూడా<br>
        అలసిపోతారు మనుషులని.<br>
        అవును, 'కారు' మరీ వాడితే అలసిపోతుంది,<br>
        వాడకపోతే తుప్పు పడుతుంది,<br>
        మనిషి కూడా అంతే మరి!"
        ),

        array("
        తరచి చూస్తే నాకు అన్నీ మతాల్లాగే అనిపిస్తాయి.<br>
        మనుషుల్లో ఎవరి మతం వారిదిలాగే<br>
        సంఘాల్లో కూడా ఎవరి అభిమతాలు వారివే.<br>
        కొన్ని మతాల్లో దేవుని పూజ ఉంటుంది<br>
        కొన్ని మతాల్లో మనుషుల పూజ.<br>
        పూజలు అన్నికీ సమానమే, 'భజనలూ'<br>
        'ఆచారాలూ' 'హారాచారాలూ'<br>
        భక్తి పూజకు కూడా, భుక్తి పూజకు కూడా."
        ),

        array("
        విద్యావంతుడు విద్యలేని వాన్నెప్పుడూ దోపిడీ చేశాడు,<br>
        చరిత్ర చెబుతుంది, అనుభవమూ చెబుతుంది,<br>
        'సంపత్తి కాదు' విద్యే దోపిడికి అసలు కారణమని,<br>
        విద్య ఎంత ఎక్కువయితే అంత దోపిడీ ఎక్కువవుతుందని.<br>
        తరచి చూడండి మీకు చరిత్ర చెబుతుంది.<br>
        అలా అని విద్యక్కరలేదని కాదు అంటున్నది,<br>
        విద్య పేరట చలామణి అవుతున్నదంతా విద్య కాదని."
        ),

        array("
        ఎన్నాళ్ళయింది పూలు చూడక<br>
        చూడక చూడక ఇవాళ చూస్తే<br>
        అయ్యో! అదేమిో కాని<br>
        మనుషుల్లాగే కనపడుతున్నాయవి కూడా<br>
        వాి అందాలను, ఆనందాలను కోల్పోయి."
        ),

        array("
        వాన కురిసింది, వెలిసింది.<br>
        ఓ పుష్పంపై ఓ వాన బిందువు అలాగే ఆగిపోయింది<br>
        అల్లారు ముద్దుగా.<br>
        ఎంత అందంగా ఉందా బిందువు దాని అధరంపై!<br>
        అందమయిన వాకిటిలో  వేసిన అతి చక్కని ముగ్గులా,<br>
        అందాల రాణి చెక్కిలిలో చెమ్మగిల్లిన ఖిరిళీచీజిలి లా<br>
        సన్నని పొన్నని ముక్కు కొసన<br>
        అటూ ఇటూ ఆడుకుోంన్న ముక్కుపోగులా."
        ),

        array("
        ఆ నది ఉప్పొంగుతూ పరవళ్ళు త్రొక్కుతోంది<br>
        నా మది కూడా దానితో పాటు ఉరకలు వేసి<br>
        పొంగులా ఉప్పొంగుతోంది.<br>
        కొన్నాళ్ళకు ఆ నది ఉడిగి నడకగా నడుస్తూంది<br>
        కాని, నా మది, ఇంకా కొట్టుకొంటూనే ఉంటుంది.<br>
        నదికీ మదికీ అదే తేడా."
        ),

        array("
        నా భావాలెన్నో,<br>
        ఏ భాషలో వ్యక్తీకరించను నేను వాటిని?<br>
        భాషకేమి తెలుసు పాపం భావం బాధ?<br>
        బైటివాళ్ళకేం తెలుస్తాయి లోపలి వాళ్ళ బాధలు?<br>
        చెబితే తెలుస్తాయి కాని కొన్ని చెప్పలేనివి కూడా<br>
        ఉంటాయి కదా మరి!<br>
        కాలమే తీరుస్తుంది వాటి కష్టాలు<br>
        బాధ పడక్కరలేదు,<br>
        కాలమే కలానికీ, గళానికీ దాని భాష నందిస్తుంది."
        ),

        array("
        నాకు పూలంటే ఇష్టం,<br>
        పూలలాంటి  మనుషులంటే, మనుసులంటే ఇష్టం<br>
        అయినా పూలెవ్వరికిష్టముండవు<br>
        పూలలాంటి  మనుషులంటే, మనసులంటే, మమతలంటే<br>
        నా పిచ్చికాని,<br>
        ఒక్క పిచ్చివాళ్ళకు తప్ప?"
        ),


       );

        return view('frontend.indext', $data);
        }else{
            
              $data['homeTestimonials']=array(
        array("
        To be a 'man' is different than to be 'manly'.<br>
        One pertains to form, another to norm.<br>
        One to being, another to becoming.<br>
        One to entity, another to identity, Velchala!
        "),

        array("
        Educated have to set an example for the uneducated.<br>
        If the educated themselves can't be the role models<br>
        How then can the illiterates, the ignorants, the innocents<br>
        And the rest, Velchala!
        "),

        array("
        Education even if it is less is better<br>
        Than education which is more but poor.<br>
        Education even if it is started late is better<br>
        Than education which is started early but poorly, Velchala!<br>
        "),

        array("
        Every past has its glory as every present has, every future<br>
        Has good too, the bad, the mad too, the odd<br>
        As every person too has<br>
        Is and can be, Velchala!
        "),

        array("
        Tradition belongs to  infinity, eternity,<br>
        It is immesurable and unfathomable,<br>
        Its history had mattered then and matters too now<br>
        It is all that that had happened before, happening now, <br>
        and continues to happen ,Velchala!
        "),

        array("
        There is no modernity without tradition,<br>
        No tradition without modernity,<br>
        As there is no present without past, no future without present<br>
        no unity without diversity, no diversity without unity, Velchala!
        "),

        array("
        To respect and honour the wise and the nice,<br>
        The elders too, not for their size but for their rise,<br>
        So that, tomorrow when you too so grow<br>
        You may also be so respected and honoured, Velchala!
        "),

        array("
        Culture flows when not only lips greet<br>
        But when the hearts, the souls too beat to lovingly endearingly meet. <br>
        When each other to each other intensely float.<br>
        Culture pertains to one’s “Conscience”,<br>
        To heart’s stir, flutter,<br>
        To that, that touches you like a ripple.<br>
        Kisses you like a being a sublime, subtle.<br>
        When the cultured meets the cultured<br>
        Culture looks on calm and quiet<br>
        As if astonished at its own elevating<br>
        Ennobling charm<br>
        Like the artist looks at “Beauty” beyond the “Beautiful”<br>
        With a glint in his eye, a gleam in his vision.
        "),

        array("
        God, or call him by any other name.<br>
        But do not probe him by reason, rationale or logic,<br>
        But by faith, devotion, contemplation,<br>
        Meditation, concentration<br>
        By nothing <br>
        But by their magic and music.<br>
        God, would only be amenable to heart, spirit and soul<br>
        But not to the pure intellectual rationalistic rigmarole,<br>
        Not to an effort to find him by some argumentative investigative tool.<br>
        God, My Friend!<br>
        Would not reveal to you<br>
        If you are only intellectually smart<br>
        Strong only in your analytical dissecting art<br>
        But if only you are acutely, avowedly assiduously after him, like a devout<br>
        A simple, truthful, straightforward sort.
        "),

        array("
        I HAVE SEEN<br>
        I have seen cold cunning.<br>
        Unashamed avarice.<br>
        Unbridled arrogance.<br>
        Unlimited cant.<br>
        Unbelievable want.<br>
        Indescribable sham.<br>
        Infinite vanity.<br>
        Naked immorality.<br>
        Astonishing snobbery.<br>
        Appalling poverty.<br>
        Unthinkable sycophancy.<br>
        Unimaginable slavery.<br>
        Unwarranted envy and jealousy.<br>
        Why then should I ask<br>
        For this life’s longevity?<br>
        Bargain for its insufferable agony?
        "),
       
    ); 
            return view('frontend.index', $data);
        }

    }

     public function velchala(Request $req){
          
         $data['active_menu'] = 'velchala';
         if( $req->session()->get('lang')=="telugu"){
         return view('frontend.velchalat',$data);
         }else{
          
             return view('frontend.velchala',$data);

         }
    }
    public function vsp(Request $req){
         
        $data['active_menu'] = 'vsp';
         if( $req->session()->get('lang')=="telugu"){
         return view('frontend.vspt',$data);
         }
         return view('frontend.vsp',$data);
    }

    
    public function cartPage()
    {
//        echo "<pre>";
//        print_r(Cart::getContent());
//        exit();

//        dd(Cart::getContent());


//        if (count(Cart::getContent()) <= 0) {
//            return redirect()->route('home');
//        }


        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['wishlistProducts'] = getWishlistProducts();
        return view('frontend.cart', $data);

    }
    public function addToCart(Request $request)
    {
        $requestData = $request->all();
        $productInfo = Products::where('p_id', $requestData['id'])->first();
        $cartorder = Cart::getContent()->count() + 1;
        // lets create first our condition instance
        $deliverychargeConditions = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'DeliverCharge' . $delivery_charge,
            'type' => 'DeliverCharge',
            'value' => '+' . $delivery_charge,
            'order' => $cartorder,
        ));
        Cart::add(array(
            array(

                'id' => $requestData['id'],
                'name' => $productInfo->p_name,
                'price' => $requestData['price'],
                'quantity' => $requestData['qty'],
            ),
        ));
        echo Cart::getContent()->count();
    }
    public function buyProduct(Request $request)
    {
        $requestData = $request->all();

//        dd($requestData);

        $productInfo = Products::where('p_id', $requestData['id'])->first();


        $cartorder = Cart::getContent()->count() + 1;

        $delivery_charge = curencyConvert(getCurency(), $requestData['delivery_charge']);

        // lets create first our condition instance
        $deliverychargeConditions = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'DeliverCharge' . $delivery_charge,
            'type' => 'DeliverCharge',
            'value' => '+' . $delivery_charge,
            'order' => $cartorder,
        ));

        Cart::add(array(
            array(
                'id' => $requestData['id'],
                'name' => $productInfo->p_name,
                'price' => curencyConvert(getCurency(), $requestData['price']),
                'quantity' => $requestData['qty'],
                'attributes' => array(
                    'currency' => getCurency(),
                    'options' => $requestData['options'],
                    'nameongift' => $requestData['nameongift'],
                    'delivery_charge' => $delivery_charge,
                    'overview' => $productInfo->p_overview,
                    'message' => $requestData['message'],
                    'delivery_date' => $requestData['delivery_date'],
                    'countryShipped' => $requestData['countryShipped'],
                    'delivery_time' => $requestData['delivery_time']
                ),
                'conditions' => $deliverychargeConditions,
            ),
        ));


//        echo Cart::getContent()->count();
        return redirect()->route('cartAdressPage');
    }


    public function updateItemFromCart(Request $request)
    {

        $requestData = $request->all();
        $CartItem = Cart::get($requestData['id']);
        $attributes = $CartItem->attributes;
        $attributes->put('countryShipped', $requestData['countryShipped']);
        Cart::update($requestData['id'], array(
            'quantity' => array(
                'relative' => false,
                'value' => $requestData['qty']
            ),
            'attributes' => $attributes
        ));
    }

    public function removeItemFromCart(Request $request)
    {
        $requestData = $request->all();

        Cart::remove($requestData['id']);

        Cart::clearItemConditions($requestData['id']);

        echo $requestData['id'];

    }


    public function publications(Request $request)
    {
//        dd($alias, $option);
      
        $limit = 12; 
        $cat_products = Products::   
        with('getCategory')
        ->where('publications_status', '1')
        ->orderBy('b_id', 'DESC')
        ->paginate($limit);
        $countTotalPublications = Products::where('publications_status',1)->get();
        $data['countTotalPublications']=count($countTotalPublications);
        $publishers = Categories::where("pub_status",'1')->orderBy('id', 'DESC')->get();
        $data = array();
        $data['active_menu'] = 'publications';
        $data['publishers']=$publishers;
        // $data['wishlistProducts'] = getWishlistProducts();
        $data['publications'] = $cat_products;
        return view('frontend.publications', $data);

    }
    public function subscriptionSave(Request $request)
    {
        $data['active_menu'] = 'velchala';
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $request->validate([
                'email' => 'required | email'
            ], [
                'email.required' => 'Enter email',
                'email.email' => 'Enter a valid email'
            ]);

            $email = Subscriptions::where('email', $request->email)->count();

            if ($email == 0) {
                $id = Subscriptions::create($requestData)->id;
            }

            if (empty($id)) {
                $id = 0;
            }
            echo $id;
            exit();
        }

    }
    public function productSendEnquiry(Request $request)
    {

        $requestData = $request->all();
        request()->validate([
            'se_name' => 'required',
            'se_email' => ['required', 'string', 'email', 'max:255'],
            'se_phone' => ['required', 'numeric'],
            'se_message' => 'required'
        ], [
            'se_name.required' => 'Please enter name',
            'se_email.required' => 'Please enter email',
            'se_email.email' => 'Email Valid Email',
            'se_email.max' => 'Email contains maximum 255 digits',
            'se_phone.required' => 'Enter mobile',
            'se_phone.numeric' => 'Enter Numbers Only',
            'se_message.required' => 'Enter Message'
        ]);
        $requestData['se_phone'] = $request->se_mobile_prefix . '-' . $request->se_phone;
        $requestData['se_ip_address'] = $request->ip();
        $enqid = SendEnquiry::create($requestData)->se_id;
        SendEnquiry::newSendEnquiryEmail($enqid);
        $mes = "Successfully Sent";
        return redirect()->back()->with('flash_message', $mes);
    }
    public function addNewRating(Request $request)
    {

        if ($request->isMethod('post')) {

            $requestData = $request->all();
            Ratings::create($requestData);
            $mes = 'Review & Rating added successfully!';


        }
        return redirect()->back()->with('flash_message', $mes);

    }
   

    public function magazines(Request $req){
        
        
        if($req->change_year!=""){
            
                        $data['active_menu'] = 'jayanthi';
                        $year= $req->change_year;
                        $data['magazines'] = Magazines::where("mag_date",'like',"%$year%")->orderBy('id','desc')->get();
                        $count=count($data). " results";
                        $view = view("frontend.ajax.ajaxmiagzines",$data)->render();
                        return response()->json(['html'=>$view,"count"=>$count]);
        }else{
            
           $data['active_menu'] = 'jayanthi';
           $year= '2005';
           $data['magazines'] = Magazines::orderBy('id','desc')->get();
           return view("frontend.jayanthi",$data); 
        }
        

    }
    public function ajax_magazines(Request $req){
        
        $data['active_menu'] = 'jayanthi';
        $year= $req->change_year;
        if($req->change_year=="" || $req->change_year=="All"){
            
            $data['magazines'] = Magazines::orderBy('id','desc')->get();
            
        }else{
            
         $data['magazines'] = Magazines::where("mag_date",'like',"%$year%")->orderBy('id','desc')->get();
         
        }
        $count=count($data['magazines']). " results";
        $view = view("frontend.ajax.ajaxmiagzines",$data)->render();
        return response()->json(['html'=>$view,"count"=>$count]);

    }
    public function jayanthi_events(){
        
        $data['active_menu'] = 'jayanthi';
        $data['events'] = JayanthiEvents::orderBy('id','desc')->get();
        return view("frontend.jayanthi-events",$data);
    }
    public function jayanthi_events_details(Request $request){
       
       $data['active_menu'] = 'jayanthi';
       $urlLink = $request->segment(2) ;
       $data['events'] = DB::table('events')
                   ->select('*')
                   ->where("event_url",$urlLink)->get()->first();
         if($data['events']){
        $data['photos'] = DB::table('photos')
                ->select('*')
                ->where('cat_type',3)
                ->where('event_name',$data['events']->id)->get();

        $data['video'] = DB::table('video')
                ->select('*')
                ->where('cat_type',3)
                ->where('item_id',$data['events']->id)->get();  
         }
       
       return view("frontend.jayanthi-event-detail",$data);
    }
    public function photo_albums()
    {
        $data['active_menu'] = 'gallery';
        $data['events'] = JayanthiEvents::orderBy('id','desc')->get();;
        $data['blogs'] =  Blogs::orderBy('blog_id', 'DESC')->get();
        return view("frontend.photo-albums",$data);
    }
    public function gallery_poems()
    {
       $data['active_menu'] = 'gallery';
       $data['poems'] = Poems::orderBy('id','desc')->get();
       return view("frontend.gallery-poems",$data);
    }
    public function gallery_videos()
    {
       $data['active_menu'] = 'gallery';
       $limit='6';
       $data['video'] = Videos::where('cat_type','!=',5)->orderBy('id',"desc")->limit($limit)->get();
       return view("frontend.gallery-videos",$data);
    }
    public function ajax_videos_load(Request $req)
    {
       
       if($req->type1!="less"){
        $limit=$req->count+8;
        }else{
           $limit='6'; 
        }
       $data['video'] = Videos::where('cat_type','!=',5)->orderBy('id',"desc")->limit($limit)->get();
       $count=count($data['video']). " results";
       $view = view("frontend.ajax.ajax_video_load",$data)->render();
       return response()->json(['html'=>$view,"count"=>$count]);
    }
    public function jayanthi_events_photos(Request $req){

        $data['active_menu'] = 'gallery';
        $limit='12';
        $data['photos'] = Photos::with("getBooksPhotos")->where('cat_type',3)
                                ->where('event_name',$req->segment(2))->limit('12')->get();
        return view("frontend.photo-detail",$data);
    }
    public function ajax_gallery_load(Request $req){

        $data['active_menu'] = 'gallery';
        if($req->type1!="less"){
        $limit=$req->count+8;
        }else{
           $limit='12'; 
        }
        if($req->dataTypePhoto=="blog"){
            $cat_type='4';
        }else{
            $cat_type='3';
        }
        $gallery_id=$req->gallery_id;
        $data['photos'] = Photos::with("getBooksPhotos")->where('cat_type',$cat_type)
                                ->where('event_name',$gallery_id)->limit($limit)->get();
        
        $count=count($data['photos']). " results";
        if($req->dataTypePhoto!="blog"){
        $view = view("frontend.ajax.ajax_gallery_load",$data)->render();
        }else{
           $view = view("frontend.ajax.blog-ajax-photo-details",$data)->render(); 
        }
        return response()->json(['html'=>$view,"count"=>$count]);
       

    }

    
    public function blog_events_photos(Request $req){

        $data['active_menu'] = 'gallery';
        $limit='12';
        $data['photos'] = Photos::with("getBooksPhotos")->where('cat_type',4)
                                ->where('event_name',$req->segment(2))->limit($limit)->get();
        
        return view("frontend.blog-photo-detail",$data);

    }
    
    public function book_details(Request $request)
    {
      
        $limit = 12; 
        $data = array();
        $cat_products = Products::
              with('getCategory')
            ->where('publications_status', '1')
            ->where('publication_slug',$request->segment(2))
            ->orderBy('b_id', 'DESC')->get()->first();
        $data['videos'] = Videos::where("item_id",$cat_products->b_id)->orderBy('id',"desc")->get();
        $data['videos'] = Videos::where("item_id",$cat_products->b_id)->orderBy('id',"desc")->get();
        $user_reviews = DB::table('user_reviews')
                ->select('*')
                ->join('users', 'users.id', '=', 'user_reviews.user_id')
                ->where('pub_id',$cat_products->b_id)->get();
        $data['active_menu'] = 'publications';
        $data['publications'] = $cat_products;
        return view('frontend.publication-detail', $data);

    }
     public function user_review(Request $request)
    { 
      //print_r($request->input()); exit();
        $data['active_menu'] = 'velchala';
        $validator = Validator::make($request->all(), [
            'message'    => 'required'  ,   
            'rating'     => 'required'        
        ]);
        
         if ($validator->fails()) {
          return redirect()->to("details/$url")->with('success', 'Please Give all Fields')->withErrors($validator) ->withInput();

        }else{
        $userId        =     auth()->user()->id;
        $pub_id        =     $request->pub_id;
        $message       =     $request->message;
        $rating        =     $request->rating;
        $url           =     $request->url;
       // $email        =     $request->email;
        
                 DB::table('user_reviews')->insert(
                    array(
                   
                    'user_id'        =>    $userId,
                    'message'        =>    $message,
                    'rating'         =>    $rating,
                    'pub_id'         =>    $pub_id,
                    'created_date'   =>    now(),
                     )
                ); //exit();
           return redirect()->to("details/$url")->with('success', 'Saved successfully');
       }
    }
    public function blog_events()
    {
        $data['active_menu'] = 'blog';
       $data['blogs'] =Blogs::orderBy('blog_id', 'DESC')->get();

       return view("frontend.blog-events",$data);
    }
    public function blog_interviews(){

         $data['active_menu'] = 'blog';
         $data['video'] = Videos::where("cat_type",5)->orderBy('id',"desc")->get();
         return view("frontend.blog-interviews",$data);

    }
     public function blog_news(){

         $data['active_menu'] = 'blog';
         $data['blog_news'] = BlogNews::orderBy('id',"desc")->get();
         return view("frontend.blog-news",$data);

    }
     public function blog_article(){

      $data['active_menu'] = 'blog';
      $data['blog_articles'] = BlogArticles::orderBy('id',"desc")->get();
      return view("frontend.blog-articles",$data);

    }
    public function blog_article_detail(Request $request)
    {
        $data['active_menu'] = 'blog';
        $urlLink = $request->segment(2) ;
        $data['blog_articles'] = DB::table('blog_articles')
                                 ->select('*')->where("blog_article_url",$urlLink)->get()->first();
                                return view("frontend.blog-article-detail",$data);
    }
     public function blog_events_details(Request $request)
    {
        $data['active_menu'] = 'blog';
        $urlLink = $request->segment(2) ;
       // $id = $request->segment(3) ;
       $data['blogs'] = DB::table('blogs')
                   ->select('*')->where("blog_url",$urlLink)->get()->first();
        $data['photos'] = DB::table('photos')
                ->select('*')
                ->where('cat_type',4)
                ->where('event_name', $data['blogs']->blog_id)->get();
        $data['video'] = DB::table('video')
                ->select('*')
                ->where('cat_type',4)
                ->where('item_id', $data['blogs']->blog_id)->get();
       return view("frontend.blog-events-detail",$data);
     
    }
    Public function autocomplete_course(Request $req){
      $data['active_menu'] = 'velchala';
      $data = $req->searchValue;
      if($req->searchValue!=""){
      $response = array();  
      $publications = DB::table('publications')
                ->select('publication_slug','publication_name')
                ->where("publications_status",1)
                ->where('publication_name','like',"%$data%")
                ->orderBy('publication_name',"Asc")->get();
      foreach($publications as $row ){
        $response[] = array("url"=>$row->publication_slug,
          "name"=>$row->publication_name);
      }
      
     echo json_encode($response);   
    }else{
        $response[]="";
        echo json_encode($response);
    }
}
    public function ajax_language_filter(Request $req){
        
      $data['active_menu'] = 'velchala';
      $filter= $req->filter;
      $filterType= $req->filterType;
      $count= $req->count+8;
      $data['data_type']=$filterType;
     
      if($filterType=="lang"){
      $wherecondition["publications_status"]='1';
            
            if($req->filterlang!="All"){
 
              $wherecondition["language"]=$req->filterlang;
              
             }
             if($req->filterpubli!="All"){

                $wherecondition["pub_name"]=$req->filterpubli;
             }   
      $publicationsTotal= DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)
                            
                            ->orderBy("publications.b_id","desc")
                            ->get();
      $data['publicationsTotal'] = count($publicationsTotal);
      $data['ajax_language'] = DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)
                            ->orderBy("publications.b_id","desc")
                            ->limit('12')->get();
        }
        elseif($filterType=="loadmore"){
             
            $wherecondition["publications_status"]='1';

            if($req->filterlang!="All"){
 
              $wherecondition["language"]=$req->filterlang;
              
             }
             if($req->filterpubli!="All"){

                $wherecondition["pub_name"]=$req->filterpubli;
             }  
            $publicationsTotal= DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)
                            
                            ->orderBy("publications.b_id","desc")
                            ->get();
          $data['publicationsTotal'] = count($publicationsTotal);
          $data['ajax_language'] = DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)
                            ->orderBy("publications.b_id","desc")
                            ->limit($count)
                            ->get();
        }
         elseif($filterType=="loadless"){
          
           $wherecondition["publications_status"]='1';
            
            if($req->filterlang!="All"){
 
              $wherecondition["language"]=$req->filterlang;
              
             }
             if($req->filterpubli!="All"){

                $wherecondition["pub_name"]=$req->filterpubli;
             }    
           $publicationsTotal= DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)
                            
                            ->orderBy("publications.b_id","desc")
                            ->get(); 
          
          $data['publicationsTotal'] = count($publicationsTotal);
          $data['ajax_language'] = DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)
                            ->orderBy("publications.b_id","desc")
                            ->limit('12')
                            ->get();
        }
        else{
         
             $wherecondition["publications_status"]='1';
         if($req->filterlang!="All"){
 
              $wherecondition["language"]=$req->filterlang;
              
             }
             if($req->filterpubli!="All"){

                $wherecondition["pub_name"]=$req->filterpubli;
             }     
          $publicationsTotal= DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)
                            
                            ->orderBy("publications.b_id","desc")
                            ->get();
          $data['publicationsTotal'] = count($publicationsTotal);
          $data['ajax_language'] = DB::table('publications')
                            ->join('publishers', 'publishers.id', '=', 'publications.publisher_id')
                            ->select('*')
                            ->where($wherecondition)                   
                            ->orderBy("publications.b_id","desc")
                            ->limit('12')
                            ->get();
        }
    
      $count=count($data['ajax_language']). " results";
      $view = view("frontend.ajax.ajax_language_filter",$data)->render();
      return response()->json(['html'=>$view,"count"=>$count]);
    }
    public function ajax_wish_list(Request $req){

      $data['active_menu'] = 'velchala';    
      if(auth()->user()->id){
      $language= $req->publicationId;

       $getId=DB::table('wishlist')->insertGetId(
            array(
            'user_id'      =>   auth()->user()->id, 
            'publi_id'     =>    $req->publicationId,
            'created_at'   =>  date("Y-m-d H:i:s")
            ) );
     
      }else{

         return Redirect::to('login');
      }
    }
     public function add_to_cart_data_ajax(Request $req){
      
        $publicationId= $req->publicationId;
       
        $priceData = DB::table('publications')
                        ->select('dis_price','price')->where("b_id",$publicationId)
                        ->get()->first();
        $cart_quanity= $req->cart_quanity;
      
        if($priceData->dis_price=="0"){
            $price=$priceData->price;
        }else{

            $price=$priceData->dis_price;
        }
       
        $totalprice= $price*$cart_quanity;
       
        if(isset(auth()->user()->id)){

          $cartId = auth()->user()->id;
        
        }else{

        if($req->session()->get('cartId')){
          
          $cartId= $req->session()->get('cartId');
          
        }else{

            $cartId = strtotime(date("Y-m-d H:i:s"));
            $req->session()->put('cartId',$cartId);
        }
        
       }
      
       $checkCart = DB::table('cart')
         ->select('total_price','id')->where("user_id",$cartId)
         ->get()->first();

        $cartQuanity = $req->cart_quanity;
        if(isset($checkCart->id)){
        if($checkCart->id!="0"){

         $checkCartItems = DB::table('cart_items')
                        ->select('*')
                        ->where("cart_id",$checkCart->id)
                        ->where("publ_id",$publicationId)
                        ->get()->first();
         if(isset($checkCartItems->publ_id)){
         if($checkCartItems->publ_id==$publicationId){
            
             if($req->segment(1)=="addtocart"){
                 $quanity=$req->cart_quanity;
                 
             }else{
             $quanity=$checkCartItems->quanity+$req->cart_quanity;
             }
             $price;
             $totalprice1=$quanity*$price;
             $affected = DB::table('cart_items')
              ->where('cart_id',$checkCart->id)
              ->where('publ_id',$publicationId)
              ->update(['quanity' => $quanity,'price'=>$totalprice1]);

         }}else{
           
         	DB::table('cart_items')->insert(
            array(
            'cart_id'     =>    $checkCart->id, 
            'publ_id'     =>    $publicationId,
            'quanity'     =>    $cartQuanity,
            'price'       =>    $totalprice,
            'created_at'  =>    date("Y-m-d H:i:s")
            )
            );
         }
           $grandTotal=$totalprice+$checkCart->total_price;
           $affected = DB::table('cart')
              ->where('user_id',$cartId)
              ->update(['total_price' => $grandTotal]);

        }}else{

            $getId=DB::table('cart')->insertGetId(
            array(
            'user_id'     =>   $cartId, 
            'total_price'   => $totalprice
            )

        );
            DB::table('cart_items')->insert(
            array(
            'cart_id'     =>    $getId, 
            'publ_id'     =>    $publicationId,
            'quanity'     =>    $cartQuanity,
            'price'       =>    $totalprice,
            'created_at'  =>    date("Y-m-d H:i:s")
            )
        );

        }
        
        //DB::table('cart')->insert(array());
        
    }
     public function remove_cart(Request $req){
       if($req->cartItemId){
          $cartTimesDelete = CartItems::destroy($req->cartItemId);          
          
            $priceTotal = DB::table('cart_items')
                        ->where("cart_id",$req->cartId)
                        ->sum('price');
            $affected = DB::table('cart')
            ->where('id',$req->cartId)
            ->update(['total_price' => $priceTotal]);
       }
    }
    public function cart(Request $req)
    {
     $data['active_menu'] = 'velchala';  
     if(isset(auth()->user()->id)){

      $userId=auth()->user()->id;

      }else{

        $userId = $req->session()->get('cartId');
      }
      $data['cart_detailes']= DB::table('cart')
       ->join('cart_items', 'cart.id', '=', 'cart_items.cart_id')
       ->join('publications', 'publications.b_id', '=', 'cart_items.publ_id')
       ->select('*',"cart.id as cart_id")->where("cart.user_id",$userId)->get();
      $data['countCartSum'] = DB::table('cart_items')
                                    ->join('cart', 'cart.id', '=', 'cart_items.cart_id')
                                    ->where("cart.user_id",$userId)
                                    ->sum("cart_items.quanity");
       return view("frontend.cart",$data);
    }
    
}
