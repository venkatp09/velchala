<?php

namespace Illuminate\Foundation\Auth;
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Mail;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected function redirectTo()
    {


        $routname = request()->segment(1);
        if ($routname == 'store') {
            $url = route('vendorlogin');
            return $url;
        } else {
            $url = route('userlogin');
            return $url;
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
//        $this->middleware('signed')->only('verify');
//        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
    public function verify(Request $request)
    {
        $user = User::find($request->id);
        $emailuser = User::find($request->id)->toArray();






        try {
            if ($request->route('id') != $user->getKey()) {
                throw new AuthorizationException;
            }

        } catch (\Exception $e){
            echo 'Message: ' .$e->getMessage();
        }

        $adminEmail = [commonSettings('adminEmail'),commonSettings('adminGmail')];




        if ($user->markEmailAsVerified()) {

//            dump($adminEmail);
//            dump($user);

//            $user=array('name'=>'Admin','email'=>'info@Velchala.com');
//            $user=(array)$user;

//            dump($emailuser);



            if($emailuser['role']=='405') {

//                dump("vendor");
//                dump($request->id);
//                dd($user);

                Mail::send('emails_templates.after_vendor_registration_verified.after_vendor_verification_to_admin', $emailuser, function ($message) use ($emailuser, $adminEmail) {

//                dd($user->email);
                    $message->from($emailuser['email'], $emailuser['name']);
//            $message->to('rajagonda@gmail.com');
                    $message->to($adminEmail);
                    //Add a subject
                    $message->subject($emailuser['name'] . " Registered as a Velchala Vendor and Verified Email");
                });
            }

            if($emailuser['role']=='1') {
//                dump("user");
//                dump($request->id);
//                dd($user);

                Mail::send('emails_templates.after_user_registration_verified.after_user_verification_to_admin', $emailuser, function ($message) use ($emailuser, $adminEmail) {

//                dd($user->email);
                    $message->from($emailuser['email'], $emailuser['name']);
//            $message->to('rajagonda@gmail.com');
                    $message->to($adminEmail);
                    //Add a subject
                    $message->subject($emailuser['name'] . " Registered as a Velchala User and Verified Email");
                });

            }


//            dd($this->redirectPath());
//            event(new Verified($user));

        }







        return redirect($this->redirectPath())->with('flash_message', 'Your email address successfully verified please login to access your account');
    }
}
