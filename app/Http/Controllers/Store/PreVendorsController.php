<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Models\Services;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PreVendorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {

//        dd('dsd');

        $data = array();
        $data['title'] = 'Vendor Home page';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.vendor', $data);

    }


    public function sellerBenefits()
    {
        $data = array();
        $data['title'] = 'Vendor benefits';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.benefits', $data);
    }

    public function sellerFaq()
    {
        $data = array();
        $data['title'] = 'Vendor benefits';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.faq', $data);
    }


    public function sellerPricing()
    {
        $data = array();
        $data['title'] = 'Vendor benefits';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.pricing', $data);
    }


    public function registerSeller()
    {

        if (Auth::check() && auth()->user()->role) {
//            dd(auth()->user()->role);


            $mes = 'Ypur Already logged!';
            return redirect()->route('sellerPreDashbord')->with('flash_message', $mes);

        }else{
            $data = array();
            $data['title'] = 'Vendor benefits';
            $data['active_menu'] = 'home';
            $data['sub_active_menu'] = 'home';
            return view('store.seller_rigister', $data);
        }



    }

    public function registerSellerDetails()
    {
        $data = array();
        $data['title'] = 'Vendor benefits';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.seller_rigister_details', $data);
    }

    public function sellerPreDashbord()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.pre_dashbord', $data);
    }

    public function sellerDashbord()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.dashbord', $data);
    }

    public function sellerListings()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.listings.list', $data);
    }

    public function sellerListingsView()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.listings.view', $data);
    }

    public function sellerNewListings()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.listings.new', $data);
    }

    public function sellerImportListings()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.listings.import', $data);
    }

    public function sellerOrdersList()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.orders.list', $data);
    }

    public function sellerOrdersListView()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.orders.view', $data);
    }

    public function paymentsOverview()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.payments.overview', $data);
    }

    public function paymentsInvoice()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.payments.invoice', $data);
    }

    public function paymentsInvoiceView()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.payments.invoice_view', $data);
    }


    public function ProfileAcount()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.account.profile', $data);
    }

    public function profileDetails()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.account.business_details', $data);
    }



    public function vendorSettings()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.account.settings', $data);
    }


}
