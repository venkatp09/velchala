<?php

namespace App\Http\Controllers;


use App\Models\Contactus;

use App\Models\Countries;
use Illuminate\Http\Request;

use App\Models\Blogs;
use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class Controller extends BaseController
{
//    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {
//        $this->middleware('auth');
        define('PAGE_LIMIT', 10);


    }


    public function setCurrency(Request $request)
    {

        if ($request->isMethod('get')) {


            session(['siteCurencyModel' => $request->get('siteCurencyModel')]);


            $newcurrency = $request->get('newcurrency');
            $getcountry = Countries::where('country_id', $newcurrency)->where('country_status', 'active')->first();
//            dd($getcountry);
            if ($getcountry != '') {
                getSiteCurency($getcountry);
            }
            $mes = 'Currency Changes By ' . $getcountry->country_currency;

//            $locale = $getcountry->country_code;
//
//            dump(url()->previous());
//            dump(app()->getLocale());
//
//
//            Session::put('locale', $locale);
//
//            app()->setLocale($locale);
//
//            App::setLocale($locale);
//
//            Session()->put('locale', $locale);
//
//
//            $parameters = ['local' => $locale];
//           dd(redirect()->back()->with($parameters));
//
//
//            dd(parse_url(url()->previous(),PHP_URL_PORT) );
//            dd(parse_url(url()->previous(),PHP_URL_PATH) );




            return redirect()->route('home')->with('flash_message', $mes);
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function faq()
    {
        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.faq', $data);
    }

    public function citieswedeliver()
    {

//        Mail::send('emails_templates.test', ['key' => 'value'], function($message)
//        {
//            $message->to('rajagonda@gmail.com', 'rajagonda')->subject('Welcome!');
//        });


        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.citieswedeliver', $data);
    }


    public function privacy()
    {
        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.privacy', $data);
    }

    public function returnpolicy()
    {
        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.returnpolicy', $data);
    }

    public function termsofuse()
    {


//        dd(getSiteCurency());

        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.termsofuse', $data);
    }


    public function about()
    {


//        exit();


        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.about', $data);
    }


    public function corevalues()
    {
        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.corevalues', $data);
    }


    public function career()
    {
        $data = array();
        $data['title'] = 'velchala Career';
        $data['meta_keywords'] = 'velchala Career';
        $data['meta_description'] = 'velchala Career';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.career', $data);
    }


    public function sitemap()
    {
        echo 'sitemap';

//        $data = array();
//        $data['title'] = 'velchala.com Home';
//        $data['active_menu'] = 'home';
//        $data['sub_active_menu'] = 'home';
//        return view('frontend.pages.career', $data);
    }


    public function bloglist()
    {
        $data = array();
        $data['title'] = 'velchala Blogs';
        $data['meta_keywords'] = 'velchala Blogs';
        $data['meta_description'] = 'velchala Blogs';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['blogs'] = Blogs::where('blog_status', 'active')->orderBy('blog_id', 'DESC')
            ->paginate(PAGE_LIMIT);
        return view('frontend.blog.list', $data);
    }

    public function blogView($id)
    {
        $data = array();
        $data['blog'] = Blogs::where('blog_id', $id)->first();
        $data['title'] = 'velchala.com | '.$data['blog']->blog_title;
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['related_blogs'] = Blogs::where('blog_id', '!=', $id)->where('blog_status', 'active')->take(4)->get();
        return view('frontend.blog.view', $data);
    }

    public function cartThankYouPage(Request $request)
    {
//        $invoice = Orders::with('orderItems')->where('order_user_id', Auth::id())->where('order_id', $request->id)->first();

//        dd($invoice);

        $data = array();
        $data['active_menu'] = 'thank-you';
        $data['sub_active_menu'] = 'thank-you';
        $data['title'] = 'Cart Thank You';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
//        $data['invoice'] = $invoice;
        return view('frontend.thank_you_cart', $data);
    }

    public function serviceThankYouPage(Request $request)
    {

//        $invoice = Services::where('s_id',$request->id)->first();


//        $invoice = Orders::with('orderItems')->where('order_user_id', Auth::id())->where('order_id', $request->id)->first();

//        dd($invoice);

        $data = array();
        $data['active_menu'] = 'thank-you';
        $data['sub_active_menu'] = 'thank-you';
        $data['title'] = 'Services Thank You';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
//        $data['invoice'] = $invoice;
        return view('frontend.thank_you_service', $data);
    }

    public function ammachethivantaThankYouPage(Request $request)
    {

//        $invoice = AmmachethivantaOrders::with('orderItems.getProduct')->where('ac_order_id', $request->id)->first();


        $data = array();
        $data['active_menu'] = 'thank-you';
        $data['sub_active_menu'] = 'thank-you';
        $data['title'] = 'Ammachethivanta Thank You';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
//        $data['invoice'] = $invoice;
        return view('frontend.thank_you_ammachethivanta', $data);
    }
}
