<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Routing\Controller as BaseController;
use App\Models\Orders;
use App\Models\PaymentInfo;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\ProductSkus;
use App\Models\SendEnquiry;
use App\Models\Services;
use App\Models\Subscriptions;
use App\User;
use App\Models\VendorAccount;
use App\Models\VendorBusiness;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use File;
use Validator;


class AdminController extends Controller
{
//    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 15);
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'velchala.com -Dashboard';
        $data['active_menu'] = 'dashboard';
        $data['sub_active_menu'] = 'dashboard';
        $data['userscount'] = User::where('role', '1')->where('status', 'active')->count();
        
        $data['productscount'] = Products::where('publications_status', '1')->count();
        $data['orderscount'] = Orders::where('order_status', 'Paid')->count();
        $data['productenquiries'] = SendEnquiry::count();
       

        $data['newvendorlist'] = User::where('role', '405')->where('status', 'active')->orderBy('id', 'desc')->limit(10)->get();
        $data['newuserslist'] = User::where('role', '1')->where('status', 'active')->orderBy('id', 'desc')->limit(10)->get();

        $data['neworders'] = Orders::with('orderItems.getProduct.productImages', 'getUser')->orderBy('order_id', 'desc')->limit(10)->get();


        return view('backend.dashboard', $data);
    }

    

    public function AccountDetails($id, Request $request)
    {
        $accountDetails = VendorAccount::where('va_vendor_id', $id)->first();
        if ($request->isMethod('post')) {
            $requestData = $request->all();


            if ($accountDetails) {
                $vendorinfo = VendorAccount::findOrFail($accountDetails->va_id);
                $vendorinfo->update($requestData);
                $mes = "Details updated successfully";
            } else {
                VendorAccount::create($requestData);
                $mes = "Details added successfully";
            }
            return redirect()->back()->with('flash_message', $mes);

        }
        $data = array();
        $data['active_menu'] = 'vendors';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['sub_active_menu'] = 'vendor-account-details';
        $data['title'] = 'Vendor Account Details';
        $data['accountDetails'] = $accountDetails;
        return view('backend.vendors.accountDetails', $data);
    }

    public function AccountDetailsApprove(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $id = $request->va_id;
            $vid = $request->va_vendor_id;
            $vendor = VendorAccount::findOrFail($id);
            if ($request->va_type_new == 'approved') {
                $requestData['va_status'] = 'approved';
                $mes = 'Approved Successfully!';
            } else {
                $requestData['va_status'] = 'rejected';
                $mes = 'Rejected Successfully!';
            }
            $vendor->update($requestData);
            return redirect()->route('admin_vendor_account_details', ['id' => $vid])->with('flash_message', $mes);
        }

    }


    public function VendorProducts($id, Request $request)
    {
        $products = Products::with('productSKUs', 'getCategory', 'getSplCat', 'getVendor')->where('p_vendor_id', $id)->orderBy('p_id', 'desc')
            ->paginate(PAGE_LIMIT);
        $data['active_menu'] = 'vendors';
        $data['sub_active_menu'] = 'venodr-products-list';
        $data['title'] = 'Vendor Products';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['products'] = $products;
        return view('backend.vendors.product-list', $data);
    }

    public function BusinessDetailsApprove(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $id = $request->vb_id;
            $vid = $request->vb_vendor_id;
            $vendor = VendorBusiness::findOrFail($id);
            if ($request->vb_type_new == 'approved') {
                $requestData['vb_status'] = 'approved';
                $mes = 'Approved Successfully!';
            } else {
                $requestData['vb_status'] = 'reject';
                $mes = 'Rejected Successfully!';
            }
            $vendor->update($requestData);
            return redirect()->route('BusinessDetailsInfo', ['id' => $vid])->with('flash_message', $mes);
        }

    }

    public function BusinessDetails($id, Request $request)
    {

        $businessDetails = VendorBusiness::where('vb_vendor_id', $id)->first();
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            if ($businessDetails) {
                $vendorinfo = VendorBusiness::findOrFail($businessDetails->vb_id);
                $vendorinfo->update($requestData);
                $mes = "Details updated successfully";
            } else {
                VendorBusiness::create($requestData);
                $mes = "Details added successfully";
            }
            return redirect()->route('BusinessDetailsInfo', ['id' => $id])->with('flash_message', $mes);

        }
        $data = array();
        $data['active_menu'] = 'vendors';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['sub_active_menu'] = 'vendor-business-details';
        $data['title'] = 'Vendor Business Details';
        $data['businessDetails'] = $businessDetails;
        return view('backend.vendors.businessDetails', $data);
    }

    public function paymentInfo($id, Request $request)
    {
        if ($request->isMethod('post')) {


            $requestData = $request->all();
            if ($request->has('submit')) {


//                $requestData['pi_date'] = Carbon::createFromFormat('d-m-Y', $requestData['pi_date'])->toDateString();
                $requestData['pi_date'] = date("Y-m-d", strtotime($requestData['pi_date']));
//                dd("test");
                if ($requestData['pi_id'] == '') {
                    PaymentInfo::create($requestData);
                    $mes = "Details updated successfully";
                } else {
                    $paymentinfo = PaymentInfo::findOrFail($requestData['pi_id']);
                    $paymentinfo->update($requestData);
                    $mes = "Details added successfully";
                }
            } else {
                PaymentInfo::destroy($requestData['pi_id']);

                $mes = "Details deleted successfully!";

            }
            return redirect()->route('paymentinfo', ['id' => $id])->with('flash_message', $mes);

        }
        $data = array();
        $data['active_menu'] = 'vendors';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['sub_active_menu'] = 'vendor-payment-info';
        $data['title'] = 'Payment Info -' . $data['userInfo']->name;
        $data['payments'] = PaymentInfo::where('pi_vendor_id', $id)->get();
        return view('backend.vendors.paymentInfo', $data);
    }

    public function profile(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
//            dd($requestData);

            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required'
            ], [
                'name.required' => 'Please enter name',
                'email.required' => 'Please enter email'
            ]);
            $id=Auth::guard('admin')->user()->id;
            $user = User::findOrFail($id);
            $user->update($requestData);
            $mes = 'profile updated successfully!';
            return redirect()->route('profile')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['active_menu'] = 'profile';
            $data['sub_active_menu'] = 'profile';
            $data['title'] = 'Sasaya-Update profile';
            return view('backend.updateProfile', $data);
        }
    }

    public function changePassword(Request $request)
    {
//        dd(Auth::id());
//        $id = Auth::user()->id;
        if ($request->isMethod('post')) {


            $requestData = $request->all();
            $request->validate([
                'password' => 'required',
                'new_password' => 'required|min:6|max:12',
                'confirm_password' => 'required|min:6|max:12|same:new_password'
            ], [
                'password.required' => 'Please enter old password',
                'new_password.required' => 'Please enter new password',
                'confirm_password.required' => 'Please confirm password'
            ]);

            $id=Auth::guard('admin')->user()->id;

            $user = User::findOrFail($id);
            if (Hash::check($requestData['password'], $user->password)) {
                $requestData['password'] = Hash::make($requestData['new_password']);
                $user->update($requestData);
                $mes = 'Password updated successfully!';
                return redirect()->route('changePassword')->with('flash_message', $mes);
            } else {
                $mes = 'Old password is wrong ...try again';
                return redirect()->route('changePassword')->with('flash_message_error', $mes);
            }

        } else {
            $data = array();
            $data['active_menu'] = 'changepassword';
            $data['sub_active_menu'] = 'changepassword';
            $data['title'] = 'velchala-Change Password';
            return view('backend.changePassword', $data);
        }
    }


    public function subscriptions()
    {
        $data = array();
        $data['title'] = 'Subscriptions';
        $data['active_menu'] = 'subscriptions';
        $data['sub_active_menu'] = 'subscriptions';
        $perPage = 10;
        $data['subscriptions'] = Subscriptions::orderBy('id', 'desc')
            ->paginate($perPage);
        return view('backend.subscriptionsList', $data);
    }

    public function product_enquiries(Request $request)
    {


        if ($request->isMethod('post')) {
            $requestData = $request->all();
//            dd($requestData);

            if ($requestData['se_id'] != '') {

                if ($requestData['action'] == 1) {
                    $otp = rand(100000, 999999);

                    $updateData['se_otp'] = $otp;
                    $updateData['se_status'] = '1';


                    SendEnquiry::where('se_id', $requestData['se_id'])->update($updateData);


                    SendEnquiry::otpSendEnquiryEmail($requestData['se_id']);

//                    dd("test");


                    $mes = 'OTP Generated successfully!';
                    return redirect()->back()->with('flash_message', $mes);

                } else {
                    $SendEnquiry = SendEnquiry::where('se_id', $requestData['se_id'])->get()->first();

                    $updateData['se_otp'] = null;
                    $updateData['se_status'] = 2;
                    $updateData['se_reply_msg'] = $requestData['se_reply_msg'];

                    $SendEnquiry->update($updateData);


                    SendEnquiry::unAvalSendEnquiryEmail($requestData['se_id']);


                    $mes = 'Cancelled!';
                    return redirect()->back()->with('flash_message', $mes);
                }

            }


        }

        $data = array();
        $data['title'] = 'Product Enquiries ';
        $data['active_menu'] = 'product_enquiries';
        $data['sub_active_menu'] = 'product_enquiries';
        $perPage = 10;
        $data['product_enquiries'] = SendEnquiry::orderBy('se_id', 'desc')
            ->paginate($perPage);
        return view('backend.products_enquires', $data);
    }

    public function storeDelete(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $products = Products::where('p_vendor_id', $requestData['id'])->get();
            if (count($products) > 0) {
                foreach ($products as $product) {

                    $productimages = ProductImages::where('pi_product_id', $product->p_id)->get();

                    foreach ($productimages as $image) {

                        File::delete('uploads/products/' . $image->pi_image_name);
                        File::delete('uploads/products/thumbs/' . $image->pi_image_name);
                        ProductImages::destroy($image->pi_id);
                    }
                    ProductSkus::where('sku_product_id', $product->p_id)->delete();
                    Products::destroy($product->p_id);
                }
            }
            User::destroy($requestData['id']);
            return redirect()->route('vendorslist')->with('flash_message', 'Vendor deleted successfully!');

        }
    }

    public function enquiryDelete(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            SendEnquiry::destroy($requestData['se_id']);
            return redirect()->route('product_enquiries')->with('flash_message', 'Enquiry deleted successfully!');

        }
    }


}
