<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;

use App\Models\Products;

use App\Models\Videos;
use App\Models\ProductVariations;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use League\Csv\Reader;
use League\Csv\Statement;


class ProductsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        $data = array();
      
        if($request->page>1){

        $data['s_no']=29;

        }else{
           
            $data['s_no']=0;

        }
        $keyword = $request->get('search');
        $pname = $request->get('book_name');
        
        $pstatus = $request->get('publications_status');

//        dump($pstatus);

        if (!empty($keyword)) {



            $products = Products::with('getCategory')
                ->when("'".$pstatus."'", function ($query) use ($pstatus) {


                    if($pstatus !=''){
                    return $query->where('publications_status', $pstatus);

                    }

                })
                ->when($pname, function ($query) use ($pname) {
                    return $query->whereRaw("LOWER(publications.publication_name) LIKE ?", '%' . strtolower($pname) . '%');
                })
               

                

                ->orderBy('b_id', 'desc')
                ->paginate(PAGE_LIMIT)->appends(request()->query());



        } elseif ($request->isMethod('post')) {

            if($request['status']!=""){
               
                if($request['status']=="0"){
                  
                    $requestData['publications_status']=1;
                    $msg='Book Successfully Published';
                    
                }else{
                   
                    $requestData['publications_status']=0;
                    $msg='Book Successfully Un  Published';

                }
                Products::where('b_id',$request['b_id'])->update($requestData);
                return redirect()->back()->with('flash_message',$msg );

            }
            elseif($request['home_page']!=""){
               
                if($request['home_page']=="0"){
                  
                    $requestData['home_page']=1;
                    
                }else{
                   
                    $requestData['home_page']=0;

                }
                Products::where('b_id',$request['b_id'])->update($requestData);
                if($requestData['home_page']==1){
                return redirect()->back()->with('flash_message', 'Book Added in Home Page successfully!');
                }else{
                    return redirect()->back()->with('flash_message', 'Book Removed in Home Page successfully!');
                    
                }

            }
            else{

            $requestData = $request->all();
            $file = Products::findOrFail($requestData['b_id']);

            if ($file->img != '') {

                File::delete('theme/uploads/publications/' . $file->img);
                
            }

            Products::destroy($requestData['b_id']);

            return redirect()->back()->with('flash_message', 'Book deleted successfully!');

        } } else {

            $products = Products::with('getCategory')->orderBy('b_id', 'desc')
                ->paginate(PAGE_LIMIT);


        }


        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products-list';
        $data['title'] = 'Products';
        $data['products'] = $products;
        $data['vendor_list'] = User::Where('role', '405')->get();
        return view('backend.products.list', $data);
    }



    public function addNewProducts(Request $request, $id = null)
    {
//        $tableInfo = new Products();
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            $returnData = Products::createProduct($request);
            if ($returnData['status'] == 'created') {
                $mes = 'Product added successfully!';
//                return redirect()->route('addProductImages', ['id' => $product_id])->with('flash_message', $mes);
                return redirect()->route('admin_books')->with('flash_message', $mes);
            } elseif ($returnData['status'] == 'updated') {

                $mes = 'Product updated successfully!';
                return redirect()->back()->with('flash_message', $mes);
            }
        } else {
            $data = array();
            $data['product_id'] = '';
            $data['product'] = '';
            if ($id) {
                $data['product_id'] = $id;
                $data['product'] = Products::findOrFail($id);
            }
           
            $data['categories'] = Categories::orderBy("id","desc")->get();
            $data['product_videos'] = Videos::where(['item_id'=>$request->segment(4),'cat_type'=>2])->get();
            $data['active_menu'] = 'products';
            $data['sub_active_menu'] = 'manage-products';
            $data['title'] = 'Manage products';
            $data['title'] = 'Manage products';
            return view('backend.products.add', $data);
        }
    }

   


    public function AdminProductView(Request $request, $id)
    {

        $data = array();
        $data['product'] = Products::with('getCategory')->findOrFail($id);
        $data['product_videos'] = Videos::where('item_id', $id)->get();
        $data['title'] = 'Product View';
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'product-view';
        return view('backend.products.view', $data);
    }

    public function deleteProductimage(Request $request)
    {
        $image = ProductImages::findOrFail($request['image']);

        File::delete('uploads/products/' . $image->pi_image_name);
        File::delete('uploads/products/thumbs/' . $image->pi_image_name);

        ProductImages::destroy($request['image']);
        exit();
    }


    public function checkProductAlias(Request $request)
    {
        switch ($request['type']) {
            case 'edit':
                $current_product_alias = $request['current_product_alias'];
                if ($request['p_alias'] === $current_product_alias) {
                    echo 'true';
                } else {
                    $countWeb = Products::where('p_alias', $request['p_alias'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb = Webseries::where('w_alias', $request['w_alias'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }


    public function productExtraInfo($id, Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = ProductVariations::findOrFail($requestData['pe_id']);

            if ($file->pe_image != '') {
                File::delete('uploads/products/' . $file->pe_image);
                File::delete('uploads/products/thumbs/' . $file->pe_image);
            }

            ProductExtraInformation::destroy($requestData['pe_id']);
            return redirect()->route('productExtraInfo', ['id' => $id])->with('flash_message', 'Product Information Deleted successfully!');
        }

        $data = array();
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products-extra-info';
        $data['title'] = 'Products';
        $data['product_id'] = $id;
        $data['product_details'] = ProductVariations::where('pv_prod_id', $id)->paginate(PAGE_LIMIT);
        return view('backend.products.extraDetails', $data);
    }

    public function addProductExtraInfo(Request $request, $id)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();


            $this->validate($request, [
                'pv_sku' => 'required',
                'pv_mrp' => 'required',
                'pv_selling_price' => 'required',
                'pv_status' => 'required'
            ], [
                'pv_sku.required' => 'Enter title',
                'pv_mrp.required' => 'Enter description',
                'pv_selling_price.required' => 'Enter position',
                'pv_status.required' => 'Select status',
            ]);


            if ($requestData['pv_id'] == '') {
                ProductVariations::create($requestData);
                $mes = 'Product Variation added successfully!';
            } else {
                $product = ProductVariations::findOrFail($requestData['pv_id']);
                $product->update($requestData);
                $mes = 'Product Info updated successfully!';
            }
            return redirect()->route('productExtraInfo', ['id' => $id])->with('flash_message', $mes);

        }
    }

   


    public function ProductApprove(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $id = $request->p_id;
            $products = Products::findOrFail($id);
            $products->update($requestData);

            if (isset($requestData['options'])) {
                
                foreach ($requestData['options'] as $key => $options) {
                    
                    $productOptions = ProductSkus::findOrFail($key);
                    $productOptions->update($options);
                    
                }
            }

            Products::ProductApproveMail($id);


            $mes = 'Updated Successfully!';
            return redirect()->back()->with('flash_message', $mes);
        }

    }


   


}
