<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Pincodes;

use File;

class PincodesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = Pincodes::removePincodes($requestData['id']);

            if ($action == true) {
                $msg = "Pincode deleted successfully!";
            } else {
                $msg = "Pincode delete Error!";

            }
            return redirect()->route('admin_pincodes')->with('flash_message', $msg);
        } else {

            $pincodes =Pincodes::orderBy('pin_id', 'DESC')->paginate(PAGE_LIMIT);
               
        }
        $data = array();
        $data['active_menu'] = 'admin_pincodes';
        $data['title'] = 'admin_pincodes';
        $data['pincodes'] = $pincodes;
        return view('backend.pincodes.list', $data);
    }

    public function addNewPincodes(Request $request, $id = null)
    {
        $tableInfo = new Pincodes();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

             if ($requestData['id'] != '') {
                $pincodes = Pincodes::findOrFail($requestData['id']);
                
            } 
             $validatedData = $request->validate([

                'pincode' => 'required',
                'address' => 'required|unique:pincodes',
                'price'   => 'required|numeric',
                
            ], [
                'pincode.required' => 'Please enter pincode',
                'address.required' => 'Please enter location',
                'price.required' => 'Please enter price',
               
            ]);
        if($requestData['id'] == '') {
            
            
           
            $requestData['status']=1;
            Pincodes::create($requestData);
            $mes = 'Pincodes  added successfully!';
            return redirect()->route('admin_pincodes')->with('flash_message', $mes);
            }else{

                $pincodes->update($requestData);
                $mes = 'Pincodes updated successfully!';
                return redirect()->route('admin_pincodes')->with('flash_message', $mes);
            }

        } else{

            $data = array();
            $data['pin_id'] = '';
            $data['pincodes'] = '';
            if ($id) {

                $data['pin_id'] = $id;
                $data['pincodes'] = Pincodes::findOrFail($id);;
            }
            $data['active_menu'] = 'admin_pincodes';
            $data['sub_active_menu'] = 'manage-poems';
            $data['title'] = 'Manage blogs';
            return view('backend.pincodes.add', $data);
        }
    }

    
}
