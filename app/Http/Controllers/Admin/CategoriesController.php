<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CategoriesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
       if($request->isMethod('post')){
            $requestData = $request->all();

            $file = Categories::findOrFail($requestData['id']);
            Categories::destroy($requestData['id']);
            return redirect()->back->with('flash_message', 'Category deleted successfully!');
        } 
        $data = array();  
        $categories = Categories::orderBy('id', 'desc')->paginate(PAGE_LIMIT);
        $data['active_menu'] = 'categories';
        $data['sub_active_menu'] = 'categories-list';
        $data['title'] = 'Categories';
        $data['categories'] = $categories;
        return view('backend.categories.list', $data);
    }

public function addNewCategories(Request $request, $id = null)
    {
        $tableInfo = new Categories();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['category_id'] != '') {
                $categories = Categories::findOrFail($requestData['category_id']);
                $imageRule = empty($categories->category_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }
            $request->validate([
                'pub_name' => 'required',
                'pub_status' => 'required'
            ], [
                'pub_name.required' => 'Please enter name',
                'pub_status.required' => 'Select status1',
            ]);

         


           
            if ($requestData['category_id'] == '') {

//                dd($requestData);
                Categories::create($requestData);

                $mes = 'Publishers added successfully!';
                return redirect()->route('admin_publishers')->with('flash_message', $mes);
            } else {

                
                $requestData['updated_at']=now();
                $categories->update($requestData);
                $mes = 'Category updated successfully!';

                return redirect()->back()->with('flash_message', $mes);

            }


        } else {
            $data = array();
            $data['category_id'] = '';
            $data['category'] = '';
            if ($id) {
				
                $data['category_id'] = $id;
                $data['category'] = Categories::findOrFail($id);
            }
            $data['active_menu'] = 'categories';
            $data['sub_active_menu'] = 'manage-categories';
            $data['title'] = 'Manage categories';
            return view('backend.categories.add', $data);
        }
    }

    public function deleteCategoryimage(Request $request)
    {
        $image = Categories::findOrFail($request['image']);

        File::delete('uploads/categories/' . $image->category_image);
        File::delete('uploads/categories/thumbs/' . $image->category_image);

        $update_data = array();
        $update_data['category_image'] = '';
        $image->update($update_data);
        exit();
    }


    public function checkNewCategoryAlias(Request $request)
    {
        switch ($request['type']) {
            case 'edit':
                $current_category_alias = $request['current_category_alias'];
                if ($request['category_alias'] === $current_category_alias) {
                    echo 'true';
                } else {
                    $countWeb = Categories::where('category_alias', $request['category_alias'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb = Webseries::where('w_alias', $request['w_alias'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }

}
