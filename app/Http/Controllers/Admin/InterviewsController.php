<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blogs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Videos;

use File;

class InterviewsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = Videos::removeVideos($requestData['id']);

            if ($action == true) {
                $msg = "Interviews deleted successfully!";
            } else {
                $msg = "Interviews delete Error!";

            }
            return redirect()->route('admin_interviews')->with('flash_message', $msg);
        } else {

            $Videos =Videos::where("cat_type","5")->orderBy('id', 'DESC')->paginate(PAGE_LIMIT);
               
        }
        $data = array();
        $data['active_menu'] = 'admin_interviews';
        $data['sub_active_menu'] = 'admin_interviews-list';
        $data['title'] = 'admin_interviews';
        $data['Videos'] = $Videos;
        return view('backend.admin_interviews.list', $data);
    }

    public function addNewPoems(Request $request, $id = null)
    {
        $tableInfo = new Videos();
        if ($request->isMethod('post')) {

            $requestData = $request->all();
                          $countImg = count($request['videos']);

                          
                  
                    for($j=0;$j<$countImg;$j++){

                            DB::table('video')->insert(
                            array(
                        
                            'video_url'     =>   $request['videos'][$j],
                            'poem_name'     =>   $request['videos_name'][$j],
                            'created_at'    =>   date("Y-m-d H:i:s"),
                            'updated_at'    =>   date("Y-m-d H:i:s"),
                            'cat_type'      =>   5,
                            'item_id'       =>    0,
                            )
                         );
                    }
            $mes = 'Interviews  added successfully!';
            return redirect()->route('admin_interviews')->with('flash_message', $mes);
        } else{
            $data = array();
            $data['blog_id'] = '';
            $data['blog'] = '';
            $data['videos'] = '';
            $data['event_images'] = '';
            if ($id) {

                $data['blog_id'] = $id;
                $data['videos'] = Videos::where(['cat_type'=>$request->segment(5)])->get();
            }
            $data['active_menu'] = 'videos';
            $data['sub_active_menu'] = 'manage-poems';
            $data['title'] = 'Manage blogs';
            return view('backend.admin_interviews.add', $data);
        }
    }

    
}
