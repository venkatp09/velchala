<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogArticles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use File;

class BlogArticalesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = BlogArticles::removeArticles($requestData['id']);

            if ($action == true) {
                $msg = "Blog Articles deleted successfully!";
            } else {
                $msg = "Blog Articles delete Error!";

            }
            return redirect()->route('admin_blog_articales')->with('flash_message', $msg);
        } else {

            $blog_articales =BlogArticles::orderBy('id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'blog_articales';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'blog_articales';
        $data['blog_articales'] = $blog_articales;
        return view('backend.blog_articales.list', $data);
    }

    public function addNewBlogArticales(Request $request, $id = null)
    {
        $tableInfo = new BlogArticles();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['id'] != '') {
                $events = BlogArticles::findOrFail($requestData['id']);
                $imageRule = empty($events->blog_article_pdf) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [

                'blog_article_name' => 'required',
                'blog_article_desc' => 'required',
                'blog_article_pdf' => $imageRule,
                
                'blog_article_desc' => 'required',
            ], [
                
                'blog_article_name.required' => 'Please enter name',
                'blog_article_desc.required' => 'select image to upload',
                'blog_article_pdf.required' => 'Select Date',
                
            ]);
               $requestData['blog_article_url'] = str_slug($requestData['blog_article_name']);
            if ($requestData['id'] == '') {

                if ($request->hasFile('blog_article_pdf')) {
                    $fileName = BlogArticles::imageUpload($request['blog_article_pdf']);
                    $requestData['blog_article_pdf'] = $fileName;
                }

                BlogArticles::create($requestData);

                $mes = 'Event added successfully!';
            } else {

                if ($request->hasFile('blog_article_pdf')) {

                    $fileName = BlogArticles::imageUpload($request['blog_article_pdf'], $events->blog_article_pdf);
                    $requestData['blog_article_pdf'] = $fileName;
                }

                $events->update($requestData);
                $mes = 'Events updated successfully!';
            }
            return redirect()->route('admin_blog_articales')->with('flash_message', $mes);

        } else {
                    $data = array();
                    $data['id'] = '';
                    $data['blog_articales'] = '';
                    if ($id) {
                    $data['id'] = $id;
                    $data['blog_articales'] = BlogArticles::findOrFail($id);;
                    }
                
                    $data['active_menu'] = 'blog_articales';
                    $data['title'] = 'Manage Articales';
                    return view('backend.blog_articales.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = BlogArticles::findOrFail($request['image']);

        File::delete('theme/uploads/events/' . $image->img);

        $update_data = array();
        $update_data['blog_image'] = '';
        $image->update($update_data);
        exit();
    }
}
