<?php

namespace App\Http\Controllers\Admin;

use App\Models\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');

    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {

            request()->validate([
                'admin_email' => 'required',
                'admin_name' => 'required',
                'address' => 'required',
                'tolfree_email' => 'required',
               
            ], [
                'admin_email.required' => 'Enter Email',
                'admin_name.required' => 'Select name',
                'address.required' => 'Enter Adress',
                'tolfree_email.required' => 'Enter Number ',
               
            ]);

            $requestData = request()->post();
            unset($requestData['_token']);

//            dd($requestData);


            $allSettings = Settings::get()->first();

            if ($allSettings) {
                $allSettings->update($requestData);
            } else {
                Settings::create($requestData);
            }


            return redirect()->back()->with('flash_message', 'Updated Settings successfully!');
        } else {

        }

        $data = array();
        $data['active_menu'] = 'settings';
        $data['sub_active_menu'] = 'basic';
        $data['title'] = 'settings';
        $data['settings'] = Settings::get()->first();

        return view('backend.settings.basic', $data);
    }


}
