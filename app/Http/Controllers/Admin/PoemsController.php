<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blogs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Videos;
use App\Models\Poems;
use File;

class PoemsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = Poems::removePoems($requestData['id']);

            if ($action == true) {
                $msg = "Poem deleted successfully!";
            } else {
                $msg = "Poem delete Error!";

            }
            return redirect()->route('admin_poems')->with('flash_message', $msg);
        } else {

            $poems =Poems::orderBy('id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'poems';
        $data['sub_active_menu'] = 'poems-list';
        $data['title'] = 'poems';
        $data['blogs'] = $poems;
        return view('backend.poems.list', $data);
    }

    public function addNewPoems(Request $request, $id = null)
    {
        $tableInfo = new Blogs();
        if ($request->isMethod('post')) {

            $requestData = $request->all();
                          $countImg = count($request['poem_image']);
                  $uploadPath1 =  "theme/uploads/gallery/";
                    for($j=0;$j<$countImg;$j++){

                            $extension = $request['poem_image'][$j]->getClientOriginalName();
                            $fileName = time() . $extension;
                            $request['poem_image'][$j]->move($uploadPath1, $fileName);
                            DB::table('poems')->insert(
                            array(
                        
                            'poem_image'    =>   $fileName,
                            'created_at'   =>   date("Y-m-d H:i:s"),
                            'updated_at'   =>   date("Y-m-d H:i:s"),
                           
                            'gal_cat_id'      => 1
                            )
                         );
                    }


                $mes = 'Poems added successfully!';
           

               
           
            return redirect()->route('admin_poems')->with('flash_message', $mes);

        } else{

            $data = array();
            $data['blog_id'] = '';
            $data['blog'] = '';
            $data['event_videos'] = '';
            $data['event_images'] = '';
            if ($id) {
                $data['blog_id'] = $id;
                
               
                $data['event_images'] = Poems::where(['id'=>$request->segment(4)])->get();
            }
            $data['active_menu'] = 'poems';
            $data['sub_active_menu'] = 'manage-poems';
            $data['title'] = 'Manage blogs';
            return view('backend.poems.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = Poems::findOrFail($request['image']);

        File::delete('them/uploads/poems/' . $image->blog_image);

        $update_data = array();
        $update_data['blog_image'] = '';
        $image->update($update_data);
        exit();
    }
}
