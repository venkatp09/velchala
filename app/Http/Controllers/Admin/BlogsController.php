<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blogs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Videos;
use App\Models\Photos;
use File;

class BlogsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = Blogs::removeBlog($requestData['blog_id']);

            if ($action == true) {
                $msg = "Blog deleted successfully!";
            } else {
                $msg = "Blog delete Error!";

            }
            return redirect()->route('blogs')->with('flash_message', $msg);
        } else {

            $blogs =Blogs::orderBy('blog_id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'blogs';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'blogs';
        $data['blogs'] = $blogs;
        return view('backend.blogs.list', $data);
    }

    public function addNewBlogs(Request $request, $id = null)
    {
        $tableInfo = new Blogs();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['blog_id'] != '') {
                $blogs = Blogs::findOrFail($requestData['blog_id']);
                $imageRule = empty($blogs->blog_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }
              
            Validator::make($request->all(), [
                'blog_title' => 'required',
                'blog_description' => 'required',
                'blog_image' => $imageRule,
                'blog_status' => 'required'
            ], [
                'blog_title.required' => 'Please enter title',
                'blog_description.required' => 'Please enter description',
                'blog_image.required' => 'select image to upload',
                'blog_status.required' => 'Select status',
            ]);


            $requestData['blog_user_id']=1;
            $requestData['blog_url']=str_replace(" ","-",$requestData['blog_title']);

             
            if ($requestData['blog_id'] == '') {
               
                if ($request->hasFile('blog_image')) {
                    $fileName = Blogs::imageUpload($request['blog_image']);
                    $requestData['blog_image'] = $fileName;
                }
               $requestData['blog_url']= str_slug($requestData['blog_title']);
                
                $eventId=Blogs::create($requestData); $mes = 'Blog added successfully!';
                if ($request->hasFile('gallery_images')){
                   
                 
                    $uploadPath1 = public_path('theme/uploads/photos/');
                    
                    if (!file_exists($uploadPath1)) {
                    mkdir($uploadPath1, 0777, true);
                    }
                    $countImg = count($request['gallery_images']);
                    
                    for($j=0;$j<$countImg;$j++){
                    $extension = $request['gallery_images'][$j]->getClientOriginalName();
                    $fileName = time() . $extension;
                   
                    $request['gallery_images'][$j]->move($uploadPath1, $fileName);
                    DB::table('photos')->insert(
                    array(
                   
                    'img'    =>   $fileName,
                    
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   4,
                    'event_name'      => $eventId->blog_id
                    )
                );
                    }

                }
                 if(isset($requestData['book_videos'])){
                $book_videosCount = count($requestData['book_videos']);
             for($i=0;$book_videosCount>$i;$i++){
               
                 DB::table('video')->insert(
                    array(
                   
                    'video_url'    =>   $requestData['book_videos'][$i],
                    'poem_name'   =>    $requestData['book_videos_name'][$i],
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   4,
                    'item_id'      =>  $eventId->blog_id
                    )
                );
            }

               
            }} else {
                  
                if ($request->hasFile('blog_image')) {
                    $fileName = Blogs::imageUpload($request['blog_image'], $blogs->blog_image);
                    $requestData['blog_image'] = $fileName;
                }

                $blogs->update($requestData);
                 $mes = 'Blog updated successfully!';

                 if ($request->hasFile('gallery_images')){
                   
                 
                    $uploadPath1 = public_path('theme/uploads/photos/');
                    
                    if (!file_exists($uploadPath1)) {
                    mkdir($uploadPath1, 0777, true);
                    }
                    $countImg = count($request['gallery_images']);
                    
                    for($j=0;$j<$countImg;$j++){
                    $extension = $request['gallery_images'][$j]->getClientOriginalName();
                    $fileName = time() . $extension;
                   
                    $request['gallery_images'][$j]->move($uploadPath1, $fileName);
                    DB::table('photos')->insert(
                    array(
                   
                    'img'    =>   $fileName,
                    
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   4,
                    'event_name'      => $requestData['blog_id']
                    )
                );
                    }

                }
                Videos::where('item_id', $requestData['blog_id'])->delete();
                 if(isset($requestData['book_videos'])){
                $book_videosCount = count($requestData['book_videos']);
             for($i=0;$book_videosCount>$i;$i++){
               
                 DB::table('video')->insert(
                    array(
                   
                    'video_url'    =>   $requestData['book_videos'][$i],
                    'poem_name'   =>    $requestData['book_videos_name'][$i],
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   4,
                    'item_id'      =>   $requestData['blog_id']
                    )
                );
            }
                
            }
             
            }
            
            return redirect()->route('blogs')->with('flash_message',$mes);

        } else {
            $data = array();
            $data['blog_id'] = '';
            $data['blog'] = '';
            $data['event_videos'] = '';
            $data['event_images'] = '';
            if ($id) {
                $data['blog_id'] = $id;
                $data['blog'] = Blogs::findOrFail($id);
                $data['event_videos'] = Videos::where(['item_id'=>$request->segment(4),'cat_type'=>4])->get();
                $data['event_images'] = Photos::where(['event_name'=>$request->segment(4),'cat_type'=>4])->get();
            }
            $data['active_menu'] = 'blogs';
            $data['sub_active_menu'] = 'manage-blogs';
            $data['title'] = 'Manage blogs';
            return view('backend.blogs.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = Blogs::findOrFail($request['image']);

        File::delete('uploads/blogs/' . $image->blog_image);

        $update_data = array();
        $update_data['blog_image'] = '';
        $image->update($update_data);
        exit();
    }
}
