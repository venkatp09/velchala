<?php

namespace App\Http\Controllers\Admin;

use App\Models\JayanthiEvents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Videos;
use App\Models\Photos;
use Validator;
use File;

class EventsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = JayanthiEvents::removeEvent($requestData['id']);

            if ($action == true) {
                $msg = "Event deleted successfully!";
            } else {
                $msg = "Event delete Error!";

            }
            return redirect()->route('events')->with('flash_message', $msg);
        } else {

            $events =JayanthiEvents::orderBy('id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'events';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'events';
        $data['events'] = $events;
        return view('backend.events.list', $data);
    }
    
   public function delete_img(Request $request){
        
        
            $requestData = $request->event_img_id;
            $action = Photos::removePhots($requestData);
            $mes="Image deleted successfully";
            echo $mes;
           

           
    }

    public function addNewEvents(Request $request, $id = null)
    {
        $tableInfo = new JayanthiEvents();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['id'] != '') {
                $events = JayanthiEvents::findOrFail($requestData['id']);
                $imageRule = empty($events->img) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [

                'event_name' => 'required',
                'event_des' => 'required',
                'img' => $imageRule,
                'event_date' => 'required',
                'event_location' => 'required',
            ], [
                'event_name.required' => 'Please enter title',
                'event_des.required' => 'Please enter description',
                'img.required' => 'select image to upload',
                'event_date.required' => 'Select Date',
                'event_location.required' => 'Select Date',
            ]);
               $requestData['event_url'] = str_slug($requestData['event_name']);
            if ($requestData['id'] == '') {
                if ($request->hasFile('img')) {
                    $fileName = JayanthiEvents::imageUpload($request['img']);
                    $requestData['img'] = $fileName;
                }

                $eventId= JayanthiEvents::create($requestData);

                if ($request->hasFile('gallery_images')){
                   
                 
                    $uploadPath1 = public_path('theme/uploads/photos/');
                    Videos::where('item_id', $requestData['id'])->delete();
                    if (!file_exists($uploadPath1)) {
                    mkdir($uploadPath1, 0777, true);
                    }
                    $countImg = count($request['gallery_images']);
                    
                    for($j=0;$j<$countImg;$j++){
                    $extension = $request['gallery_images'][$j]->getClientOriginalName();
                    $fileName = time() . $extension;
                   
                    $request['gallery_images'][$j]->move($uploadPath1, $fileName);
                    DB::table('photos')->insert(
                    array(
                   
                    'img'    =>   $fileName,
                    
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   3,
                    'event_name'      => $eventId->id
                    )
                );
                    }

                }
                if(isset($requestData['book_videos'])){
                $book_videosCount = count($requestData['book_videos']);
             for($i=0;$book_videosCount>$i;$i++){
               
                 DB::table('video')->insert(
                    array(
                   
                    'video_url'    =>   $requestData['book_videos'][$i],
                    'poem_name'   =>    $requestData['book_videos_name'][$i],
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   3,
                    'item_id'      =>  $eventId->id
                    )
                );
            } }

                $mes = 'Event added successfully!';
            } else {
               
                if ($request->hasFile('img')) {

                    $fileName = JayanthiEvents::imageUpload($request['img'], $events->img);
                    $requestData['img'] = $fileName;
                }  
                 $events->update($requestData);
                   if ($request->hasFile('gallery_images')){
                   
                 
                    $uploadPath1 = public_path('theme/uploads/photos/');
                    
                    if (!file_exists($uploadPath1)) {
                    mkdir($uploadPath1, 0777, true);
                    }
                    $countImg = count($request['gallery_images']);
                    
                    for($j=0;$j<$countImg;$j++){
                    $extension = $request['gallery_images'][$j]->getClientOriginalName();
                    $fileName = time() . $extension;
                   
                    $request['gallery_images'][$j]->move($uploadPath1, $fileName);
                    DB::table('photos')->insert(
                    array(
                   
                    'img'    =>   $fileName,
                    
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   3,
                    'event_name'      => $requestData['id']
                    )
                );
                    }

                }
                Videos::where('item_id', $requestData['id'])->where('cat_type','3')->delete();
                if(isset($requestData['book_videos'])){
                $book_videosCount = count($requestData['book_videos']);
             for($i=0;$book_videosCount>$i;$i++){
               
                 DB::table('video')->insert(
                    array(
                   
                    'video_url'    =>   $requestData['book_videos'][$i],
                    'poem_name'   =>    $requestData['book_videos_name'][$i],
                    'created_at'   =>   date("Y-m-d H:i:s"),
                    'updated_at'   =>   date("Y-m-d H:i:s"),
                    'cat_type'     =>   3,
                    'item_id'      =>   $requestData['id']
                    )
                );
            }
               
            }
             $mes = 'Events updated successfully!';
        }
            
            return redirect()->route('events')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['id'] = '';
            $data['event'] = '';
            $data['event_videos'] = '';
            $data['event_images'] = '';
            if ($id) {
                $data['id'] = $id;
                $data['event'] = JayanthiEvents::findOrFail($id);
                $data['event_videos'] = Videos::where(['item_id'=>$request->segment(4),'cat_type'=>3])->get();
                $data['event_images'] = Photos::where(['event_name'=>$request->segment(4),'cat_type'=>3])->get();
            }
            $data['active_menu'] = 'events';
            
            $data['title'] = 'Manage Events';
            return view('backend.events.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = JayanthiEvents::findOrFail($request['image']);

        File::delete('theme/uploads/events/' . $image->img);

        $update_data = array();
        $update_data['blog_image'] = '';
        $image->update($update_data);
        exit();
    }
}
