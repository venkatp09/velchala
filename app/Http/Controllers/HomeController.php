<?php

namespace App\Http\Controllers;

use App\Models\Contactus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function Contactus(Request $request)
    {



        if ($request->isMethod('post')) {

            $requestData = $request->all();
            request()->validate([
                'first_name' => 'required',
                'email' => 'required | email',
                'phone_number' => 'required | numeric',
                'subject' => 'required',
                'message' => 'required',
            ], 
            [
                'first_name.required' => 'Enter First Name',
                'email.required' => 'Enter email',
                'email.email' => 'Enter a valid email',
                'phone_number.required' => 'Enter Phone Numbers',
                'phone_number.numeric' => 'Enter Numbers Only',
                'subject.required' => 'Enter Subject',
                'message.required' => 'Enter Message',
            ]);

         
            $conatct_id = Contactus::create($requestData)->cu_id;

            Contactus::contactUsEmails($conatct_id);


            $mes = "Your request sent successfully. We will get back to you soon.";
            return redirect()->back()->with('flash_message', $mes);

        }


        $data = array();
        $data['title'] = 'velchala.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'velchala.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.contact', $data);
    }

}
