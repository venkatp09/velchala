<?php


use App\Models\Products;


define('ADMIN_EMAIL', 'info@velchala.com');



function get_cart_details()
{
    if(isset(auth()->user()->id)){

      $userId=auth()->user()->id;

      }else{

        $userId = $req->session()->get('cartId');
      }
    $array_data = array();
    $countCart = DB::table('cart_items')
       ->join('cart', 'cart.id', '=', 'cart_items.cart_id')
       ->where("cart.user_id",$userId)->count('cart_items.id');
  
    return $countCart;

}
function getBreadcrumbs($parameters, $title, $url = null)
{
    if ($url) {
        $img = $url;
    } else {
        $img = '/plugins/images/heading-title-bg.jpg';
    }


    ?>

    <?php


    $html = '<div class="breadcrumb-holder">
        <div class="container-fluid">
            
                    <ul class="breadcrumb ">';
    foreach ($parameters as $key => $value) {
        if (is_array($value)) {

            $route = $key;
            $name = '';
            $options = array();

            foreach ($value as $akey => $avalue) {
                if ($akey == 'name') {
                    $name = $avalue;
                } else {
                    $options = $avalue;
                }
            }

            $url = route($route, $options);

            if (!next($parameters)) {
                $html .= '<li class="breadcrumb-item active">' . $name . '</li>';

            } else {
                $html .= '<li class="breadcrumb-item"><a href=' . $url . '>' . $name . '</a></li>';
            }

        } else {
            if (!next($parameters)) {
                $html .= '<li class="breadcrumb-item active">' . $value . '</li>';
            } else {
                $url = route($key);
                $html .= '<li class="breadcrumb-item"><a href=' . $url . '>' . $value . '</a></li>';
            }
        }
    }
    $html .= '</ul>
               
        </div>
    </div>';
    return $html;
}
function getCategory($id = '')
{

    $list = '';

    if ($id != '') {

        $list = \App\Models\Categories::findOrFail($id);
    } else {
//        \Illuminate\Support\Facades\DB::enableQueryLog();
        $list = \App\Models\Categories::with(['getSubCategoryTypes' => function ($query) {
            $query->where('category_status', 'active');
        }, 'getSubCategoryOthers' => function ($query) {
            $query->where('category_status', 'active');
        }])->where('category_parent_id', 0)->where('category_status', 'active')->get();
//dd(\Illuminate\Support\Facades\DB::getQueryLog());

    }

    return $list;

}

function availability($flag = null)
{

    $return_data = array('in_stock' => 'In Stock', 'out_of_stock' => 'Out Of Stock');
    if ($flag) {
        $return_data = $return_data[$flag];
    }
    return $return_data;

}

function sendSms($no, $msg, $country = 91)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https:pi/sendhttp.php");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "mobiles=$=$msg&country=$country");
//    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=sonygolden&password=info1234&to=$no&from=SGSCHT&message=$msg");
    $buffer = curl_exec($ch);

    if (empty($buffer)) {
        echo "buffer is empty";
    }

    curl_close($ch);
}


function vdcSettings($colum = null)
{

    $settings = \App\Models\Settings::get()->first();

//    dd($settings);

    if ($colum != '' && isset($settings->$colum)) {
        $settings = $settings->$colum;
    }


    return $settings;


}

function commonSettings($option)
{
    switch ($option) {
        case 'adminEmail':
            return 'info@velchala.com';
            break;
        case 'adminGmail':
            return 'info@velchala.com';
            break;
        case 'adminName':
            return 'velchal.com';
            break;
    }
}


function wordSort($word)
{

    $string = str_replace('-', ' ', $word);

    return ucwords($string);

}

function getPhoneNumber($number)
{

    $expolde = explode('-', $number);

    if (count($expolde) == 2) {

        return list($prefix, $mobile) = $expolde;
    } else {

        array_unshift($expolde, 91);
        return list($prefix, $mobile) = $expolde;
    }

//    dd($expolde);


}







function orderItemStatus($status)
{

//    $orderItems->oitem_status
    ?>
    <div class="col-md-12">
        <div class="progress">
            <?php
            switch ($status) {
                case('Shipped'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Payment Received
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning"
                         style="width:25%">
                        <?= $status ?>
                    </div>
                    <?php
                    break;
                case('OutForDelivery'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Payment Received
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning"
                         style="width:25%">
                        Shipped
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info"
                         style="width:25%">
                        <?= $status ?>
                    </div>
                    <?php
                    break;
                case('Delivered'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Payment Received
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning"
                         style="width:25%">
                        Shipped
                    </div>

                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info"
                         style="width:25%">
                        Out For Delivery
                    </div>

                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        <?= $status ?>
                    </div>
                    <?php
                    break;
                case('PendingPaymnet'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:100%">
                        Failed Payment
                    </div>
                    <?php
                    break;

                default:
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Placed
                    </div>
                    <?php
                    break;
                    ?>


                <?php
            }
            ?>
        </div>
    </div>
    <?php
}




function allStatuses($module, $status = null)
{
    $return_data = "";
    $displayStatus =
        array(
            "general" => array('1' => 'Active', '0' => 'Inactive'),
            "order" => array('Pending' => 'Pending', 'Completed' => 'Completed', 'Cancel' => 'Cancel'),
        );

    if ($module && $status) {


        $return_data = $displayStatus[$module][$status];
    } else {
        $return_data = $displayStatus[$module];
    }
    return $return_data;

}





function snedSMS($mobile, $message, $county = 91, $ruote = 'SKVAPE', $authkey = '248828AyDlWFTTn95bf8f4b8')
{
// {{snedSMS(9848090537, 'Hi test mesasage By developer \n\n\n By skvapee.com')}}

    $mobile = $mobile;
    $message = str_replace('\n', '%0a', $message);
    $message = str_replace('\r', '%0a', $message);
    $message = str_replace('<br/>', '%0a', $message);
    $message = str_replace('<br />', '%0a', $message);
    $message = urlencode($message);

    $response = '';
    if (is_array($mobile)) {
        if (is_numeric($mobile)) {
            $response = \Ixudra\Curl\Facades\Curl::to('http://api.msg91.com/api/sendhttp.php?country=' . $county . '&sender=' . $ruote . '&route=4&mobiles=' . implode(',', $mobile) . '&authkey=' . $authkey . '&message=' . $message . '&unicode=1')->get();
        }
    } else {
        if (is_numeric($mobile)) {
            $response = \Ixudra\Curl\Facades\Curl::to('http://api.msg91.com/api/sendhttp.php?country=' . $county . '&sender=' . $ruote . '&route=4&mobiles=' . $mobile . '&authkey=' . $authkey . '&message=' . $message . '&unicode=1')->get();
        }
    }
    return $response;
}



function productInftroTheme($arraydata)
{
    $returnarray = '';
    if (count($arraydata) > 0) {
        foreach ($arraydata AS $arrayItam) {

            switch ($arrayItam->pe_position) {
                case '1':

                    $returnarray .= '<div class="py-4">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <h2 class="h3 fmed">' . $arrayItam->pe_title . '</h2>
                            <p class="py-3">' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="py-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';
                    break;

                case '2':
                    $returnarray .= '<div class="graybg reviewpad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 align-self-center">
                            <h3 class="reviewsectitle pb-2">' . $arrayItam->pe_title . '</h3>
                            <p>' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-6">
                            <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>';

                    break;
                case '3':

                    $returnarray .= '<div class="reviewpad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 align-self-center order-last">
                            <p>' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-6">
                            <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>';

                    break;

                case '4':

                    $returnarray .= '<div class="graybg reviewpadtop">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 text-center">
                            <h2 class="h3 fmed pb-5">' . $arrayItam->pe_description . '</h2>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="pt-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';

                    break;

                default:

                    $returnarray .= '<div class="graybg reviewpadtop">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 text-center">
                            <h2 class="h3 fmed pb-5">' . $arrayItam->pe_description . '</h2>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="pt-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';

                    break;
            }
        }
    }

    return $returnarray;
}

function g_print($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}


function javascriptFunctions()
{
    ?>
    <script type="text/javascript">
        function discountCalcWithParent(parent, original, type, discount, discountValue, add = null) {

            var p_regular_price = $(parent + ' ' + original).val();
            var discount_type = $(parent + ' ' + type).val();
            var p_vdc_vendor_discount = $(parent + ' ' + discount).val();


            var percentage;

            if (discount_type == 'fixed') {

                if (add) {
                    percentage = (parseInt(p_regular_price) + parseInt(p_vdc_vendor_discount));
                } else {
                    percentage = (parseInt(p_regular_price) - parseInt(p_vdc_vendor_discount));
                }


            } else if (discount_type == 'percentage') {
                var p_vdc_vendor_discount_price = (p_regular_price / 100 * p_vdc_vendor_discount);

                if (add) {
                    percentage = (parseInt(p_regular_price) + parseInt(p_vdc_vendor_discount_price));
                } else {
                    percentage = (parseInt(p_regular_price) + -parseInt(p_vdc_vendor_discount_price));
                }

            }

            $(parent + ' ' + discountValue).val(percentage);

            console.log(discount_type + ' - discountCalc- ' + percentage + 'p_regular_price ' + p_regular_price + 'p_vdc_vendor_discount ' + p_vdc_vendor_discount);

            // console.log(p_regular_price + " <> " + discount_type + " <> " + p_vdc_vendor_discount + " <> " + percentage);


        }


        function discountCalc(original, type, discount, discountValue) {

            var p_regular_price = $(original).val();
            var discount_type = $(type).val();
            var p_vdc_vendor_discount = $(discount).val();


            var percentage;

            if (discount_type == 'fixed') {

                percentage = (p_regular_price - p_vdc_vendor_discount);


            } else if (discount_type == 'percentage') {
                var p_vdc_vendor_discount_price = (p_regular_price / 100 * p_vdc_vendor_discount);
                percentage = (p_regular_price - p_vdc_vendor_discount_price);
            }

            $(discountValue).val(percentage);


        }

        function imageUploadValidation(input, limit) {
            console.log(limit);

            $(input).on("change", function () {
                if ($(input)[0].files.length > limit) {
                    alert("Select Maximum 5 Images");
                    // alert("Select Maximum " + limit + " Images");
                    $(input).val('');
                } else {

                }
            });
        }

    </script>
    <?php
}


function skuTypes()
{
    return [
        'weight',
        'color',
        'quantity',
        'Size'
    ];
}


function product_skus($action, $skues, $id = null)
{
    $array_return = array();

    if ($action = 'sort') {
        if (count($skues) > 0) {
            foreach ($skues AS $sku) {
//                    dd($sku);

                $array_return[$sku['sku_type']][$sku['sku_id']] = $sku;

            }
        }


        return $array_return;

    }


}


function getProduct_Price_calc($sortingSkus, $getparams)
{

    $return_array = array();

    if (count($getparams) > 0) {
        foreach ($getparams AS $getparamKye => $getparam) {
//                dump($sortingSkus);
//                dump($getparamKye);
            if (array_key_exists($getparamKye, $sortingSkus)) {

                if (isset($sortingSkus[$getparamKye][$getparam])) {
                    $finaPriceData = $sortingSkus[$getparamKye][$getparam];


                    $return_array['vdcPrice'][] = $finaPriceData->sku_vdc_final_price;
                    $return_array['storePrice'][] = $finaPriceData->sku_store_price;
                } else {

//                        dd($sortingSkus);

                    $finaPriceData = array_first($sortingSkus[$getparamKye]);


                    $return_array['vdcPrice'][] = $finaPriceData->sku_vdc_final_price;
                    $return_array['storePrice'][] = $finaPriceData->sku_store_price;

                }


//                    dump($sortingSkus[$getparamKye][$getparam]);

            }
        }
    } else {
        if (count($sortingSkus) > 0) {
            foreach ($sortingSkus AS $sortingSku) {
//                    dump($sortingSku);


                $finaPriceData = array_first($sortingSku);


                $return_array['vdcPrice'][] = $finaPriceData->sku_vdc_final_price;
                $return_array['storePrice'][] = $finaPriceData->sku_store_price;

            }
        }

    }


    $return_array['vdcPrice'] = array_sum($return_array['vdcPrice']);
    $return_array['storePrice'] = array_sum($return_array['storePrice']);


    return $return_array;
}


function catSubSortByTypes($catid, $alias = null)
{
    $returndata = '';

    if ($alias == 'alias') {
        switch ($catid) {
            case '':
                break;
            default:
                break;
        }
    } else {

        switch ($catid) {
            case '117':
                $returndata = "Home Made Sweets";

                break;

//            case '6':
//                $returndata = "By millers";
//
//                break;
            default:
                $returndata = "By Type";

                break;
        }
    }

    return $returndata;


}




function formValidationValue($inputname, $value = null)
{

    if ($value != null) {
        echo !empty(old($inputname)) ? old($inputname) : ((($value) && ($value->$inputname)) ? $value->$inputname : '');

    } else {

        echo !empty(old($inputname)) ? old($inputname) : '';
    }


}

function formValidationError($errors, $inputname)
{
    if ($errors->has($inputname)) {
        ?>
        <span class="invalid-feedback" role="alert">
           <strong>
               <?php
               echo $errors->first($inputname)
               ?>
            </strong>
        </span>
        <?php
    }
}


function laravelReturnMessageShow()
{
    if (Session::has('flash_message')) {
        ?>
        <br/>
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= Session::get('flash_message') ?></strong>
        </div>
        <?php
    }

    if (Session::has('flash_message_error')) {
        ?>

        <br/>
        <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= Session::get('flash_message_error') ?></strong>
        </div>

        <?php
    }

}


function shippingPriceList($country)
{
    $returnAmount = null;

    $countryInfo = \App\Models\Countries::where('country_id', $country)->first();
    if ($countryInfo->country_weight_description) {
        $returnAmount = splitPrice($countryInfo->country_weight_description);
    }

    return $returnAmount;
}

function caliclateShippingAmount($weight, $country)
{

    $returnAmount = '';

    $countryInfo = \App\Models\Countries::where('country_id', $country)->first();
    if ($countryInfo->country_weight_description) {
        $returnAmount = packagePriseSort($countryInfo->country_weight_description, $weight);
    } else {
        $returnAmount = 0;
    }


    $returnAmount = curencyConvert(getCurency(), $returnAmount);

    return $returnAmount;


}

function sortProductOption($options, $option = null)
{
    $splitPriceData = explode(',', $options);

    $splitPriceDataList = array();

    if (count($splitPriceData) > 0) {
        foreach ($splitPriceData AS $splitPriceItem) {

            $otiponsInfo = explode('/', $splitPriceItem);

            if ($option == 'weight') {

                $splitPriceDataList[$otiponsInfo[0]] = $otiponsInfo[1];
//                $splitPriceDataList[$otiponsInfo[0]] = $otiponsInfo[1];
            } else {

                $splitPriceDataList[$otiponsInfo[0]] = $otiponsInfo[1];
            }

        }
    }


    if (isset($splitPriceDataList[$option])) {
        return $splitPriceDataList[$option];
    } else {
        return 0;
    }
}

function strSkuToGetSkuId($strSku)
{
    $returnArray = array();


    $arraySplit = explode('/', $strSku);
    if(count($arraySplit) == 3){
        $returnArray = $arraySplit[2];
    }else{
        $returnArray = null;
    }


//    dd($arraySplit);


    return $returnArray;
}

function sortProductOptions($options)
{


    $returnData = array();


    if (is_array($options)) {

        $optionslists = $options;
    } else {

        $optionslists = explode(',', $options);
    }


    $optionslistsFinal = array();

    if (count(array_filter($optionslists)) > 0) {
        foreach ($optionslists AS $optionslist) {


            $optionArray = explode('/', $optionslist);

            list($optName, $optval, $optID) = $optionArray;

//            dump($optName);

            if ($optName == 'weight') {
//                $optionslistsFinal[$optName] = $optval . 'Kgs';
//                dump($optval);
//                dd(productWightFarmart($optval));

                if (empty(productWightFarmart($optval))) {
                    $optionslistsFinal[$optName] = $optval . productWightFarmart($optval);
                } else {
//                    dd(productWightFarmart($optval));
                    $optionslistsFinal[$optName] = productWightFarmart($optval)['weight'] . productWightFarmart($optval)['type'];
//                    $optionslistsFinal[$optName] = $optval.productWightFarmart($optval)['weight'];

                }


            } else {
                $optionslistsFinal[$optName] = $optval;
            }


        }
    }

//    dd($optionslistsFinal);

    $returnData = http_build_query($optionslistsFinal, ' ', ', ');

    $returnData = str_replace('=', ': ', $returnData);
    $returnData = str_replace('+', ' ', $returnData);

    return $returnData;

}

function PayapPaymentStatusShow($status)
{
    $returnData = '';

    switch ($status) {
        case 'approved':
            $returnData = "<span class='greencolor'> Payment Completed </span>";
            break;
        default:
            $returnData = "<span class='redcolor'> Payment Failed</span>";
            break;
    }

    return $returnData;
}

function OrderPaymentStatus($status)
{
    $returnData = '';

    switch ($status) {
        case 'approved':
            $returnData = "Paid";
            break;
        default:
            $returnData = "";
            break;
    }

    return $returnData;
}

function OrderStatus($status)
{
    $returnData = '';

    switch ($status) {
        case 'PendingPayment':
            $returnData = "<span class='redcolor'> Payment Failed </span>";
            break;
        case 'Paid':
            $returnData = "<span class='redcolor'> Placed </span>";
            break;
        default:
            $returnData = "<span class='greencolor'> $status </span>";
            break;
    }

    return $returnData;
}


//ALL SMS 9032615531
define('MSG_ADMIN_NUMBER', '+919573302201');
//define('MSG_ADMIN_NUMBER', '8977111142');
define('MSG_USER_REGISTER_NOTFICATION_ADMIN', '{USER_NAME} Registered with you as User');
define('MSG_USER_REGISTER_NOTFICATION_USER', 'Dear Velchala Customer, you have registered successfully. Please check your email for verification link.');


define('MSG_VENDOR_REGISTER_NOTFICATION_ADMIN', '{USER_NAME} Registered with you as Velchala Vendor');
define('MSG_VENDOR_REGISTER_NOTFICATION_VENDOR', 'Dear Velchala Vendor, you have registered successfully. Please check your email for verification link.');


define('MSG_SERVICE_REQUEST_ADMIN', 'Dear Velchala Admin, You received a "{SERVICE_NAME}" enquiry from "{USER_NAME}". Please review the request.');
define('MSG_SERVICE_REQUEST_USER', 'Dear Velchala Customer, we received enquiry for "{SERVICE_NAME}". We will review and get back to you soon.');

define('MSG_SERVICE_INVOICE_CREATE_USER', 'Dear Velchala Customer, you have received incovice for {SERVICE_NAME}. Please login to Velchala.com to make payment.');

define('MSG_PRODUCT_ENQUERY_ACCEPT_USER', 'Dear Velchala Customer, your enquiry for "{PRODUCT_NAME}" is received. We have the product available please use this OTP {OTP} number to purchase. visit to to https://www.Velchala.com');
define('MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR', 'Dear Velchala Vendor, you received an enquiry for "{PRODUCT_NAME}". Please check and notify Velchala Admin on the availability of the product.');
define('MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN', 'Dear Velchala Admin, you received an enquiry by {USER_NAME} for "{PRODUCT_NAME}". Please check the availability with Vendor.');

define('MSG_PRODUCT_ENQUERY_REJECT_USER', 'Dear Velchala Customer, the "{PRODUCT_NAME}" you enquired is not available. Check Velchala.com for more products. ({ADMINMSG})');


define('MSG_CART_PAYMENT_DONE_USER', 'Dear Velchala Customer, your order for {PRODUCT_NAME} is successfully placed. Your Order id is #{ORDER_ID}. Login to Velchala.com for checking the order status.');
define('MSG_CART_PAYMENT_DONE_VENDOR', 'Dear Velchala Vendor, you got an order for {PRODUCT_NAME}. Order id is #{ORDER_ID}. For more details please, login to https://www.Velchala.com/store/login.');
define('MSG_CART_PAYMENT_DONE_ADMIN', 'Dear Velchala Admin, you got an order for {PRODUCT_NAME}. Order id is #{ORDER_ID}. For more details please, login to https://www.Velchala.com/admin/login.');


//'https://api.msg91.com/api/sendhttp.php?mobiles=9642123254,7995165789&authkey=281207Ap4gZfODekY5d05cC&message=Hello!%20This%20is%20a%20test%20message&country=91';

