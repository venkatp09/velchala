<?php

function vendorValidation($vendorId)
{
    $profileAccount = \App\Models\VendorAccount::where('va_vendor_id', $vendorId)->first();
//    $data['profileAccount'] = VendorAccount::where('va_vendor_id', Auth::guard('store')->user()->id)->first();
    $profileBusiness = \App\Models\VendorBusiness::where('vb_vendor_id', $vendorId)->first();
//    $data['profileBusiness'] = VendorBusiness::where('vb_vendor_id', Auth::guard('store')->user()->id)->first();


    if (isset($profileAccount) && $profileAccount->va_status == 'approved' && isset($profileBusiness) && $profileBusiness->vb_status == 'approved') {
        return true;
    } else {
        return false;
    }

}



function getAllCountries($id = '', $all = '')
{

    if ($id != '') {
        if ($all != '') {
            $countries = \App\Models\Countries::where('country_id', $id)->where('country_status', 'active')->orderBy('country_id', 'desc')->get();

        } else {

            $countries = \App\Models\Countries::where('country_id', $id)->where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
        }

    } else {
        if ($all != '') {
            $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->get();

        } else {

            $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
        }
    }


    return $countries;

}

function getCountries($type = null)
{
//    dump($type);


    switch ($type) {
        case '1':

            $countries = \App\Models\Countries::where('country_id', 1)->where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
            return $countries;

            break;
        case '2':

            $countries = \App\Models\Countries::where('country_id', '!=', 1)->where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
            return $countries;

            break;
//        case '3':
//            break;
        default:
            $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
            return $countries;
            break;
    }


//    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_id');
//    return $countries;

}

function getCategories()
{
    $categories = \App\Models\Categories::where('category_status', 'active')->where('category_parent_id', 0)->orderBy('category_order', 'asc')->pluck('category_name', 'category_id');
    return $categories;

}

function getCategoriesByID()
{
    $categories = \App\Models\Categories::where('category_status', 'active')->where('category_parent_id', 0)->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
    return $categories;

}

function getTypesByID($catid = null)
{
    if ($catid) {
        $categories = \App\Models\Categories::where('category_status', 'active')->where('category_id', $catid)->where('category_options', 'types')->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
    } else {
        $categories = \App\Models\Categories::where('category_status', 'active')->where('category_options', 'types')->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
    }
    return $categories;

}

function getItemSpecialsByID($catid = null)
{
    if ($catid) {
        $categories = \App\Models\Categories::where('category_status', 'active')->where('category_id', $catid)->where('category_options', 'others')->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
    } else {
        $categories = \App\Models\Categories::where('category_status', 'active')->where('category_options', 'others')->orderBy('category_id', 'asc')->pluck('category_name', 'category_id');
    }
    return $categories;

}

function getBrands($catid = null)
{
    if ($catid) {
        $brands = \App\Models\Brands::whereRaw('FIND_IN_SET(' . $catid . ', brand_catid)')->where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_alias');
    } else {
        $brands = \App\Models\Brands::where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_alias');
    }
    return $brands;

}

function getBrandsByID($catid = null)
{
    if ($catid) {
        $brands = \App\Models\Brands::where('brand_catid', $catid)->where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_id');
    } else {

        $brands = \App\Models\Brands::where('brand_status', 'active')->orderBy('brand_id', 'desc')->pluck('brand_name', 'brand_id');
    }
    return $brands;

}

function getCountriesByCode($location)
{
    App::setLocale($location);
    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'desc')->pluck('country_name', 'country_code');
    return $countries;

}

function getCountriesByPrefixes()
{
    $countries = \App\Models\Countries::where('country_status', 'active')->orderBy('country_id', 'asc')->pluck('country_phone_prefix');
    return $countries;

}

function getWishlistProducts()
{
    $products = \App\Models\Wishlist::where('user_id', \Illuminate\Support\Facades\Auth::id())->pluck('publi_id')->toArray();
    return $products;

}

function getCountryByCode($country)
{
    $countries = \App\Models\Countries::where('country_code', $country)->first();
    return $countries;
}

function getProductImage($productid)
{
    $image = \App\Models\ProductImages::where('pi_product_id', $productid)->first();
    return $image;
}

function getProduct($productid)
{
    $product = \App\Models\Products::with('getCategory')->where('p_id', $productid)->first();
    return $product;
}


function getCountrycodes($countryid = null)
{
    if ($countryid) {
        $list = \App\Models\Countries::where('country_id', $countryid)->where('country_status', 'active')->pluck('country_phone_prefix', 'country_phone_prefix');

    } else {
        $list = \App\Models\Countries::where('country_status', 'active')->pluck('country_phone_prefix', 'country_phone_prefix');

    }
    return $list;
}

//function getAmmachethivantaProductPrice($parentid, $weight)
//{
//    $info = \App\Models\AmmachethivantaSKUS::where('as_weight', $weight)->where('as_ac_id', $parentid)->first();
//    return $info;
//}

function getAmmachethivantaParentInfo($parentid)
{
    $info = \App\Models\Ammachethivanta::where('ac_id', $parentid)->first();
    return $info;
}

//function getOptionInfo($optionid){
//    $option = \App\Models\ProductSkus::where('sku_id', $optionid)->first();
//    return $option;
//}


function paypalResponce($data)
{
    $responceData = unserialize($data);

    return array_first($responceData);

//    dd($data);
}