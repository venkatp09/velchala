-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2020 at 08:08 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_velchala`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `blog_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_user_id` int(11) DEFAULT NULL,
  `blog_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_articles`
--

CREATE TABLE `blog_articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_article_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_article_pdf` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_article_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_article_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_articles`
--

INSERT INTO `blog_articles` (`id`, `blog_article_name`, `blog_article_pdf`, `blog_article_url`, `blog_article_desc`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(4, 'The Republic Day1', '1602253671readbook.pdf', 'the-republic-day1', '<h4 style=\"margin-right: 0px; margin-left: 0px; padding: 0px; list-style: none; font-family: Poppins, sans-serif; font-weight: 400; line-height: 1.2; color: rgb(0, 0, 0); font-size: 1.5rem;\">Me unpleasing impossible</h4><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">Its sometimes her behaviour are contented. Do listening am eagerness oh objection collected. Together gay feelings continue juvenile had off one. Unknown may service subject her letters one bed. Child years noise ye in forty. Loud in this in both hold. My entrance me is disposal bachelor remember relation.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">Oh acceptance apartments up sympathize astonished delightful. Waiting him new lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">So insisted received is occasion advanced honoured.Among ready to which up. Attacks smiling and may out assured moments man nothing outward.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem quisquam mollitia suscipit possimus reprehenderit corporis neque dolores ipsum sint porro eos repellat velit nisi doloribus quibusdam, delectus tenetur similique. Cupiditate id unde repellat doloremque non ut vel excepturi numquam nostrum inventore, itaque nihil laudantium sint tempore dignissimos obcaecati labore rem nesciunt est molestiae tenetur, quia nemo illo. Sint saepe dolor, quis mollitia, ipsa voluptates aut enim rerum vero asperiores assumenda eligendi hic. Ad aperiam ullam maiores nam assumenda! Sit perspiciatis dolorum repellendus ducimus aperiam error explicabo animi quod fugit ea, temporibus quo facilis voluptas magni eveniet ipsum, voluptate praesentium fugiat!</p>', 'The Republic Day1', 'The Republic Day1', 'The Republic Day1', '2020-10-10 02:57:51', '2020-10-10 03:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `blog_news`
--

CREATE TABLE `blog_news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_news_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_news_img` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_news_date` date NOT NULL,
  `blog_news_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_news_paper_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `blog_news_des` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_news`
--

INSERT INTO `blog_news` (`id`, `blog_news_name`, `blog_news_img`, `blog_news_date`, `blog_news_url`, `blog_news_paper_name`, `created_at`, `updated_at`, `blog_news_des`) VALUES
(3, 'Velchala Kondal Rao : An eminent educationist The Hans India   |  26 July 2013 5:50 AM IST', '1602216100news01.jpg', '2013-07-26', 'https://www.thehansindia.com/posts/index/Opinion/2013-07-26/Velchala-Kondal-Rao-An-eminent-educationist/66968?infinitescroll=1', 'The Hans India', '2020-10-09 16:31:40', '2020-10-09 16:31:40', '<p><font color=\"#212529\" face=\"Source Sans Pro, sans-serif\">The 80th birthday of Velchala Kondal Rao falls on July 21 All this changed after Kondal Rao took charge. Every day he would go round the college twice or thrice.</font><br style=\"padding: 0px; margin: 0px; color: rgb(33, 37, 41); font-family: &quot;Source Sans Pro&quot;, sans-serif; background-color: rgb(235, 235, 235);\"><br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `cart_coupons_usage`
--

CREATE TABLE `cart_coupons_usage` (
  `cu_id` bigint(20) UNSIGNED NOT NULL,
  `cu_user_id` int(11) NOT NULL,
  `cu_coupan_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cu_order_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_coupons_usage`
--

INSERT INTO `cart_coupons_usage` (`cu_id`, `cu_user_id`, `cu_coupan_code`, `cu_order_id`, `created_at`, `updated_at`) VALUES
(1, 53, 'Rakhis2019', '5023', '2019-08-12 20:44:23', '2019-08-12 20:44:23');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_parent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `category_shipped` int(10) NOT NULL DEFAULT 0,
  `category_options` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'type,others',
  `category_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_order` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `category_meta_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_meta_keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_alias`, `category_parent_id`, `category_shipped`, `category_options`, `category_image`, `category_order`, `category_meta_title`, `category_meta_keywords`, `category_meta_description`, `category_status`, `created_at`, `updated_at`) VALUES
(1, 'Cakes', 'cakes', '0', 1, NULL, '1553670530cakes-navigation-image.jpg', '1', 'Order Cake Online Hyderabad| Online Cake Delivery in Hyderabad', 'Order Cake Online Hyderabad, Online Cake Delivery in Hyderabad', 'velchala online cake shop in Hyderabad offering variety of cakes for all occasions. Best cakes in Hyderabad with same day & midnight cake delivery in Hyderabad including free shipping.', 'active', '2019-03-27 15:38:50', '2019-09-06 10:58:20'),
(2, 'Flowers', 'flowers', '0', 1, NULL, '1553670575flowers-boutique-navigation-img.jpg', '2', 'Flower Online Order| Flower Online Delivery', 'Flower Online Order in hyderabad, Flower Online Delivery', 'Flower Online Order in Hyderabad, Flower Online Delivery Hyderabad', 'active', '2019-03-27 15:39:35', '2019-09-03 15:55:46'),
(3, 'Gifts', 'gifts', '0', 0, NULL, '1553670602gifts-navigation-image.jpg', '3', 'Online Gifts Delivery - Send Gifts to India, Best Gift Ideas From velchala', 'Online Gifts Delivery - Send Gifts to India, Best Gift Ideas From velchala', 'Online Gift Delivery for every occasion from velchala with FREE Shipping. Send gifts with Same Day Delivery, Midnight and Fixed Time Delivery options.', 'active', '2019-03-27 15:40:02', '2019-08-31 11:49:55'),
(4, 'Chocolates', 'chocolates', '0', 1, NULL, '1553670620chocklates-navigation-image.jpg', '4', 'Buy Chocolates Online in India | Best Chocolate Box', 'Buy Chocolates Online in India, Best Chocolate Box', 'In the age of internet you can buy chocolates onlinein India and get home delivery easily. And velchala bring for you a whole range of tempting chocolate.', 'active', '2019-03-27 15:40:21', '2019-08-31 11:26:55'),
(5, 'Silver Items', 'silver-items', '0', 1, NULL, '1553670644jewellery-navigation-image.jpg', '5', 'Silver Gift Items Online India | Silver Pooja Articles velchala', 'Silver Gift Items Online India, Silver Pooja Articles velchala', 'Shop for silver accessories such as kumkum dabbis, silver puja hampers, prayer bells and more from velchala. Elegantly designed, these silver accessories help bring light into your house.', 'active', '2019-03-27 15:40:44', '2019-08-31 11:52:17'),
(6, 'Millets', 'millets', '0', 0, NULL, '1553670670millets-navigation-img.jpg', '6', 'Millets - Buy Organic Millets Online In Hyderabad At Best Price', 'Millets - Buy Organic Millets Online, Siridhanya| velchala', 'Order Cereals online now and Millets - Buy Organic Millets Online In Hyderabad At Best Price and reasonable price.', 'active', '2019-03-27 15:41:10', '2019-08-31 11:51:24'),
(99, 'Temple Jewellery', 'temple-jewellery', '142', 0, 'types', NULL, '0', 'Temple Jewellery: temple jewellery online Collection', 'temple jewellery online Collection in hyderabad', 'Temple Jewellery: temple jewellery online Collection in Hyderabad, Hyderabad temple jewellery.', 'active', '2019-04-30 20:13:26', '2019-09-03 15:48:54'),
(98, 'Antique Jewellery', 'antique-jewellery', '142', 0, 'types', NULL, '0', 'Antique Jewellery| Buy Antique Jewellery Online - Antique Jewellery Collection', 'Antique Jewellery in hyderabad, Buy Antique Jewellery Online, Antique Jewellery Collection', 'Buy Antique Jewellery, Buy Antique Jewellery Online - Antique Jewellery Collection, Antique Jewellery in Hyderabad, Antique Jewellery in India, UK, USA and Australia.', 'active', '2019-04-30 20:13:08', '2019-09-03 15:17:21'),
(97, 'Hip Belts', 'hip-belts', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Hip Chains Vaddanam| Hip Belts| Waist Chain', 'Hip Chains Vaddanam, Hip Belts, Waist Chain in hyderabad', 'Fashion Jewellery: Hip Chains Vaddanam, Hip Belts, Waist Chain', 'active', '2019-04-30 20:12:42', '2019-09-03 15:39:52'),
(96, 'Toe Rings', 'toe-rings', '142', 0, 'types', NULL, '0', 'Toe Rings: Buy Silver, Gold Toe Rings Online', 'Toe Rings in hyderabad Buy Silver, Gold Toe Rings Online', 'Toe Rings: Buy Silver, Gold Toe Rings Online, order Toe Rings online in Hyderabad India.', 'active', '2019-04-30 20:12:17', '2019-09-03 15:49:17'),
(95, 'Rings', 'rings', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Shop Designer and Buy Fashion Rings Online', 'Shop Designer and Buy Fashion Rings Online', 'Fashion Jewellery: Shop Designer and Buy Fashion Rings Online in Hyderabad, Buy Rings from US & Delivery in Hyderabad for your friends, relatives.', 'active', '2019-04-30 20:11:59', '2019-09-03 15:48:03'),
(94, 'Necklaces', 'necklaces', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Necklaces for Women| Buy Indian Necklace', 'Necklaces for Women, Buy Indian Necklace', 'Fashion Jewellery Hyderabad, Necklaces for Women, Buy Indian Necklace, Order Necklaces Cheap Price in Hyderabad', 'active', '2019-04-30 20:10:53', '2019-09-03 15:47:22'),
(93, 'Long Chain', 'long-chain', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Long Chain| Long Chain - Buy Long Chain online', 'Long Chain in hyderabad, Long Chain, Buy Long Chain online', 'Fashion Jewellery: Long Chain, Long Chain - Buy Long Chain online', 'active', '2019-04-30 20:10:31', '2019-09-03 15:46:40'),
(92, 'Bracelets', 'bracelets', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Buy Fashion Bracelets Jewellery Online', 'Fashion Jewellery in hyderabad, Buy Fashion Bracelets Jewellery Online', 'Fashion Jewellery: Buy Fashion Bracelets Jewellery Online, Bracelets Hyderabad, Bracelets India, Bracelets US, Bracelets UK, Bracelets Australia.', 'active', '2019-04-30 20:10:11', '2019-09-03 15:38:44'),
(91, 'Ear Rings', 'ear-rings', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Earrings, Online Earrings Shopping', 'Earrings, Online Earrings Shopping in hyderabad', 'Fashion Jewellery: Earrings, Online Earrings Shopping,Earrings Hyderabad,Buy Earrings Hyderabad, OnlineEarrings Hyderabad, Cheap PriceEarrings in Hyderabad.', 'active', '2019-04-30 20:06:01', '2019-09-03 15:39:11'),
(90, 'Bangles & Kadas', 'bangles-kadas', '142', 0, 'types', NULL, '0', 'Bangles Kadas| Bangles: Buy Bracelets For Women online', 'Bangles Kadas, Bangles: Buy Bracelets For Women online', 'Bangles Kadas, Bangles: Buy Bracelets For Women online, Bangles Hyderabad, Buy Bangles from UK, USA and Australia. Order Bangles Online by near me.', 'active', '2019-04-30 19:55:42', '2019-09-03 15:38:05'),
(89, 'Artificial Jewellery', 'artificial-jewellery', '142', 0, 'types', NULL, '0', NULL, NULL, NULL, 'inactive', '2019-04-30 19:53:56', '2019-07-21 08:12:35'),
(88, 'Chocolate flower bouquet', 'chocolate-flower-bouquet', '2', 1, 'types', NULL, '0', 'Chocolate Flower Bouquet| Chocolates Bouquet Online', 'Chocolate Flower Bouquet,Chocolates Bouquet Online', 'Chocolate Flower Bouquet, Chocolates Bouquet Online in Hyderabad.', 'active', '2019-04-29 21:39:51', '2019-09-03 15:57:10'),
(87, 'Multi colour flower bouquet', 'multi-colour-flower-bouquet', '2', 0, 'types', NULL, '0', 'Flowers: Multi Colour Flower Bouquet| Multi-Colored Roses Delivery', 'Flowers: Multi Colour Flower Bouquet, Multi-Colored Roses Delivery', 'Flowers: Multi Colour Flower Bouquet, Multi-Colored Roses Delivery in Hyderabad.', 'active', '2019-04-29 21:39:37', '2019-09-03 15:57:46'),
(86, 'Special flower bouquet', 'special-flower-bouquet', '2', 0, 'types', NULL, '0', 'Order Special Flower Bouquet in Hyderabad, India, USA and UK', 'Order Special Flower Bouquet in Hyderabad, India, USA and UK', 'Order Special Flower Bouquet in Hyderabad, India, USA and UK', 'active', '2019-04-29 21:39:24', '2019-09-03 16:00:48'),
(85, 'Seasonal flower bouquet', 'seasonal-flower-bouquet', '2', 0, 'types', NULL, '0', 'Buy Seasonal Flower Bouquet in Hyderabad, India, USA and UK', 'Buy Seasonal Flower Bouquet in Hyderabad, India, USA and UK', 'Buy Seasonal Flower Bouquet in Hyderabad, India, USA, UK and Australia', 'active', '2019-04-29 21:39:12', '2019-09-03 15:59:04'),
(84, 'Regular rose bouquet', 'regular-rose-bouquet', '2', 0, 'types', NULL, '0', 'Flowers: Regular Rose Bouquet| Roses Delivery - Send Rose Bouquets', 'Flowers: Regular Rose Bouquet, Roses Delivery - Send Rose Bouquets', 'Flowers: Regular Rose Bouquet, Roses Delivery - Send Rose Bouquets', 'active', '2019-04-29 21:39:00', '2019-09-03 15:58:25'),
(83, 'Silver Antique Items', 'silver-antique-items', '5', 0, 'types', NULL, '0', 'Silver Antique Items| Silver Plated Antiques Online Hyderabad', 'Silver Antique Items, Silver Plated Antiques Online Hyderabad', 'Silver Antique Items, Silver Plated Antiques Online Hyderabad', 'active', '2019-04-28 00:38:46', '2019-09-03 16:13:06'),
(82, 'Silver Plated', 'silver-plated', '5', 0, 'types', NULL, '0', 'Silver Plated| silver plated online shopping Hyderabad', 'Silver Plated, silver plated online shopping Hyderabad', 'Silver Plated, silver plated online shopping Hyderabad', 'active', '2019-04-28 00:38:34', '2019-09-03 16:18:23'),
(80, 'Coins', 'coins', '5', 0, 'types', NULL, '0', 'Silver Coins| Silver Coins for Sale - Buy Silver Coins Online', 'Silver Coins, Silver Coins for Sale - Buy Silver Coins Online', 'Silver Coins, Silver Coins for Sale - Buy Silver Coins Online in hyderabad India.', 'active', '2019-04-28 00:38:08', '2019-09-03 16:10:41'),
(81, 'Pure', 'pure', '5', 0, 'types', NULL, '0', 'Silver Pure| Buy Silver Online: Bullion Bars & Coins', 'Silver Pure, Buy Silver Online: Bullion Bars & Coins', 'Silver Pure, Buy Silver Online: Bullion Bars & Coins Hyderabad', 'active', '2019-04-28 00:38:18', '2019-09-03 16:11:11'),
(79, 'Sterling Silver', 'sterling-silver', '5', 0, 'types', NULL, '0', 'Buy Sterling Silver in Hyderabad, India, USA and UK', 'Buy Sterling Silver in Hyderabad, India, USA and UK', 'Buy Sterling Silver in Hyderabad, India, USA and UK', 'active', '2019-04-28 00:37:56', '2019-09-03 16:21:17'),
(78, 'Silver Baby Sets', 'silver-baby-sets', '5', 0, 'types', NULL, '0', 'Silver Baby Sets| Silver Baby Sets Online | Buy Silver Gifts', 'Silver Baby Sets, Silver Baby Sets Online | Buy Silver Gifts', 'Silver Baby Sets, Silver Baby Sets Online, Buy Silver Gifts in Hyderabad, Silver Baby Sets hyderabad india.', 'active', '2019-04-28 00:37:44', '2019-09-03 16:13:37'),
(77, 'Silver Lamps', 'silver-lamps', '5', 0, 'types', NULL, '0', 'Buy Silver Lamp | Silver Gifts Online| Silver Pooja Items Hyderabad', 'Buy Silver Lamp, Silver Gifts Online, Silver Pooja Items Hyderabad', 'Buy Silver Lamp, Silver Gifts Online, Silver Pooja Items Hyderabad', 'active', '2019-04-28 00:37:33', '2019-09-03 16:18:01'),
(76, 'Silver Pooja  Items', 'silver-pooja-items', '5', 0, 'types', NULL, '0', 'Silver Pooja Items: Buy Silver Pooja Items Online Hyderabad', 'Silver Pooja Items: Buy Silver Pooja Items Online Hyderabad', 'Silver Pooja Items: Buy Silver Pooja Items Online Hyderabad', 'active', '2019-04-28 00:37:22', '2019-09-03 16:20:43'),
(75, 'Silver Idols', 'silver-idols', '5', 0, 'types', NULL, '0', 'Silver Idols in Hyderabad| Silver Idols inOnline Hyderabad', 'Silver Idols in Hyderabad, Silver Idols inOnline Hyderabad', 'Silver Idols in Hyderabad, Silver Idols inOnline Hyderabad', 'active', '2019-04-28 00:37:11', '2019-09-03 16:17:18'),
(74, 'Silver Fruit Bowls', 'silver-fruit-bowls', '5', 0, 'types', NULL, '0', 'Silver Fruit Bowls| Silver Fruit Bowl - send Silver Articles to Hyderabad', 'Silver Fruit Bowls, Silver Fruit Bowl - send Silver Articles to Hyderabad', 'Silver Fruit Bowls, Silver Fruit Bowl - send Silver Articles to Hyderabad', 'active', '2019-04-28 00:36:59', '2019-09-03 16:16:13'),
(73, 'Silver Jug', 'silver-jug', '5', 0, 'types', NULL, '0', 'Silver Jug in Hyderabad| Buy Silver jug in Hyderabad', 'Silver Jug in Hyderabad, Buy Silver jug in Hyderabad', 'Silver Jug in Hyderabad, Buy Silver jug in Hyderabad', 'active', '2019-04-28 00:36:47', '2019-09-03 16:17:44'),
(72, 'Silter Flower Basket', 'silter-flower-basket', '5', 0, 'types', NULL, '0', 'Silver Flower Basket| Silver Flower Basket| Silver Pooja items', 'Silver Flower Basket, Silver Flower Basket| Silver Pooja items', 'Silver Flower Basket, Silver Flower Basket, Silver Pooja items Hyderabad.', 'active', '2019-04-28 00:36:34', '2019-09-03 16:12:35'),
(71, 'Silver Dinner Sets', 'silver-dinner-sets', '5', 0, 'types', NULL, '0', 'Silver Dinner Sets| Buy Silver Dinner Set in Hyderabad', 'Silver Dinner Sets, Buy Silver Dinner Set in Hyderabad', 'Silver Dinner Sets, Buy Silver Dinner Set in Hyderabad', 'active', '2019-04-28 00:36:22', '2019-09-03 16:15:45'),
(70, 'Silver Bowls', 'silver-bowls', '5', 0, 'types', NULL, '0', 'Buy Silver Bowls| Buy Silver Bowls Online Hyderabad', 'Buy Silver Bowls, Buy Silver Bowls Online Hyderabad', 'Buy Silver Bowls, Buy Silver Bowls Online Hyderabad, best online Buy Silver Bowls in Hyderabad.', 'active', '2019-04-28 00:36:09', '2019-09-03 16:14:50'),
(69, 'Silver Chembu', 'silver-chembu', '5', 0, 'types', NULL, '0', 'Silver Chembu| Buy Silver Pooja Items Online Hyderabad', 'Silver Chembu, Buy Silver Pooja Items Online Hyderabad', 'Silver Chembu, Buy Silver Pooja Items Online Hyderabad', 'active', '2019-04-28 00:35:57', '2019-09-03 16:15:14'),
(68, 'Silver Binde', 'silver-binde', '5', 0, 'types', NULL, '0', 'Silver Binde| Buy Silver Chombu Online | Silver Kudam Online', 'Silver Binde,Buy Silver Chombu Online, Silver Kudam Online', 'Silver Binde, Buy Silver Chombu Online, Silver Kudam Online, Silver Binde hyderabad, buy Silver Binde in india, Silver Binde online hyderabad.', 'active', '2019-04-28 00:35:42', '2019-09-03 16:14:15'),
(67, 'Silver Plates', 'silver-plates', '5', 0, 'types', NULL, '0', 'Silver Plate in Hyderabad| Silver Plate Price in Hyderabad', 'Silver Plate in Hyderabad, Silver Plate Price in Hyderabad', 'Silver Plate in Hyderabad, Silver Plate Price in Hyderabad', 'active', '2019-04-28 00:35:29', '2019-09-03 16:19:04'),
(66, 'Silver Glass', 'silver-glass', '5', 0, 'types', NULL, '0', 'Silver Glass| Buy & Send Silver Glass Online Hyderabad India', 'Silver Glass, Buy & Send Silver Glass Online Hyderabad India', 'Silver Glass, Buy & Send Silver Glass Online Hyderabad India', 'active', '2019-04-28 00:34:47', '2019-09-03 16:16:52'),
(65, 'Drip Cakes', 'drip-cakes', '1', 0, 'types', NULL, '0', 'Chocolate Shot Drip Cakes| Online Cake Delivery| Order Cake Online', 'Chocolate Shot Drip Cakes in hyderabad, Online Cake Delivery, Order Cake Online', 'Chocolate Shot Drip Cakes, Online Cake Delivery,Order Cake Online Hyderabad, Order Birthday Drip Cakes in Hyderabad india usa, uk and california.', 'active', '2019-04-28 00:33:50', '2019-09-10 13:45:43'),
(64, 'Theme Cakes', 'theme-cakes', '1', 0, 'types', NULL, '0', 'Buy Theme Cake| Theme Cakes Online| Order Theme Cakes', 'Buy Theme Cake, Theme Cakes Online, Order Theme Cakes in hyderabad', 'Buy Theme Cake, Theme Cakes Online, Order Theme Cakes in Hyderabad, Order UK Theme Cakes, Buy US Theme Cakes to Hyderabad. Birthday Theme Cake, wedding Theme Cakes.', 'active', '2019-04-28 00:33:35', '2019-08-31 11:43:52'),
(63, 'Fondant Cakes', 'fondant-cakes', '1', 0, 'types', NULL, '0', 'Order fondant cakes online| Birthday wedding Cakes Hyderabad', 'Order fondant cakes online, Birthday wedding Cakes Hyderabad', 'Order fondant cakes online, Birthday wedding Cakes Hyderabad, buy fondant cakes, send fondant cakes to your parients, friends in Hyderabad.', 'active', '2019-04-28 00:33:21', '2019-08-31 11:40:33'),
(62, 'Designer Cakes', 'designer-cakes', '1', 0, 'types', NULL, '0', 'Designer Cakes| Send Designer Cakes for Birthday Online', 'Designer Cakes hyderabad, Send Designer Cakes for Birthday Online', 'Designer Cakes hyderabad, Send Designer Cakes for Birthday Online and designer cakes for birthday.', 'active', '2019-04-28 00:32:36', '2019-08-31 11:39:52'),
(61, 'Regular Cakes', 'regular-cakes', '1', 0, 'types', NULL, '0', 'Best Offers On Cakes, Online Regular Cakes Delivery Hyderabad', 'Best Offers On Cakes, Online Regular Cakes Delivery Hyderabad', 'Best Offers On Cakes, Online Regular Cakes Delivery Hyderabad, buy Regular Cakes hyderabad, Regular Cakes Order from NRI to Hyderabad deliver.', 'active', '2019-04-28 00:32:19', '2019-08-31 11:43:06'),
(100, 'Nose Ring', 'nose-ring', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Buy Nose Ring Online| Pretty Nose Rings', 'Buy Nose Ring Online, Pretty Nose Rings in hyderabad', 'Fashion Jewellery: Buy Nose Ring Online Hyderabad, Pretty Nose Rings in Hyderabad.', 'active', '2019-04-30 20:15:43', '2019-09-03 15:47:45'),
(101, 'Anklets', 'anklets', '142', 0, 'types', NULL, '0', 'Anklets for Women: Buy Anklets for Women Online at Best Prices', 'Anklets for Women: Buy Anklets for Women Online at Best Prices', 'Anklets for Women: Buy Anklets for Women Online at Best Prices, Anklets for Women in Hyderabad, Anklets india, Anklets USA,UK and Australia.', 'active', '2019-04-30 20:17:35', '2019-09-03 15:16:38'),
(102, 'Silver Jewellery (German)', 'silver-jewellery-german-', '142', 0, 'types', NULL, '0', 'Fashion Jewellery: Buy Silver Jewellery German', 'Buy Silver Jewellery German in hyderabad', 'Fashion Jewellery: Buy Silver Jewellery German', 'active', '2019-04-30 20:17:58', '2019-09-03 15:48:31'),
(103, 'Foxtail Millet', 'foxtail-millet', '6', 0, 'types', NULL, '0', 'Foxtail Millet| Foxtail Millets in Hyderabad Online', NULL, 'Foxtail Millet, Foxtail Millets in Hyderabad Online, 5kg Foxtail Millets free shipping, 10kg Foxtail Millets order hyderabad.', 'active', '2019-05-02 15:28:45', '2019-08-28 15:51:24'),
(104, 'Kodo Millet', 'kodo-millet', '6', 0, 'types', NULL, '1', 'kodo Millet| Kodo Millets Online in Hyderabad', NULL, 'kodo Millet, Kodo Millets Online in Hyderabad, buy 5kg kodo Millet with free shipping, buy 10kg kodo Millet with free shipping in Hyderabad.', 'active', '2019-05-02 15:29:54', '2019-08-28 15:51:48'),
(105, 'Barnyard Millet', 'barnyard-millet', '6', 0, 'types', NULL, '3', 'Buy Barnyard Millet| Buy Barnyard Millets Hyderabad', NULL, 'Buy Barnyard Millet, Buy Barnyard Millets Online in Hyderabad, order 5kg 10kg barnyard millets in hyderabad, free shipping 5kg barnyard millets Hyderabad.', 'active', '2019-05-02 15:30:40', '2019-08-28 15:50:42'),
(106, 'Little Millet', 'little-millet', '6', 0, 'types', NULL, '4', 'Little Millet| Buy Little Millets Online Hyderabad', NULL, 'Little Millet, Buy Little Millets Online Hyderabad, buy 5kg Little Millet in Hyderabad, 10kg Little Millet in hyderabad, buy Little Millet from velchala.', 'active', '2019-05-02 15:31:04', '2019-08-28 15:52:08'),
(107, 'Brown Millet', 'brown-millet', '6', 0, 'types', NULL, '5', 'Brown Millet| Buy Brown Millets Hyderabad Online', NULL, 'Brown Millet, Buy Brown Millets Hyderabad Online, 5kg Brown Millets hyderabad, order 10kg Brown Millets in hyderabad free shipping, buy 10kg brown millets hyderabd.', 'active', '2019-05-02 15:32:19', '2019-08-28 15:51:02'),
(108, 'Pineapple', 'pineapple', '1', 0, 'others', NULL, '0', 'Pineapple Cakes in Hyderabad| Order Fruit Cake From velchala', 'Pineapple Cakes in Hyderabad, Order Fruit Cake From velchala', 'Pineapple Cakes in Hyderabad, Order Fruit Cake From velchala, online Pineapple Cakes in Hyderabad, Buy Pineapple Cakes in Hyderabad.', 'active', '2019-05-17 23:41:36', '2019-08-31 11:42:44'),
(109, 'Butterscotch', 'butterscotch', '1', 0, 'others', NULL, '0', 'Butter scotch Cakes Online | Buy Butterscotch Cake', 'Butter scotch Cakes Online, Buy Butterscotch Cake', 'Butter scotch Cakes Online,Buy Butterscotch Cake, butterscotch cakes online in Hyderabad, butterscotch cake recipe and order butterscotch cake in Hyderabad.', 'active', '2019-05-18 00:14:23', '2019-08-31 11:38:44'),
(110, 'Chocolate', 'chocolate', '1', 0, 'others', NULL, '0', 'Chocolates: Buy Chocolates, Candies and other branded chocolates', 'Buy Chocolates Online India, Send Branded Chocolates Online in hyderabad', 'Chocolates: Buy Chocolates, Candies and other branded chocolates, hyderabad chocolates delivery in Hyderabad, homemade chocolates hyderabad.', 'active', '2019-05-18 00:14:55', '2019-08-31 11:44:23'),
(111, 'Fruit', 'fruit', '1', 0, 'others', NULL, '0', 'Fruit cakes| Online Fruit Cake delivery in Hyderabad', 'Fruit cakes hyderabad, Online Fruit Cake delivery in Hyderabad', 'Buy Fruit cakes, Online Fruit Cake delivery in Hyderabad, Order Fruit cakes in Hyderabad, Send best Fruit cakes from Hyderabad.', 'active', '2019-05-18 00:16:45', '2019-08-31 11:41:01'),
(112, 'Strawberry', 'strawberry', '1', 0, 'others', NULL, '0', 'Strawberry Cake Delivery in Hyderabad| Strawberry Delight Cake', 'Strawberry Cake Delivery in Hyderabad, Strawberry Delight Cake', 'Strawberry Cake Delivery in Hyderabad, Strawberry Delight Cake, Strawberry Delight Cake hyderabad, Strawberry Delight Cake USA,Strawberry Delight Cake UK.', 'active', '2019-05-18 00:17:05', '2019-08-31 11:43:32'),
(113, 'Any', 'any', '2', 0, 'others', NULL, '0', 'Buy Flowers Online Wedding| Buy Flowers Online', 'Buy Flowers Online Wedding, Buy Flowers Online', 'Buy Flowers Online Wedding, Buy Flowers Online in Hyderabad', 'active', '2019-05-18 01:07:06', '2019-09-03 15:56:37'),
(114, 'His/Her', 'his-her', '3', 0, 'others', NULL, '0', NULL, NULL, NULL, 'active', '2019-05-18 01:16:20', '2019-05-18 01:16:20'),
(115, 'Personalized', 'personalized', '3', 0, 'types', NULL, '0', 'Personalized Gifts', NULL, NULL, 'active', '2019-05-18 01:25:07', '2019-08-28 15:46:42'),
(116, 'Design Chocolates', 'design-chocolates', '4', 0, 'types', NULL, '0', 'Send Chocolate Gifts| Chocolate Delivery| Design Chocolates', 'Send Chocolate Gifts, Chocolate Delivery in hyderabad, Design Chocolates', 'Send Chocolate Gifts, Chocolate Delivery, Design Chocolates', 'active', '2019-05-18 01:32:35', '2019-09-02 21:12:30'),
(117, 'Home Foods', 'home-foods', '0', 0, NULL, '1558507525homefood-navigation-banner.jpg', '0', 'Order Home Cooked Food| Homemade food| From Home Chefs', 'Order Home Cooked Food, Homemade food, From Home Chefs', 'Our weekly deliveries of fresh, perfectly-portioned ingredients have everything you need to prepare home-cooked food', 'active', '2019-05-22 19:15:25', '2019-08-31 11:50:53'),
(133, 'Anarkali', 'anarkali', '130', 0, 'others', NULL, '0', 'Anarkali| Buy Latest Designer Anarkali Suits Online for Women', 'Anarkali, Buy Latest Designer Anarkali Suits Online for Women', 'Anarkali, Buy Latest Designer Anarkali Suits Online for Women and anarkali suits online shopping, anarkali suits for wedding in Hyderabad India.', 'active', '2019-05-26 05:19:47', '2019-08-31 11:32:39'),
(132, 'Dresses', 'dresses', '130', 0, 'types', NULL, '0', 'Dresses for Women| Shop Online Boutique Dresses| Buy Dresses', 'Dresses for Women, Shop Online Boutique Dresses, Buy Dresses', 'Dresses for Women, Shop Online Boutique Dresses, Buy Dresses, women dresses for wedding in Hyderabad and USA.', 'active', '2019-05-26 05:17:38', '2019-08-31 11:35:08'),
(131, 'Sarees', 'sarees', '130', 0, 'types', NULL, '0', 'Sarees - Buy Latest Sari Collection Online in Hyderabad India', 'Sarees , Buy Latest Sari Collection Online in Hyderabad', 'New Offer for Sarees - Buy Latest Sari Collection Online in India, sarees boutique hyderabad, hyderabadi sarees online from velchala.', 'active', '2019-05-26 05:15:50', '2019-08-31 11:37:32'),
(130, 'Boutique', 'boutique', '0', 0, NULL, '1558802550test1.jpg', '0', 'Discover Your Favorite Online Boutique Clothes Today| velchala.com', 'Discover Your Favorite Online Boutique Clothes', 'Discover Your Favorite Online Boutique Clothes Today, boutique dresses for women and boutique dresses for girls and boutique clothes in Hyderabad.', 'active', '2019-05-26 05:12:31', '2019-08-31 11:31:25'),
(129, 'Appadalu', 'appadalu', '117', 0, 'types', NULL, '0', 'Home Foods Appadalu in Hyderabad', 'Home Foods Appadalu in Hyderabad', 'Order Home Foods Appadalu Hyderabad and buy delicious home foods appadalu in bulk', 'active', '2019-05-22 19:27:47', '2019-09-03 16:02:25'),
(127, 'Fry', 'fry', '117', 0, 'types', NULL, '0', 'Home Foods Fry| Fried Foods Hyderabad', 'Home Foods Fry, Fried Foods Hyderabad', 'Home Foods Fry, Fried Foods Hyderabad, order Fried Foods in Hyderabad, buy Fried Foods Hyderabad, online Fried Foods Hyderabad.', 'active', '2019-05-22 19:24:36', '2019-09-03 16:03:15'),
(128, 'Laddu', 'laddu', '117', 0, 'types', NULL, '0', 'Home Foods Laddu Hyderabad| Place Order From USA Laddu to Hyderabad', 'Home Foods Laddu Hyderabad, Place Order From USA Laddu to Hyderabad', 'Home Foods Laddu Hyderabad, Place Order From USA Laddu to Hyderabad, buy laddu in Hyderabad, order laddu in Hyderabad.', 'active', '2019-05-22 19:24:59', '2019-09-03 16:04:16'),
(126, 'Pickles', 'pickles', '117', 0, 'types', NULL, '0', 'Home Foods Pickles Hyderabad| Buy Pickles Online Hyderabad', 'Home Foods Pickles Hyderabad, Buy Pickles Online Hyderabad', 'Home Foods Pickles Hyderabad, Buy Pickles Online Hyderabad', 'active', '2019-05-22 19:24:15', '2019-09-03 16:04:35'),
(134, 'Long Frocks', 'long-frocks', '130', 0, 'others', NULL, '0', 'Long Frocks| Long Dresses for Women| Dresses for Sale Online', 'Long Frocks, Long Dresses for Women, Dresses for Sale Online', 'Long Frocks, Long Dresses for Women,Dresses for Sale Online, long frocks images for ladies, long frocks indian, long frocks for teenage girl in Hyderabad.', 'active', '2019-05-26 07:06:24', '2019-08-31 11:36:52'),
(135, 'Kurtis', 'kurtis', '130', 0, 'others', NULL, '0', 'Kurtis Online - Buy Designer Kurtis & Suits for Women', 'Kurtis Online, Buy Designer Kurtis & Suits for Women', 'Kurtis Online - Buy Designer Kurtis & Suits for Women, kurtis designer collection, designer kurtis for wedding, kurtis designer handwork in Hyderabad.', 'active', '2019-05-26 07:09:10', '2019-08-31 11:36:17'),
(136, 'Design Blouses', 'design-blouses', '130', 0, 'others', NULL, '0', 'Design Blouses| Buy Indian Saree Blouses Online| Blouses For Women', 'Design Blouses, Buy Indian Saree Blouses Online, Blouses For Women', 'Design Blouses, Buy Indian Saree Blouses Online, Blouses For Women, blouses for girls, blouses for work, blouses for work for ladies.', 'active', '2019-05-26 07:11:39', '2019-08-31 11:34:26'),
(137, 'Dupattas', 'dupattas', '130', 0, 'others', NULL, '0', 'Dupattas - Dupattas Designs Online for Women at Best Prices', 'Dupattas, Dupattas Designs Online for Women', 'Dupattas - Dupattas Designs Online for Women at Best Prices, hyderabadi dupatta designs. Buy dupatta for girl hyderabad.', 'active', '2019-05-26 07:12:13', '2019-08-31 11:35:43'),
(144, 'Raakhis', 'raakhis', '0', 2, NULL, '15649420991563779570rakhi.jpg', '0', 'Raakhis', NULL, NULL, 'inactive', '2019-08-02 19:46:59', '2019-08-28 15:52:34'),
(142, 'Fashion Jewellery', 'fashion-jewellery', '0', 0, NULL, '1563540283fashionjewellery.jpg', '0', 'Jewellery - Shop from 500+ Jewellery Designs Online Hyderabad', 'Jewellery - Shop from 500+ Jewellery Designs Online Hyderabad', 'Buy latest collection of artificial, imitation, traditional & designer Jewellery Online in India. Shop for latest fashion Jewellery Designs at velchala.', 'active', '2019-07-19 18:14:44', '2019-09-03 15:15:43'),
(143, 'Rakhis', 'rakhis', '0', 1, NULL, '1563779570rakhi.jpg', '0', 'Rakhi Gifts - Buy Rakhi Gifts Online, Send Rakhi Gifts to India', NULL, 'Rakhi Online Send rakhi to India from USA, UK, Canada, Australia &Worldwide with Free Shipping. Online rakhi delivery in India. Best Rakhi store for Rakhi online.', 'inactive', '2019-07-21 20:53:12', '2019-08-28 15:53:14'),
(145, 'Single Rakhis', 'single-rakhis', '143', 0, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2019-08-04 21:32:51', '2019-08-04 21:32:51'),
(146, 'Double Rakhis', 'double-rakhis', '143', 0, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2019-08-04 21:33:31', '2019-08-04 21:33:31'),
(147, 'Bhayya Bhabhi Rakhis', 'bhayya-bhabhi-rakhis', '143', 0, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2019-08-04 21:34:03', '2019-08-04 21:34:03'),
(148, 'Single Raakhis', 'single-raakhis', '144', 2, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2019-08-04 23:08:21', '2019-08-04 23:08:21'),
(149, 'Double Raakhis', 'double-raakhis', '144', 2, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2019-08-04 23:08:48', '2019-08-04 23:09:57'),
(150, 'Bhayya Bhabhi Raakhis', 'bhayya-bhabhi-raakhis', '144', 2, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2019-08-04 23:09:19', '2019-08-04 23:09:33'),
(151, 'Arisalu', 'arisalu', '117', 0, 'others', NULL, '0', 'Home Foods Arisalu| Home Cooked Ariselu Bellam or sugar', 'Home Foods Arisalu, Home Cooked Ariselu Bellam or sugar', 'Home Foods Arisalu, Home Cooked Ariselu Bellam or sugar, Arisalu hyderabad, Order Arisalu Hyderabad, buy Arisalu in Hyderabad, telugu Arisalu making in hyderabad.', 'active', '2019-08-06 16:14:45', '2019-09-03 16:02:51'),
(152, 'Home Made Sweets & Snacks', 'home-made-sweets-snacks', '117', 0, 'types', NULL, '0', 'Buy Home Made Sweets Snacks in Hyderabad| Online Home Made Sweets', 'Buy Home Made Sweets Snacks in Hyderabad, Online Home Made Sweets', 'Buy Home Made Sweets Snacks in Hyderabad, Online Home Made Sweets', 'active', '2019-08-06 23:01:27', '2019-09-03 16:03:49'),
(154, 'Diwali Gifts', 'diwali-gifts', '0', 1, NULL, NULL, '0', NULL, NULL, NULL, 'inactive', '2019-09-30 02:16:39', '2020-06-19 15:26:39'),
(155, 'Diwaali Gifts', 'diwaali-gifts', '0', 2, NULL, NULL, '0', NULL, NULL, NULL, 'inactive', '2019-09-30 02:18:55', '2020-06-19 15:26:26'),
(156, 'Thalli', 'thalli', '155', 2, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2019-10-01 09:13:03', '2019-10-01 09:13:03'),
(157, 'Valentine\'s Day', 'valentine-s-day', '3', 0, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2020-02-10 09:09:35', '2020-02-10 09:09:35'),
(158, 'Father\'s day', 'father-s-day', '3', 0, 'types', NULL, '0', NULL, NULL, NULL, 'active', '2020-06-19 15:34:56', '2020-06-19 15:56:10'),
(159, 'Cake', 'cake', '130', 1, 'types', NULL, '10', 'd', 'ewdewd', 'ew', 'active', '2020-10-04 06:01:54', '2020-10-04 06:01:54');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `cu_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` bigint(20) NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`cu_id`, `first_name`, `email`, `phone_number`, `subject`, `message`, `login_user_id`, `created_at`, `updated_at`) VALUES
(456, 'assad', 'venkat@gmail.com', 9573302201, 'dg', 'jhgdjhsd', NULL, '2020-10-10 23:45:19', '2020-10-10 23:45:19'),
(457, 'dsfdsf', 'venkatp686@gmail.com', 9573302201, 'venkatesh', 'need book details', NULL, '2020-10-10 23:48:27', '2020-10-10 23:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_phone_prefix` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_weight_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_code`, `country_phone_prefix`, `country_currency`, `country_language`, `country_status`, `country_weight_description`, `created_at`, `updated_at`) VALUES
(1, 'India', 'IN', '91', 'USD', 'English', 'active', NULL, '2019-02-20 04:03:54', '2019-07-11 10:21:59'),
(2, 'United States', 'US', '1', 'USD', 'English', 'active', '1:1800,2:2800,3:3300,4:3700,5:4740,6:5530,7:6320,8:7110,9:7900,10:6380', '2019-06-04 22:11:55', '2019-09-30 02:36:23'),
(3, 'Australia', 'AU', '61', 'USD', 'English', 'active', '1:1800,2:3200,3:3400,4:3800,5:3650,6:4380,7:5110,8:5480,9:6050,10:5800', '2019-07-10 18:30:37', '2019-08-09 10:28:00'),
(13, 'United Kingdom', 'UK', '1', 'USD', 'English', 'active', '1:1800,2:3200,3:3400,4:3800,5:3650,6:4380,7:5110,8:5480,9:6050,10:5800', '2019-08-09 07:47:47', '2019-09-30 02:36:01'),
(14, 'Canada', 'CN', '1', 'USD', 'English', 'active', '1:1800,2:3200,3:3400,4:3800,5:3650,6:4380,7:5110,8:5480,9:6050,10:5800', '2019-09-30 02:58:05', '2019-09-30 03:00:00'),
(15, 'UAE', 'UAE', '971', 'USD', 'English', 'active', '1:1800,2:3200,3:3400,4:3800,5:3650,6:4380,7:5110,8:5480,9:6050,10:5800', '2019-09-30 02:59:27', '2019-09-30 03:00:33');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_discount_value` double(8,2) DEFAULT NULL,
  `coupon_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_quantity` int(11) DEFAULT NULL,
  `coupon_country` int(11) DEFAULT NULL COMMENT 'From country table',
  `coupon_usage_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_expiry_date` date DEFAULT NULL,
  `coupon_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupon_id`, `coupon_code`, `coupon_discount_value`, `coupon_discount_type`, `coupon_quantity`, `coupon_country`, `coupon_usage_type`, `coupon_expiry_date`, `coupon_status`, `created_at`, `updated_at`) VALUES
(7, 'FAMILY10', 10.00, 'Percentage', 8, 2, 'Multi', '2019-10-31', 'active', '2019-09-14 00:38:06', '2019-09-14 00:38:06'),
(2, 'owners', 40.00, 'Percentage', 2, 1, 'Multi', '2019-12-31', 'active', '2019-07-11 00:02:29', '2019-07-11 00:02:29'),
(3, 'USFAMILY10', 10.00, 'Percentage', 2, 2, 'Once', '2019-07-31', 'active', '2019-07-11 03:16:33', '2019-07-12 10:50:21'),
(4, 'FRIENDS10', 10.00, 'Percentage', 2, 2, 'Once', '2019-07-20', 'active', '2019-07-17 07:20:38', '2019-07-17 07:20:38'),
(5, 'RAAKHIS2019', 10.00, 'Percentage', 2, 2, 'Once', '2019-08-12', 'active', '2019-08-04 23:41:53', '2019-08-06 19:11:03'),
(6, 'Rakhis2019', 10.00, 'Percentage', 1, 1, 'Once', '2019-08-15', 'active', '2019-08-07 09:49:14', '2019-08-12 20:44:23'),
(8, 'Diwali', 100.00, 'Percentage', 1, 2, 'Once', '2019-11-29', 'active', '2019-10-17 11:21:11', '2019-10-17 11:21:11'),
(9, 'VDESIDIWALI', 25.00, 'Fixed', 1, 2, 'Once', '2019-12-31', 'active', '2019-10-23 22:49:57', '2019-10-23 22:49:57'),
(10, 'Test1$', 1.00, 'Fixed', 2, 1, 'Once', '2019-10-28', 'active', '2019-10-25 09:12:57', '2019-10-25 09:12:57');

-- --------------------------------------------------------

--
-- Table structure for table `earnings`
--

CREATE TABLE `earnings` (
  `e_id` bigint(20) UNSIGNED NOT NULL,
  `e_vendor_id` int(11) DEFAULT NULL,
  `e_orders_id` int(11) DEFAULT NULL,
  `e_product_id` int(11) DEFAULT NULL,
  `e_user_id` int(11) DEFAULT NULL,
  `e_product_price` double(8,2) DEFAULT NULL,
  `e_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_date` date NOT NULL,
  `meta_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `event_location` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `events_status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_url`, `img`, `event_date`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`, `event_location`, `event_des`, `events_status`) VALUES
(2, 'Indulgence unreserved is the alteration appearance my an astonished', 'indulgence-unreserved-is-the-alteration-appearance-my-an-astonished', '1602251893gal03.JPG', '2020-10-13', 'Indulgence unreserved is the alteration appearance my an astonished', 'Indulgence unreserved is the alteration appearance my an astonished', 'Indulgence unreserved is the alteration appearance my an astonished', '2020-10-10 02:28:13', '2020-10-10 02:28:13', 'Hyderabad', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; text-align: justify; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">Its sometimes her behaviour are contented. Do listening am eagerness oh objection collected. Together gay feelings continue juvenile had off one. Unknown may service subject her letters one bed. Child years noise ye in forty. Loud in this in both hold. My entrance me is disposal bachelor remember relation.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; text-align: justify; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">Oh acceptance apartments up sympathize astonished delightful. Waiting him new lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; text-align: justify; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">So insisted received is occasion advanced honoured.Among ready to which up. Attacks smiling and may out assured moments man nothing outward.</p><h2 class=\"h2\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; list-style: none; font-family: Poppins, sans-serif; font-weight: 400; line-height: 1.2; color: rgb(0, 0, 0); font-size: 2rem;\">Me unpleasing impossible</h2><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; list-style: none; text-align: justify; color: rgb(0, 0, 0); font-family: Poppins, sans-serif; font-size: 14px;\">Pianoforte solicitude so decisively unpleasing conviction is partiality he. Or particular so diminution entreaties oh do. Real he me fond show gave shot plan. Mirth blush linen small hoped way its along. Resolution frequently apartments off all discretion devonshire. Saw sir fat spirit seeing valley. He looked or valley lively. If learn woody spoil of taken he cause.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `magazines`
--

CREATE TABLE `magazines` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mag_profile_pic` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mag_pdf` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mag_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `occassion_reminder`
--

CREATE TABLE `occassion_reminder` (
  `or_id` bigint(20) UNSIGNED NOT NULL,
  `or_user_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `or_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `or_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `or_date` date NOT NULL,
  `or_remind` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `or_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `occassion_reminder`
--

INSERT INTO `occassion_reminder` (`or_id`, `or_user_id`, `or_name`, `or_type`, `or_date`, `or_remind`, `or_desc`, `created_at`, `updated_at`) VALUES
(1, '41', 'Srinivas Birthday', 'Birthday', '2019-11-06', '1 Day Before', 'Srinivas Birthday', '2019-07-23 11:17:19', '2019-07-23 11:17:19'),
(2, '41', 'Bhuvana Birthday', 'Birthday', '2019-08-02', '1 Day Before', 'Bhuvana Birthday', '2019-07-23 11:17:45', '2019-07-23 11:17:45'),
(3, '41', 'Praveen Birthday', 'Birthday', '2019-09-13', '1 Day Before', 'my birthday', '2019-09-07 15:35:03', '2019-09-07 15:35:03');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_user_id` int(11) NOT NULL,
  `order_currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_total_weight` double(8,2) DEFAULT NULL,
  `order_delivery_charge` double(8,2) DEFAULT NULL,
  `order_sub_total_price` double(8,2) DEFAULT NULL,
  `order_shipping_price` double(8,2) DEFAULT NULL,
  `order_coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_coupon_discount` double(8,2) DEFAULT NULL,
  `order_coupon_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_coupon_discount_amount` double(8,2) DEFAULT NULL,
  `order_total_price` double(8,2) NOT NULL,
  `order_delivery_country` int(11) DEFAULT NULL,
  `order_delivery_state` int(11) DEFAULT NULL,
  `order_delivery_address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_billing_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_reference_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `order_note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_payment_invoice_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_payment_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_payment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_payment_mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_payment_date` datetime DEFAULT NULL,
  `order_payment_response_dump` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `oitem_id` bigint(20) UNSIGNED NOT NULL,
  `oitem_order_id` int(11) NOT NULL,
  `oitem_product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oitem_product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oitem_product_options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_qty` int(11) NOT NULL,
  `oitem_product_price` double(8,2) NOT NULL,
  `oitem_delivery_charge` double(8,2) DEFAULT NULL,
  `oitem_delivery_date` date DEFAULT NULL,
  `oitem_delivery_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_nameongift` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_discount_price` double(8,2) DEFAULT NULL,
  `oitem_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '% or fixed price',
  `oitem_sub_total` double(8,2) NOT NULL,
  `oitem_note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `oitem_payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `oitem_item_sku_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_item_sku_vendor_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_item_sku_vdc_commission_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_item_sku_vdc_commission_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_item_sku_vdc_final_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_item_sku_store_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_item_sku_store_discount_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oitem_item_sku_store_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('suhasini.susu@gmail.com', '$2y$10$CfJpa/ZH64InfQYO1m5Ba.jobLDCCxXT5vVunURVQmQyeugUBphKO', '2019-05-08 21:57:26'),
('sarithabandaru@yahoo.com', '$2y$10$xfrKcP8PpZ9q.VDmbpA98uBZ3NefxiATXBBQxZLU5yxzHNHv0dnii', '2019-05-09 05:20:11'),
('ganesnar@gmail.com', '$2y$10$69PCwquFLcUxnGygYvBFxuxbUZXgbYNgy21QAYEvtrQJpasG3s1ZW', '2019-11-21 08:41:23'),
('vdc_rcj@yahoo.com', '$2y$10$CFoSn87XD6O6rykMCshvMevGJXvi9nWwNTlgdQ6uPZmBjuyS8pvRW', '2019-05-22 01:54:32'),
('vdc_rcjknl@yahoo.com', '$2y$10$XNxPxLL.pZhQyndJW7KVLOOXOwprXbQeZxK95EoTif5LhOKq3Ny22', '2019-05-22 06:31:56'),
('itzursai@gmail.com', '$2y$10$JaWJccXYTCLVNd5BmAQkReBj1FcOya4ijVZJy.ZjBKs8BV2B4J92u', '2019-06-13 01:50:35'),
('venkatp686@gmail.com', '$2y$10$aTVlwHzjFDmgwAdTiFsQVu.XdXHsf4nDTuzIS2yf4HGw1MnzUVis2', '2020-10-08 19:14:29'),
('Venkatp06@gmail.com', '$2y$10$tAhmqwkgqCZjx/5sJbHb0O.HgupOYyAfU346uJE5KnqxfsvBGthKq', '2020-10-08 04:26:12');

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

CREATE TABLE `payment_info` (
  `pi_id` bigint(20) UNSIGNED NOT NULL,
  `pi_vendor_id` int(11) DEFAULT NULL,
  `pi_current_balance` double(8,2) DEFAULT NULL,
  `pi_paying_amount` double(8,2) DEFAULT NULL,
  `pi_balance_amount` double(8,2) DEFAULT NULL,
  `pi_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cat_type` int(11) NOT NULL COMMENT '1-gallery,2-books,3-jayanthi-events,4-blog events',
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `img`, `event_name`, `created_at`, `updated_at`, `cat_type`, `status`) VALUES
(1, '1602221268gal03.JPG', '1', '2020-10-09 17:57:48', '2020-10-09 17:57:48', 3, 1),
(2, '1602223656gal02.JPG', '3', '2020-10-09 18:37:36', '2020-10-09 18:37:36', 4, 1),
(3, '1602223656gal03.JPG', '3', '2020-10-09 18:37:38', '2020-10-09 18:37:38', 4, 1),
(4, '1602224967gal02.JPG', '3', '2020-10-09 18:59:27', '2020-10-09 18:59:27', 4, 1),
(5, '1602224967gal03.JPG', '3', '2020-10-09 18:59:27', '2020-10-09 18:59:27', 4, 1),
(6, '1602224967gal04.JPG', '3', '2020-10-09 18:59:27', '2020-10-09 18:59:27', 4, 1),
(7, '1602224967gal05.JPG', '3', '2020-10-09 18:59:27', '2020-10-09 18:59:27', 4, 1),
(8, '1602224967gal06.JPG', '3', '2020-10-09 18:59:28', '2020-10-09 18:59:28', 4, 1),
(9, '1602224968gal07.JPG', '3', '2020-10-09 18:59:28', '2020-10-09 18:59:28', 4, 1),
(10, '1602224968gal08.JPG', '3', '2020-10-09 18:59:28', '2020-10-09 18:59:28', 4, 1),
(11, '1602224968news01.jpg', '3', '2020-10-09 18:59:28', '2020-10-09 18:59:28', 4, 1),
(12, '1602225096gal05.JPG', '1', '2020-10-09 19:01:36', '2020-10-09 19:01:36', 3, 1),
(13, '1602225096gal06.JPG', '1', '2020-10-09 19:01:36', '2020-10-09 19:01:36', 3, 1),
(14, '1602225096gal07.JPG', '1', '2020-10-09 19:01:36', '2020-10-09 19:01:36', 3, 1),
(15, '1602225096gal08.JPG', '1', '2020-10-09 19:01:36', '2020-10-09 19:01:36', 3, 1),
(16, '1602225096news01.jpg', '1', '2020-10-09 19:01:36', '2020-10-09 19:01:36', 3, 1),
(17, '1602226190gal07.JPG', '4', '2020-10-09 19:19:50', '2020-10-09 19:19:50', 4, 1),
(18, '1602226190gal08.JPG', '4', '2020-10-09 19:19:50', '2020-10-09 19:19:50', 4, 1),
(19, '1602226190news01.jpg', '4', '2020-10-09 19:19:50', '2020-10-09 19:19:50', 4, 1),
(20, '1602251893gal01.JPG', '2', '2020-10-10 02:28:13', '2020-10-10 02:28:13', 3, 1),
(21, '1602251893gal02.JPG', '2', '2020-10-10 02:28:14', '2020-10-10 02:28:14', 3, 1),
(22, '1602251894gal03.JPG', '2', '2020-10-10 02:28:14', '2020-10-10 02:28:14', 3, 1),
(23, '1602251894gal04.JPG', '2', '2020-10-10 02:28:14', '2020-10-10 02:28:14', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `poems`
--

CREATE TABLE `poems` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `poem_image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poem_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gal_cat_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `poems`
--

INSERT INTO `poems` (`id`, `poem_image`, `poem_name`, `created_at`, `updated_at`, `gal_cat_id`) VALUES
(1, '1602227888gal02.JPG', NULL, '2020-10-09 19:48:08', '2020-10-09 19:48:08', 1),
(2, '1602227888gal03.JPG', NULL, '2020-10-09 19:48:08', '2020-10-09 19:48:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(10) UNSIGNED NOT NULL,
  `p_unique_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_vendor_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_cat_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_sub_cat_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_item_spl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_main_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_dishtype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_overview` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_specifications` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_quality_care_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_delivery_charges` int(11) DEFAULT NULL,
  `p_status` int(11) NOT NULL DEFAULT 0,
  `p_meta_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_meta_keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `p_related` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_unique_id`, `p_vendor_id`, `p_name`, `p_alias`, `p_cat_id`, `p_sub_cat_id`, `p_item_spl`, `p_main_image`, `p_dishtype`, `p_availability`, `p_overview`, `p_specifications`, `p_quality_care_info`, `p_delivery_charges`, `p_status`, `p_meta_title`, `p_meta_keywords`, `p_meta_description`, `created_at`, `updated_at`, `p_related`) VALUES
(1, 'VC-1', '4', 'Single face Lamp', 'single-face-lamp', '5', '77', NULL, NULL, NULL, 'in_stock', '<p>Single face lamp. Available in five faces also. Price includes delivery charges also.</p>', '<p>Single face Plain Lamp in Pair</p>', '<p>Silver Purity 80% Regular Silver for Daily Use.</p>', NULL, 0, NULL, NULL, NULL, '2019-05-22 06:40:53', '2019-05-22 06:40:53', NULL),
(2, 'VC-2', '4', 'Single face Lamp', 'single-face-lamp', '5', '77', NULL, NULL, NULL, 'in_stock', '<p>Single face lamp. Available in five faces also. Price includes delivery charges also.</p>', '<p>Single face Plain Lamp in Pair</p>', '<p>Silver Purity 80% Regular Silver for Daily Use.</p>', NULL, 0, NULL, NULL, NULL, '2019-05-22 06:41:16', '2019-05-22 06:41:16', NULL),
(3, 'VC-3', '4', 'Silver Lamp', 'silver-lamp', '5', '77', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\"><b>Single face lamp</b></span></p><p><span style=\"display: inline !important;\"><span style=\"display: inline !important;\"><span style=\"display: inline !important;\">By R C Jewellers</span><span style=\"display: inline !important;\">        </span><br></span></span></p><p><span style=\"display: inline !important;\"><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><b><br></b></span></p><p><br></p>', '<p>Single face Plain Lamps in Pair</p><p>Product Weight: 24 Grams</p><p>Available Weights: 10,20,30,40,50 up to 60 Grams (Pair)</p>', '<p>Regular Silver Deepam for Pooja.</p><p>This can be used as Wedding or Occational Gift.</p>', NULL, 1, 'Order Silver Lamp in Hyderabad| Buy Silver Lamp', 'Order Silver Lamp in Hyderabad| Buy Silver Lamp', 'Order Silver Lamp in Hyderabad and Buy Silver Lamp india', '2019-05-22 06:41:46', '2019-09-03 16:49:02', NULL),
(4, 'VC-4', '4', 'Glass', 'glass', '5', '66', NULL, NULL, NULL, 'in_stock', 'Plain Glass of weight 10.5 grams', '<p>Plain Glass</p>', NULL, NULL, 0, NULL, NULL, NULL, '2019-05-22 06:49:23', '2019-05-22 06:49:23', NULL),
(5, 'VC-5', '4', 'Silver Glass', 'silver-glass', '5', '66', NULL, NULL, NULL, 'in_stock', '<p><b>Plain Silver Glass </b></p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><b><br></b></p><p>By R C Jewellers</p><p><br></p>', '<p>Plain Glass</p><p>Product Weight: 10 Grams to Approx 10.5 Grams.</p><p>Available Weights: 10,20,30,40,50 up to 60 Grams each Approx....<br></p>', '<p>Regular Silver Glass. </p><p>It can be used as prasadam glass</p><p>It can be used as a gift article for any occation.</p>', NULL, 1, 'Buy Silver Glass in Hyderabad India', 'Buy Silver Glass in Hyderabad India', 'Buy Silver Glass in Hyderabad India', '2019-05-22 06:50:42', '2019-09-03 16:48:33', NULL),
(7, 'VC-7', '7', 'Heart Chocolate', 'heart-chocolate', '4', '116', NULL, NULL, NULL, 'in_stock', '<p>Home made chocolate.</p>', '<p><span style=\"display: inline !important;\">Home made chocolate.</span><br></p>', '<p>100% Chocolate.</p>', 0, 1, NULL, NULL, NULL, '2019-05-22 13:34:00', '2019-06-08 20:03:48', NULL),
(8, 'VC-8', '11', 'BlueBerry Cake', 'blueberry-cake', '1', '61', '111', NULL, 'Egg', 'in_stock', '<p>Overview</p>', '<p>Specifications</p>', '<p>Quality Information</p>', NULL, 1, 'Blueberry Cake Online Hyderabad| Blueberry Cakes Hyderabad', NULL, 'Blueberry Cake Online Hyderabad, Blueberry Cakes Hyderabad', '2019-05-23 01:18:46', '2019-08-28 16:27:08', NULL),
(9, 'VC-9', '11', 'Rich Chocolate Cake', 'rich-chocolate-cake', '1', '61', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:21:10', '2019-05-30 19:24:39', NULL),
(10, 'VC-10', '11', 'Chocolate Truffle Cake', 'chocolate-truffle-cake', '1', '61', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:23:03', '2019-05-30 19:23:00', NULL),
(11, 'VC-11', '11', 'Black Forest Cake', 'black-forest-cake', '1', '61', '110', NULL, 'Egg', 'in_stock', '<p>Overview</p>', '<p>Product Specifications</p>', '<p>Product Information</p>', NULL, 1, 'Black Forest Cake in Hyderabad| Order Black Forest Cake', 'Black Forest Cake in Hyderabad, Order Black Forest Cake', 'Black Forest Cake in Hyderabad, Order Black Forest Cake', '2019-05-23 01:25:52', '2019-09-03 16:32:08', NULL),
(12, 'VC-12', '11', 'Pineapple Cake', 'pineapple-cake', '1', '61', '108', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:27:01', '2019-05-30 19:24:06', NULL),
(13, 'VC-13', '11', 'Mango Cake', 'mango-cake', '1', '61', '111', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:27:51', '2019-05-30 19:23:37', NULL),
(14, 'VC-14', '11', 'Strawberry Cake', 'strawberry-cake', '1', '61', '112', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:29:13', '2019-05-30 19:22:31', NULL),
(15, 'VC-15', '11', 'Vanilla Cake', 'vanilla-cake', '1', '61', NULL, NULL, 'Egg', 'in_stock', '<p>Simple Vanilla Cake.</p>', '<p>Eggless Cake.</p>', '<p>Yummy!!!!!!!!!!!!!!!</p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 01:31:17', '2020-06-18 06:41:45', NULL),
(16, 'VC-16', '11', 'Choco Vanilla Cake', 'choco-vanilla-cake', '1', '61', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:33:09', '2019-05-24 14:13:03', NULL),
(17, 'VC-17', '11', 'Guava Cake', 'guava-cake', '1', '61', '111', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:41:43', '2019-05-24 14:12:13', NULL),
(18, 'VC-18', '11', 'Butterscotch cake', 'butterscotch-cake', '1', '61', '109', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 01:43:05', '2019-05-24 08:19:33', NULL),
(20, 'VC-20', '4', 'Silver Fancy Lakshmi Chembu', 'silver-fancy-lakshmi-chembu', '5', '69', NULL, NULL, NULL, 'in_stock', '<p>Silver Fancy Lakshmi Chembu.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers .</span>        <br></p>', '<p><span style=\"display: inline !important;\">Product Weight: 203 Grams</span><br></p><p><span style=\"display: inline !important;\">Feasible Weights: 180, 200, 250, 300 up to 360 Grams can be provided on order basis.</span></p><p><span style=\"display: inline !important;\">Weights given are Approx.....<br></span><br></p>', '<p>Silver Purity : 78%</p><p>Silver considered to be a holy & pure metal.</p><p>On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding & Occasional Return Gifts.</p>', NULL, 1, 'Silver Fancy Lakshmi Chembu Hyderabad', 'Silver Fancy Lakshmi Chembu Hyderabad', 'Silver Fancy Lakshmi Chembu', '2019-05-23 02:56:57', '2019-09-03 16:47:58', NULL),
(21, 'VC-21', '4', 'Silver Asta Lakshmi Chembu', 'silver-asta-lakshmi-chembu', '5', '69', NULL, NULL, NULL, 'in_stock', '<p>Silver Nakhas Asta Lakshmi Chembu.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p>By R C Jewellers<br></p>', '<p>Product Weight: 160 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: 180, 200, 250, 300 up to 360 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span><br></p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding & Occasional Return Gifts.</span><br></p><div><br></div>', NULL, 1, 'Silver Asta Lakshmi Chembu Hyderabad', 'Silver Asta Lakshmi Chembu Hyderabad', 'Silver Asta Lakshmi Chembu Hyderabad', '2019-05-23 02:58:45', '2019-09-03 16:47:20', NULL),
(22, 'VC-22', '4', 'Silver Plain Chembu', 'silver-plain-chembu', '5', '69', NULL, NULL, NULL, 'in_stock', '<p>Silver Plain Chembu.</p><p>Price inclusive of All Taxes, Packing and Courier Charges.</p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 128 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 10 grams up to 350 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span><br></p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding & Occasional Return Gifts.</span><br></p><div><br></div>', NULL, 1, NULL, NULL, NULL, '2019-05-23 03:02:37', '2019-05-24 15:43:25', NULL),
(23, 'VC-23', '4', 'Silver Fancy Flower Basket', 'silver-fancy-flower-basket', '5', '72', NULL, NULL, NULL, 'in_stock', '<p>Silver Fancy Flower Basket. </p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p><br></p><p>Product Weight: 180.5 Grams.</p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 120 up to 300 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding & Occasional Return Gifts.</span><br></p>', NULL, 2, NULL, NULL, NULL, '2019-05-23 03:12:54', '2019-10-17 19:46:27', NULL),
(24, 'VC-24', '4', 'Silver Fancy Flower Basket', 'silver-fancy-flower-basket', '5', '72', NULL, NULL, NULL, 'in_stock', '<p>Silver Fancy Flower Basket.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 160 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 120 up to 300 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p><span style=\"display: inline !important;\">Silver Purity : 78%</span><br></p><p><span style=\"display: inline !important;\">Silver considered to be a holy &amp; pure metal.</span><br></p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding &amp; Occasional Return Gifts.</span><br></p>', NULL, 2, NULL, NULL, NULL, '2019-05-23 03:15:46', '2019-10-16 06:36:49', NULL),
(25, 'VC-25', '11', 'Red Velvet Cake', 'red-velvet-cake', '1', '62', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:33:48', '2019-05-23 13:23:46', NULL),
(26, 'VC-26', '11', 'Cookie Cream Cake', 'cookie-cream-cake', '1', '62', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:35:14', '2019-05-24 08:20:03', NULL),
(27, 'VC-27', '11', 'White Forest cake', 'white-forest-cake', '1', '62', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:37:51', '2019-05-24 08:17:54', NULL),
(28, 'VC-28', '11', 'Chocolate Drip cake', 'chocolate-drip-cake', '1', '65', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:39:47', '2019-05-24 14:11:24', NULL),
(29, 'VC-29', '11', 'Oreo Drip cake', 'oreo-drip-cake', '1', '65', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:41:47', '2019-05-24 08:18:47', NULL),
(30, 'VC-30', '11', 'Ferrero  Rocher Drip Cake', 'ferrero-rocher-drip-cake', '1', '65', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:44:29', '2019-05-24 08:17:12', NULL),
(31, 'VC-31', '4', 'Silver Glass Engraved', 'silver-glass-engraved', '5', '66', NULL, NULL, NULL, 'in_stock', '<p>Silver Glass Engraved.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 126 Grams (2 PC)</p><p><span style=\"display: inline !important;\">Feasible Weights:15, 20, 30, 40 50 up to 60 Grams Each Glass can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p><div><br></div>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding & Occasional Return Gifts.</span><br></p><div>A Set of 6 Glasses With a Tray can be provided.</div>', NULL, 1, NULL, NULL, NULL, '2019-05-23 03:44:35', '2019-06-29 08:30:32', NULL),
(32, 'VC-32', '11', 'Caramel Drip cake', 'caramel-drip-cake', '1', '65', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:46:18', '2019-05-24 08:13:28', NULL),
(33, 'VC-33', '11', 'Oreo Cake', 'oreo-cake', '1', '62', '110', NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:51:51', '2019-05-24 08:18:17', NULL),
(34, 'VC-34', '11', 'Pista Cake', 'pista-cake', '1', '62', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:53:50', '2019-05-24 08:16:33', NULL),
(35, 'VC-35', '11', 'Shopping Cake', 'shopping-cake', '1', '64', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:56:20', '2019-05-24 08:16:09', NULL),
(36, 'VC-36', '11', 'Diva  Cake', 'diva-cake', '1', '64', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:57:51', '2019-05-24 08:15:39', NULL),
(37, 'VC-37', '11', 'Foot Ball cake', 'foot-ball-cake', '1', '64', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 03:58:30', '2019-05-24 08:15:14', NULL),
(38, 'VC-38', '11', 'Valentine cake', 'valentine-cake', '1', '64', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 04:00:27', '2019-05-24 08:14:48', NULL),
(39, 'VC-39', '11', 'Heart Cake', 'heart-cake', '1', '64', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 04:06:35', '2019-05-24 07:14:43', NULL),
(40, 'VC-40', '11', 'Rainbow cake', 'rainbow-cake', '1', '62', NULL, NULL, 'Egg', 'in_stock', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-05-23 04:09:20', '2019-05-24 07:16:13', NULL),
(41, 'VC-41', '11', 'Vancho cake', 'vancho-cake', '1', '62', '110', NULL, 'Egg', 'in_stock', '<p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\"><font color=\"#000000\" face=\"Roboto\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\">Product  Overview</span></font></p>', '<p>Specification</p>', '<p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\"><font color=\"#000000\" face=\"Arial\"><span style=\"font-size: 14.6667px; white-space: pre-wrap;\">Product Quality Information</span></font></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 04:10:24', '2019-08-02 16:07:45', NULL),
(42, 'VC-42', '11', 'White Chocolate cake', 'white-chocolate-cake', '1', '61', '110', NULL, 'Egg', 'in_stock', '<p>test</p>', '<p>test</p>', '<p>test</p>', NULL, 1, NULL, 'cake, chocolate cake', '<p>Chocolate Cake is best cakes in india from velchala</p>', '2019-05-23 04:12:24', '2019-08-26 19:05:15', '179'),
(43, 'VC-43', '4', 'Silver Fancy Lakshmi Binde', 'silver-fancy-lakshmi-binde', '5', '68', NULL, NULL, NULL, 'out_of_stock', '<p>Silver Fancy Lakshmi Binde.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 770 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights:600, 750, 900 and 1100 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p><div><br></div>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span></p><p><span style=\"display: inline !important;\"> Used As A Best Wedding Gift.</span></p><p><span style=\"display: inline !important;\">Used for Pooja Purpose.</span></p><p><br></p><p><span style=\"display: inline !important;\"></span><br></p><p><br></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:05:26', '2019-06-29 08:28:37', NULL),
(44, 'VC-44', '4', 'Silver Fancy Bowl', 'silver-fancy-bowl', '5', '70', NULL, NULL, NULL, 'in_stock', '<p>Silver Fancy Bowl.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 84 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: 50,60,70,80,90,100,120 up to 150 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding &amp; Occasional Return Gifts.</span><br></p><p>It can be used for Pooja Purpose.</p><p>It can be used for kids to feed food.</p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:13:35', '2019-06-29 08:27:31', NULL),
(45, 'VC-45', '4', 'Silver Peacock Kum Kum cups', 'silver-peacock-kum-kum-cups', '5', '76', NULL, NULL, NULL, 'in_stock', '<p>Silver Peacock Kum Kum cups.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 44 Grams</p><p><span style=\"display: inline !important;\">Available in set of 3 cups and 4 cups, can be provided on order basis.</span><br></p><p><br></p><div><br></div>', '<p>Silver Purity : 78%</p><div><div><span style=\"display: inline !important;\">It is used for Pooja Purpose.&nbsp;</span></div><div><span style=\"display: inline !important;\"><br>On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding &amp; Occasional Return Gifts.</span><br></div></div>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:18:48', '2019-06-29 08:26:58', NULL),
(46, 'VC-46', '4', 'Silver Plain Kum Kum cups', 'silver-plain-kum-kum-cups', '5', '76', NULL, NULL, NULL, 'in_stock', '<p><b>Silver Plain Kum Kum cups.</b></p><p>Price inclusive of All Taxes, Packing and Courier Charges.</p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 44 Grams</p><p>Available in set of 2 cups and 4 cups also.</p>', '<p><span style=\"display: inline !important;\">Silver Purity : 78%</span></p><p><span style=\"display: inline !important;\">It is used for Pooja Purpose.</span></p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding &amp; Occasional Return Gifts.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:22:12', '2019-06-29 08:26:02', NULL),
(47, 'VC-47', '4', 'Silver Pancha Patra Set', 'silver-pancha-patra-set', '5', '76', NULL, NULL, NULL, 'in_stock', '<p>Silver Pancha Patra Set.</p><p>Price inclusive of All Taxes, Packing and Courier Charges.</p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 116 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: It depends on size of plate and pancha patra. It can be provided on order basis.</span><br></p><p><br></p><div><br></div>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy &amp; pure metal.</span></p><p><span style=\"display: inline !important;\">It is used on holy festivals for Pooja and also in other occasions.<br></span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:27:11', '2019-06-29 08:25:26', NULL),
(48, 'VC-48', '4', 'Silver Baby Rattle', 'silver-baby-rattle', '5', '78', NULL, NULL, NULL, 'in_stock', '<p>Silver Baby rattle</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 19 Grams.</p><p><br></p>', '<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px;\"><li style=\"color: rgb(148, 148, 148); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Best thing For gifting to any kids at any occasion as Nam-karan function / Birthday</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"overflow-wrap: break-word; display: block;\"><font face=\"Amazon Ember, Arial, sans-serif\">Silver Purity : 78%</font><br></span></li><li style=\"color: rgb(148, 148, 148); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); overflow-wrap: break-word; display: block;\">Precious &amp; memorable thing for kids</span></li></ul>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:37:41', '2019-06-29 08:25:03', NULL),
(49, 'VC-49', '4', 'Silver Baby Feeder', 'silver-baby-feeder', '5', '78', NULL, NULL, NULL, 'in_stock', '<p>Silver Baby Feeder.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p><div><br></div>', '<p>Product Weight: 10 Grams</p><p>&nbsp;&nbsp;&nbsp;&nbsp;<br></p>', '<ul class=\"a-unordered-list a-vertical a-spacing-none\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 18px; padding: 0px;\"><li style=\"color: rgb(148, 148, 148); font-family: &quot;Amazon Ember&quot;, Arial, sans-serif; font-size: 13px; list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"color: rgb(17, 17, 17); font-family: Poppins;\">Best gift for newborn baby and useful for long time.</span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"font-family: Poppins;\"><font face=\"Amazon Ember, Arial, sans-serif\">Silver Purity : 78%</font></span></li><li style=\"list-style: disc; overflow-wrap: break-word; margin: 0px;\"><span class=\"a-list-item\" style=\"\">It can be provided in 92.5 Purity on order basis.</span></li></ul>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:44:08', '2019-06-29 08:24:41', NULL),
(50, 'VC-50', '4', 'Silver Bucket', 'silver-bucket', '5', '70', NULL, NULL, NULL, 'in_stock', '<p>Silver Bucket.</p><p>Price inclusive of All Taxes, Packing and Courier Charges.</p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 104 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: 10,20,30,40,50 up to 120 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 78%</p><div>It can be used for Pooja Purpose.</div>', NULL, 1, NULL, NULL, NULL, '2019-05-23 05:56:44', '2019-06-29 08:24:20', NULL),
(51, 'VC-51', '4', 'Silver Lakshmi Idol', 'silver-lakshmi-idol', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Silver Lakshmi Idol.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p><div><br></div>', '<p>Product Weight: 32&nbsp;<span style=\"display: inline !important;\">Grams</span></p><p><span style=\"display: inline !important;\">Feasible Weights:Starting from 10 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy &amp; pure metal.</span></p><p><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Best thing for gifting to friends , family , relatives and kanya-daan.</span></p><p><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Keep them to worship at holy place in your home/office.</span></p><p><span style=\"display: inline !important;\"></span><br></p><div><br></div>', NULL, 1, NULL, NULL, NULL, '2019-05-23 06:04:40', '2019-06-29 08:23:52', NULL),
(52, 'VC-52', '9', 'Ganesha Pendent', 'ganesha-pendent', '142', '95', NULL, NULL, NULL, 'in_stock', '<p>Ganesha Pendent and Ear Rings Set.</p>', '<p><span style=\"display: inline !important;\">Ganesha Pendent and Ear Rings Set.</span><br></p>', '<p><span style=\"display: inline !important;\">Ganesha Pendent and Ear Rings Set.</span><br></p>', NULL, 1, 'Ganesha Pendants - Buy Ganesha Pendant Designs Online in Hyderabad', 'Ganesha Pendants - Buy Ganesha Pendant Designs Online in Hyderabad', 'Ganesha Pendants - Buy Ganesha Pendant Designs Online in Hyderabad', '2019-05-23 07:07:56', '2019-09-03 16:37:52', NULL),
(53, 'VC-53', '15', 'Delightful Dry Seeds Laddoo', 'delightful-dry-seeds-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<b><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Cambria\",\"serif\";\r\nmso-ascii-theme-font:major-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Tahoma;background:#FEFEFE;mso-ansi-language:EN-US;\r\nmso-fareast-language:EN-US;mso-bidi-language:AR-SA\">Dry Seeds Ladoo</span></b><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Cambria\",\"serif\";\r\nmso-ascii-theme-font:major-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Tahoma;background:#FEFEFE;mso-ansi-language:EN-US;\r\nmso-fareast-language:EN-US;mso-bidi-language:AR-SA\">:\r\n<span style=\"color:#943634;mso-themecolor:accent2;mso-themeshade:191\">Pumpkin\r\nSeeds are considered </span></span><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3745769/\" target=\"_blank\"><span style=\"font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;\r\nmso-hansi-theme-font:major-latin;mso-bidi-font-family:Tahoma;color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191;background:#FEFEFE\">excellent sources</span></a></span><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Cambria\",\"serif\";\r\nmso-ascii-theme-font:major-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Tahoma;color:#943634;mso-themecolor:accent2;mso-themeshade:\r\n191;background:#FEFEFE;mso-ansi-language:EN-US;mso-fareast-language:EN-US;\r\nmso-bidi-language:AR-SA\"> of </span><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://www.medicalnewstoday.com/articles/287212.php\" title=\"Everything you need to know about potassium\"><span style=\"font-family:\r\n\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Tahoma;color:#943634;mso-themecolor:accent2;mso-themeshade:\r\n191;background:#FEFEFE\">potassium</span></a></span><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:major-latin;mso-bidi-font-family:Tahoma;color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191;background:#FEFEFE;mso-ansi-language:\r\nEN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\">, magnesium,\r\nand </span><span style=\"font-size:11.0pt;line-height:115%;font-family:\r\n\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;mso-fareast-font-family:\r\n\"Times New Roman\";mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:\r\nminor-latin;mso-bidi-font-family:\"Times New Roman\";mso-bidi-theme-font:minor-bidi;\r\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><a href=\"https://www.medicalnewstoday.com/articles/248958.php\" title=\"Calcium: Health benefits, foods, and deficiency\"><span style=\"font-family:\r\n\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Tahoma;color:#943634;mso-themecolor:accent2;mso-themeshade:\r\n191;background:#FEFEFE\">calcium</span></a></span><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:major-latin;mso-bidi-font-family:Tahoma;color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191;background:#FEFEFE;mso-ansi-language:\r\nEN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\">, they are good\r\nsource of healthy oils, and  nutrients\r\nthat enhance the health of the heart and bones. Watermelon seeds are rich\r\nsource of proteins, vitamins, omega 3 and omega 6 fatty acids, magnesium, zinc,\r\ncopper, potassium and more. Sunflower seeds are rich in  B complex <b>vitamins</b>, which are\r\nessential for a healthy nervous system, and are a good source of phosphorus, magnesium, iron, calcium, potassium, proteins and vitamin E. </span>', '<p>Dates, Pumpkin Seeds, Sunflower Seeds, Watermelon Seeds, Raisins, Poppy Seeds.<br></p>', '<p><span style=\"display: inline !important;\">Will be fresh for 15 days kept in cool dry place away from direct sun light. can be fresh for 30 days if kept in the Fridge.</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-05-23 07:08:01', '2019-08-01 10:10:39', NULL),
(54, 'VC-54', '15', 'Delicious Dry Fruit  Laddoo', 'delicious-dry-fruit-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<p>Dry fruits are traditionally known to be a Powerhouse of nutrients, to have antioxidant, disease preventing and health promoting properties. Ingredients: Dates, Almonds, Pistachios (Pista), Cashew nuts, Figs (Angeer), Honey and Poppy seeds. <br></p>', '<p>Dates, Figs, Almonds, Pistachios, Cashews, Poppy Seeds.</p>', '<p><span style=\"display: inline !important;\">Will be fresh for 15 days kept in cool dry place away from direct sun light. can be fresh for 30 days if kept in the Fridge.</span><br></p>', NULL, 1, 'Delicious Dry Fruit Laddoo| Delicious Dry Fruit Laddu Hyderabad', 'Delicious Dry Fruit Laddoo, Delicious Dry Fruit Laddu Hyderabad', 'Delicious Dry Fruit Laddoo, Delicious Dry Fruit Laddu Hyderabad', '2019-05-23 13:06:45', '2019-09-03 16:44:00', NULL),
(55, 'VC-55', '15', 'Assorted Dry Fruit Laddoo\'s', 'assorted-dry-fruit-laddoos', '117', '128', NULL, NULL, NULL, 'in_stock', '<p>Out Outstandingly Delicious, No Sugar, Home Made Laddoo\'s. </p>', '<p>Assorted with Five Fabulous  Home Made Laddoo\'s</p>', '<p><span style=\"display: inline !important;\">Will be fresh for 15 days kept in cool dry place away from direct sun light. can be fresh for 30 days if kept in the Fridge.</span><br></p>', NULL, 1, 'Buy Assorted Dry Fruit Laddoos| Online Assorted Dry Fruit Laddoos Hyderabad', 'Buy Assorted Dry Fruit Laddoos, Online Assorted Dry Fruit Laddoos Hyderabad', 'Buy Assorted Dry Fruit Laddoos andOnline Assorted Dry Fruit Laddoos Hyderabad', '2019-05-23 17:57:30', '2019-09-03 16:43:15', NULL),
(56, 'VC-56', '4', 'Silver Lotus Ganesh Idol', 'silver-lotus-ganesh-idol', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Silver Ganesha Idol.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers.</span></p>', '<p>Product Weight:  45 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 10 Grams can be provided ready or on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p style=\"color: rgb(68, 68, 68);\">Silver Purity : 78%</p><p style=\"color: rgb(68, 68, 68);\"><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Best thing for gifting to friends , family , relatives.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Keep them to worship at holy place in your home/office.</span></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 18:23:13', '2019-08-27 07:42:56', NULL),
(57, 'VC-57', '15', 'Exotic Kiwi Fruit & Nut', 'exotic-kiwi-fruit-nut', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:major-latin;mso-bidi-font-family:Tahoma;color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191;background:#FEFEFE;mso-ansi-language:\r\nEN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"> </span><b><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Cambria\",\"serif\";\r\nmso-ascii-theme-font:major-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Tahoma;background:#FEFEFE;mso-ansi-language:EN-US;\r\nmso-fareast-language:EN-US;mso-bidi-language:AR-SA\">Fruit & Nut:</span></b><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Cambria\",\"serif\";\r\nmso-ascii-theme-font:major-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Tahoma;color:#943634;mso-themecolor:accent2;mso-themeshade:\r\n191;background:#FEFEFE;mso-ansi-language:EN-US;mso-fareast-language:EN-US;\r\nmso-bidi-language:AR-SA\"> </span><strong><i><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> </span></i></strong><i style=\"\"><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">High Source of\r\nVitamin C, Good Source of Dietary Fiber, Helps in Digestion,</span></i><strong><i><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> </span></i></strong><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Kiwi fruit is loaded with vitamins and minerals such\r\nas Vitamins A, B6, B12, E, and </span><span style=\"font-size:11.0pt;line-height:\r\n115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://food.ndtv.com/food-drinks/5-health-benefits-pomegranate-cook-with-it-1214988\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">potassium</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, </span><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://food.ndtv.com/health/does-your-daily-diet-include-calcium-rich-food-supplements-to-the-rescue-756812\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">calcium</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, iron and magnesium. Raisins </span><span style=\"color: rgb(148, 54, 52); font-family: Cambria, serif; font-size: 11pt; display: inline !important;\">help in digestion,\r\nRaisins contain potassium and magnesium in good levels. These help reduce\r\nacidity and help remove the toxins from the system, preventing diseases like\r\narthritis, gout, kidney stones and heart diseases. </span></p>', '<p>Kiwi Dry Fruit, Oats, Raisins, Almonds, Cashew, Jaggery.</p>', '<p><span style=\"display: inline !important;\">Will be fresh for 15 days kept in cool dry place away from direct sun light. can be fresh for 30 days if kept in the Fridge.</span><br></p>', NULL, 1, 'Buy Exotic Kiwi Fruit Nut Hyderabad in India', 'Buy Exotic Kiwi Fruit Nut Hyderabad in India', 'Buy Exotic Kiwi Fruit Nut Hyderabad in India', '2019-05-23 18:34:29', '2019-09-03 16:44:25', NULL),
(70, 'VC-70', '9', 'Ear Rings and Tops', 'ear-rings-and-tops', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>Perfect party wear</p>', '<p>German silver with green stones</p>', '<p>Good</p>', NULL, 1, 'Earrings and Tops: Buy Earrings online at best prices in Hyderabad', 'Earrings and Tops, Buy Earrings online at best prices in Hyderabad', 'Earrings and Tops: Buy Earrings online at best prices in Hyderabad', '2019-05-24 01:38:39', '2019-09-03 16:35:42', NULL),
(71, 'VC-71', '9', 'Lakshmi kasu', 'lakshmi-kasu', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Beautiful Lakshmi kasu  for traditional wear</p>', '<p>Beautiful earrings and necklace with pearl dropping </p>', 'Excellent ', NULL, 1, 'Lakshmi Kasu: Buy Lakshmi Kasu Online in Hyderabad', 'Lakshmi Kasu, Buy Lakshmi Kasu Online in Hyderabad', 'Lakshmi Kasu: Buy Lakshmi Kasu Online in Hyderabad', '2019-05-24 05:19:47', '2019-09-03 16:38:27', NULL),
(72, 'VC-72', '4', 'Silver 925 Enamel Leaf Lamp', 'silver-925-enamel-leaf-lamp', '5', '77', NULL, NULL, NULL, 'in_stock', '<p>Silver 925 Enamel Leaf Lamp.</p><p><span style=\"display: inline !important;\"><span style=\"display: inline !important;\">By R C Jewellers</span></span></p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><br></p>', '<p><span style=\"background-color: rgb(255, 255, 0);\"><br></span></p><p><span style=\"background-color: rgb(255, 255, 0);\">Product Weight: 33 Grams Approx</span></p><div>Available Enamel Colors: Pink, red, Gold Colors</div>', '<p>Silver Purity : 92.5 </p><p><span style=\"display: inline !important;\">On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding & Occasional Return Gifts.</span><br></p><div><br></div>', NULL, 1, NULL, NULL, NULL, '2019-05-24 05:25:05', '2019-08-22 17:54:36', NULL),
(58, 'VC-58', '4', 'Silver Sai Baba Idol', 'silver-sai-baba-idol', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Silver Sai Baba Idol</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 52 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights:Starting from 10 Grams can be provided ready or on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p style=\"color: rgb(68, 68, 68);\">Silver Purity : 78%</p><p style=\"color: rgb(68, 68, 68);\"><span style=\"display: inline !important;\">Silver considered to be a holy &amp; pure metal.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Best thing for gifting to friends , family , relatives.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Keep them to worship at holy place in your home/office.</span></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 18:47:40', '2019-06-29 08:22:46', NULL),
(59, 'VC-59', '4', 'Silver Hanuman Idol', 'silver-hanuman-idol', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Silver Hanuman Idol.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 34 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 10 Grams can be provided ready or on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p style=\"color: rgb(68, 68, 68);\">Silver Purity : 78%</p><p style=\"color: rgb(68, 68, 68);\"><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Best thing for gifting to friends , family , relatives.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Keep them to worship at holy place in your home/office.</span></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 18:52:15', '2019-06-29 08:22:10', NULL),
(60, 'VC-60', '15', 'Tasty Tathi Bellam(Palm Jaggery) Laddoo', 'tasty-tathi-bellampalm-jaggery-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Palm jaggery is rich\r\nin essential minerals, Its regular consumption increases&nbsp;</span><span style=\"font-size:11.0pt;line-height:115%;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;\r\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><a href=\"https://www.stylecraze.com/articles/hemoglobin-rich-foods/\">hemoglobin level</a>s</span></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">and treats anemia. Magnesium regulates the nervous system. It is\r\nequally rich in calcium, potassium, and phosphorus.</span><br></p>', NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-05-23 18:52:23', '2019-05-23 18:52:23', NULL),
(61, 'VC-61', '15', 'Sesame Black Pearls Laddu', 'sesame-black-pearls-laddu', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><span style=\"font-size:11.0pt;line-height:115%;\r\nfont-family:\"Cambria\",\"serif\";mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-bidi-font-family:Arial;color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\">Sesame seeds know to be <span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">High Source of Cholesterol-Lowering, Good Source of Vitamins and\r\nMinerals like Iron, Fiber and Magnesium, Fights Cancer, Hair fall, can help\r\nreduce blood sugar, cholesterol and blood pressure. Black Sesame seeds in\r\nparticular have anti-ageing benefits, decreases\r\nthe risk of cancer. </span></span><br></p>', '<p>Black Sesame Seeds, White Sesame Seeds, Palm Jaggery, Ghee.</p>', '<p><span style=\"display: inline !important;\">Will be fresh for 15 days kept in cool dry place away from direct sun light. can be fresh for 30 days if kept in the Fridge.</span><br></p>', NULL, 1, 'Buy Sesame Black Pearls Laddu in Hyderabad', 'Buy Sesame Black Pearls Laddu in Hyderabad', 'Buy Sesame Black Pearls Laddu in Hyderabad', '2019-05-23 19:01:40', '2019-09-03 16:45:02', NULL),
(62, 'VC-62', '4', 'Silver Shiva Lingam With Naga Padiga', 'silver-shiva-lingam-with-naga-padiga', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Silver Shiva Lingam With Naga Padiga.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 37 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 30 Grams above 100 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy &amp; pure metal.</span><br></p><p><span style=\"display: inline !important;\">Keep them to worship at holy place in your home/office.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 19:03:43', '2019-06-29 08:21:35', NULL);
INSERT INTO `products` (`p_id`, `p_unique_id`, `p_vendor_id`, `p_name`, `p_alias`, `p_cat_id`, `p_sub_cat_id`, `p_item_spl`, `p_main_image`, `p_dishtype`, `p_availability`, `p_overview`, `p_specifications`, `p_quality_care_info`, `p_delivery_charges`, `p_status`, `p_meta_title`, `p_meta_keywords`, `p_meta_description`, `created_at`, `updated_at`, `p_related`) VALUES
(63, 'VC-63', '15', 'Simply Cinnamon', 'simply-cinnamon', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Cinnamon is one of the most beneficial spices on\r\nearth, giving it antioxidant, </span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(192, 80, 77); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><a href=\"https://draxe.com/recipe/anti-inflammatory-juice/\" target=\"_blank\"><span style=\"color: rgb(192, 80, 77);\">anti-inflammatory</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(192, 80, 77); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">,</span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> anti-diabetic,\r\nanti-microbial, immunity-boosting and potential cancer and heart\r\ndisease-protecting abilities.</span><br></p>', '<p>Dates, Oats, Cinnamon, Jaggery etc.</p>', '<p><span style=\"display: inline !important;\">Will be fresh for 15 days kept in cool dry place away from direct sun light. can be fresh for 30 days if kept in the Fridge.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 19:17:04', '2019-08-04 22:28:42', NULL),
(64, 'VC-64', '15', 'Crunchy Ginger Laddoo', 'crunchy-ginger-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif;\">Ginger</span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif;\"> </span><span style=\"font-size:11.0pt;line-height:\r\n115%;font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;mso-fareast-font-family:\r\n\"Times New Roman\";mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:\r\nmajor-latin;mso-bidi-font-family:Arial;color:#943634;mso-themecolor:accent2;\r\nmso-themeshade:191;mso-ansi-language:EN-US;mso-fareast-language:EN-US;\r\nmso-bidi-language:AR-SA\">has been called a </span><span style=\"font-size:\r\n11.0pt;line-height:115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:\r\nminor-latin;mso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:\r\nminor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://chopra.com/articles/5-superfoods-that-belong-in-your-diet\"><span style=\"font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;\r\nmso-hansi-theme-font:major-latin;mso-bidi-font-family:Arial;color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191\">superfood</span></a></span><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Cambria\",\"serif\";\r\nmso-ascii-theme-font:major-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:major-latin;\r\nmso-bidi-font-family:Arial;color:#943634;mso-themecolor:accent2;mso-themeshade:\r\n191;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"> time\r\nand again. Some of the Health Benefits of Ginger are, </span><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Calibri\",\"sans-serif\";\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:Arial;color:#943634;mso-themecolor:accent2;mso-themeshade:\r\n191;mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA;\r\nmso-bidi-font-weight:bold\">Stimulates Digestion, Lowers Blood Pressure, Reduces\r\nNausea, Reduces Inflammation,  Regulates Blood-Sugar.<b></b></span><br></p>', '<p>Crunchy Ginger, Raisins, Oats, Jaggery, Corn flacks.<br></p>', '<p><span style=\"display: inline !important;\">Will be fresh for 15 days when kept in a cool place away from sunlight. Can be fresh for 30 days when kept in a fridge.</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-05-23 19:25:23', '2019-08-05 22:21:49', NULL),
(65, 'VC-65', '15', 'Yummy Chocolate Laddoo', 'yummy-chocolate-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><a name=\"section2\"><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Walnut is Super Source of <b>Omega-3 </b></span></a><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Fats.<b> </b>Walnut\r\nlooks like a tiny brain; research suggests that this nut indeed good for your <b>Brain</b>\r\ndevelopment. This rich nutrient contributes to the many health benefits.  Also Good for Skin and Hair. Wallnuts\r\ncontains, </span><span style=\"font-size: 11.5pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">iron,\r\nselenium, calcium, zinc, vitamin E and some B vitamins.</span><br></p>', '<p>Oats, Walnuts, Coco, Ghee etc</p>', '<p>Kept in a cool place away from direct sunlight will be fresh for 15days. Can be fresh if kept in a Fridge.</p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 19:38:05', '2019-08-04 22:20:16', NULL),
(66, 'VC-66', '15', 'Refreshing Coffee Laddoo', 'refreshing-coffee-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:\r\njustify;text-justify:inter-ideograph;line-height:normal\"><b><span style=\"font-family: Cambria, serif; color: rgb(192, 80, 77); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Refreshing</span></b><span style=\"font-family: Cambria, serif; color: rgb(192, 80, 77); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> Coffee with\r\nHealthy Ragi, Oats, Peanuts,  Black\r\nResins, Jaggery and Pure Ghee.</span><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> Black Rasins are a rich source of energy, vitamins,\r\nminerals and electrolytes. Some of the Health benefits are, Beautiful healthy\r\nhair, keeps your Blood Free from Impurities, Fights against Bad Cholesterol\r\netc. <o:p></o:p></span></p>', '<p>Refreshing Coffee Laddoo<br></p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 19:47:17', '2019-08-04 22:19:50', NULL),
(67, 'VC-67', '15', 'Scrumptious Sarghum(Jonna) Laddoo', 'scrumptious-sarghumjonna-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">The major health benefits of sorghum include its\r\nability to prevent certain types of </span><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://www.organicfacts.net/home-remedies/home-remedies-for-cancer.html?utm_source=internal&utm_medium=link&utm_campaign=smartlinks\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">cancer</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, help control </span><span style=\"font-size:\r\n11.0pt;line-height:115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:\r\nminor-latin;mso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:\r\nminor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://www.organicfacts.net/diabetes.html?utm_source=internal&utm_medium=link&utm_campaign=smartlinks\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">diabetes</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, offer a dietary option to people with </span><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Calibri\",\"sans-serif\";\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:\"Times New Roman\";mso-bidi-theme-font:minor-bidi;\r\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><a href=\"https://www.organicfacts.net/coeliac-disease-gluten-intolerance.html?utm_source=internal&utm_medium=link&utm_campaign=smartlinks\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">celiac</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> disease, improve </span><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Calibri\",\"sans-serif\";\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:\"Times New Roman\";mso-bidi-theme-font:minor-bidi;\r\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><a href=\"https://www.organicfacts.net/home-remedies/20-tips-to-improve-digestive-health.html?utm_source=internal&utm_medium=link&utm_campaign=smartlinks\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">digestive health</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, build strong bones, promote red blood cell development,\r\nand boost energy and fuel production. </span><br></p>', '<p>No Sugar Pure Ghee with Almonds & Raisins.</p>', '<p>Will be fresh for 15 days kept in cool dry place away from direct sun light. can be fresh for 30 days if kept in the Fridge.</p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 19:57:26', '2019-08-04 20:58:36', NULL),
(68, 'VC-68', '15', 'Royal Ragi Laddoo', 'royal-ragi-laddoo', '117', '128', NULL, NULL, NULL, 'in_stock', '<p><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Ragi is one of the best non-dairy sources of\r\ncalcium when compared to any other grains. Ragi is <strong><span style=\"font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;\r\nmso-hansi-theme-font:major-latin;mso-bidi-font-family:Arial\">Loaded with\r\nCalcium, helps in Controlling Diabetes, Reverts Skin Ageing, </span></strong><b>Helps</b> in<b> Weight Loss, </b>Ragi reduces\r\n“bad” cholesterol, prevents cardiovascular disease. <b>Jaggery</b> is loaded with antioxidants and minerals such as zinc and\r\nselenium, which in turn </span><span style=\"font-size:11.0pt;line-height:\r\n115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://food.ndtv.com/food-drinks/say-bye-bye-to-winter-troubles-with-these-superfoods-1258226\" target=\"_blank\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">help prevent free-radical damag</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">e and also </span><span style=\"font-size:11.0pt;\r\nline-height:115%;font-family:\"Calibri\",\"sans-serif\";mso-ascii-theme-font:minor-latin;\r\nmso-fareast-font-family:\"Times New Roman\";mso-fareast-theme-font:minor-fareast;\r\nmso-hansi-theme-font:minor-latin;mso-bidi-font-family:\"Times New Roman\";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA\"><a href=\"https://food.ndtv.com/health/eat-this-to-boost-your-immune-system-756074\" target=\"_blank\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">boost resistance against infections</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">. Jaggery also helps increase the total count of </span><span style=\"font-size:11.0pt;line-height:115%;font-family:\"Calibri\",\"sans-serif\";\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:\"Times New Roman\";\r\nmso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-font-family:\"Times New Roman\";mso-bidi-theme-font:minor-bidi;\r\nmso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA\"><a href=\"https://food.ndtv.com/food-drinks/5-iron-rich-foods-for-a-stronger-you-good-health-1201734\" target=\"_blank\"><span style=\"font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">hemoglobin</span></a></span><span style=\"font-size: 11pt; line-height: 115%; font-family: Cambria, serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> in the blood.<b> </b></span><br></p>', '<p>Ragi(Finger Millet), Sesame Seeds, Flax Seeds, Cashew, Jaggery & Ghee.</p>', '<p>Will be fresh for 15 days when kept in a cool place away from sunlight. Can be fresh for 30 days when kept in a fridge.</p>', NULL, 0, NULL, NULL, NULL, '2019-05-23 20:00:23', '2019-08-05 22:23:16', NULL),
(69, 'VC-69', '15', 'Kakarakaya (Bitter Gourd) Karam', 'kakarakaya-bitter-gourd-karam', '117', '126', NULL, NULL, NULL, 'in_stock', '<div style=\"mso-element:para-border-div;border:none;border-bottom:solid #F2F2F2 1.0pt;\r\nmso-border-bottom-alt:solid #F2F2F2 .75pt;padding:0in 0in 0in 0in\">\r\n\r\n<p class=\"MsoNormal\" style=\"mso-margin-bottom-alt:auto;line-height:normal;\r\nborder:none;mso-border-bottom-alt:solid #F2F2F2 .75pt;padding:0in;mso-padding-alt:\r\n0in 0in 0in 0in\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Bitter gourd\r\ncontains a train of important nutrients ranging from </span><span style=\"color:#943634;mso-themecolor:accent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/food-drinks/5-iron-rich-foods-for-a-stronger-you-good-health-1201734\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">iron</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">,</span><span style=\"color:#943634;mso-themecolor:accent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/health/magnesium-plays-role-in-regulating-bodys-internal-clock-1396650\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">magnesium</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> and </span><span style=\"color:#943634;mso-themecolor:accent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/health/vitamin-b12-rich-foods-are-you-getting-enough-of-it-1290563\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">vitamin</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"> to\r\npotassium and </span><span style=\"color:#943634;mso-themecolor:\r\naccent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/health/vitamin-c-can-help-fight-cancer-says-study-694676\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">vitamin C</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">. \r\nAn excellent source of </span><span style=\"color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/food-drinks/new-diet-mantra-dietary-fibre-from-various-food-sources-could-be-more-beneficial-781688\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">dietary fiber</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, it also\r\ncontains twice the </span><span style=\"color:#943634;mso-themecolor:\r\naccent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/food-drinks/4-delicious-spinach-recipes-from-south-india-load-up-on-greens-1281577\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">calcium of spinach</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, </span><span style=\"color:#943634;mso-themecolor:accent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/food-drinks/why-broccoli-is-good-for-you-693936\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">beta-carotene of broccoli</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">, and the </span><span style=\"color:#943634;mso-themecolor:accent2;mso-themeshade:191\"><a href=\"https://food.ndtv.com/food-drinks/benefits-of-banana-how-to-include-the-fruit-in-your-daily-diet-1216006\"><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">potassium of a banana</span></a></span><span style=\"font-family: Arial, sans-serif; color: rgb(148, 54, 52); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">.</span><span style=\"font-family:\"Cambria\",\"serif\";mso-ascii-theme-font:major-latin;\r\nmso-hansi-theme-font:major-latin;mso-bidi-font-family:Tahoma;color:#943634;\r\nmso-themecolor:accent2;mso-themeshade:191;background:#FEFEFE\"><o:p></o:p></span></p>\r\n\r\n</div>', '<p>Bitter Guard, Curry Leaves, cumin seeds etc.</p>', '<p>kept in a air tight container will be fresh for more than 45days.</p>', NULL, 1, NULL, NULL, NULL, '2019-05-23 20:11:46', '2019-08-08 10:31:22', NULL),
(73, 'VC-73', '9', 'Black Beads haram', 'black-beads-haram', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>New look matte finish </p>', '<p>Matte finish pendant with matching earrings </p><p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">different colours available in pendant </span></p>', '<p>Good</p>', NULL, 1, 'Black Beads Haram in Hyderabad India', 'Black Beads Haram in Hyderabad India', 'Black Beads Haram in Hyderabad India', '2019-05-24 05:31:59', '2019-09-03 16:35:04', NULL),
(74, 'VC-74', '4', 'Silver Engraved Plate With Gold Flower', 'silver-engraved-plate-with-gold-flower', '5', '67', NULL, NULL, NULL, 'in_stock', '<p>Silver Engraved Plate With Gold Flower.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 503 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: 300, 350, 400, 450, 500, 600 Grams&nbsp; up to 1 Kilo can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p><div><br></div>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Best thing for gifting to Friends , Family , Relatives.</span><br></p><div>It can be used as Lunch Plate.</div>', NULL, 1, NULL, NULL, NULL, '2019-05-24 05:35:56', '2019-06-29 08:19:48', NULL),
(75, 'VC-75', '9', 'Beads haram', 'beads-haram', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>New  look in haram </p>', '<p>Pearl haram with silver Lakshmi pendant and Beautiful Lakshmi hangings</p>', '<p>Nice</p>', NULL, 1, NULL, NULL, NULL, '2019-05-24 05:37:50', '2019-08-06 16:41:42', NULL),
(76, 'VC-76', '4', 'Silver 925 Rose Kum Kum Cup Gold Plated', 'silver-925-rose-kum-kum-cup-gold-plated', '5', '79', NULL, NULL, NULL, 'in_stock', '<p>Silver 925 Rose Kum Kum Cup Gold Plated.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p><div><br></div>', '<p>Product Weight: 45 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: 40,50,60,70 up to 100 Grams can be provided on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span><br></p>', '<p>Silver Purity : 92.5%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy &amp; pure metal.</span><br></p><p><span style=\"display: inline !important;\">Best thing for gifting to friends , family , relatives.</span></p><p>On particular occasions these items can be readily turned into a wonderful Gift Item. Also Use As A Best Wedding &amp; Occasional Return Gifts.</p><p><br></p><p><span style=\"display: inline !important;\"></span><br></p><div><br></div>', NULL, 1, NULL, NULL, NULL, '2019-05-24 05:42:14', '2019-06-29 08:18:47', NULL),
(77, 'VC-77', '9', 'Lakshmi haram', 'lakshmi-haram', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>Trendy look </p>', '<p>Beautiful Lakshmi pendant with matching earrings </p><p>matte silver finish</p>', '<p>Fine Quality </p>', NULL, 1, NULL, NULL, NULL, '2019-05-24 05:43:11', '2019-08-06 16:40:27', NULL),
(78, 'VC-78', '4', 'Silver 925 Tulasi Kota Gold Plated', 'silver-925-tulasi-kota-gold-plated', '5', '76', NULL, NULL, NULL, 'in_stock', '<p>Silver 925 Tulasi Kota Gold Plated.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p><div><br></div>', '<p>Product Weight: 8 Grams</p><p>Available in 8 & 11.5 Grams</p><p><br></p>', '<p>Silver Purity : 92.5%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span><br></p><p><span style=\"display: inline !important;\">Best thing for gifting to friends , family , relatives.</span><br></p><p><span style=\"display: inline !important;\">Keep them to worship at holy place in your home/office.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-05-24 05:46:21', '2019-08-29 18:59:48', NULL),
(79, 'VC-79', '4', 'Silver Tulasi Kota', 'silver-tulasi-kota', '5', '76', NULL, NULL, NULL, 'in_stock', '<p>Silver Tulasi Kota .</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p>', '<p>Product Weight: 10 Grams</p><p>Available in 5,10 &20 Grams</p>', '<p>Silver Purity : 78%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span><br></p><p><span style=\"display: inline !important;\">Best thing for gifting to friends , family , relatives.</span><br></p><p><span style=\"display: inline !important;\">Keep them to worship at holy place in your home/office.</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-05-24 05:49:22', '2019-10-11 22:50:32', NULL),
(80, 'VC-80', '4', 'Silver Rosewood Peetam', 'silver-rosewood-peetam', '5', '76', NULL, NULL, NULL, 'in_stock', '<p>Silver Rosewood Peetam</p><p>Price inclusive of All Taxes, Packing and Courier Charges.</p><p><br></p>', '<p>Product Weight: 6 Grams</p><p>Available in 4  grams Small Peetam</p>', '<p>Silver Purity : 92.5%</p><p>Silver considered to be a holy & pure metal.</p><p><span style=\"display: inline !important;\">Best thing for gifting to Family, Friends & Relatives.</span></p><p><span style=\"display: inline !important;\">It can be used to place Idols at Pooja room.<br></span><br></p><div><br></div><div><br></div>', NULL, 1, NULL, NULL, NULL, '2019-05-24 05:54:58', '2019-06-10 09:17:08', NULL),
(81, 'VC-81', '4', 'Silver 925 Lotus Mandapam Gold Plated', 'silver-925-lotus-mandapam-gold-plated', '5', '83', NULL, NULL, NULL, 'in_stock', '<p>Silver 925 Lotus Mandapam Gold Plated</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p><div><br></div>', '<p>Product Weight: 55 Grams<br></p>', '<p>Silver Purity : 92.5%</p><p><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span><br></p><p><span style=\"display: inline !important;\">Best thing for gifting to Family, Friends & Relatives.</span><br></p><div>Best Article to place Idols at Pooja Room.</div>', NULL, 0, NULL, NULL, NULL, '2019-05-24 05:59:08', '2019-09-03 10:49:32', NULL),
(82, 'VC-82', '4', 'Silver Coins', 'silver-coins', '5', '80', NULL, NULL, NULL, 'in_stock', '<p>Silver Pure Coins</p><p>Price inclusive of All Taxes, Packing and Courier Charges.</p><p><span style=\"display: inline !important;\">By R C Jewellers</span><br></p><div><br></div>', '<p>Product Weight: 1 Grams.</p><p>Feasible Weights: Any Weight can be provided on order basis.</p><p>The Print on the coins can be any Idol or flower.</p>', '<p>Silver Purity : 999%</p><p>Also available in 92% & 80%.</p><div><br></div>', 0, 0, NULL, NULL, NULL, '2019-05-24 06:09:38', '2019-10-11 22:48:14', NULL),
(83, 'VC-83', '9', 'Mini Haram', 'mini-haram', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>Nice look</p>', '<p>Mini haram with gold finish </p>', '<p>Fine Quality </p>', NULL, 1, NULL, NULL, NULL, '2019-05-24 16:47:22', '2019-07-21 08:33:14', NULL),
(84, 'VC-84', '9', 'Guttapusalu', 'guttapusalu', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Grand look for marriages and reception </p>', '<p>Gutta pusaluwith green and red stones matte finish little reddish look </p>', '<p>Fine Quality </p>', NULL, 1, NULL, NULL, NULL, '2019-05-24 17:00:22', '2019-07-21 08:31:05', NULL),
(85, 'VC-85', '9', 'Lakshmi kasu model', 'lakshmi-kasu-model', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>New design in 3 different colours </p>', '<p>Lakshmi kasu model necklaces in German silver finish</p><p>dual tone model</p>', '<p>Good quality </p>', NULL, 1, NULL, NULL, NULL, '2019-05-24 22:00:34', '2019-07-21 08:34:12', NULL),
(86, 'VC-86', '9', 'Matte Jhumki', 'matte-jhumki', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>Trendy design perfect for party look </p>', '<p>Matte finish big size jhumki </p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-05-24 23:58:08', '2019-07-21 08:27:33', NULL),
(87, 'VC-87', '9', 'Silver Jhumka', 'silver-jhumka', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>Simple and stylish </p>', '<p>Silver finish with pink stones</p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-05-26 00:18:00', '2019-08-06 10:33:14', NULL),
(88, 'VC-88', '9', 'Silver Color Jhumka', 'silver-color-jhumka', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>Elegant look </p>', '<p>Silver finish with peacock design</p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-05-26 00:35:53', '2019-08-06 10:25:34', NULL),
(89, 'VC-89', '9', 'Temple kada', 'temple-kada', '5', '89', NULL, NULL, NULL, 'in_stock', '<p>Beautiful temple design </p>', '<p>Kada with temple design gives elegant style </p>', '<p>Fine</p>', NULL, 2, NULL, NULL, NULL, '2019-05-26 00:40:50', '2019-05-29 06:55:25', NULL),
(187, 'VC-187', '9', 'Kanchi silk', 'kanchi-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Perfect for wedding and any traditional occasions </p>', '<p>Light weight Kanchi silk with contrast Blouse </p><p>smooth fabric</p><p>silk mark certified </p>', '<p>Excellent </p>', NULL, 1, 'Order Kanchi Silk in Hyderabad in Hyderabad, India, USA, UK', 'Order Kanchi Silk in Hyderabad in Hyderabad, India, USA, UK', 'Order Kanchi Silk in Hyderabad in Hyderabad, India, USA, UK', '2019-06-29 21:48:18', '2019-09-03 16:30:39', NULL),
(188, 'VC-188', '9', 'Pure Kanchipuram', 'pure-kanchipuram', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Perfect for bridal wear </p><p>Gives elegant look </p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">Pure kanchipuram silk sarees handwoven with unique collection with wide range of colour combination </span></p>', '<p>Super Quality </p>', NULL, 1, 'Buy Pure Kanchipuram in Hyderabad, India, USA and UK', 'Buy Pure Kanchipuram in Hyderabad, India, USA and UK', 'Buy Pure Kanchipuram in Hyderabad, India, USA and UK', '2019-07-02 14:13:45', '2019-09-03 16:25:27', NULL),
(91, 'VC-91', '9', 'Kanchipuram sarees', 'kanchipuram-sarees', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Perfect bridal wear</p>', '<p>Pure kanchipuram silk sarees handwoven with bridal pattern&nbsp;</p><p><br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-05-27 02:52:32', '2019-05-28 12:22:29', NULL),
(92, 'VC-92', '9', 'Banaras khatan', 'banaras-khatan', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>ExclusivePure Khatan &nbsp;from Banaras&nbsp;</p>', '<p>Pure silk katan pattu pure zari waeving handloom work saree banarasi with blouse&nbsp;</p><p><br></p>', '<p>Good&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-05-27 02:59:26', '2019-05-28 12:21:05', NULL),
(93, 'VC-93', '9', 'Kora by kora sarees', 'kora-by-kora-sarees', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Soft and elegant look with Floral design for party wear</p>', '<p>* Pure KORA by KORA Floral Digital Printed Sarees with Weaving Borders&nbsp;</p><p>* Lines Zari Pallu</p><p>* Printed Pure Running Blouse</p><p>*Transparent&nbsp;</p>', '<p>Fine&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-05-27 03:15:49', '2019-05-28 12:20:34', NULL),
(94, 'VC-94', '9', 'Paithani sarees', 'paithani-sarees', '130', NULL, NULL, NULL, NULL, 'in_stock', '<p>New. Design In paithani&nbsp;</p>', '<p>* Pure Handloom Soft Silk Paithani Sarees&nbsp;</p><p>* Contrast &amp; Rich Paithani Pallu</p><p>* Plain Running Blouse with Border</p><p>* Smooth &amp; Soft Fabric&nbsp;</p><p>* With Silkmark Certified</p><p><br></p>', '<p>Good&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-05-27 03:23:26', '2019-05-28 12:20:03', NULL),
(95, 'VC-95', '9', 'Test Cotton Saree by Srini', 'test-cotton-saree-by-srini', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Test Cotton Saree by Srini - To Be Deleted<br></p>', '<p><span style=\"display: inline !important;\">Test Cotton Saree by Srini - To Be Deleted</span><br></p>', '<p><span style=\"display: inline !important;\">Test Cotton Saree by Srini - To Be Deleted</span><br></p>', 0, 2, NULL, NULL, NULL, '2019-05-27 05:39:07', '2019-06-11 02:13:12', NULL),
(153, 'VC-153', '9', 'Paithani silk', 'paithani-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Paithani border gives rich and elegant look&nbsp;</p>', '<p>* Pure Handloom Soft Silk Paithani Sarees</p><p><br></p><p>* Contrast &amp; Rich Paithani Pallu</p><p><br></p><p>* Blouse as in Pic</p><p><br></p><p>* Smooth &amp; Soft Fabric</p><p><br></p><p>* With Silkmark Certified</p><p><br></p>', '<p>Excellent quality&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-10 17:49:05', '2019-06-11 02:11:22', NULL),
(125, 'VC-125', '9', 'Demo Product - Test Cake 1', 'demo-product-test-cake-1', '1', '61', '109', NULL, 'Egg Less', 'in_stock', '<p>Demo Product - Test Cake 1<br></p>', '<p>Demo Product - Test Cake 1<br></p>', '<p>Demo Product - Test Cake 1<br></p>', NULL, 2, NULL, NULL, NULL, '2019-06-03 06:40:14', '2019-07-27 08:29:21', NULL),
(126, 'VC-126', '11', 'Father\'s Day Spl Cake - T', 'fathers-day-spl-cake-t', '1', '62', '110', NULL, 'Egg', 'in_stock', '<p>A Father\'s Day special cake for your dear father.&nbsp;<span style=\"font-size: 1rem; display: inline !important;\">We will make in the flavour of your choice specified in the custom message field.</span></p>', '<p>Chocolate flavour. </p>', '<p>Good</p>', NULL, 2, NULL, NULL, NULL, '2019-06-05 12:51:45', '2019-07-27 08:33:39', NULL),
(201, 'VC-201', '9', 'Banaras khaddi Georgette', 'banaras-khaddi-georgette', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Party wear sarees for trendy women&nbsp;</p>', '<p>s&nbsp;<span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">* Pure Handloom Banarasi Kaddi Georgette Sarees with PAITHANI Weaving Border &amp; Pallu</span></p><p><br></p><p>* Contrast &amp; Rich Pallu&nbsp;</p><p><br></p><p>* Blouse as in Pic</p><p><br></p><p>* Smooth &amp; Soft Fabric</p><p><br></p><p>* With Silkmark Certified</p><p><br></p><p><br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 17:24:00', '2019-07-02 22:17:52', NULL),
(145, 'VC-145', '9', 'Mangalsutra haram', 'mangalsutra-haram', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Simple and traditional purpose </p>', '<p>Black beads mangalsutra haram with ball shaped ruby pendant in middle with CZ pendant on either si</p>', '<p>Fine </p>', NULL, 1, NULL, NULL, NULL, '2019-06-08 06:53:52', '2019-07-21 08:18:25', NULL),
(199, 'VC-199', '9', 'Kanchipuram Bridal', 'kanchipuram-bridal', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Elite and very beautiful wear for Indian occasions like weddings saree functions</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">Pure kanchipuram silk sarees handwoven with classic korvai brocade pattern&nbsp;</span></p><p>1 g pure jari</p><p><br></p><p><br></p>', '<p>Excellent and superb&nbsp;</p>', NULL, 2, NULL, NULL, NULL, '2019-07-02 17:18:18', '2019-07-09 01:31:08', NULL),
(200, 'VC-200', '9', 'Kanchipuram Bridal Korvai', 'kanchipuram-bridal-korvai', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Elite and very beautiful wear for Indian occasions like weddings saree functions</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">Pure kanchipuram silk sarees handwoven with classic korvai brocade pattern </span></p><p>1 g pure jari</p><p><br></p><p><br></p>', '<p>Excellent and superb </p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 17:19:40', '2019-07-09 01:28:13', NULL),
(194, 'VC-194', '9', 'Gold finish Chain', 'gold-finish-chain', '5', '89', '93', NULL, NULL, 'in_stock', '<p>Simple wear for all ages</p>', '<p>3layer gold finish long chain with ball shape beads</p>', '<p>Good&nbsp;</p>', NULL, 2, NULL, NULL, NULL, '2019-07-02 15:11:46', '2019-07-09 01:31:26', NULL),
(195, 'VC-195', '9', 'Gold finish', 'gold-finish', '5', '89', '93', NULL, NULL, 'in_stock', '<p>Simple wear for all ages</p>', '<p>3layer gold finish long chain with ball shape beads</p>', '<p>Good&nbsp;</p>', NULL, 2, NULL, NULL, NULL, '2019-07-02 15:12:37', '2019-07-09 01:31:16', NULL),
(196, 'VC-196', '9', '3 Gold  finish', '3-gold-finish', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>Simple wear for all ages</p>', '<p>3layer gold finish long chain with ball shape beads</p>', '<p>Good </p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 15:14:19', '2019-07-21 08:16:46', NULL),
(197, 'VC-197', '9', 'Ikkat designer anarkalis', 'ikkat-designer-anarkalis', '130', '132', '133', NULL, NULL, 'in_stock', '<p>Simple but elegant look for weekend parties&nbsp;</p><p>ikkat is trending now grab urs 😍😍</p>', '<p>Ikkat designer anarkalis&nbsp;</p><p>with embroidery in yoke</p><p>size 42/44(L/XL) acc to Indian measurements</p><p>length 56</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 17:02:15', '2019-07-09 01:29:52', NULL),
(198, 'VC-198', '9', 'Ikkat anarkalis', 'ikkat-anarkalis', '130', '132', '133', NULL, NULL, 'in_stock', '<p>Perfect party wear 🥳</p>', '<p>W<span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">*handloom anarkalis*</span></p><p><br></p><p>ikkat body attached with cotton kanchi borders paired with pure chiffon &nbsp;dupatta with attached border....</p><p><br></p><p>size 42/44(L/XL) according to Indian measurements chart</p><p>lenth 56</p><p><br></p><p><br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 17:06:44', '2019-07-09 01:29:12', NULL),
(191, 'VC-191', '9', 'Handloom Kanchi cotton', 'handloom-kanchi-cotton', '130', '132', '133', NULL, NULL, 'in_stock', '<p>Trendy wear for all ages&nbsp;</p><p>party and also traditional wear</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">&nbsp;handloom anarkalis with contrast kanchi borders.....</span></p><p>Fabric : Kanchi cotton&nbsp;</p><p>size 40/42</p><p>lenth 55</p><p>Can be customised &nbsp; as per measurements on pre order basis booking of 15 days time&nbsp;</p><p><br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 14:31:52', '2019-07-09 01:33:54', NULL),
(192, 'VC-192', '9', 'Gold forming necklace', 'gold-forming-necklace', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>Rich and elegant look for wedding occasions </p><p>looks beautiful for bridal wear</p>', '<p>1gm gold finishing</p><p>Forming pattern </p>', '<p>Fine Quality </p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 14:58:05', '2019-08-08 10:24:29', NULL),
(193, 'VC-193', '9', 'Diamond finish pattern', 'diamond-finish-pattern', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Elite look with diamond finish and perfect for wedding occasions </p>', '<p>Diamond finish with emerald stone excellent design </p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 15:03:26', '2019-07-21 08:14:56', NULL),
(107, 'VC-107', '9', 'Kanchipuram', 'kanchipuram', '130', '131', NULL, NULL, NULL, 'in_stock', 'Classic collection', 'Pure kanchipuram silk sarees handwoven', 'Good quality', NULL, 1, NULL, NULL, NULL, '2019-05-30 07:10:22', '2019-05-30 19:20:09', NULL),
(108, 'VC-108', '9', 'Bridal wear', 'bridal-wear', '130', '131', NULL, NULL, NULL, 'in_stock', 'Perfect red for bridal wear\r\nElite look', 'Pure kanchipuram silk sarees handwoven with unique pattern', 'Very nice', NULL, 1, NULL, NULL, NULL, '2019-05-30 07:12:59', '2019-05-30 19:19:48', NULL),
(109, 'VC-109', '9', 'Kota sarees', 'kota-sarees', '130', '131', NULL, NULL, NULL, 'in_stock', 'Light weight and beautiful  look', 'Kota designer sarees', 'Excellent', NULL, 1, NULL, NULL, NULL, '2019-05-30 07:15:52', '2019-05-30 19:19:05', NULL),
(110, 'VC-110', '9', 'Hangings 11', 'hangings-11', '142', '91', NULL, NULL, NULL, 'in_stock', 'Trendy design', 'Big size earrings for perfect party wear', 'Fine', NULL, 1, NULL, NULL, NULL, '2019-05-30 07:19:57', '2019-07-21 08:23:50', NULL),
(111, 'VC-111', '9', 'Georgette fancy', 'georgette-fancy', '130', '131', NULL, NULL, NULL, 'in_stock', 'Fancy look', 'Georgette sarees with semi stitched blouse', 'Fine', NULL, 1, NULL, NULL, NULL, '2019-05-30 07:23:54', '2019-05-30 19:17:41', NULL),
(112, 'VC-112', '9', 'Ikkat dupattas', 'ikkat-dupattas', '130', '132', '137', NULL, NULL, 'in_stock', 'Grand and traditional look', 'Ikkat dupattas 2.5 m\r\nDifferent designs available', 'Excellent', NULL, 1, NULL, NULL, NULL, '2019-05-31 02:32:04', '2019-06-02 08:10:42', NULL),
(113, 'VC-113', '9', 'Kancheepuram', 'kancheepuram', '130', NULL, NULL, NULL, NULL, 'in_stock', 'Traditional Kanchipuram', 'TRADITIONAL FASCINATION 😍\r\n\r\nPURE KANCHEEPURAM HANDLOOM SOFT SILK PATTU SAREES WITH RICH PALLU- UNIQUE BORDERS N CONTRAST BLOUSE \r\nSILK MARK CERTIFIED', 'Excellent', NULL, 1, NULL, NULL, NULL, '2019-05-31 02:43:59', '2019-06-02 07:50:53', NULL),
(114, 'VC-114', '9', 'Gadwal pattu', 'gadwal-pattu', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Traditional Gadwal sarees with Kanchi border gives Elite look&nbsp;</p>', '<p>Gadwal pure sarees with kanchi borders &nbsp;with contrast pallu and blouse&nbsp;<br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-01 16:13:31', '2019-06-02 07:43:26', NULL),
(115, 'VC-115', '9', 'Banaras khatan', 'banaras-khatan', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Wonderful sarees from Banaras😍😍</p>', '<p><br></p><p>PURE BANARASI NATURAL TUSSER SILK KATHAN WITH TRADITIONAL BORDER - RICH PALLU N CONTRAST BLOUSE&nbsp;</p><p>SILK MARK CERTIFIED 🤩❤</p>', '<p>Excellent quality 👌</p>', NULL, 1, NULL, NULL, NULL, '2019-06-01 16:29:14', '2019-06-02 07:50:23', NULL),
(116, 'VC-116', '9', 'Crystal hangings 1', 'crystal-hangings-1', '142', '102', NULL, NULL, NULL, 'in_stock', '<p>Earrings perfect for party wear </p>', '<p>Crystal hangings with different colours of beads made of German silver</p>', '<p>Fine </p>', NULL, 2, NULL, NULL, NULL, '2019-06-02 02:13:17', '2019-08-06 09:01:12', NULL),
(117, 'VC-117', '9', 'Lakshmi Kasu Haram', 'lakshmi-kasu-haram', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Traditional wear perfect  for weddings and bridal wear</p>', '<p>Long Lakshmi kasu haram with ruby stones embedded with beautiful pendant </p><p>micro gold finish </p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-06-02 02:28:18', '2019-07-21 08:20:37', NULL),
(118, 'VC-118', '9', 'Crystal hangings', 'crystal-hangings', '5', '89', '91', NULL, NULL, 'in_stock', '<p>Trendy and party look😍😍💃</p>', '<p>Crystal Hangings with German silver finish</p>', '<p>Super</p>', NULL, 2, NULL, NULL, NULL, '2019-06-02 02:33:05', '2019-07-21 08:24:25', NULL),
(119, 'VC-119', '9', 'Kanchipuram sarees', 'kanchipuram-sarees', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Kanchipuram sarees apt for all occasions&nbsp;</p>', '<p>Pure kanchipuram silk sarees handwoven with classic pattern&nbsp;</p><p><br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-02 02:38:21', '2019-06-02 07:46:22', NULL),
(120, 'VC-120', '9', 'Silver Tissue Linen', 'silver-tissue-linen', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Tissue is trending now</p>', '<p>Silver tissue linen saree with contrast pallu and stripes blouse...</p><p><br></p>', '<p>Food</p>', NULL, 1, NULL, NULL, NULL, '2019-06-02 02:41:18', '2019-06-02 07:45:02', NULL);
INSERT INTO `products` (`p_id`, `p_unique_id`, `p_vendor_id`, `p_name`, `p_alias`, `p_cat_id`, `p_sub_cat_id`, `p_item_spl`, `p_main_image`, `p_dishtype`, `p_availability`, `p_overview`, `p_specifications`, `p_quality_care_info`, `p_delivery_charges`, `p_status`, `p_meta_title`, `p_meta_keywords`, `p_meta_description`, `created_at`, `updated_at`, `p_related`) VALUES
(121, 'VC-121', '9', 'Ikkath Long gown', 'ikkath-long-gown', '130', '132', '134', NULL, NULL, 'in_stock', '<p>Perfect for weekend parties 💃</p>', '<p>Ikkat cotton frocks vth gold border available in different colors&nbsp;</p><p>Can be customised according to the measurements given only on preorder of 15 to 20 days time</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-02 03:54:46', '2019-06-02 07:44:14', NULL),
(122, 'VC-122', '9', 'Kanchi Organza Long Gown', 'kanchi-organza-long-gown', '130', '132', '134', NULL, NULL, 'in_stock', '<p>Gives Gorgeous &nbsp;look for wedding occasions&nbsp;</p><p>perfect for Sangeet</p>', '<p>Kanchi organza long gowns&nbsp;</p><p>without dupatta&nbsp;</p><p>embroidery on Yoke</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-02 04:03:20', '2019-06-02 07:43:46', NULL),
(127, 'VC-127', '11', 'Father\'s Day Spl Cake -Sh', 'fathers-day-spl-cake-sh', '1', '62', '110', NULL, 'Egg', 'in_stock', '<p>A Father\'s Day special cake for your dear father.&nbsp;</p><p>We will make in the flavour of your choice specified in the custom message field.</p>', '<p><span style=\"display: inline !important;\">Moist, fudgy chocolate cake .</span><br></p>', '<p>Good</p>', NULL, 2, NULL, NULL, NULL, '2019-06-05 12:53:06', '2019-07-27 08:33:27', NULL),
(128, 'VC-128', '11', 'Father\'s Day Spl Cake - Bl', 'fathers-day-spl-cake-bl', '1', '62', '109', NULL, 'Egg', 'in_stock', '<p>A Father\'s Day special cake for your dear father. <span style=\"font-size: 1rem; display: inline !important;\">We will make in the flavour of your choice specified in the custom message field.</span></p>', '<p><br></p>', '<p>Good</p>', NULL, 2, NULL, NULL, NULL, '2019-06-05 12:54:57', '2019-07-27 08:33:18', NULL),
(129, 'VC-129', '11', 'Father\'s Day Spl Cake - Specs', 'fathers-day-spl-cake-specs', '1', '62', '109', NULL, 'Egg', 'in_stock', '<p>A Father\'s Day special cake for your dear father. </p><p>We will make in the flavour of your choice specified in the custom message field.</p>', '<p><br></p>', '<p>Good</p>', NULL, 2, NULL, NULL, NULL, '2019-06-05 12:56:07', '2019-07-27 08:32:36', NULL),
(130, 'VC-130', '11', 'Father\'s Day Spl Cake Msh', 'fathers-day-spl-cake-msh', '1', '62', '108', NULL, 'Egg', 'in_stock', '<p><span style=\"display: inline !important;\">A Father\'s Day&nbsp;</span><span style=\"font-size: 1rem; display: inline !important;\">special</span><span style=\"font-size: 1rem; display: inline !important;\">&nbsp;</span><span style=\"font-size: 1rem; display: inline !important;\">cake for your dear father. We will make in the flavour of your choice specified in the custom message field</span><br></p>', '<p>N/A</p>', '<p>Good</p>', NULL, 2, NULL, NULL, NULL, '2019-06-05 12:58:19', '2019-07-27 08:33:00', NULL),
(131, 'VC-131', '11', 'Father\'s Day Spl Cake - Bow tie', 'fathers-day-spl-cake-bow-tie', '1', '62', '109', NULL, 'Egg', 'in_stock', '<p>A Father\'s Day <span style=\"font-size: 1rem; display: inline !important;\">special</span><span style=\"font-size: 1rem; display: inline !important;\"> </span><span style=\"font-size: 1rem; display: inline !important;\">cake for your dear father. We will make in the flavour of your choice specified in the custom message field. </span></p>', '<p>Father\'s Day Spl Cake - Bow tie. Available in 1 kg and 2kgs, any flavor of your choice.</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-06-05 12:59:16', '2020-06-17 14:21:29', NULL),
(132, 'VC-132', '7', 'Chocolates 3 Pieces', 'chocolates-3-pieces', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">DummyProduct Overview</span><br></p>', '<p><span style=\"display: inline !important;\">DummyProduct Overview</span><br></p>', '<p><span style=\"display: inline !important;\">DummyProduct Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:06:38', '2019-06-07 18:06:38', NULL),
(133, 'VC-133', '7', 'Chocolates 3 Pieces', 'chocolates-3-pieces', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">DummyProduct Overview</span><br></p>', '<p><span style=\"display: inline !important;\">DummyProduct Overview</span><br></p>', '<p><span style=\"display: inline !important;\">DummyProduct Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:06:39', '2019-06-07 18:06:39', NULL),
(134, 'VC-134', '7', '5 Pieces  chocolate', '5-pieces-chocolate', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:07:49', '2019-06-07 18:07:49', NULL),
(135, 'VC-135', '7', '7 Pieces chocolates', '7-pieces-chocolates', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:08:21', '2019-06-07 18:08:21', NULL),
(136, 'VC-136', '7', 'Love letter Chocolates', 'love-letter-chocolates', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:08:53', '2019-06-07 18:08:53', NULL),
(137, 'VC-137', '7', 'Heart Shape Chocolate', 'heart-shape-chocolate', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:09:18', '2019-06-07 18:09:18', NULL),
(138, 'VC-138', '7', 'Chocolates for Custom Arrangements', 'chocolates-for-custom-arrangements', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Chocolates for Custom Arrangement\'s</span><br></p>', '<p><span style=\"display: inline !important;\">Home Made Chocolate. Can use for any Custom arrangements.</span><br></p>', '<p><span style=\"display: inline !important;\">Good.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-07 18:09:47', '2019-06-08 20:19:11', NULL),
(139, 'VC-139', '7', 'Dry Fruits Package', 'dry-fruits-package', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:10:21', '2019-06-07 18:10:21', NULL),
(140, 'VC-140', '7', 'Chocolate Package', 'chocolate-package', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:10:50', '2019-06-07 18:10:50', NULL),
(141, 'VC-141', '7', 'Dry Fruits Flower', 'dry-fruits-flower', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dry Fruits Flower</span><br></p>', '<p><span style=\"display: inline !important;\">Dry Fruits Flower</span><br></p>', '<p><span style=\"display: inline !important;\">Good.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-07 18:11:28', '2019-06-08 20:24:36', NULL),
(142, 'VC-142', '7', 'Black Chocklate Packat', 'black-chocklate-packat', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:11:58', '2019-06-07 18:11:58', NULL),
(143, 'VC-143', '7', 'Gold Rosette Chocolate Bouquet', 'gold-rosette-chocolate-bouquet', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">24 Rose Shaped Chocolates</span><br></p>', '<p>Chocolate</p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-06-07 18:12:31', '2019-06-08 18:35:56', NULL),
(144, 'VC-144', '7', 'Flower with Heart Chocolate', 'flower-with-heart-chocolate', '4', '116', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', '<p><span style=\"display: inline !important;\">Dummy Product Overview</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-07 18:13:12', '2019-06-07 18:13:12', NULL),
(146, 'VC-146', '9', 'Kota sarees', 'kota-sarees', '130', NULL, NULL, NULL, NULL, 'in_stock', '<p>Beautiful Rich Kota with awesome blouse &nbsp;for party wear</p>', '<p>Beautiful Kota sarees with beautiful matching boat neck blouse withembroidery done</p><p>free size blouse can be altered to required size</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-09 04:05:16', '2019-06-29 07:59:52', NULL),
(147, 'VC-147', '9', 'Kora silk', 'kora-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Kanchi border gives rich look</p>', '<p>New Collection&nbsp;</p><p><br></p><p>Banaras handloom kora silk Kanchi border tanchui Sarees.with brocade blouse</p><p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">Limited stock</span></p>', '<p>Good&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-09 04:21:00', '2019-06-10 07:24:23', NULL),
(148, 'VC-148', '9', 'Digital organza', 'digital-organza', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Very nice looking digital organza&nbsp;</p>', '<p>Digital pure organza sarees with silver weaving kanchi border.....blouse self printed with borde</p><p>❗ready for dispatch</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-06-09 04:24:59', '2019-06-10 07:08:09', NULL),
(149, 'VC-149', '9', 'Kasu necklace', 'kasu-necklace', '5', '89', '94', NULL, NULL, 'in_stock', NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, '2019-06-10 06:00:11', '2019-06-18 10:43:17', NULL),
(150, 'VC-150', '11', 'Blue Orchids', 'blue-orchids', '2', '84', '113', NULL, NULL, 'in_stock', '<p>10 Blue Orchids. <br></p>', '<p>Fresh.</p>', '<p>Good</p>', NULL, 1, 'Blue Orchids: Buy Blue Orchids Online in Hyderabad', 'Blue Orchids, Buy Blue Orchids Online in Hyderabad', 'Blue Orchids: Buy Blue Orchids Online in Hyderabad', '2019-06-10 08:51:24', '2019-09-03 16:38:49', NULL),
(151, 'VC-151', '11', 'Colour Roses Bouquets', 'colour-roses-bouquets', '2', '87', '113', NULL, NULL, 'in_stock', '<p>10 Colour Roses.<br></p>', '<p>Fresh Flowers.<br></p>', '<p>Good.</p>', NULL, 1, 'Colour Roses Bouquets: Buy Rose Bouquets Online in Hyderabad', 'Colour Roses Bouquets, Buy Rose Bouquets Online in Hyderabad', 'Colour Roses Bouquets: Buy Rose Bouquets Online in Hyderabad', '2019-06-10 08:56:49', '2019-09-03 16:39:23', NULL),
(154, 'VC-154', '9', 'Silver earring', 'silver-earring', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>Simple for college and party wear too</p>', '<p>Silver finish look with 5 different colours stones matches all types of outfits </p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-06-10 18:16:00', '2019-08-06 10:26:29', NULL),
(155, 'VC-155', '9', 'Silver earrings', 'silver-earrings', '5', '89', '91', NULL, NULL, 'in_stock', '<p>Simple for college and party wear too</p>', '<p>Silver finish look with 5 different colours stones matches all types of outfits&nbsp;</p>', '<p>Fine</p>', NULL, 2, NULL, NULL, NULL, '2019-06-10 18:55:44', '2019-06-18 10:43:02', NULL),
(190, 'VC-190', '9', 'Kanjivaram silk', 'kanjivaram-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Checks pattern gives a different look and trendy wear for all occasions&nbsp;</p>', '<p>Pure kanjivaram silk saree contrast pallu and blouse... with silk marked&nbsp;<br></p>', '<p>Super</p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 14:24:48', '2019-07-09 01:34:38', NULL),
(189, 'VC-189', '9', 'Linen silk', 'linen-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Available in 2 different lovely colours&nbsp;</p><p>perfect for linen lovers&nbsp;</p>', '<p>S<span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">🛑</span></p><p>🔷Pure Handloom silk linen with Kanchi weaving border&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-02 14:17:45', '2019-07-09 01:35:29', NULL),
(169, 'VC-169', '11', 'Orange Carnations', 'orange-carnations', '2', '85', NULL, NULL, NULL, 'in_stock', '<p>Orange Carnations</p>', '<p>Quantity : 10</p>', '<p><span style=\"display: inline !important;\">Orange Carnations</span><br></p>', NULL, 1, 'Orange Carnations: Buy Carnations Online in Hyderabad', 'Orange Carnations, Buy Carnations Online in Hyderabad', 'Orange Carnations: Buy Carnations Online in Hyderabad', '2019-06-22 23:31:30', '2019-09-03 16:39:54', NULL),
(170, 'VC-170', '11', 'Pink and White Carnations', 'pink-and-white-carnations', '2', '85', NULL, NULL, NULL, 'in_stock', '<p>Pink and White Carnations<br></p>', '<p>Quantify :10</p>', '<p>Pink and White Carnations<br></p>', NULL, 1, 'Buy Pink and White Carnations Online in Hyderabad', 'Buy Pink and White Carnations Online in Hyderabad', 'Buy Pink and White Carnations Online in Hyderabad', '2019-06-22 23:32:36', '2019-09-03 16:40:21', NULL),
(171, 'VC-171', '11', 'Pink Carnations', 'pink-carnations', '2', '86', NULL, NULL, NULL, 'in_stock', '<p>Pink Carnations</p>', '<p>Quantity:10</p>', '<p><span style=\"display: inline !important;\">Pink Carnations</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:33:54', '2019-06-28 02:55:08', NULL),
(172, 'VC-172', '11', 'Purple Orchids', 'purple-orchids', '2', '84', NULL, NULL, NULL, 'in_stock', '<p>Purple Orchids<br></p>', '<p>Quantity:10</p>', '<p>Purple Orchids<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:35:12', '2019-06-28 02:54:47', NULL),
(173, 'VC-173', '11', 'Red and White Carnations', 'red-and-white-carnations', '2', '87', NULL, NULL, NULL, 'in_stock', '<p>Red and White Carnations</p>', '<p>Quantity:10</p>', '<p><span style=\"display: inline !important;\">Red and White Carnations</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:37:42', '2019-06-28 02:53:59', NULL),
(174, 'VC-174', '11', 'Red Rose Flowers', 'red-rose-flowers', '2', '84', NULL, NULL, NULL, 'in_stock', '<p>Red Rose Flowers</p>', '<p>Quantity:10</p>', '<p><span style=\"display: inline !important;\">Red Rose Flowers</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:38:41', '2019-06-28 02:53:42', NULL),
(175, 'VC-175', '11', 'White Carnations', 'white-carnations', '2', '85', NULL, NULL, NULL, 'in_stock', '<p>White Carnations<br></p>', '<p>Quantify:10</p>', '<p>White Carnations<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:40:10', '2019-06-28 02:53:19', NULL),
(176, 'VC-176', '11', 'White Orchids', 'white-orchids', '2', '85', NULL, NULL, NULL, 'in_stock', '<p>White Orchids</p>', '<p>Quantity: 10</p>', '<p><span style=\"display: inline !important;\">White Orchids</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:41:29', '2019-06-28 02:53:00', NULL),
(177, 'VC-177', '11', 'White Roses', 'white-roses', '2', '84', NULL, NULL, NULL, 'in_stock', '<p>White Roses<br></p>', '<p><br>Quantity:10</p>', '<p>White Roses<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:42:23', '2019-06-28 02:52:29', NULL),
(178, 'VC-178', '11', 'Yelllow Roses', 'yelllow-roses', '2', '86', NULL, NULL, NULL, 'in_stock', '<p>Yelllow Roses<br></p>', '<p>Qty:10</p>', '<p>Yelllow Roses<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:43:29', '2019-06-28 02:52:10', NULL),
(179, 'VC-179', '11', 'Orange Lilly', 'orange-lilly', '2', '84', NULL, NULL, NULL, 'in_stock', '<p>Orange Lilly<br></p>', '<p>Qty:12</p>', '<p>Orange Lilly<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:44:32', '2019-06-28 02:51:51', NULL),
(180, 'VC-180', '11', 'Pink Lilly', 'pink-lilly', '2', '85', NULL, NULL, NULL, 'in_stock', '<p>Pink Lilly<br></p>', '<p>Qty:12</p>', '<p>Pink Lilly<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:45:49', '2019-06-28 02:51:29', NULL),
(181, 'VC-181', '11', 'White Lilly', 'white-lilly', '2', '85', NULL, NULL, NULL, 'in_stock', '<p>White Lilly<br></p>', '<p>Qty:12</p>', '<p>White Lilly<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:46:44', '2019-06-28 02:50:56', NULL),
(182, 'VC-182', '11', 'Yellow Lilly', 'yellow-lilly', '2', '86', NULL, NULL, NULL, 'in_stock', '<p>Yellow Lilly<br></p>', '<p>Qty:12</p>', '<p>Yellow Lilly<br></p>', NULL, 1, NULL, NULL, NULL, '2019-06-22 23:47:32', '2019-08-02 12:12:47', '179'),
(183, 'VC-183', '11', 'Demo Cake Jul 21st', 'demo-cake-jul-21st', '1', '61', '108', NULL, 'Egg Less', 'in_stock', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-family: roboto; color: rgb(78, 78, 78); font-size: 13px;\">Irish Coffee, Vanilla sponge cake loaded with mocha flavour topped with rich roasted coffee beans - A perfect combination for a latte coffee addict. Note the images, shade, size, design of the cake are only indicative in nature.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-family: roboto; color: rgb(78, 78, 78); font-size: 13px;\"><br></p>', '<p><span style=\"font-family: Poppins;\">﻿</span><span style=\"font-family: Verdana;\">﻿</span><span style=\"font-family: Poppins;\">﻿</span><span style=\"font-family: Poppins;\">﻿</span><span style=\"font-family: Poppins;\">﻿Shape: Rounded</span></p><p><span style=\"font-family: Poppins;\">Weight: 1 kg</span></p><p><span style=\"font-family: Poppins;\"><br></span><br></p>', '<p><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">The cakes are handcrafted by our skilled, world class chefs and since each chef has his/her own way of baking and designing a cake, there might be slight variation in the product in terms of design and shape.</span><br></p>', NULL, 0, NULL, NULL, NULL, '2019-06-23 09:58:57', '2019-08-02 11:40:34', NULL),
(186, 'VC-186', '9', 'Test Item 1 sd sdds', 'test-item-1-sd-sdds', '5', '66', '92', NULL, NULL, 'in_stock', '<p>asdasdas</p>', '<p>asdas</p>', '<p>asdasd</p>', NULL, 0, NULL, NULL, NULL, '2019-06-28 11:11:06', '2019-06-28 11:11:06', NULL),
(167, 'VC-167', '9', 'Demo Prodcut-123', 'demo-prodcut-123', '1', '61', '110', NULL, 'Egg Less', 'in_stock', '<p><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">A parade of energy and passion emits form this feisty collection of assorted 20 colored carnations that will fill your loved ones day with vibrant luxury</span><br></p>', '<p>Taste: Very Taste</p><p>Weight: 1 kg</p>', '<p><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">The cakes are handcrafted by our skilled, world class chefs and since each chef has his/her own way of baking and designing a cake, there might be slight variation in the product in terms of design and shape.</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">The chosen delivery time is an estimate and depends on the availability & requested location for delivery.</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">Since cakes are perishable in nature, we attempt delivery of your order only once. The delivery cannot be redirected to any other address. Cakes are perishable products. Recommend to consume it within hours of buying. Storing is not recommended.Upon receiving the cake, immediately refrigerate it.</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">Please allow it to come to room temperature before serving/eating.</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px; display: inline !important;\"><span style=\"font-family: Poppins;\">The cake should be placed back in the fridge and should be consumed within 48 hours. This product is hand delivered and will not be delivered along with courier products.</span><span style=\"font-family: Poppins;\"> </span></span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">The images are only for representation purposes only. The shade, colour, design may vary. Occasionally, substitutions of flavours/designs is necessary due to temporary and/or regional unavailability issues. Send Delicious Cake To Your Loved Ones & Friends & Family!</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"font-weight: 700; color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px;\">Gift Type, Cookies and Biscuits etc</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">Your gift may be delivered a day prior or a day after the chosen date of delivery, since this product is shipped using the services of our courier partners, the date of delivery is an estimate.</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">We recommend that you provide an address at which someone will be present to receive the package and the contact number is reachable. Please note that the delivery cannot be redirected to any other address.</span><br style=\"color: rgb(78, 78, 78); font-family: Roboto, sans-serif; font-size: 13px;\"><span style=\"color: rgb(78, 78, 78); font-family: Poppins; font-size: 13px; display: inline !important;\">All courier orders are carefully packed and shipped from our warehouse. Soon after the order has been dispatched, you will receive a tracking number that will help you trace your gift. No deliveries are made on Sundays and National Holidays.</span><br></p>', NULL, 2, NULL, NULL, NULL, '2019-06-20 10:33:10', '2019-08-02 11:41:59', NULL),
(202, 'VC-202', '38', 'Foxtail Millet', 'foxtail-millet', '6', '103', NULL, NULL, NULL, 'in_stock', '<p>Foxtail Millets</p>', '<p>Weight: 1kg</p>', '<p><span style=\"display: inline !important;\">Foxtail Millets</span><br></p>', NULL, 1, 'Foxtail Millet| Foxtail Millets in Hyderabad Online', 'Foxtail Millet, Foxtail Millets in Hyderabad Online', 'Foxtail Millet, Foxtail Millets in Hyderabad Online, 5kg Foxtail Millets free shipping, 10kg Foxtail Millets order hyderabad.', '2019-07-11 16:50:53', '2019-09-03 16:07:25', NULL),
(203, 'VC-203', '38', 'Kodo Millet', 'kodo-millet', '6', '104', NULL, NULL, NULL, 'in_stock', '<p>Kodo Millets</p>', '<p>Kodo Millet</p>', '<p>Kodo Millet</p>', NULL, 1, 'kodo Millet| Kodo Millets Online in Hyderabad', 'kodo Millet, Kodo Millets Online in Hyderabad', 'kodo Millet, Kodo Millets Online in Hyderabad, buy 5kg kodo Millet with free shipping, buy 10kg kodo Millet with free shipping in Hyderabad.', '2019-07-11 16:52:37', '2019-09-03 16:08:15', NULL),
(204, 'VC-204', '38', 'Barnyard Millet', 'barnyard-millet', '6', '105', NULL, NULL, NULL, 'in_stock', '<p>Barnyard Millet<br></p>', '<p>Barnyard Millet<br></p>', '<p>Barnyard Millet<br></p>', NULL, 1, 'Buy Barnyard Millet| Buy Barnyard Millets Hyderabad', 'Buy Barnyard Millet, Buy Barnyard Millets Hyderabad', 'Buy Barnyard Millet, Buy Barnyard Millets Online in Hyderabad, order 5kg 10kg barnyard millets in hyderabad, free shipping 5kg barnyard millets Hyderabad.', '2019-07-11 16:53:16', '2019-09-06 18:11:15', NULL),
(205, 'VC-205', '38', 'Little Millets', 'little-millets', '6', '106', NULL, NULL, NULL, 'in_stock', '<p>Little Millets<br></p>', '<p>Little Millets</p>', '<p>Little Millets</p>', NULL, 1, 'Little Millet| Buy Little Millets Online Hyderabad', 'Little Millet, Buy Little Millets Online Hyderabad', 'Little Millet, Buy Little Millets Online Hyderabad, buy 5kg Little Millet in Hyderabad, 10kg Little Millet in hyderabad, buy Little Millet from velchala.', '2019-07-11 16:53:57', '2019-09-03 16:09:09', NULL),
(206, 'VC-206', '38', 'Brown Millets', 'brown-millets', '6', '103', NULL, NULL, NULL, 'in_stock', '<p>Brown Millets</p>', '<p>Brown Millets</p>', '<p>Brown Millets</p>', NULL, 1, 'Brown Millet| Buy Brown Millets Hyderabad Online', 'Brown Millet, Buy Brown Millets Hyderabad Online', 'Brown Millet, Buy Brown Millets Hyderabad Online, 5kg Brown Millets hyderabad, order 10kg Brown Millets in hyderabad free shipping, buy 10kg brown millets hyderabd.', '2019-07-11 16:54:54', '2019-09-03 16:06:43', '205'),
(207, 'VC-207', '9', 'Kanchi Kora by kora', 'kanchi-kora-by-kora', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Perfect fancy wear for elite parties and weddings&nbsp;</p>', '<p>* Pure Handloom Kanchi Premium Kora by Kora Sarees With Allover Checks &amp; Weaving Borders</p><p>* Contrast Pallu as in Pic</p><p>* Contrast Checks Blouse as in Pic</p><p>* With Silkmark Certified</p><p><br></p>', '<p>Premium quality&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 14:47:21', '2019-07-18 02:22:49', NULL),
(208, 'VC-208', '9', 'Kanchi kora by kora 2', 'kanchi-kora-by-kora-2', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Elite look perfect for parties and weddings&nbsp;</p>', '<p><span style=\"caret-color: rgb(68, 68, 68); color: rgb(68, 68, 68); -webkit-text-size-adjust: 100%; display: inline !important;\">Pure Handloom Kanchi Premium Kora by Kora Sarees With Allover Checks &amp; Weaving Borders</span></p><p style=\"caret-color: rgb(68, 68, 68); color: rgb(68, 68, 68); -webkit-text-size-adjust: 100%;\">* Contrast Pallu as in Pic</p><p style=\"caret-color: rgb(68, 68, 68); color: rgb(68, 68, 68); -webkit-text-size-adjust: 100%;\">* Contrast Checks Blouse as in Pic</p><p style=\"caret-color: rgb(68, 68, 68); color: rgb(68, 68, 68); -webkit-text-size-adjust: 100%;\">* With Silkmark Certified</p><div><br></div>', '<p>Premium quality&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 14:52:32', '2019-07-18 02:24:27', NULL),
(209, 'VC-209', '9', 'Lenin by lenin', 'lenin-by-lenin', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Awesome look for simple occasions</p><p>embroided designer blouse gives great look</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">pure linen by linen silk sarees with embroidery paired with *designer blouse*.....</span></p><p>Blouse size 42/44</p><p><br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 14:57:10', '2019-07-18 02:21:59', NULL),
(210, 'VC-210', '9', 'Silver bangles', 'silver-bangles', '142', '90', NULL, NULL, NULL, 'in_stock', '<p>Silver jewellery is trending now </p><p>grab urs</p>', '<p>Silver finish with kempu stones </p><p>sizes : 2.4,2.6,2.8</p>', '<p>Good </p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 15:02:20', '2019-07-21 08:13:50', NULL),
(211, 'VC-211', '9', 'Ikkat-kanchi', 'ikkat-kanchi', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Best for wedding occasions&nbsp;</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">Ikkat pure silk big kanchi borders saree contrast pallu and blouse</span></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 18:16:31', '2019-07-18 02:20:32', NULL),
(212, 'VC-212', '9', 'Silver  finish 6', 'silver-finish-6', '142', '90', NULL, NULL, NULL, 'in_stock', '<p>Simple and elegant </p>', '<p>Silver finish </p><p>set of 6 bangles </p><p>size available 2.4,2.6,2.8</p><p>price given for each of 6 set bangles </p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 18:23:11', '2019-07-21 08:10:24', NULL),
(213, 'VC-213', '9', 'Kasu bangle', 'kasu-bangle', '142', '90', NULL, NULL, NULL, 'in_stock', '<p>Perfect for Bracelet lovers</p>', '<p>Bracelet/kada with kasu hangings gives new look and stones add beauty</p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 19:51:37', '2019-07-21 08:07:02', NULL),
(214, 'VC-214', '9', 'Bangle kada1', 'bangle-kada1', '142', '90', NULL, NULL, NULL, 'in_stock', '<p>Simple traditional wear </p>', '<p>Simple design with kempu emerald stones</p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-07-17 19:58:26', '2019-08-06 16:54:51', NULL),
(215, 'VC-215', '9', 'Multy choker', 'multy-choker', '142', '98', NULL, NULL, NULL, 'in_stock', '<p>Apt for traditional occasions&nbsp;</p>', '<p>Antique finish&nbsp;</p><p>multi stone choker</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-07-27 19:28:57', '2019-08-08 10:18:09', NULL),
(216, 'VC-216', '9', 'Kempu Jhumki', 'kempu-jhumki', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>For all traditional occasions&nbsp;</p>', '<p>Antique finish</p><p>Real kempu jhumki&nbsp;</p><p>length 3.5 inches</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-27 19:33:22', '2019-08-08 10:23:41', NULL),
(217, 'VC-217', '9', 'Belimoda jhumki', 'belimoda-jhumki', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>Simple and elegant&nbsp;</p>', '<p>Belimoda jhumki&nbsp;</p><p>real kempu/ green stones</p><p>length 1.5 inches</p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-07-27 19:39:27', '2019-08-08 10:23:17', NULL),
(218, 'VC-218', '9', 'Micro gold necklace', 'micro-gold-necklace', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Simple and elegant&nbsp;</p>', '<p>Micro gold finish necklace with kempu stones</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-27 19:50:11', '2019-08-04 21:18:59', NULL),
(219, 'VC-219', '9', 'Matte silver', 'matte-silver', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Trendy party wear design &nbsp;in beautiful colours&nbsp;</p>', '<p>Matte silver necklace available in different colours&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-27 19:54:36', '2019-08-08 10:22:50', NULL),
(220, 'VC-220', '9', 'Antique jhumka', 'antique-jhumka', '142', '91', NULL, NULL, NULL, 'in_stock', '<p>Apt for traditional occasions&nbsp;</p>', '<p>Antique finish jhumki with pearl and stone combination</p>', '<p>Premium&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-27 19:59:12', '2019-08-04 21:18:30', NULL),
(221, 'VC-221', '9', 'Kempu stone jewellery', 'kempu-stone-jewellery', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Elite look </p><p>perfect for wedding and all traditional occasions </p>', '<p>Kempu and green stone necklaces with beautiful design</p>', '<p>Premium </p>', NULL, 1, NULL, NULL, NULL, '2019-07-27 20:17:13', '2019-08-02 11:57:12', '220'),
(222, 'VC-222', '9', 'Banaras georgette', 'banaras-georgette', '130', '132', '137', NULL, NULL, 'in_stock', '<p>Best for party and traditional occasions&nbsp;</p>', '<p>Banaras Georgette fabric&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-29 11:51:13', '2019-08-08 10:20:31', NULL),
(223, 'VC-223', '9', 'Banaras georgette2', 'banaras-georgette2', '130', '132', '137', NULL, NULL, 'in_stock', '<p>Perfect for party and weddings&nbsp;</p><p>trendy to wear now on beautiful long gowns 😍</p>', 'Banaras georgette fabric&nbsp;', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-29 11:55:36', '2019-08-04 21:17:55', NULL),
(224, 'VC-224', '9', 'Pure designer silk', 'pure-designer-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Elegant and rich look for wedding and parties&nbsp;</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">Banarasi Pure Designer Silk Sarees with 1000 Butta Model &amp; Kanchipuram Elegant Border</span></p><p>* Contrast Rich Pallu&nbsp;</p><p>* Contrast Plain Blouse</p><p>* Smooth &amp; Soft Fabric</p><p>* With Silkmark Certified</p><p><br></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-29 13:06:07', '2019-08-08 10:19:49', NULL),
(225, 'VC-225', '9', 'Banaras designer silk', 'banaras-designer-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Apt for weddings and other traditional occasions&nbsp;</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">&nbsp;Banarasi Pure Designer Silk Sarees with 1000 Butta Model &amp; Kanchipuram Elegant Border</span></p><p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">* Contrast Rich Pallu&nbsp;</span><br></p><p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">* Contrast Plain Blouse&nbsp;</span><br></p><p>* Smooth &amp; Soft Fabric</p><p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">* With Silkmark Certified&nbsp;</span></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-07-29 14:42:28', '2019-08-08 10:18:42', NULL),
(226, 'VC-226', '9', 'Lenin big border', 'lenin-big-border', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Trending now&nbsp;</p>', '<p>Apt for parties&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 0, NULL, NULL, NULL, '2019-07-29 14:54:33', '2019-07-29 14:54:33', NULL),
(227, 'VC-227', '9', 'Lenin big border2', 'lenin-big-border2', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Trending now </p>', '<p>Apt for parties </p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-07-29 14:55:48', '2019-08-02 11:53:24', '221'),
(228, 'VC-228', '9', 'Kanchipuram2', 'kanchipuram2', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Perfect for wedding and all traditional occasions </p>', '<p>Pure Kanchipuram unique pattern</p><p>light weight</p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-07-29 15:03:36', '2019-08-02 16:01:50', NULL),
(229, 'VC-229', '15', 'Munagaku(Drumstick Leaf) Karam', 'munagakudrumstick-leaf-karam', '117', '126', NULL, NULL, NULL, 'in_stock', '<p><span style=\"color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif; font-size: 19px; display: inline !important;\">Drumstick leaves are packed with </span><a href=\"https://www.healthbenefitstimes.com/nutrition/calcium/\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 19px; font-family: Roboto, Arial, sans-serif; color: rgb(1, 118, 195); transition-duration: 0.25s; transition-timing-function: ease-in-out; background-color: rgb(255, 255, 255);\">calcium</a><span style=\"color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif; font-size: 19px; display: inline !important;\">, </span><a href=\"https://www.healthbenefitstimes.com/nutrition/protein/\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 19px; font-family: Roboto, Arial, sans-serif; color: rgb(1, 118, 195); transition-duration: 0.25s; transition-timing-function: ease-in-out; background-color: rgb(255, 255, 255);\">protein</a><span style=\"color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif; font-size: 19px; display: inline !important;\">, </span><a href=\"https://www.healthbenefitstimes.com/nutrition/beta-carotene/\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 19px; font-family: Roboto, Arial, sans-serif; color: rgb(1, 118, 195); transition-duration: 0.25s; transition-timing-function: ease-in-out; background-color: rgb(255, 255, 255);\">beta carotene</a><span style=\"color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif; font-size: 19px; display: inline !important;\">, </span><a href=\"https://www.healthbenefitstimes.com/nutrition/iron/\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 19px; font-family: Roboto, Arial, sans-serif; color: rgb(1, 118, 195); transition-duration: 0.25s; transition-timing-function: ease-in-out; background-color: rgb(255, 255, 255);\">iron</a><span style=\"color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif; font-size: 19px; display: inline !important;\">, </span><a href=\"https://www.healthbenefitstimes.com/nutrition/vitamin-c/\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-size: 19px; font-family: Roboto, Arial, sans-serif; color: rgb(1, 118, 195); transition-duration: 0.25s; transition-timing-function: ease-in-out; background-color: rgb(255, 255, 255);\">Vitamin C</a><span style=\"color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif; font-size: 19px; display: inline !important;\"> and more.</span><br></p>', '<p>Munagaku(Drumstick Leaf), Curry Leaf, etc.</p>', '<p>Can Be Fresh for 30 days if kept in a air tight container.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-01 10:19:25', '2019-08-03 23:11:26', NULL),
(230, 'VC-230', '44', 'Six Photo Rotating Cube', 'six-photo-rotating-cube', '3', '115', '114', NULL, NULL, 'in_stock', '<p>This is a wonderful Six Photo Rotating Cube. Very good for keeping in Living Room or Bed Room and recollect your memories. </p><p>Also good option to gift to your loved ones.</p>', '<p>Will fit 6 photos.</p>', '<p>Good Quality.</p>', NULL, 1, 'Six Photo Rotating Cube| Buy Six Photo Rotating Cube Hyderabad', 'Six Photo Rotating Cube, Buy Six Photo Rotating Cube Hyderabad', 'Six Photo Rotating Cube Online and Buy Six Photo Rotating Cube Hyderabad', '2019-08-03 21:58:25', '2019-09-03 16:42:23', NULL),
(231, 'VC-231', '44', 'Aluminium Sipper Bottle (White)', 'aluminium-sipper-bottle-white', '3', '115', '114', NULL, NULL, 'in_stock', '<p>This is a attractive and personalized water bottle for your kids. You can personalize with the photo you want on the water bottle.  </p><p>This is good for your kids or your self and also nice to give as a gift to your loved ones. When every you drink water you can see your photo and can recollect your memories.</p><p>What are you waiting on? Order right away.</p>', '<p>You need to order this and send your photo to info@velchala.com with your order number, name etc., details</p>', '<p>Good Quality.</p>', NULL, 1, 'Buy Aluminium Sipper Bottle White online in Hyderabad', 'Buy Aluminium Sipper Bottle White online in Hyderabad', 'Buy Aluminium Sipper Bottle White online in Hyderabad', '2019-08-03 22:04:39', '2019-09-03 16:41:02', NULL),
(232, 'VC-232', '44', 'Metal Pencil Box with Photo', 'metal-pencil-box-with-photo', '3', '115', '114', NULL, NULL, 'in_stock', '<p>This is an attractive and wonderful pencil box for your kids. The best part of it is you can personalize with your kids photo or family photo.</p><p>Even if they miss it other students will return it to them as they can recognize with the photo on it and know it is yours.</p><p>Why are you waiting. Go and order right away and present it to your kids.</p>', '<p>Metal pencil box.</p>', '<p>Good.</p>', NULL, 1, 'Buy Metal Pencil Box with Photo Online in Hyderabad', 'Buy Metal Pencil Box with Photo Online in Hyderabad', 'Buy Metal Pencil Box with Photo Online in Hyderabad', '2019-08-03 22:11:58', '2019-09-03 16:42:00', NULL),
(233, 'VC-233', '44', 'Handcrafted with Heart Tile', 'handcrafted-with-heart-tile', '3', '115', '114', NULL, NULL, 'in_stock', '<p>This is a wonderful hand crafted frame with personalization. Good for decorating your living room or mantel. This is also a good choice to give as gift, surprise them with personalization and need not worry that you don\'t have their photo.</p>', '<p>Hand crafted frame.</p>', '<p>Good.</p>', NULL, 1, 'Buy Handcrafted with Heart Tile Online in Hyderabad', 'Buy Handcrafted with Heart Tile Online in Hyderabad', 'Buy Handcrafted with Heart Tile Online in Hyderabad', '2019-08-03 22:19:39', '2019-09-03 16:41:34', NULL),
(234, 'VC-234', '44', 'Personalized Black Coffee Mug', 'personalized-black-coffee-mug', '3', '115', '114', NULL, NULL, 'in_stock', '<p>This is a wonderful black coffee mug. You can personalize it with your photo or your loved ones. You can recollect your memories every time you have coffee/tea in it.</p><p>Also good to buy for your self or gift and surprise your loved ones with this personalized coffee mug</p>', '<p>Black Coffee Mug.</p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-03 22:32:14', '2019-08-03 22:35:18', NULL),
(235, 'VC-235', '44', 'Personalized White Coffee Mug', 'personalized-white-coffee-mug', '3', '115', '114', NULL, NULL, 'in_stock', '<p style=\"color: rgb(136, 136, 136); font-family: Roboto, sans-serif; font-size: 16px;\">This is a wonderful black coffee mug. You can personalize it with your photo or your loved ones. You can recollect your memories every time you have coffee/tea in it.</p><p style=\"color: rgb(136, 136, 136); font-family: Roboto, sans-serif; font-size: 16px;\">Also good to buy for your self or gift and surprise your loved ones with this personalized coffee mug.</p>', '<p>White Coffee Mug.</p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-03 22:36:54', '2019-09-06 18:15:41', NULL),
(236, 'VC-236', '44', 'Love Wooden Photo Frame', 'love-wooden-photo-frame', '3', '115', '114', NULL, NULL, 'in_stock', '<p>Love Wooden Photo Frame.<br></p>', '<p>Love Wooden Photo Frame.<br></p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-03 22:41:38', '2019-08-03 22:42:03', NULL),
(237, 'VC-237', '43', 'Single Rakhi', 'single-rakhi', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Overview</p>', '<p>Specifications</p>', '<p>Product Quality Information</p>', NULL, 0, NULL, NULL, NULL, '2019-08-04 21:36:25', '2019-08-04 21:36:25', NULL),
(238, 'VC-238', '43', 'Double Rakhi', 'double-rakhi', '143', '146', NULL, NULL, NULL, 'in_stock', '<p>Product Overview&nbsp;</p>', '<p>Product Specifications</p>', '<p>Product Quality</p>', NULL, 0, NULL, NULL, NULL, '2019-08-04 21:37:58', '2019-08-04 21:37:58', NULL),
(239, 'VC-239', '43', 'Bhayya Bhabhi Rakhi', 'bhayya-bhabhi-rakhi', '143', '147', NULL, NULL, NULL, 'in_stock', '<p>Product Overview</p>', '<p>Specifications</p>', '<p>Quality Information</p>', NULL, 0, NULL, NULL, NULL, '2019-08-04 21:38:55', '2019-08-04 21:38:55', NULL),
(240, 'VC-240', '43', 'Rakhi with Dairy Milk', 'rakhi-with-dairy-milk', '143', '145', NULL, NULL, NULL, 'in_stock', '<p class=\"MsoNormal\"><b><span style=\"font-size:14.0pt;line-height:115%;font-family:\"Cambria\",serif;\r\nmso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin\">Rakhi with\r\ndairy milk chocolate<o:p></o:p></span></b></p><p class=\"MsoNormal\"><span style=\"font-size:14.0pt;line-height:115%;mso-bidi-font-family:\r\nCalibri;mso-bidi-theme-font:minor-latin\">Festival of brother and sister love\r\nRakshabandhan is here; celebrate your rakhi with evergreen and most favorite of\r\neveryone- Dairy Milk chocolate. Give your brother sweetness of happiness with\r\ndairy milk chocolate – roast almond and fruit n nuts. These two tasty flavors of\r\ndairy milk  chocolate  will make his day sweeter.  <o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\"><span style=\"font-size:14.0pt;line-height:115%;mso-bidi-font-family:\r\nCalibri;mso-bidi-theme-font:minor-latin\">This combo contains an attractive\r\nRakhi with kumkum, rice and dairy milk chocolate. What are you waiting for?\r\nOrder your rakhi now.<o:p></o:p></span></p>', '<p><span style=\"font-size: 18.6667px; display: inline !important;\">Rakhi with kumkum, rice and 2 dairy milk chocolates.</span><br></p>', '<p>Good.</p>', 0, 1, NULL, NULL, NULL, '2019-08-04 21:46:59', '2019-08-06 08:37:39', NULL),
(245, 'VC-245', '15', 'Raksha Bandhan Special with Home Made Healthy Laddu\'s Pack', 'raksha-bandhan-special-with-home-made-healthy-laddus-pack', '143', '145', NULL, NULL, NULL, 'in_stock', '<p style=\"margin-bottom: 0px; padding-bottom: 15px; text-align: justify; color: rgb(33, 37, 41);\">Special Raakhi Bandhan Package Available with Assorted or single variety of Special Home Made Healthy Laddoo\'s.  </p><p style=\"margin-bottom: 0px; padding-bottom: 15px; text-align: justify; color: rgb(33, 37, 41);\">Note: Diya Not Included in the Package.</p>', '<p><span style=\"color: rgb(33, 37, 41); text-align: justify; display: inline !important;\">No Sugar, Pure Ghee, Home Made, Healthy Dry fruits Laddoo\'s.</span><br></p>', '<p><span style=\"color: rgb(33, 37, 41); text-align: justify; display: inline !important;\">Can be fresh for 15 days when kept in cool place away from direct sun light. Can be fresh for 30 days if kept in fridge.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2019-08-06 08:46:49', '2019-08-06 08:48:40', NULL),
(241, 'VC-241', '43', 'Rakhi with Fererro Rocher', 'rakhi-with-fererro-rocher', '143', '145', NULL, NULL, NULL, 'in_stock', '<p class=\"MsoNormal\"><b><span style=\"font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:\r\n\"Cambria\",serif;mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin\">Rakhi\r\nwith Ferrero Rocher:<o:p></o:p></span></b></p><p class=\"MsoNormal\"><span style=\"font-size:14.0pt;mso-bidi-font-size:12.0pt;\r\nline-height:115%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Festival\r\nof love is here, a great day for brothers and sisters to tell, how much they\r\nlove each other</span><span style=\"font-size:16.0pt;mso-bidi-font-size:11.0pt;\r\nline-height:115%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">.\r\n</span><span style=\"font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:\r\n115%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Celebrate\r\nthis rakhi with one of the premium crunch layered chocolate Ferrero Rocher.\r\nOrder a special rakhi with premium chocolate for your <span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">loved</span> brother, showing your faith in\r\nquality for him. This combo is especially for you with shagun of rakhi, kumkum,\r\nrice and Ferrero Rocher. <o:p></o:p></span></p><p class=\"MsoNormal\"><span style=\"font-size:14.0pt;mso-bidi-font-size:11.0pt;\r\nline-height:115%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Order<span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">ing</span> now this combo is\r\nabsolutely ideal for gifting. This is gonna make your brother’s day more\r\nspecial and sweet.<o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\"><span style=\"font-size:14.0pt;mso-bidi-font-size:11.0pt;\r\nline-height:115%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">So\r\nwhat are <span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">you</span>\r\nwaiting for? This day comes only once in a year order now.<o:p></o:p></span></p>', '<p><span style=\"font-size: 18.6667px; display: inline !important;\">Rakhi, kumkum, rice and Ferrero Rocher</span><br></p>', '<p>Good.</p>', 0, 1, NULL, NULL, NULL, '2019-08-04 22:17:46', '2019-08-04 22:45:16', NULL);
INSERT INTO `products` (`p_id`, `p_unique_id`, `p_vendor_id`, `p_name`, `p_alias`, `p_cat_id`, `p_sub_cat_id`, `p_item_spl`, `p_main_image`, `p_dishtype`, `p_availability`, `p_overview`, `p_specifications`, `p_quality_care_info`, `p_delivery_charges`, `p_status`, `p_meta_title`, `p_meta_keywords`, `p_meta_description`, `created_at`, `updated_at`, `p_related`) VALUES
(242, 'VC-242', '43', 'Rakhi with Choclates', 'rakhi-with-choclates', '143', '146', NULL, NULL, NULL, 'in_stock', '<p class=\"MsoNormal\"><b><span style=\"font-size:12.0pt;line-height:115%;font-family:\"Cambria\",serif;\r\nmso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin\">Rakhi with\r\nkitkat, 5star and dairy milk<o:p></o:p></span></b></p><p class=\"MsoNormal\"><span style=\"font-size:12.0pt;line-height:115%;mso-bidi-font-family:\r\nCalibri;mso-bidi-theme-font:minor-latin\">Festival of love is here, a great day\r\nfor brothers and sisters to tell, how much they love each other. Every sister\r\nlove to tie the best rakhi to their brothers, so we are having a blasting combo\r\nfor you, this combo contains a beautiful rakhi, kumkum, rice and chocolates\r\nlikes 5 star, dairy milk and kitkat. This combo is available all over India;\r\nhurry up order your rakhi with lots of love for your brother. <o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\"><span style=\"font-size:12.0pt;line-height:115%;mso-bidi-font-family:\r\nCalibri;mso-bidi-theme-font:minor-latin\">This combo is specially made for those\r\nbrothers and sisters who are away from each other, so now this year send this\r\ncombo to your brother’s address with shagun of kumkum and rice with chocolates\r\nand sweet memories for a year.<o:p></o:p></span></p>', '<p><span style=\"font-size: 16px; display: inline !important;\">A beautiful rakhi, kumkum, rice and chocolates likes 5 star, dairy milk and kitkat</span><br></p>', '<p>Good</p>', 0, 1, NULL, NULL, NULL, '2019-08-04 23:02:59', '2019-08-04 23:03:42', NULL),
(243, 'VC-243', '43', 'Raakhi with Pulla Reddy Sweets', 'raakhi-with-pulla-reddy-sweets', '144', '148', NULL, NULL, NULL, 'in_stock', '<p class=\"MsoNormal\"><b><span style=\"font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:115%;font-family:\r\n\"Cambria\",serif;mso-ascii-theme-font:major-latin;mso-hansi-theme-font:major-latin\">Rakhi\r\nwith sweet box<o:p></o:p></span></b></p><p class=\"MsoNormal\"><span style=\"font-size:14.0pt;mso-bidi-font-size:11.0pt;\r\nline-height:115%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">Rakshabandhan\r\nis almost here don’t think so much order this rakhi combo made especially for\r\nyou. Send this combo to your brother even if he is in the US. Ya! Now sending\r\nRaakhis to US is just one click far, order this combo online for your brother\r\nin the US and we will help you to send your love to him. This combo is gonna\r\ngive him a homely emotional feeling. <o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\"><span style=\"font-size:14.0pt;mso-bidi-font-size:11.0pt;\r\nline-height:115%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin\">If\r\nyou want to send this combos in India, then don’t worry this combo is available\r\nin India too. So what are you waiting for? Order today and get your rakhi combo\r\nwith Shagun Kumkum, Rice &  Pulla Reddy\r\nSweet (Kaju Burfi-Daimond shaped) and loads of Love from you.<o:p></o:p></span></p>', '<p>Rakhi with s<span style=\"font-family: Calibri, sans-serif; font-size: 14pt; display: inline !important;\">hagun\r\nKumkum, Rice & </span><span style=\"font-family: Calibri, sans-serif; font-size: 14pt; display: inline !important;\"> </span><span style=\"font-family: Calibri, sans-serif; font-size: 14pt; display: inline !important;\">Pulla Reddy Sweet</span></p>', '<p>Good</p>', 0, 1, NULL, NULL, NULL, '2019-08-04 23:17:36', '2019-08-04 23:39:04', NULL),
(244, 'VC-244', '15', 'Raakhi Bandhan Special Home Made Healty Laddoo\'s Pack', 'raakhi-bandhan-special-home-made-healty-laddoos-pack', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Special Raakhi Bandhan Package Available with Assorted or single variety of Special Home Made Healthy Laddoo\'s.  </p><p>Note: Diya Not Included in the Package.</p>', '<p>No Sugar, Pure Ghee, Home Made, Healthy Dry fruits Laddoo\'s.</p>', '<p>Can be fresh for 15 days when kept in cool place away from direct sun light. Can be fresh for 30 days if kept in fridge.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-05 21:24:40', '2019-08-06 08:39:24', NULL),
(246, 'VC-246', '43', 'Rakhi & Sweet', 'rakhi-sweet', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Rakhi and Sweets for your sweet and loved brother.</p>', '<p>Rakhi and 250gms Kaju Burfi sweet. Pics are for demonstration purpose only, actual sweet can be of any make/company/store.</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-06 10:38:37', '2019-08-06 10:40:00', NULL),
(247, 'VC-247', '43', 'Angry Bird Raakhi', 'angry-bird-raakhi', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Angry Bird Raakhi which your kids will love a lot.</p><p>What are you waiting on? Order now using coupo code \'<span style=\"color: rgb(33, 37, 41); font-size: 14.4px; background-color: rgba(0, 0, 0, 0.05); display: inline !important;\">Rakhis2019</span>\' to get 10% discount.</p>', '<p>Rakhi, Kum kum and Rice (akshitalu).</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 09:34:29', '2019-08-07 10:00:13', NULL),
(248, 'VC-248', '43', 'Red Angry Bird Rakhi with Shagun', 'red-angry-bird-rakhi-with-shagun', '143', '145', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Angry Bird Raakhi which your kids will love a lot.</span><br></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupo code \'<span style=\"color: rgb(33, 37, 41); font-size: 14.4px; background-color: rgba(0, 0, 0, 0.05); display: inline !important;\">Rakhis2019</span>\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,  Rice (Akshitalu)</p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 09:57:43', '2019-08-07 10:00:38', NULL),
(249, 'VC-249', '43', 'Green Butterfly Rakhi', 'green-butterfly-rakhi', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Green Butterfly&nbsp;<span style=\"display: inline !important;\">Raakhi which your kids will love a lot.</span></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Rakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,  Rice (Akshitalu)<br></p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 10:03:54', '2019-08-07 10:15:09', NULL),
(251, 'VC-251', '43', 'Rakhi with Green stone', 'rakhi-with-green-stone', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Green stone&nbsp;<span style=\"display: inline !important;\">Raakhi which your kids will love a lot.</span></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Rakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,&nbsp; Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 10:25:24', '2019-08-07 10:25:59', NULL),
(250, 'VC-250', '43', 'Yellow Butterfly Rakhi', 'yellow-butterfly-rakhi', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Yellow Butterfly <span style=\"display: inline !important;\">Raakhi which your kids will love a lot.</span></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Rakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,  Rice (Akshitalu)<br></p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 10:07:53', '2019-08-07 10:14:57', NULL),
(252, 'VC-252', '43', 'Rakhi with White Stone Rd', 'rakhi-with-white-stone-rd', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Whtie Stone&nbsp;<span style=\"display: inline !important;\">Raakhi which your kids will love a lot.</span></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Rakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,&nbsp; Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 10:34:31', '2019-08-07 10:50:37', NULL),
(253, 'VC-253', '43', 'Rakhi with Green stones', 'rakhi-with-green-stones', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Raakhi with Green Stone</p><p>What are you waiting on? Order now using coupon code \'Rakhis2019\' to get 10% discount.</p>', '<p>Rakhi, Kum Kum,  Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 10:43:09', '2019-08-07 10:49:58', NULL),
(254, 'VC-254', '43', 'Green Rakhi with white stone', 'green-rakhi-with-white-stone', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Raakhi _Green with White Stone</p><p><br></p><p>What are you waiting on? Order now using coupon code \'Rakhis2019\' to get 10% discount.</p>', '<p>Rakhi, Kum Kum,  Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 10:45:26', '2019-08-07 10:47:58', NULL),
(255, 'VC-255', '9', 'CZ Vaddanam 1', 'cz-vaddanam-1', '142', '97', NULL, NULL, NULL, 'in_stock', '<p>Perfect for CZ lovers for grand weddings</p>', '<p>CZ vaddanam with pink stones with beautiful design&nbsp;</p>', '<p>Premium </p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 16:10:44', '2019-08-08 10:08:41', NULL),
(256, 'VC-256', '9', 'Cz vaddanam2', 'cz-vaddanam2', '142', '97', NULL, NULL, NULL, 'in_stock', '<p>Best for wedding occasions&nbsp;</p>', '<p>CZ vaddanam with green studded stones&nbsp;</p><p>adult size only</p>', '<p>Premium&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 16:20:55', '2019-08-08 10:08:10', NULL),
(257, 'VC-257', '9', 'Kanchipuram designer', 'kanchipuram-designer', '130', '131', '136', NULL, NULL, 'in_stock', '<p>Apt for traditional wear&nbsp;</p><p>trendy designer blouse gives elegant look&nbsp;</p>', '<p>Pure Kanchipuram sarees with designer blouse&nbsp;</p><p>size margin available&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 16:27:23', '2019-08-08 10:00:28', NULL),
(258, 'VC-258', '9', 'Designer Kuppadam', 'designer-kuppadam', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Latest model in designer kuppadam </p><p>Excellent wardrobe collection for kuppadam lovers</p>', '<p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">&nbsp;Latest Designer Premium Kuppadam Silk Sarees with Silver/gold Jari Weaving Borders&nbsp;</span></p><p>* Contrast Rich Pallu&nbsp;</p><p>* Contrast Plain Blouse&nbsp;</p><p><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">* Unique Color Combinations</span><span style=\"-webkit-text-size-adjust: 100%; display: inline !important;\">* Premium Quality</span></p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 16:33:07', '2019-08-08 09:59:33', NULL),
(259, 'VC-259', '43', 'Red Angry Bird Rakhi with Shagun to Outside India', 'red-angry-bird-rakhi-with-shagun-to-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p><span style=\"display: inline !important;\">Angry Bird Raakhi which your kids will love a lot.</span><br></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Raakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,  Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 18:52:03', '2019-08-07 20:04:15', NULL),
(260, 'VC-260', '43', 'Yelow Angry Bird Kids Rakhi with Shagun to outside India', 'yelow-angry-bird-kids-rakhi-with-shagun-to-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Angry Bird Rakhi which your kids  will love a lot.</p><p>What are you waiting on ? Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p><p><br></p>', '<p>Rakhi, Kumkum, Rice (Akshintalu)</p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 19:58:08', '2019-08-07 20:06:24', NULL),
(261, 'VC-261', '43', 'Kids Yellow Butterfly Rakhi  for Outside India', 'kids-yellow-butterfly-rakhi-for-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Yellow Butterfly Rakhi&nbsp;which your kids will love a lot.</p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Raakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,&nbsp; Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 20:16:36', '2019-08-07 20:20:22', NULL),
(262, 'VC-262', '43', 'Kids Green Butterfly Rakhi for Outside India', 'kids-green-butterfly-rakhi-for-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Green Butterfly&nbsp;<span style=\"display: inline !important;\">Raakhi which your kids will love a lot.</span></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Raakhis2019\' to get 10% discount.</span><span style=\"display: inline !important;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><br></p>', '<p>Rakhi, Kum Kum,&nbsp; Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 20:22:18', '2019-08-07 20:22:39', NULL),
(263, 'VC-263', '43', 'Green Stone Rakhi for Outside India', 'green-stone-rakhi-for-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Green stone Raakhi which you will like a lot.</p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Raakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,&nbsp; Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 20:25:48', '2019-08-07 20:26:39', NULL),
(264, 'VC-264', '43', 'Green Rakhi with white stone for Deliver to Outside India', 'green-rakhi-with-white-stone-for-deliver-to-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Green colored Rakhi with White Stone which you will like a lot.</p><p>What are you waiting on? Order now using coupon code \'Raakhis2019\' to get 10% discount.<br></p>', '<p>Rakhi, Kum Kum,&nbsp; Rice (Akshitalu)<br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 20:29:05', '2019-08-07 20:29:56', NULL),
(265, 'VC-265', '43', 'Red Angry Bird Rakhi', 'red-angry-bird-rakhi', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Angry Bird Rakhi which your kids will love a lot.</p><p>What are you waiting on? Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshintalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 20:31:26', '2019-08-07 20:59:43', NULL),
(266, 'VC-266', '43', 'Pink Rakhi with White Stone to deliver outside India', 'pink-rakhi-with-white-stone-to-deliver-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Pink color&nbsp;<span style=\"display: inline !important;\">Raakhi with white stones which you will like a lot.</span></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Raakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,&nbsp; Rice (Akshitalu)<br></p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 20:32:55', '2019-08-07 20:33:37', NULL),
(267, 'VC-267', '43', 'White Stone Round Rakhi to deliver outside India', 'white-stone-round-rakhi-to-deliver-outside-india', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Round White Stone&nbsp;<span style=\"display: inline !important;\">Raakhi which you will like a lot.</span></p><p><span style=\"display: inline !important;\">What are you waiting on? Order now using coupon code \'Raakhis2019\' to get 10% discount.</span><br></p>', '<p>Rakhi, Kum Kum,  Rice (Akshitalu)<br></p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 20:35:53', '2019-08-07 20:37:16', NULL),
(268, 'VC-268', '43', 'Rakhi with Homemade Chocolates', 'rakhi-with-homemade-chocolates', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Special Rakhi for Special Brother! with home made chocolates</p><p>What are you waiting on ? Order now using coupon code ‘Raakhis2019’ to get 10% discount.<br></p>', '<p>Rakhi, Chocolates.</p>', '<p>Very Tasty.</p>', NULL, 1, NULL, NULL, NULL, '2019-08-07 21:50:05', '2019-08-07 21:51:21', NULL),
(271, 'VC-271', '43', 'Butterfly Kids Rakhi', 'butterfly-kids-rakhi', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Butterfly Rakhi which your kids will love a lot.</p><p>what are you waiting on? Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice(Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 16:56:29', '2019-08-08 16:57:16', NULL),
(272, 'VC-272', '43', 'Yellow Angry Bird Rakhi with Sweets', 'yellow-angry-bird-rakhi-with-sweets', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Angry Bird Rakhi which your kids will love a lot.</p><p>what are you waiting on? Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 17:08:28', '2019-08-08 17:21:42', NULL),
(273, 'VC-273', '43', 'Kundan Rakhi with Sweets', 'kundan-rakhi-with-sweets', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Pink thread kundan rakhi with mysorepak sweet from Pulla Reddy Sweets.</p><p>Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 17:19:07', '2019-08-08 18:13:18', NULL),
(274, 'VC-274', '43', 'Red Angry Bird Rakhi with Sweets', 'red-angry-bird-rakhi-with-sweets', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Red Angry Bird rakhi with Ajmeer Kalakand Sweet from Pulla Reddy Sweets.</p><p>what are you waiting on? Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshintalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 17:30:17', '2019-08-09 07:43:58', NULL),
(275, 'VC-275', '43', 'Kundan Rakhi with Ferraro Rocher Choc', 'kundan-rakhi-with-ferraro-rocher-choc', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', 'Rakhi, Kumkum, Rice (Akshintalu)', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 17:46:14', '2019-08-08 17:48:54', NULL),
(290, 'VC-290', '4', 'Silver Ganesh Idol', 'silver-ganesh-idol', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Silver Ganesha Idol.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers.</span></p>', '<p><br></p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 10 Grams can be provided ready or on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span></p>', '<p style=\"color: rgb(68, 68, 68);\">Silver Purity : 78%</p><p style=\"color: rgb(68, 68, 68);\"><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Best thing for gifting to friends , family , relatives.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Keep them to worship at holy place in your home/office.</span></p>', 0, 0, NULL, NULL, NULL, '2019-08-22 17:29:37', '2019-10-11 22:47:01', NULL),
(277, 'VC-277', '43', 'Kundan Rakhi With Laddu', 'kundan-rakhi-with-laddu', '144', '148', NULL, NULL, NULL, 'in_stock', '<p>Pink Thread kundan Rakhi with mothichur Laddu from Pulla Reddy Sweets.</p><p>Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshintalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 18:07:48', '2019-08-08 18:09:25', NULL),
(278, 'VC-278', '43', 'Kundan Rakhi with Mothichur Laddu', 'kundan-rakhi-with-mothichur-laddu', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Pink thread kundan Rakhi with mothichur laddu from Pulla Reddy Sweets.</p><p>Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 22:22:14', '2019-08-08 22:23:23', NULL),
(291, 'VC-291', '4', 'Silver Standing Ganesh Idol', 'silver-standing-ganesh-idol', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Silver Standing Ganesha Idol.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers.</span></p>', '<p>Product Weight:  47.7 Grams</p><p><span style=\"display: inline !important;\">Feasible Weights: Starting from 30 Grams can be provided ready or on order basis.</span><br></p><p><span style=\"display: inline !important;\">Weights given are Approx.....</span></p>', '<p style=\"color: rgb(68, 68, 68);\">Silver Purity : 78%</p><p style=\"color: rgb(68, 68, 68);\"><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Best thing for gifting to friends , family , relatives.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Keep them to worship at holy place in your home/office.</span></p>', 0, 0, NULL, NULL, NULL, '2019-08-22 17:32:59', '2019-10-11 22:45:40', NULL),
(280, 'VC-280', '43', 'Kundan Rakhi with Dairy Milk choco', 'kundan-rakhi-with-dairy-milk-choco', '143', '145', NULL, NULL, NULL, 'in_stock', '<p><font color=\"#808080\"><span style=\"caret-color: rgb(128, 128, 128); -webkit-text-size-adjust: 100%;\">Pink thread kundan Rakhi with Dairy milk chocolate.</span></font></p><p><font color=\"#808080\"><span style=\"caret-color: rgb(128, 128, 128); -webkit-text-size-adjust: 100%;\">order now using coupon code ‘Raakhis2019’ to get 10% discount.</span></font></p>', '<p><span style=\"display: inline !important;\">Rakhi, Kumkum, Rice (Akshinthalhu )</span><br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-08 22:39:41', '2019-08-09 06:30:51', NULL),
(281, 'VC-281', '43', 'Kundan Rakhi with Chocos', 'kundan-rakhi-with-chocos', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Brown thread kundan Rakhi with kit kat and 5 star chocolates.</p><p>order now using coupon code ‘Raakhis2019’ to get 10% discount&nbsp;</p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-09 06:50:43', '2019-08-09 06:51:32', NULL),
(282, 'VC-282', '43', 'Kundan Rakhi with Dairy Milk', 'kundan-rakhi-with-dairy-milk', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Green thread kundan Rakhi with Dairy milk chocolate.</p><p>order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-09 07:01:21', '2019-08-09 07:02:10', NULL),
(283, 'VC-283', '43', 'Red Butterfly kids Rakhi', 'red-butterfly-kids-rakhi', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Red Butterfly kids Rakhi which your kids will love a lot.</p><p>what are you waiting on? Order now using coupon code ‘Raakhis2019’ to get 10% discount.</p><p><br></p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-09 07:12:01', '2019-08-09 07:12:36', NULL),
(284, 'VC-284', '43', 'Yellow Angry Bird Rakhi with Sweet', 'yellow-angry-bird-rakhi-with-sweet', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Yellow Angry Bird Rakhi with kaju Kathli sweet from pulla reddy Sweets.</p><p>order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-09 07:20:33', '2019-08-09 07:21:07', NULL),
(285, 'VC-285', '43', 'Kundan Rakhi with Mysorepak Sweet', 'kundan-rakhi-with-mysorepak-sweet', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Pink thread kundan Rakhi with Mysorepak sweet from pulla reddy Sweets.</p><p>order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice ( Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-09 07:29:36', '2019-08-09 07:30:09', NULL),
(289, 'VC-289', '43', 'Kundan Rakhi with Ferraro Rocher', 'kundan-rakhi-with-ferraro-rocher', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Kundan Rakhi with Ferraro Rocher chocolate.</p><p>order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshintalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-09 07:51:08', '2019-08-09 07:52:08', NULL),
(287, 'VC-287', '43', 'Red Angry Bird Rakhi with Kalakand', 'red-angry-bird-rakhi-with-kalakand', '143', '145', NULL, NULL, NULL, 'in_stock', '<p>Red Angry Bird Rakhi with Ajmeer kalakand sweet from pulla reddy Sweets.</p><p>order now using coupon code ‘Raakhis2019’ to get 10% discount.</p>', '<p>Rakhi, Kumkum, Rice (Akshinthalu)</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-08-09 07:40:10', '2019-08-09 07:42:47', NULL),
(292, 'VC-292', '4', 'Wax Silver Coated Ganesh Idol', 'wax-silver-coated-ganesh-idol', '5', '75', NULL, NULL, NULL, 'in_stock', '<p>Wax Silver Coated Ganesha Idol.</p><p><span style=\"display: inline !important;\">Price inclusive of All Taxes, Packing and Courier Charges.</span><br></p><p><span style=\"display: inline !important;\">By R C Jewellers.</span></p>', '<p><span style=\"display: inline !important;\">Product Height starts from 2 inch</span></p><p><span style=\"display: inline !important;\"><br></span><br></p>', '<p style=\"color: rgb(68, 68, 68);\"><span style=\"display: inline !important;\">Silver considered to be a holy & pure metal.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Best thing for gifting to friends , family , relatives.</span></p><p style=\"color: rgb(68, 68, 68);\"><span style=\"color: rgb(17, 17, 17); font-family: Poppins; font-size: 13px; display: inline !important;\">Keep them to worship at holy place in your home/office.</span></p>', NULL, 1, NULL, NULL, NULL, '2019-08-22 17:43:55', '2019-08-27 07:22:34', NULL),
(293, 'VC-293', '9', 'Beautiful choker', 'beautiful-choker', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Two colors&nbsp; available&nbsp;</p>', '<p>Beautifully designed choker&nbsp;&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 0, NULL, NULL, NULL, '2019-09-16 13:12:07', '2019-09-16 13:12:07', NULL),
(294, 'VC-294', '9', 'Beautiful  choker', 'beautiful-choker', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Two different&nbsp; designs&nbsp;</p>', '<p>Beautifully designed&nbsp; choker with matching&nbsp; earrings&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 0, NULL, NULL, NULL, '2019-09-16 13:15:19', '2019-09-16 13:15:19', NULL),
(295, 'VC-295', '9', 'Green beads choker', 'green-beads-choker', '142', '94', NULL, NULL, NULL, 'in_stock', 'Apt for simple occasions', '<p>Green beads with white stone pendant gives beautiful&nbsp; look for choker&nbsp;</p><p>Matching earrings&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-09-16 17:47:53', '2019-09-17 14:23:12', NULL),
(296, 'VC-296', '9', 'Ruby beads choker', 'ruby-beads-choker', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Simple party wear&nbsp;</p>', '<p>Ruby beads with white&nbsp; stone pendant&nbsp; gives beautiful&nbsp; look along with&nbsp; matching&nbsp; earrings&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 0, NULL, NULL, NULL, '2019-09-16 17:50:10', '2019-09-16 17:50:10', NULL),
(297, 'VC-297', '9', 'Ruby beads choker1', 'ruby-beads-choker1', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Simple party wear </p>', '<p>Ruby beads with white  stone pendant  gives beautiful  look along with  matching  earrings </p>', '<p>Excellent </p>', NULL, 0, NULL, NULL, NULL, '2019-09-16 17:52:07', '2019-09-16 17:52:07', NULL),
(298, 'VC-298', '9', 'Ruby choker2', 'ruby-choker2', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Simple party wear </p>', '<p>Ruby beads with white  stone pendant  gives beautiful  look along with  matching  earrings </p>', '<p>Excellent </p>', NULL, 0, NULL, NULL, NULL, '2019-09-16 17:52:41', '2019-09-16 17:52:41', NULL),
(299, 'VC-299', '9', 'Multi stone  long chain', 'multi-stone-long-chain', '142', '93', NULL, NULL, NULL, 'in_stock', 'Perfect&nbsp; for wedding or any grand parties&nbsp;', 'Beautiful&nbsp; studded green pink and white&nbsp; stones gives diamond&nbsp; finish look&nbsp;', '<p>Superb</p>', NULL, 1, NULL, NULL, NULL, '2019-09-16 18:54:32', '2019-09-17 14:22:24', NULL),
(300, 'VC-300', '9', 'Green stone long chain', 'green-stone-long-chain', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>Perfect&nbsp; for wedding&nbsp; occasions&nbsp;</p><p>Apt for bridal&nbsp; wear</p>', '<p>Green stone long chain&nbsp; with matching earrings&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-09-16 19:01:39', '2019-09-17 14:21:39', NULL),
(301, 'VC-301', '9', 'Lenin  jute silk', 'lenin-jute-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Simple looking  linen with ikkat blouse  gives trendy look</p>', '<p><span style=\"display: inline !important;\">PURE JUTE LINEN*</span></p><p>💯🎉🎉🎉🎉🎉🎉🎉🎉🎉🎉Lauching exclusive plain linen jute weaving collection with rich contrast tassels n cotton/rayon printed *ikkat* blouse</p><p><br></p><p><br></p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-09-16 19:08:29', '2019-09-17 14:23:54', '295'),
(302, 'VC-302', '9', 'Long silver Ruby', 'long-silver-ruby', '142', '93', NULL, NULL, NULL, 'in_stock', '<p>Simple and new look</p>', '<p>Silver finish&nbsp; jewelry&nbsp; with Rubystones</p><p>Matching&nbsp; earrings&nbsp;</p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-09-16 19:16:26', '2019-09-17 14:20:04', NULL),
(303, 'VC-303', '43', 'Tomato pickle', 'tomato-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh Home made pickle from scratch.</p>', '<p>Tamato, Oil, Chillipoweder and Spices.</p>', '<p>Good.</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 00:41:58', '2019-09-24 00:52:31', NULL),
(304, 'VC-304', '43', 'Gongura Pickle', 'gongura-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh home made from scratch.</p>', '<p>Gongura, Oil, Red Chilies and Sprices.</p>', '<p>Tastey!</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 00:57:08', '2019-09-24 00:58:24', NULL),
(305, 'VC-305', '43', 'Usiri Pickle', 'usiri-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh made from Scratch</p>', '<p>Usirikaya, Oil, Chillipowder and Spices.</p>', '<p>Tastey!</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 01:07:44', '2019-09-24 01:08:23', NULL),
(306, 'VC-306', '43', 'Mirapa Pandu Pickle', 'mirapa-pandu-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh homemade pickle from scratch.</p>', '<p>Mirapa Pandu, Oil and Chili power and Spices.</p>', '<p>Tasty!</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 01:21:36', '2019-09-24 01:22:12', NULL),
(307, 'VC-307', '43', 'Nimmakaya Pickle', 'nimmakaya-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh home made from scratch.</p>', '<p>Nimmakaya, Chili powder, Oil and Spices.</p>', '<p>Tasty!</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 01:25:20', '2019-09-24 01:25:46', NULL),
(308, 'VC-308', '43', 'Mango Pickle', 'mango-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh home made pickle from scratch.</p>', '<p>Mango,Oil, Chili Powder and Spices.</p>', '<p>Tasty!</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 01:47:08', '2019-09-24 01:47:39', NULL),
(309, 'VC-309', '43', 'Chicken Pickle', 'chicken-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh pickle made from scratch</p>', '<p>Chicken, Oil, Chilli powder and spices.</p>', '<h1><h2><span style=\"color: rgb(255, 156, 0); background-color: rgb(255, 255, 0);\">Yummy!</span></h2></h1>', NULL, 1, NULL, NULL, NULL, '2019-09-24 02:03:05', '2019-09-24 02:03:37', NULL),
(310, 'VC-310', '43', 'Nuvvu Arisalu', 'nuvvu-arisalu', '117', '152', '151', NULL, NULL, 'in_stock', '<p>Fresh homemade from scratch.</p>', '<p>Rice flour, jaggery and sesame seeds.</p>', '<p>Very Tasty!</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 02:11:42', '2019-09-24 02:12:08', NULL),
(311, 'VC-311', '43', 'Mutton Pickle', 'mutton-pickle', '117', '126', NULL, NULL, NULL, 'in_stock', '<p>Fresh home made pickle from scratch.</p>', '<p>Mutton, Chilli powder, Oil and Spices.</p>', '<p>Tasty!</p>', NULL, 1, NULL, NULL, NULL, '2019-09-24 02:34:39', '2019-09-24 02:35:07', NULL),
(312, 'VC-312', '43', 'Diwali Thali', 'diwali-thali', '155', '156', NULL, NULL, NULL, 'in_stock', 'Diwali Thali!', '<p>Circular Thalis!</p>', '<p>Very Elegant!</p>', NULL, 2, NULL, NULL, NULL, '2019-10-01 09:19:19', '2019-10-01 20:25:15', '54'),
(316, 'VC-316', '9', 'Gold finish  kumkum box', 'gold-finish-kumkum-box', '142', '99', NULL, NULL, NULL, 'in_stock', '<p>Gives traditional&nbsp; and beautiful&nbsp; look</p>', '<p>Kumkum box for Puja purpose&nbsp;</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-10-11 09:44:34', '2019-10-11 23:42:10', NULL),
(315, 'VC-315', '9', 'Crop top lehengas', 'crop-top-lehengas', '130', '132', NULL, NULL, NULL, 'in_stock', '<p>Perfect&nbsp; choice for party wear and garbha&nbsp; occasions&nbsp;</p>', '<p>Crop top lehenga includes bnarasi dupatta</p><p><br></p>', '<p>Super&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-10-01 16:59:53', '2019-10-01 20:15:50', NULL),
(317, 'VC-317', '9', 'Kubera pattu', 'kubera-pattu', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Perfect  party wear</p>', '<p>Kubera soft silk</p><p><br></p><p><br></p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-10-11 21:57:18', '2019-10-11 22:56:24', NULL),
(318, 'VC-318', '9', 'Kalamkari Khadi silk', 'kalamkari-khadi-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Trendy kalamkari design</p>', '<p>Kalamkari design with kanchi borders&nbsp;</p><p><br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-10-11 21:59:54', '2019-10-11 22:57:34', NULL),
(319, 'VC-319', '9', 'Tissue silk', 'tissue-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Trending now</p>', '<p>Silver colour tissue saree</p><p><br></p><p>🥳Silver /gold tissue silk designer borders saree....</p><p>Running blouse .</p><p>🥳lite weight &amp; awesome looking saree❤❤</p><p>Ready stock</p><p><br></p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-10-12 13:59:10', '2019-10-12 20:01:18', NULL),
(320, 'VC-320', '9', 'Banaras Dupion paithani silk', 'banaras-dupion-paithani-silk', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Festive special</p>', '<p><span style=\"font-size: 1rem; display: inline !important;\">😍 Banaras dupian *paithani* lightweight soft kadhi Pattu saree.....</span><br></p><p><span style=\"font-size: 1rem; display: inline !important;\">🥰Kanchi weaving big borders all-over butees ....</span><br></p><p><span style=\"font-size: 1rem; display: inline !important;\">🥰contrast blouse with borders ...</span><br></p><p><span style=\"font-size: 1rem; display: inline !important;\">🥰Lite weight n party wear saree👌👌</span><br></p><p><span style=\"font-size: 1rem; display: inline !important;\">Multiples Available</span><br></p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-10-12 21:41:31', '2019-10-12 23:19:10', NULL),
(321, 'VC-321', '9', 'Ikkat kalamkari  combo', 'ikkat-kalamkari-combo', '130', '132', NULL, NULL, NULL, 'in_stock', '<p>Daily wear</p><p><span style=\"font-size: 1rem; display: inline !important;\">Hand wash seperately,&nbsp;</span><span style=\"font-size: 1rem; display: inline !important;\">Salt water wash suggested for first wash,</span><span style=\"font-size: 1rem; display: inline !important;\">No machine wash</span></p><p><br></p>', '<p>Ikkat anarkali  with kalamkari  border and kalamkari  dupatta </p><p>Sizes available <span style=\"font-size: 1rem; display: inline !important;\">M, L,XL,XXL</span></p><p><br></p><p><br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-10-14 11:46:01', '2019-10-14 20:38:00', NULL),
(322, 'VC-322', '9', 'Ikkat kalamkari  combo2', 'ikkat-kalamkari-combo2', '130', '132', NULL, NULL, NULL, 'in_stock', '<p>Daily wear</p><p>No machine wash</p>', '<p>Ikkat anarkali with kalamkari  border and kalamkari  dupatta </p><p>No bottom will be given</p>', '<p>Good </p>', NULL, 0, NULL, NULL, NULL, '2019-10-14 12:15:49', '2019-10-14 12:19:09', NULL),
(323, 'VC-323', '9', 'Ikkat kalamkari  combo1', 'ikkat-kalamkari-combo1', '130', '132', NULL, NULL, NULL, 'in_stock', '<p>Daily wear</p><p>No machine wash</p>', '<p>Ikkat anarkali with kalamkari  border and kalamkari  dupatta </p><p>No bottom will be given</p>', '<p>Good </p>', NULL, 1, NULL, NULL, NULL, '2019-10-14 12:16:43', '2019-10-14 20:34:27', NULL),
(324, 'VC-324', '9', 'Ikkat kalamkari 1vFb', 'ikkat-kalamkari-1vfb', '130', '132', NULL, NULL, NULL, 'in_stock', '<p>Daily wear</p><p>No machine wash</p>', '<p>Ikkat anarkali with kalamkari  border and kalamkari  dupatta </p><p>No bottom will be given</p>', '<p>Good </p>', NULL, 0, NULL, NULL, NULL, '2019-10-14 12:17:24', '2019-10-14 12:17:24', NULL),
(325, 'VC-325', '9', 'Silver linen daily wear', 'silver-linen-daily-wear', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Daily  wear</p>', '<p>🌹🌹🌹🌹🌹🌹🌹🌹</p><p>FABRIC : Linen With Silver Border </p><p>BLOUSE -  Linen</p><p>*SARI LENGTH *-5.50</p><p>BLOUSE LENGTH-0.8 </p><p>VDc price 1440</p>', '<p>Good</p>', NULL, 0, NULL, NULL, NULL, '2019-10-14 16:15:25', '2019-10-14 16:33:05', NULL),
(326, 'VC-326', '9', 'Daily  wear', 'daily-wear', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Daily  wear</p>', '<p>🌹🌹🌹🌹🌹🌹🌹🌹</p><p>FABRIC : Linen With Silver Border&nbsp;</p><p>BLOUSE -&nbsp; Linen</p><p>*SARI LENGTH *-5.50</p><p>BLOUSE LENGTH-0.8&nbsp;</p><p><br></p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-10-14 16:30:10', '2019-10-15 03:26:37', NULL),
(327, 'VC-327', '9', 'Linen long frock', 'linen-long-frock', '130', '132', '134', NULL, NULL, 'in_stock', '<p>Trendy looking  long frocks</p>', '<p>*Designer Lenin&nbsp; Cotton Kurties/Gowns</p><p>*Size : L/Xl/XXL*&nbsp;<span style=\"font-size: 1rem; display: inline !important;\">*Chest: 40/42/44”*</span></p><p><span style=\"display: inline !important;\">*Length: 54” approx&nbsp;</span></p><p><br></p>', '<p>Good </p>', NULL, 1, NULL, NULL, NULL, '2019-10-17 10:08:28', '2019-10-17 10:11:40', NULL),
(328, 'VC-328', '9', 'Kanchi kuttu  sarees', 'kanchi-kuttu-sarees', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Pick this For Wedding  and other traditional  occasions </p><p>😍</p>', '<p>G<span style=\"display: inline !important;\">NEW ARRIVALS OF KANCHI HANDLOOM KUTTU SAREES WITH RICH PALLU AND TRADITIONAL BORDER WITH CONTRAST BLOUSE </span><span style=\"display: inline !important;\">Silk Mark Certified</span></p><p><br></p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-10-28 00:50:14', '2019-10-28 01:28:28', NULL),
(331, 'VC-331', '9', 'Soft Georgette', 'soft-georgette', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Trendy and perfect&nbsp; party wear</p>', '<p>Soft Georgette&nbsp; Floral&nbsp; saree with designer&nbsp; blouse&nbsp;</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-11-02 23:35:53', '2019-11-05 09:19:45', NULL),
(329, 'VC-329', '9', 'Blue Venkatagiri pattu', 'blue-venkatagiri-pattu', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Apt  for Traditional  occasions  </p>', '<p>Pure Venkatagiri pattu&nbsp; with contrast&nbsp; blouse&nbsp; and pallu&nbsp;</p><p><br></p><p><br></p>', '<p>Excellent </p>', NULL, 1, NULL, NULL, NULL, '2019-10-28 01:14:42', '2019-10-28 01:31:43', NULL),
(330, 'VC-330', '9', 'Tissue  saree/silver', 'tissue-sareesilver', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Trending now </p>', '<p><span style=\"display: inline !important;\">😍 Kanjivaram Soft gold tissue big zari Ganga ..Jamuna borders saree....👌</span></p><p><span style=\"display: inline !important;\">😍gold blouse with borders...&nbsp;&nbsp;</span></p><p><span style=\"display: inline !important;\">😍allover weaving butees. ..</span><span style=\"display: inline !important;\">lite weight n Awesome saree...</span></p><p><br class=\"Apple-interchange-newline\"></p><p><br></p>', '<p>Good </p>', NULL, 1, NULL, NULL, NULL, '2019-10-28 01:22:26', '2019-10-28 01:35:29', NULL),
(338, 'VC-338', '9', 'Embroidered linen', 'embroidered-linen', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Simple and beautiful  pastel linen with embroidery done</p>', '<p>Linen embroidery&nbsp; with running blouse&nbsp;</p><p><br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-11-18 11:11:23', '2019-11-18 19:29:22', NULL),
(339, 'VC-339', '9', 'Pure kanchi', 'pure-kanchi', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Perfect for wedding&nbsp; or any traditional&nbsp; occasions&nbsp;</p>', '<p>Pure kanchipuram&nbsp; with contrast&nbsp; blouse</p>', '<p>Excellent&nbsp;</p>', NULL, 1, NULL, NULL, NULL, '2019-12-06 21:41:19', '2019-12-06 22:00:08', NULL),
(340, 'VC-340', '9', 'Mangalagiri pattu', 'mangalagiri-pattu', '130', '131', NULL, NULL, NULL, 'in_stock', 'Apt for simple occasions or traditional ', '<p>Mangalagiri pattu plain with SPECIAL IKKAT BORDER Saree&nbsp;</p><p>Contrast pallu and blouse</p><p><span style=\"display: inline !important;\">BEST QUALITY WEAVE</span></p><p>VDESI price4650/-</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-12-10 09:22:56', '2019-12-10 10:38:11', NULL),
(335, 'VC-335', '9', 'Matte Nallapusalu', 'matte-nallapusalu', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Simple goes with sarees and ethnic dresses</p>', '<p>Matte pendant with black beads chain</p><p><br></p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-11-03 17:28:25', '2019-11-05 23:31:14', NULL),
(341, 'VC-341', '9', 'Blueorganza', 'blueorganza', '130', '131', NULL, NULL, NULL, 'in_stock', '<p>Simple wear looks grand for simple occasions </p>', '<p>*Brand new design *</p><p>*Traditional wear *🥰</p><p>*Pure heavy organza silk *2.O*😊</p><p>*With heavy rich pallu *</p><p>*With big skirt boder *</p><p>With full saree self weaving *</p><p>*With contrast blouse *</p><p><span style=\"display: inline !important;\">*Available in multiples *💃💃💃</span></p><p>VDESI price-2500/-</p>', '<p>Good</p>', NULL, 1, NULL, NULL, NULL, '2019-12-10 09:25:33', '2019-12-10 10:37:03', NULL),
(337, 'VC-337', '9', 'Choker green', 'choker-green', '142', '94', NULL, NULL, NULL, 'in_stock', '<p>Gives beautiful&nbsp; look&nbsp; on sarees and ethnic dresses</p>', '<p>Green beads choker with beautiful pendant and matching&nbsp; earrings</p>', '<p>Fine</p>', NULL, 1, NULL, NULL, NULL, '2019-11-05 11:02:23', '2019-11-05 12:04:02', NULL),
(342, 'VC-342', '43', 'Re union T shirt', 're-union-t-shirt', '130', '132', NULL, NULL, NULL, 'in_stock', '<p>Re union T shirt</p>', '<p>All sizes</p>', '<p>Pure cotton T shirt</p>', NULL, 0, NULL, NULL, NULL, '2020-01-09 20:11:25', '2020-01-09 20:11:25', NULL),
(343, 'VC-343', '11', 'Valentine\'s Combo1', 'valentines-combo1', '3', '157', NULL, NULL, 'Egg', 'in_stock', '<p>This is a Valentine\'s day special combo with Biryani, 0.5kg ake and 250Ml coke.</p><p><span style=\"display: inline !important;\">What are  you waiting one. Order this special combo to your loved ones.</span><br></p>', '<p>Biryani+0.5kgCake+250ml Coke</p>', '<p>The best in town quality.</p>', NULL, 1, 'Valnetine\'s Day Combo', 'Valnetine\'s Day Combo', 'Valnetine\'s Day Combo', '2020-02-10 09:15:52', '2020-02-10 09:18:02', NULL),
(344, 'VC-344', '11', 'Valentine\'s Combo2', 'valentines-combo2', '3', '157', NULL, NULL, NULL, 'in_stock', '<p style=\"margin-bottom: 0px; font-size: 14px; padding-bottom: 15px; font-family: Poppins, sans-serif; text-align: justify; color: rgb(33, 37, 41); line-height: 24px !important;\">This is a Valentine\'s day special combo with Family pack Biryani, 0.5kg cake and 650Ml Thumbsup/Coke.</p><p style=\"margin-bottom: 0px; font-size: 14px; padding-bottom: 15px; font-family: Poppins, sans-serif; text-align: justify; color: rgb(33, 37, 41); line-height: 24px !important;\"><span style=\"line-height: 24px !important; display: inline !important;\">What are&nbsp; you waiting one. Order this special combo to your loved ones.</span></p>', '<p><span style=\"color: rgb(33, 37, 41); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">Family Pack Biryani+0.5kgCake+650ml Thumbsup/Coke</span><br></p>', '<p><span style=\"color: rgb(33, 37, 41); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">The best quality </span><span style=\"color: rgb(33, 37, 41); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">in city.</span><br></p>', NULL, 1, NULL, NULL, NULL, '2020-02-10 10:49:28', '2020-02-10 10:58:01', NULL),
(346, 'VC-346', '11', 'Valentine\'s Combo4', 'valentines-combo4', '3', '157', NULL, NULL, NULL, 'in_stock', '<p style=\"margin-bottom: 0px; font-size: 14px; padding-bottom: 15px; font-family: Poppins, sans-serif; text-align: justify; color: rgb(33, 37, 41); line-height: 24px !important;\">This is a Valentine\'s day special combo with 0.5kg ake and Flower Bouquet (Dozen Roses)..</p><p style=\"margin-bottom: 0px; font-size: 14px; padding-bottom: 15px; font-family: Poppins, sans-serif; text-align: justify; color: rgb(33, 37, 41); line-height: 24px !important;\"><span style=\"line-height: 24px !important; display: inline !important;\">What are&nbsp; you waiting one. Order this special combo to your loved ones.</span></p>', '<p><span style=\"color: rgb(33, 37, 41); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">0.5kgCake+ Flower Bouquet (Dozen Roses).</span><br></p>', '<p>The best quality in city.</p>', NULL, 1, NULL, NULL, NULL, '2020-02-10 11:01:32', '2020-02-10 11:02:36', NULL),
(345, 'VC-345', '11', 'Valentine\'s Combo3', 'valentines-combo3', '3', '157', NULL, NULL, NULL, 'in_stock', '<p style=\"margin-bottom: 0px; font-size: 14px; padding-bottom: 15px; font-family: Poppins, sans-serif; text-align: justify; color: rgb(33, 37, 41); line-height: 24px !important;\">This is a Valentine\'s day special combo with Family pack Biryani, 1kg cake and 1.25ltr Thumbsup/Coke.</p><p style=\"margin-bottom: 0px; font-size: 14px; padding-bottom: 15px; font-family: Poppins, sans-serif; text-align: justify; color: rgb(33, 37, 41); line-height: 24px !important;\"><span style=\"line-height: 24px !important; display: inline !important;\">What are&nbsp; you waiting one. Order this special combo to your loved ones.</span></p>', '<p><span style=\"color: rgb(33, 37, 41); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">Family Pack Biryani+1kgCake+1.25ltrl Thumbsup/Coke</span><br></p>', '<p>The best quality in city.</p>', NULL, 1, NULL, NULL, NULL, '2020-02-10 10:57:25', '2020-02-10 11:02:12', NULL),
(347, 'VC-347', '43', 'Jute Bag with Flower', 'jute-bag-with-flower', '3', '115', NULL, NULL, NULL, 'in_stock', '<p>Jute bags for personal use or for gifting.</p><p><span style=\"font-size: 1rem; display: inline !important;\"><b>Qty: 50 bgs.</b></span><br></p>', '<p><span style=\"display: inline !important;\">Size 12x12</span><br></p>', '<p>Very nice</p>', NULL, 1, NULL, 'Jute bag', NULL, '2020-03-05 07:38:29', '2020-03-07 02:48:28', NULL),
(348, 'VC-348', '43', 'Father\'s Day Spl Cake', 'fathers-day-spl-cake', '3', '158', NULL, NULL, NULL, 'in_stock', '<p><span style=\"color: rgb(99, 107, 111); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">A Father\'s Day </span><span style=\"font-size: 1rem; font-family: Poppins, sans-serif; text-align: justify; color: rgb(99, 107, 111); line-height: 24px !important; display: inline !important;\">special</span><span style=\"font-size: 1rem; font-family: Poppins, sans-serif; text-align: justify; color: rgb(99, 107, 111); line-height: 24px !important; display: inline !important;\"> </span><span style=\"font-size: 1rem; font-family: Poppins, sans-serif; text-align: justify; color: rgb(99, 107, 111); line-height: 24px !important; display: inline !important;\">cake for your dear father. We will make in the flavour of your choice specified in the custom message field. </span>    <br></p>', '<p><span style=\"color: rgb(99, 107, 111); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">Father\'s Day Spl Cake - Bow tie. Available in 1 kg and 2kgs, any flavor of your choice.</span><br></p>', '<p><span style=\"color: rgb(99, 107, 111); font-family: Poppins, sans-serif; font-size: 14px; text-align: justify; display: inline !important;\">Good</span><br></p>', NULL, 1, NULL, NULL, NULL, '2020-06-19 22:40:42', '2020-06-20 16:27:04', NULL),
(349, 'VC-349', '43', 'New Guava Cake', 'new-guava-cake', '3', '158', NULL, NULL, NULL, 'in_stock', '<p>New Product for Father\'s day special</p>', '<p>1 kg cake, round shape</p>', '<p>Good Quality</p>', NULL, 1, NULL, NULL, NULL, '2020-06-19 22:43:21', '2020-06-20 16:25:59', NULL),
(350, 'VC-350', '43', 'New Choco Vanilla Cake', 'new-choco-vanilla-cake', '3', '158', NULL, NULL, NULL, 'in_stock', '<p>Chocolate Cake</p>', '<p>1 kg with round shape</p>', '<p>Good Quality</p>', NULL, 1, NULL, NULL, NULL, '2020-06-19 22:46:03', '2020-06-20 16:24:15', NULL),
(351, 'VC-351', '43', 'New Strawberry Cake', 'new-strawberry-cake', '3', '158', NULL, NULL, NULL, 'in_stock', '<p>New Strawberry Cake</p>', '<p>1 kg with Round shape</p>', '<p>Good Quality</p>', NULL, 1, NULL, NULL, NULL, '2020-06-19 22:47:52', '2020-06-20 03:16:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `pi_id` int(10) UNSIGNED NOT NULL,
  `pi_product_id` int(11) NOT NULL,
  `pi_image_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pi_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`pi_id`, `pi_product_id`, `pi_image_name`, `pi_status`, `created_at`, `updated_at`) VALUES
(1, 3, '155846230620190419_131255.jpg', 'active', '2019-05-22 06:41:46', '2019-05-22 06:41:46'),
(2, 3, '155846255120190419_131255.jpg', 'active', '2019-05-22 06:45:51', '2019-05-22 06:45:51'),
(3, 5, '155846284220190419_180417.jpg', 'active', '2019-05-22 06:50:42', '2019-05-22 06:50:42'),
(252, 146, '1560008116792C036D-1418-4C24-887A-16773CB60F77.jpeg', 'active', '2019-06-09 04:05:17', '2019-06-09 04:05:17'),
(5, 7, '1558487040VDC_CHK_0010.jpg', 'active', '2019-05-22 13:34:00', '2019-05-22 13:34:00'),
(6, 7, '1558487632VDC_CHK_0020.jpg', 'active', '2019-05-22 13:43:52', '2019-05-22 13:43:52'),
(7, 8, '1558529326Blueberry cake.jpg', 'active', '2019-05-23 01:18:50', '2019-05-23 01:18:50'),
(8, 9, '1558529470Rich chocolate cake.jpg', 'active', '2019-05-23 01:21:10', '2019-05-23 01:21:10'),
(9, 10, '1558529583Chocolate truffle cake.jpg', 'active', '2019-05-23 01:23:03', '2019-05-23 01:23:03'),
(10, 11, '1558529752Blackforest cake.jpg', 'active', '2019-05-23 01:25:52', '2019-05-23 01:25:52'),
(11, 12, '1558529821Pineapple cake.jpg', 'active', '2019-05-23 01:27:01', '2019-05-23 01:27:01'),
(12, 13, '1558529871Mango cake.jpg', 'active', '2019-05-23 01:27:51', '2019-05-23 01:27:51'),
(13, 14, '1558529953Strawberry chocolate cake.jpg', 'active', '2019-05-23 01:29:13', '2019-05-23 01:29:13'),
(14, 15, '1558530077Vanila cake.jpg', 'active', '2019-05-23 01:31:17', '2019-05-23 01:31:17'),
(15, 16, '1558530189Choco vanilla cake.jpg', 'active', '2019-05-23 01:33:09', '2019-05-23 01:33:09'),
(16, 17, '1558530703Guava cake.jpg', 'active', '2019-05-23 01:41:43', '2019-05-23 01:41:43'),
(17, 18, '1558530785Butterscotch cake.jpg', 'active', '2019-05-23 01:43:05', '2019-05-23 01:43:05'),
(123, 19, '1558668537chocolate.jpg', 'active', '2019-05-24 15:58:57', '2019-05-24 15:58:57'),
(19, 20, '1558535217139.jpg', 'active', '2019-05-23 02:56:58', '2019-05-23 02:56:58'),
(20, 20, '1558535218140.jpg', 'active', '2019-05-23 02:56:58', '2019-05-23 02:56:58'),
(23, 23, '1558536174131.jpg', 'active', '2019-05-23 03:12:54', '2019-05-23 03:12:54'),
(24, 23, '1558536174132.jpg', 'active', '2019-05-23 03:12:54', '2019-05-23 03:12:54'),
(25, 24, '1558536346129.jpg', 'active', '2019-05-23 03:15:46', '2019-05-23 03:15:46'),
(26, 24, '1558536346130.jpg', 'active', '2019-05-23 03:15:46', '2019-05-23 03:15:46'),
(27, 25, '1558537428red velvet cream cake.jpg', 'active', '2019-05-23 03:33:48', '2019-05-23 03:33:48'),
(28, 26, '1558537514cookie cream cake.jpg', 'active', '2019-05-23 03:35:14', '2019-05-23 03:35:14'),
(29, 27, '1558537671white chocolate.jpg', 'active', '2019-05-23 03:37:51', '2019-05-23 03:37:51'),
(30, 28, '1558537787chocolate drip cake.jpg', 'active', '2019-05-23 03:39:47', '2019-05-23 03:39:47'),
(31, 29, '1558537907oreo drip cake.jpg', 'active', '2019-05-23 03:41:47', '2019-05-23 03:41:47'),
(32, 30, '1558538069Ferrero Rocher cake (2).jpg', 'active', '2019-05-23 03:44:29', '2019-05-23 03:44:29'),
(33, 31, '1558538075151.jpg', 'active', '2019-05-23 03:44:36', '2019-05-23 03:44:36'),
(34, 32, '1558538178caramel drip cake.jpg', 'active', '2019-05-23 03:46:18', '2019-05-23 03:46:18'),
(35, 33, '1558538511oreo cake.jpg', 'active', '2019-05-23 03:51:51', '2019-05-23 03:51:51'),
(36, 34, '1558538630pista cake.jpg', 'active', '2019-05-23 03:53:50', '2019-05-23 03:53:50'),
(37, 35, '1558538780shopping cakee.jpeg', 'active', '2019-05-23 03:56:20', '2019-05-23 03:56:20'),
(38, 36, '1558538871diva cake 2.jpg', 'active', '2019-05-23 03:57:51', '2019-05-23 03:57:51'),
(39, 37, '1558538910foot ball cake.jpg', 'active', '2019-05-23 03:58:30', '2019-05-23 03:58:30'),
(40, 38, '1558539027valentine cake.png', 'active', '2019-05-23 04:00:27', '2019-05-23 04:00:27'),
(41, 39, '1558539395Heart cake.jpg', 'active', '2019-05-23 04:06:35', '2019-05-23 04:06:35'),
(42, 40, '1558539560rainbow-cake.jpg', 'active', '2019-05-23 04:09:20', '2019-05-23 04:09:20'),
(43, 41, '1558539624white vancho cake.jpg', 'active', '2019-05-23 04:10:24', '2019-05-23 04:10:24'),
(44, 42, '1558539744White Chocolate Cake.jpg', 'active', '2019-05-23 04:12:24', '2019-05-23 04:12:24'),
(45, 43, '155854292660.jpg', 'active', '2019-05-23 05:05:26', '2019-05-23 05:05:26'),
(46, 43, '155854292661.jpg', 'active', '2019-05-23 05:05:26', '2019-05-23 05:05:26'),
(47, 44, '1558543415144.jpg', 'active', '2019-05-23 05:13:35', '2019-05-23 05:13:35'),
(48, 45, '1558543728145.jpg', 'active', '2019-05-23 05:18:48', '2019-05-23 05:18:48'),
(49, 46, '1558543932174.jpg', 'active', '2019-05-23 05:22:12', '2019-05-23 05:22:12'),
(50, 46, '1558543932175.jpg', 'active', '2019-05-23 05:22:12', '2019-05-23 05:22:12'),
(51, 47, '1558544231149.jpg', 'active', '2019-05-23 05:27:11', '2019-05-23 05:27:11'),
(52, 47, '1558544231150.jpg', 'active', '2019-05-23 05:27:11', '2019-05-23 05:27:11'),
(53, 48, '1558544861146.jpg', 'active', '2019-05-23 05:37:41', '2019-05-23 05:37:41'),
(54, 48, '1558544861147.jpg', 'active', '2019-05-23 05:37:41', '2019-05-23 05:37:41'),
(55, 49, '1558545248148.jpg', 'active', '2019-05-23 05:44:08', '2019-05-23 05:44:08'),
(56, 50, '1558546004157.jpg', 'active', '2019-05-23 05:56:44', '2019-05-23 05:56:44'),
(57, 50, '1558546004158.jpg', 'active', '2019-05-23 05:56:44', '2019-05-23 05:56:44'),
(58, 51, '1558546480128.jpg', 'active', '2019-05-23 06:04:40', '2019-05-23 06:04:40'),
(59, 51, '1558546480141.jpg', 'active', '2019-05-23 06:04:40', '2019-05-23 06:04:40'),
(60, 51, '1558546480142.jpg', 'active', '2019-05-23 06:04:40', '2019-05-23 06:04:40'),
(61, 51, '1558546480143.jpg', 'active', '2019-05-23 06:04:41', '2019-05-23 06:04:41'),
(62, 52, '1558550276VDC_TRW_00010.jpg', 'active', '2019-05-23 07:07:56', '2019-05-23 07:07:56'),
(462, 229, '1564634965_5d426f5505073.jpg', 'active', '2019-08-01 10:19:25', '2019-08-01 10:19:25'),
(559, 55, '1565971352_5d56d3980def7.jpg', 'active', '2019-08-16 21:32:32', '2019-08-16 21:32:32'),
(454, 54, '1564632149_5d426455c809d.jpg', 'active', '2019-08-01 09:32:29', '2019-08-01 09:32:29'),
(459, 53, '1564634439_5d426d473c8f4.jpg', 'active', '2019-08-01 10:10:39', '2019-08-01 10:10:39'),
(67, 56, '1558590793117.jpg', 'active', '2019-05-23 18:23:13', '2019-05-23 18:23:13'),
(68, 56, '1558590793118.jpg', 'active', '2019-05-23 18:23:13', '2019-05-23 18:23:13'),
(461, 57, '1564634604_5d426dec34825.jpg', 'active', '2019-08-01 10:13:24', '2019-08-01 10:13:24'),
(70, 58, '1558592319119.jpg', 'active', '2019-05-23 18:48:39', '2019-05-23 18:48:39'),
(71, 58, '1558592319120.jpg', 'active', '2019-05-23 18:48:39', '2019-05-23 18:48:39'),
(72, 58, '1558592319121.jpg', 'active', '2019-05-23 18:48:39', '2019-05-23 18:48:39'),
(73, 59, '1558592535113.jpg', 'active', '2019-05-23 18:52:15', '2019-05-23 18:52:15'),
(74, 59, '1558592535114.jpg', 'active', '2019-05-23 18:52:15', '2019-05-23 18:52:15'),
(75, 59, '1558592535115.jpg', 'active', '2019-05-23 18:52:15', '2019-05-23 18:52:15'),
(76, 59, '1558592535116.jpg', 'active', '2019-05-23 18:52:15', '2019-05-23 18:52:15'),
(456, 61, '1564633949_5d426b5d4e3ba.jpg', 'active', '2019-08-01 10:02:29', '2019-08-01 10:02:29'),
(457, 61, '1564633951_5d426b5f86369.jpg', 'active', '2019-08-01 10:02:31', '2019-08-01 10:02:31'),
(79, 62, '1558593223122.jpg', 'active', '2019-05-23 19:03:43', '2019-05-23 19:03:43'),
(80, 62, '1558593223123.jpg', 'active', '2019-05-23 19:03:43', '2019-05-23 19:03:43'),
(81, 62, '1558593223124.jpg', 'active', '2019-05-23 19:03:43', '2019-05-23 19:03:43'),
(455, 63, '1564632614_5d42662667bb1.jpg', 'active', '2019-08-01 09:40:14', '2019-08-01 09:40:14'),
(494, 64, '1565023909_5d485ea552cca.jpg', 'active', '2019-08-05 22:21:49', '2019-08-05 22:21:49'),
(453, 65, '1564631556_5d426204bdbcb.jpg', 'active', '2019-08-01 09:22:36', '2019-08-01 09:22:36'),
(85, 66, '155859583720181122_073332.jpg', 'active', '2019-05-23 19:47:17', '2019-05-23 19:47:17'),
(451, 67, '1564630528_5d425e0039980.jpg', 'active', '2019-08-01 09:05:28', '2019-08-01 09:05:28'),
(463, 230, '1564849705_5d45b629e9ff6.jpg', 'active', '2019-08-03 21:58:26', '2019-08-03 21:58:26'),
(452, 69, '1564631211_5d4260ab2ee12.jpg', 'active', '2019-08-01 09:16:51', '2019-08-01 09:16:51'),
(89, 70, '15586169196460a2f4-0eda-48af-ba01-e7dda818f91b.JPG', 'active', '2019-05-24 01:38:39', '2019-05-24 01:38:39'),
(90, 70, '1558616919b512f138-ce49-4208-814c-9ba17183d6ff.JPG', 'active', '2019-05-24 01:38:39', '2019-05-24 01:38:39'),
(91, 71, '155863018714E44FFD-3465-4D24-BF7B-132F61175C1E.jpeg', 'active', '2019-05-24 05:19:52', '2019-05-24 05:19:52'),
(92, 72, '15586305051.jpg', 'active', '2019-05-24 05:25:05', '2019-05-24 05:25:05'),
(93, 72, '15586305052.jpg', 'active', '2019-05-24 05:25:05', '2019-05-24 05:25:05'),
(94, 72, '155863050511.jpg', 'active', '2019-05-24 05:25:05', '2019-05-24 05:25:05'),
(95, 72, '1558630505163.jpg', 'active', '2019-05-24 05:25:06', '2019-05-24 05:25:06'),
(96, 73, '1558630919B92C23EA-2CE8-49DF-8364-C1E3BC555C72.jpeg', 'active', '2019-05-24 05:31:59', '2019-05-24 05:31:59'),
(97, 74, '155863115622.jpg', 'active', '2019-05-24 05:35:56', '2019-05-24 05:35:56'),
(98, 74, '155863115623.jpg', 'active', '2019-05-24 05:35:56', '2019-05-24 05:35:56'),
(99, 75, '1558631270DECF0865-85D5-4B74-BA3C-1C0755FB9613.jpeg', 'active', '2019-05-24 05:37:51', '2019-05-24 05:37:51'),
(100, 76, '155863153457.jpg', 'active', '2019-05-24 05:42:14', '2019-05-24 05:42:14'),
(101, 76, '155863153458.jpg', 'active', '2019-05-24 05:42:14', '2019-05-24 05:42:14'),
(102, 76, '155863153459.jpg', 'active', '2019-05-24 05:42:14', '2019-05-24 05:42:14'),
(103, 77, '1558631591DAF11E4F-3E0A-4966-9E75-B654B195716B.jpeg', 'active', '2019-05-24 05:43:11', '2019-05-24 05:43:11'),
(104, 77, '15586315911D0460EC-3E40-45B9-A2D6-9E6D3E951482.jpeg', 'active', '2019-05-24 05:43:11', '2019-05-24 05:43:11'),
(105, 77, '155863159181A0FBD9-1649-47C6-99B3-49A5D6C63EB6.jpeg', 'active', '2019-05-24 05:43:12', '2019-05-24 05:43:12'),
(106, 77, '1558631592B14DAA9D-EA8B-40D8-8EBF-5EA8384EB8AE.jpeg', 'active', '2019-05-24 05:43:12', '2019-05-24 05:43:12'),
(107, 77, '1558631592656CDF44-0989-4367-83C8-8B4826198038.jpeg', 'active', '2019-05-24 05:43:12', '2019-05-24 05:43:12'),
(108, 78, '155863178188.jpg', 'active', '2019-05-24 05:46:21', '2019-05-24 05:46:21'),
(109, 78, '155863178189.jpg', 'active', '2019-05-24 05:46:21', '2019-05-24 05:46:21'),
(110, 78, '155863178191.jpg', 'active', '2019-05-24 05:46:22', '2019-05-24 05:46:22'),
(111, 79, '155863196292.jpg', 'active', '2019-05-24 05:49:22', '2019-05-24 05:49:22'),
(112, 79, '155863196293.jpg', 'active', '2019-05-24 05:49:22', '2019-05-24 05:49:22'),
(113, 79, '155863196294.jpg', 'active', '2019-05-24 05:49:22', '2019-05-24 05:49:22'),
(114, 73, '1558632194D3838DB9-B8C7-4FDE-9B62-1D15C30F9FE7.jpeg', 'active', '2019-05-24 05:53:14', '2019-05-24 05:53:14'),
(115, 73, '155863219430D6431F-04DD-4D85-B1E2-7FD5B372FFFE.jpeg', 'active', '2019-05-24 05:53:14', '2019-05-24 05:53:14'),
(116, 73, '1558632194ADD2AB4C-85E7-4ABA-BC99-687952A73BE5.jpeg', 'active', '2019-05-24 05:53:14', '2019-05-24 05:53:14'),
(117, 80, '1558632298172.jpg', 'active', '2019-05-24 05:54:58', '2019-05-24 05:54:58'),
(118, 80, '1558632298173.jpg', 'active', '2019-05-24 05:54:58', '2019-05-24 05:54:58'),
(119, 81, '1558632548187.jpg', 'active', '2019-05-24 05:59:08', '2019-05-24 05:59:08'),
(120, 82, '1558633178images.jpg', 'active', '2019-05-24 06:09:38', '2019-05-24 06:09:38'),
(121, 82, '1558633178silvercoin._10-gm-ag-999-pure-silver-coin.jpg', 'active', '2019-05-24 06:09:38', '2019-05-24 06:09:38'),
(122, 82, '1558633178s-l300.jpg', 'active', '2019-05-24 06:09:38', '2019-05-24 06:09:38'),
(124, 19, '1558668537Chocolates-Gift.jpg', 'active', '2019-05-24 15:58:58', '2019-05-24 15:58:58'),
(126, 83, '1558671515E252CA56-E429-4DC5-AF6B-6E129EC170DE.jpeg', 'active', '2019-05-24 16:48:35', '2019-05-24 16:48:35'),
(127, 83, '1558671515A082C232-D1EF-43F2-9155-65AC6F77144F.jpeg', 'active', '2019-05-24 16:48:35', '2019-05-24 16:48:35'),
(128, 83, '1558671515713E1959-7BDE-4F30-A0C2-327BECC72A53.jpeg', 'active', '2019-05-24 16:48:35', '2019-05-24 16:48:35'),
(129, 83, '1558671515762C3584-610E-4E3B-95B8-2A09B2D1D42B.jpeg', 'active', '2019-05-24 16:48:35', '2019-05-24 16:48:35'),
(130, 84, '15586722225E9E2F9E-DF43-49D8-A96D-2E146D540E63.jpeg', 'active', '2019-05-24 17:00:22', '2019-05-24 17:00:22'),
(131, 84, '1558672382CA0E442E-3AB3-476B-925F-295DAC3E63E2.jpeg', 'active', '2019-05-24 17:03:02', '2019-05-24 17:03:02'),
(132, 85, '15586902343C0E8E63-B5CA-4849-B14C-381FC938B06D.jpeg', 'active', '2019-05-24 22:00:35', '2019-05-24 22:00:35'),
(133, 85, '1558690235147D986C-D2BA-4DF5-804A-7E7C9D95049F.jpeg', 'active', '2019-05-24 22:00:35', '2019-05-24 22:00:35'),
(134, 85, '1558690235CBF53BA3-2B6F-4330-99A3-232083FF9042.jpeg', 'active', '2019-05-24 22:00:35', '2019-05-24 22:00:35'),
(135, 86, '155869728882F80C1A-724E-43FC-8123-358402DE93FF.jpeg', 'active', '2019-05-24 23:58:13', '2019-05-24 23:58:13'),
(136, 87, '155878488044948A9F-7CA7-45AF-B094-102BF6E8B1F7.jpeg', 'active', '2019-05-26 00:18:01', '2019-05-26 00:18:01'),
(137, 88, '155878595381F0E7A9-B7EF-4A6E-8A10-77359C15FCEE.jpeg', 'active', '2019-05-26 00:35:54', '2019-05-26 00:35:54'),
(138, 89, '15587862508AEDDA06-D5A2-48F6-8CBB-4D74E1917B95.jpeg', 'active', '2019-05-26 00:40:51', '2019-05-26 00:40:51'),
(139, 90, '1558812020Foxtail Millets.jpg', 'active', '2019-05-26 07:50:21', '2019-05-26 07:50:21'),
(140, 90, '1558812021foxtal jgg.jpeg', 'active', '2019-05-26 07:50:21', '2019-05-26 07:50:21'),
(141, 90, '1558812021foxtal1.jpeg', 'active', '2019-05-26 07:50:21', '2019-05-26 07:50:21'),
(142, 90, '1558812021foxtal3.jpeg', 'active', '2019-05-26 07:50:21', '2019-05-26 07:50:21'),
(143, 91, '1558880552DE080699-00CA-4757-A252-8CAFD4CEA727.jpeg', 'active', '2019-05-27 02:52:33', '2019-05-27 02:52:33'),
(144, 91, '1558880553CF7BC935-AB0B-41EF-9C66-57E90667AF6F.jpeg', 'active', '2019-05-27 02:52:34', '2019-05-27 02:52:34'),
(145, 91, '15588805549AB5609B-1E18-44C2-8A90-87D0BDC717A2.jpeg', 'active', '2019-05-27 02:52:34', '2019-05-27 02:52:34'),
(146, 92, '15588809660A7145F0-0642-4B3A-9F3C-170E27D17D9A.jpeg', 'active', '2019-05-27 02:59:26', '2019-05-27 02:59:26'),
(147, 92, '15588809662E12C684-A3AB-48D4-B3D6-14720A073715.jpeg', 'active', '2019-05-27 02:59:26', '2019-05-27 02:59:26'),
(148, 92, '155888096622A5ABFB-9E29-4591-9E61-8E04B412E752.jpeg', 'active', '2019-05-27 02:59:26', '2019-05-27 02:59:26'),
(149, 93, '1558881949910092AB-E92C-4EF5-A4EB-368D3E7B154F.jpeg', 'active', '2019-05-27 03:15:49', '2019-05-27 03:15:49'),
(150, 93, '1558881949B2D4CE58-8258-47E3-8689-4E450D43F007.jpeg', 'active', '2019-05-27 03:15:49', '2019-05-27 03:15:49'),
(151, 93, '1558881949D331520A-9B3D-4BB3-BC79-46DCF8E49AA7.jpeg', 'active', '2019-05-27 03:15:49', '2019-05-27 03:15:49'),
(152, 93, '15588819492B16BBE2-B612-4F3B-B22F-A44DFE87907C.jpeg', 'active', '2019-05-27 03:15:49', '2019-05-27 03:15:49'),
(153, 93, '15588819495863E8D3-4A55-4E83-98FD-85DB1BBFBAF9.jpeg', 'active', '2019-05-27 03:15:49', '2019-05-27 03:15:49'),
(154, 94, '15588824063A04B8BC-7380-430B-BAB1-A30858117D7F.jpeg', 'active', '2019-05-27 03:23:26', '2019-05-27 03:23:26'),
(155, 94, '155888240672A9279B-AFB5-4E85-8803-E913B1008FA5.jpeg', 'active', '2019-05-27 03:23:27', '2019-05-27 03:23:27'),
(156, 94, '15588824074580076E-73CA-43EC-A12C-AE2AAFC7791D.jpeg', 'active', '2019-05-27 03:23:27', '2019-05-27 03:23:27'),
(157, 94, '155888240745BACF46-D83F-4E7F-BBF6-B83CBE997571.jpeg', 'active', '2019-05-27 03:23:27', '2019-05-27 03:23:27'),
(158, 95, '1558890547Cotton Saree1.jpg', 'active', '2019-05-27 05:39:08', '2019-05-27 05:39:08'),
(159, 96, '1558948281chock15.jpg', 'active', '2019-05-27 21:41:21', '2019-05-27 21:41:21'),
(161, 96, '1558948464chock15.jpg', 'active', '2019-05-27 21:44:24', '2019-05-27 21:44:24'),
(162, 96, '1558948476chock20.jpg', 'active', '2019-05-27 21:44:36', '2019-05-27 21:44:36'),
(163, 19, '1558948903chock13.jpg', 'active', '2019-05-27 21:51:43', '2019-05-27 21:51:43'),
(164, 19, '1558948914chock09.jpg', 'active', '2019-05-27 21:51:54', '2019-05-27 21:51:54'),
(165, 19, '1558948919chock10.jpg', 'active', '2019-05-27 21:51:59', '2019-05-27 21:51:59'),
(166, 96, '1558948959chock19.jpg', 'active', '2019-05-27 21:52:39', '2019-05-27 21:52:39'),
(167, 96, '1558948959chock20.jpg', 'active', '2019-05-27 21:52:39', '2019-05-27 21:52:39'),
(168, 97, '1558950000cake01.jpg', 'active', '2019-05-27 22:10:00', '2019-05-27 22:10:00'),
(169, 97, '1558950000cake02.jpg', 'active', '2019-05-27 22:10:00', '2019-05-27 22:10:00'),
(170, 97, '1558950000cake03.jpg', 'active', '2019-05-27 22:10:00', '2019-05-27 22:10:00'),
(171, 97, '1558950000cake04.jpg', 'active', '2019-05-27 22:10:00', '2019-05-27 22:10:00'),
(172, 97, '1558950000cake05.jpg', 'active', '2019-05-27 22:10:00', '2019-05-27 22:10:00'),
(270, 153, '15601439454D62283A-1388-4ABD-9064-4666C1DC9535.jpeg', 'active', '2019-06-10 17:49:06', '2019-06-10 17:49:06'),
(174, 99, '1559062856kodomillets.jpg', 'active', '2019-05-29 05:30:59', '2019-05-29 05:30:59'),
(175, 100, '1559062967barnyardmillet.jpg', 'active', '2019-05-29 05:32:47', '2019-05-29 05:32:47'),
(176, 101, '1559063146littlemillet.jpg', 'active', '2019-05-29 05:35:46', '2019-05-29 05:35:46'),
(177, 102, '1559063421brownmillet.jpg', 'active', '2019-05-29 05:40:21', '2019-05-29 05:40:21'),
(178, 103, '15590639069.jpg', 'active', '2019-05-29 05:48:27', '2019-05-29 05:48:27'),
(179, 104, '155906403410.jpg', 'active', '2019-05-29 05:50:34', '2019-05-29 05:50:34'),
(180, 105, '15590641541.jpg', 'active', '2019-05-29 05:52:35', '2019-05-29 05:52:35'),
(279, 106, '1560335398flower10.jpg', 'active', '2019-06-12 22:59:59', '2019-06-12 22:59:59'),
(182, 107, '15591552221D61C00E-04C5-4C86-B10C-1302764B1725.jpeg', 'active', '2019-05-30 07:10:22', '2019-05-30 07:10:22'),
(183, 107, '15591552227BEBC56E-7E87-4FBB-9AEF-1F014F5C1AA7.jpeg', 'active', '2019-05-30 07:10:22', '2019-05-30 07:10:22'),
(184, 108, '1559155379550E88DD-0CB8-41F7-947D-D93D8F1FC466.jpeg', 'active', '2019-05-30 07:13:00', '2019-05-30 07:13:00'),
(185, 108, '15591553803CC6FA83-4AAE-4C9F-B123-E3D3C9039B0C.jpeg', 'active', '2019-05-30 07:13:00', '2019-05-30 07:13:00'),
(186, 108, '1559155380D0239C74-B584-4958-9100-E5CDE6E8BEF1.jpeg', 'active', '2019-05-30 07:13:00', '2019-05-30 07:13:00'),
(187, 109, '1559155623554C4F02-21C6-494E-AB85-F60E818DEE5D.jpeg', 'active', '2019-05-30 07:17:03', '2019-05-30 07:17:03'),
(188, 109, '15591556235701F314-6F47-4462-96C2-24AB105AE194.jpeg', 'active', '2019-05-30 07:17:03', '2019-05-30 07:17:03'),
(189, 109, '155915562372F3D639-F72A-41C0-B88E-668342914DF9.jpeg', 'active', '2019-05-30 07:17:03', '2019-05-30 07:17:03'),
(190, 109, '1559155623771B3C91-CB1B-4DAB-88EB-0E3AAE70FE0B.jpeg', 'active', '2019-05-30 07:17:03', '2019-05-30 07:17:03'),
(191, 110, '1559155797EA962CB1-BFA6-43F1-9BC2-8D043BD10B61.jpeg', 'active', '2019-05-30 07:19:57', '2019-05-30 07:19:57'),
(192, 110, '15591557974A1493C9-1A1B-484D-8B7E-1CC795DFCB60.jpeg', 'active', '2019-05-30 07:19:57', '2019-05-30 07:19:57'),
(193, 111, '155915603486B8A17E-E901-4A2B-B70E-537DF6C73985.jpeg', 'active', '2019-05-30 07:23:54', '2019-05-30 07:23:54'),
(194, 111, '1559156034B45EF734-C0AB-4177-B3CE-0FDF94A007CB.jpeg', 'active', '2019-05-30 07:23:54', '2019-05-30 07:23:54'),
(195, 111, '155915603465201AF9-1B9E-4280-96B5-03AD5BA46BA0.jpeg', 'active', '2019-05-30 07:23:55', '2019-05-30 07:23:55'),
(196, 111, '15591560355AF68062-518F-49DD-9293-D0A7CD73E759.jpeg', 'active', '2019-05-30 07:23:55', '2019-05-30 07:23:55'),
(197, 111, '155915603595024D89-1081-4C43-AD45-63CB25BC009A.jpeg', 'active', '2019-05-30 07:23:55', '2019-05-30 07:23:55'),
(198, 112, '1559224924712F72AE-2938-4E62-AA98-4492C3A21AC8.jpeg', 'active', '2019-05-31 02:32:04', '2019-05-31 02:32:04'),
(199, 112, '1559224924D4441380-32C6-4920-BF1A-C8DCD469A14F.jpeg', 'active', '2019-05-31 02:32:04', '2019-05-31 02:32:04'),
(200, 112, '1559224924A06C24AC-422C-4F46-8BF4-1B9FC237925B.jpeg', 'active', '2019-05-31 02:32:04', '2019-05-31 02:32:04'),
(201, 112, '1559224924B1163F3C-EEFB-4A37-8F80-E1582D9BA44B.jpeg', 'active', '2019-05-31 02:32:04', '2019-05-31 02:32:04'),
(202, 112, '155922492476C9153F-1035-49C4-917C-21BDBD7BCE27.jpeg', 'active', '2019-05-31 02:32:05', '2019-05-31 02:32:05'),
(203, 113, '1559225639FB9BF900-6478-4D01-9C53-6101DEF4D12E.jpeg', 'active', '2019-05-31 02:43:59', '2019-05-31 02:43:59'),
(204, 113, '1559225639FBFC1055-51CB-4ACF-8A06-286F4617BBFE.jpeg', 'active', '2019-05-31 02:43:59', '2019-05-31 02:43:59'),
(205, 114, '1559360611B06CABCD-6920-451F-9094-C62AFD84BC11.jpeg', 'active', '2019-06-01 16:13:32', '2019-06-01 16:13:32'),
(206, 114, '1559360612E1F0EB0E-3A90-4DF4-8C4B-04CAC0341FA1.jpeg', 'active', '2019-06-01 16:13:32', '2019-06-01 16:13:32'),
(207, 114, '155936061226BF1EA3-0F30-401B-ADD5-5FC082DA13AA.jpeg', 'active', '2019-06-01 16:13:32', '2019-06-01 16:13:32'),
(208, 115, '155936155455E077D1-F0A9-4020-84AC-91677A146895.jpeg', 'active', '2019-06-01 16:29:14', '2019-06-01 16:29:14'),
(209, 116, '1559396597695F6F59-AABA-49CA-9AB4-27051C3CE736.jpeg', 'active', '2019-06-02 02:13:17', '2019-06-02 02:13:17'),
(210, 116, '1559396597274706CE-E40A-48BF-AC15-5A40D8016A47.jpeg', 'active', '2019-06-02 02:13:17', '2019-06-02 02:13:17'),
(211, 116, '1559396597143DEB1A-D7D3-41EA-B6A4-8A47D5B67445.jpeg', 'active', '2019-06-02 02:13:17', '2019-06-02 02:13:17'),
(212, 117, '1559397498C465BB52-5955-493F-B18E-6418153658B7.jpeg', 'active', '2019-06-02 02:28:18', '2019-06-02 02:28:18'),
(213, 118, '15593977851401FA5E-A365-4FA0-880F-66FF060D2F74.jpeg', 'active', '2019-06-02 02:33:06', '2019-06-02 02:33:06'),
(214, 118, '1559397786D211C111-F292-4189-9A4A-F1DCCF3ABAE8.jpeg', 'active', '2019-06-02 02:33:06', '2019-06-02 02:33:06'),
(215, 118, '1559397786EAAB6247-1480-4BDD-90A4-2362BC5995F6.jpeg', 'active', '2019-06-02 02:33:06', '2019-06-02 02:33:06'),
(216, 119, '1559398101B1D8344F-9C2F-4820-A9A3-33962EB77380.jpeg', 'active', '2019-06-02 02:38:21', '2019-06-02 02:38:21'),
(217, 119, '15593981018EF8B3A7-CB4C-4985-8FB0-3F14684A1B1D.jpeg', 'active', '2019-06-02 02:38:21', '2019-06-02 02:38:21'),
(218, 119, '15593981012EA22CDF-64A9-4234-8C97-3905CE4C6109.jpeg', 'active', '2019-06-02 02:38:21', '2019-06-02 02:38:21'),
(219, 119, '155939810181ED2954-CE41-4B9E-A94D-8806B526BAF8.jpeg', 'active', '2019-06-02 02:38:21', '2019-06-02 02:38:21'),
(220, 119, '1559398101281858E7-BA36-4048-BD97-0C179715786F.jpeg', 'active', '2019-06-02 02:38:22', '2019-06-02 02:38:22'),
(221, 120, '15593982787A45D051-B1E9-4F0E-8B5F-CD7A2403E0BC.jpeg', 'active', '2019-06-02 02:41:18', '2019-06-02 02:41:18'),
(222, 121, '15594026867FF2B8C9-2DC7-43CD-A1A3-AEE4F04ADD8F.jpeg', 'active', '2019-06-02 03:54:46', '2019-06-02 03:54:46'),
(223, 122, '1559403200B7436158-89BA-479D-9CD5-3A9E752C9BE0.jpeg', 'active', '2019-06-02 04:03:20', '2019-06-02 04:03:20'),
(224, 122, '1559403200696ED552-0E4D-4D53-8091-65A6C166F970.jpeg', 'active', '2019-06-02 04:03:20', '2019-06-02 04:03:20'),
(233, 127, '1559694186fatherday-img02.jpg', 'active', '2019-06-05 12:53:06', '2019-06-05 12:53:06'),
(232, 126, '1559694105fatherday-img01.jpg', 'active', '2019-06-05 12:51:46', '2019-06-05 12:51:46'),
(231, 125, '1559499014Cake-2.jpg', 'active', '2019-06-03 06:40:14', '2019-06-03 06:40:14'),
(234, 128, '1559694297fatherday-img03.jpg', 'active', '2019-06-05 12:54:58', '2019-06-05 12:54:58'),
(235, 129, '1559694367fatherday-img04.jpg', 'active', '2019-06-05 12:56:07', '2019-06-05 12:56:07'),
(236, 130, '1559694499fatherday-img05.jpg', 'active', '2019-06-05 12:58:19', '2019-06-05 12:58:19'),
(237, 131, '1559694556fatherday-img06.jpg', 'active', '2019-06-05 12:59:16', '2019-06-05 12:59:16'),
(238, 132, '15598857981.jpg', 'active', '2019-06-07 18:06:38', '2019-06-07 18:06:38'),
(239, 133, '15598857991.jpg', 'active', '2019-06-07 18:06:39', '2019-06-07 18:06:39'),
(240, 134, '15598858692.jpg', 'active', '2019-06-07 18:07:49', '2019-06-07 18:07:49'),
(241, 135, '15598859013.jpg', 'active', '2019-06-07 18:08:21', '2019-06-07 18:08:21'),
(242, 136, '15598859334.jpg', 'active', '2019-06-07 18:08:53', '2019-06-07 18:08:53'),
(243, 137, '15598859585.jpg', 'active', '2019-06-07 18:09:18', '2019-06-07 18:09:18'),
(244, 138, '15598859876.jpg', 'active', '2019-06-07 18:09:47', '2019-06-07 18:09:47'),
(245, 139, '15598860217.jpg', 'active', '2019-06-07 18:10:21', '2019-06-07 18:10:21'),
(246, 140, '15598860508.jpg', 'active', '2019-06-07 18:10:50', '2019-06-07 18:10:50'),
(247, 141, '15598860889.jpg', 'active', '2019-06-07 18:11:28', '2019-06-07 18:11:28'),
(248, 142, '155988611810.jpg', 'active', '2019-06-07 18:11:58', '2019-06-07 18:11:58'),
(249, 143, '155988615111.jpg', 'active', '2019-06-07 18:12:31', '2019-06-07 18:12:31'),
(250, 144, '155988619212.jpg', 'active', '2019-06-07 18:13:12', '2019-06-07 18:13:12'),
(251, 145, '1559931832A831AB1E-B8AF-4D9B-AE0E-1F5D050E5327.jpeg', 'active', '2019-06-08 06:53:52', '2019-06-08 06:53:52'),
(253, 146, '1560008117B6B12E5D-A92A-4162-A8A9-8ADC3BAF49CB.jpeg', 'active', '2019-06-09 04:05:17', '2019-06-09 04:05:17'),
(254, 146, '1560008117347D2C3C-53B6-452D-8135-2FF0D8B6CCB9.jpeg', 'active', '2019-06-09 04:05:17', '2019-06-09 04:05:17'),
(255, 146, '15600081177BB90CA9-E086-475B-9FB3-AA979FD3BD71.jpeg', 'active', '2019-06-09 04:05:17', '2019-06-09 04:05:17'),
(256, 147, '15600090604CA07757-0F84-4628-B5F7-DA23CF486281.jpeg', 'active', '2019-06-09 04:21:00', '2019-06-09 04:21:00'),
(257, 147, '1560009060BEFE8CF4-DD87-4053-AD54-007D55C4D567.jpeg', 'active', '2019-06-09 04:21:00', '2019-06-09 04:21:00'),
(258, 147, '1560009060FA1B7735-B156-4353-BDF7-8A832E3AD07E.jpeg', 'active', '2019-06-09 04:21:01', '2019-06-09 04:21:01'),
(259, 147, '1560009061D23311F2-70B6-430B-8409-EA0AA5595B46.jpeg', 'active', '2019-06-09 04:21:01', '2019-06-09 04:21:01'),
(329, 183, '1561264137cake03.jpg', 'active', '2019-06-23 09:58:57', '2019-06-23 09:58:57'),
(261, 148, '156000929946A30945-F815-4AFC-ABB7-732B7157629F.jpeg', 'active', '2019-06-09 04:24:59', '2019-06-09 04:24:59'),
(262, 148, '1560009299F9DCF094-A94D-42AF-B842-5FD919313A76.jpeg', 'active', '2019-06-09 04:24:59', '2019-06-09 04:24:59'),
(263, 148, '156000929989C648C0-4C0F-42FA-9A7B-0D5E37939AEC.jpeg', 'active', '2019-06-09 04:24:59', '2019-06-09 04:24:59'),
(264, 148, '156000929993EFB288-8F61-42AF-BA6C-AE695FD7C231.jpeg', 'active', '2019-06-09 04:24:59', '2019-06-09 04:24:59'),
(265, 7, '15601111931.jpg', 'active', '2019-06-10 08:43:13', '2019-06-10 08:43:13'),
(266, 7, '15601111934.jpg', 'active', '2019-06-10 08:43:13', '2019-06-10 08:43:13'),
(267, 7, '15601111935.jpg', 'active', '2019-06-10 08:43:13', '2019-06-10 08:43:13'),
(268, 150, '156011168410 blue orchids 1000.jpeg', 'active', '2019-06-10 08:51:24', '2019-06-10 08:51:24'),
(269, 151, '156011200910 colour roses 450.jpeg', 'active', '2019-06-10 08:56:50', '2019-06-10 08:56:50'),
(271, 153, '15601439462DD4856A-191D-4B2A-A7CF-6C9E107E1093.jpeg', 'active', '2019-06-10 17:49:06', '2019-06-10 17:49:06'),
(272, 153, '156014394686F057AD-7FCA-442A-BA06-A27A8B099E13.jpeg', 'active', '2019-06-10 17:49:06', '2019-06-10 17:49:06'),
(273, 154, '1560148200681FEEC6-C587-4FB8-B809-6AD54B885456.jpeg', 'active', '2019-06-10 19:00:00', '2019-06-10 19:00:00'),
(274, 154, '156014820096D66159-1EA3-4ACE-B0C6-29854C590203.jpeg', 'active', '2019-06-10 19:00:00', '2019-06-10 19:00:00'),
(275, 154, '15601482003C37C689-3B35-493D-9FBC-3A09007DEF9D.jpeg', 'active', '2019-06-10 19:00:00', '2019-06-10 19:00:00'),
(285, 160, '1560336496cake11.jpg', 'active', '2019-06-12 23:18:16', '2019-06-12 23:18:16'),
(284, 160, '1560336485cake20.jpg', 'active', '2019-06-12 23:18:05', '2019-06-12 23:18:05'),
(327, 183, '1561264137cake01.jpg', 'active', '2019-06-23 09:58:57', '2019-06-23 09:58:57'),
(287, 162, '1560339294cake04.jpg', 'active', '2019-06-13 00:04:54', '2019-06-13 00:04:54'),
(288, 162, '1560339294cake05.jpg', 'active', '2019-06-13 00:04:54', '2019-06-13 00:04:54'),
(289, 162, '1560339294cake06.jpg', 'active', '2019-06-13 00:04:55', '2019-06-13 00:04:55'),
(290, 162, '1560339295cake07.jpg', 'active', '2019-06-13 00:04:55', '2019-06-13 00:04:55'),
(301, 166, '15609392087.jpg', 'active', '2019-06-19 15:43:28', '2019-06-19 15:43:28'),
(300, 166, '15609392086.jpg', 'active', '2019-06-19 15:43:28', '2019-06-19 15:43:28'),
(337, 184, '1561553139world-environment-day.jpg', 'active', '2019-06-26 18:15:40', '2019-06-26 18:15:40'),
(330, 183, '1561264137cake04.jpg', 'active', '2019-06-23 09:58:58', '2019-06-23 09:58:58'),
(328, 183, '1561264137cake02.jpg', 'active', '2019-06-23 09:58:57', '2019-06-23 09:58:57'),
(302, 167, '1561006990cake05.jpg', 'active', '2019-06-20 10:33:10', '2019-06-20 10:33:10'),
(303, 167, '1561006990cake06.jpg', 'active', '2019-06-20 10:33:10', '2019-06-20 10:33:10'),
(304, 167, '1561006990cake07.jpg', 'active', '2019-06-20 10:33:10', '2019-06-20 10:33:10'),
(305, 167, '1561006990cake08.jpg', 'active', '2019-06-20 10:33:10', '2019-06-20 10:33:10'),
(306, 167, '1561006990cake09.jpg', 'active', '2019-06-20 10:33:10', '2019-06-20 10:33:10'),
(336, 168, '1561548425world-environment-day.jpg', 'active', '2019-06-26 16:57:06', '2019-06-26 16:57:06'),
(310, 169, '156122649010 orange carnations 550.jpeg', 'active', '2019-06-22 23:31:30', '2019-06-22 23:31:30'),
(311, 170, '156122655610 pink and white carnations 550.jpeg', 'active', '2019-06-22 23:32:36', '2019-06-22 23:32:36'),
(312, 171, '156122663410 pink carnations 550.jpeg', 'active', '2019-06-22 23:33:54', '2019-06-22 23:33:54'),
(313, 172, '156122671210-purple-orchids-1000.jpg', 'active', '2019-06-22 23:35:12', '2019-06-22 23:35:12'),
(314, 171, '156122675710-pink-carnations-550.jpg', 'active', '2019-06-22 23:35:57', '2019-06-22 23:35:57'),
(315, 173, '156122686210-red-and-white-carnations-550.jpg', 'active', '2019-06-22 23:37:42', '2019-06-22 23:37:42'),
(316, 174, '156122693510-red-roses-450.jpg', 'active', '2019-06-22 23:38:55', '2019-06-22 23:38:55'),
(317, 175, '156122701010-white-carnations-550.jpg', 'active', '2019-06-22 23:40:10', '2019-06-22 23:40:10'),
(318, 176, '156122708910-white-orchids-1000.jpg', 'active', '2019-06-22 23:41:29', '2019-06-22 23:41:29'),
(319, 177, '156122714310-white-roses-450.jpg', 'active', '2019-06-22 23:42:23', '2019-06-22 23:42:23'),
(320, 178, '156122720910-yellow-roses-450.jpg', 'active', '2019-06-22 23:43:29', '2019-06-22 23:43:29'),
(321, 179, '156122727212-orange-lilly-1350.jpg', 'active', '2019-06-22 23:44:32', '2019-06-22 23:44:32'),
(322, 180, '156122734912-pink-lilly-1350.jpg', 'active', '2019-06-22 23:45:49', '2019-06-22 23:45:49'),
(323, 181, '156122740412-white-lilly-1350.jpg', 'active', '2019-06-22 23:46:44', '2019-06-22 23:46:44'),
(326, 182, '156123087312-yellow-lilly-1350.jpg', 'active', '2019-06-23 00:44:33', '2019-06-23 00:44:33'),
(338, 186, '15617004661517650670.png', 'active', '2019-06-28 11:11:07', '2019-06-28 11:11:07'),
(339, 187, '1561825098B026CC28-A4B3-463F-8BA1-A605ACCA9136.jpeg', 'active', '2019-06-29 21:48:18', '2019-06-29 21:48:18'),
(340, 188, '156205702585CE1223-8E2E-48C2-810F-68DD8A41BC6D.jpeg', 'active', '2019-07-02 14:13:46', '2019-07-02 14:13:46'),
(341, 188, '1562057026F8AFD719-AC42-46BF-9BE1-803A4E746B16.jpeg', 'active', '2019-07-02 14:13:46', '2019-07-02 14:13:46'),
(342, 188, '15620570261F26E2C6-7BC3-40FD-B079-988D4DC18821.jpeg', 'active', '2019-07-02 14:13:46', '2019-07-02 14:13:46'),
(343, 189, '1562057265273EEB29-DE99-41E3-86B4-53EFA3172141.jpeg', 'active', '2019-07-02 14:17:45', '2019-07-02 14:17:45'),
(344, 189, '1562057265A6C04975-0C84-452E-BA81-32793B6B4C03.jpeg', 'active', '2019-07-02 14:17:45', '2019-07-02 14:17:45'),
(345, 189, '156205726544CA02F3-7F21-4B08-8992-171861D7BBFC.jpeg', 'active', '2019-07-02 14:17:45', '2019-07-02 14:17:45'),
(346, 189, '156205726532702AED-8307-4C94-A28A-39B3ED4DD9A1.jpeg', 'active', '2019-07-02 14:17:45', '2019-07-02 14:17:45'),
(347, 190, '15620576887AFE1938-6D42-476A-AB47-DB339CE0B864.jpeg', 'active', '2019-07-02 14:24:48', '2019-07-02 14:24:48'),
(348, 190, '1562057688D657582E-1DAE-47B2-9275-EDD5F7F2F6C3.jpeg', 'active', '2019-07-02 14:24:49', '2019-07-02 14:24:49'),
(349, 190, '15620576895C539EFE-29AB-475C-8071-57B200797C22.jpeg', 'active', '2019-07-02 14:24:49', '2019-07-02 14:24:49'),
(350, 190, '1562057689BE7A6F85-08EB-49D0-BBC7-15AE754A7715.jpeg', 'active', '2019-07-02 14:24:49', '2019-07-02 14:24:49'),
(351, 191, '1562058112C74AFB2C-96F6-4C6C-8F55-309CBF8D5A1C.jpeg', 'active', '2019-07-02 14:31:52', '2019-07-02 14:31:52'),
(352, 191, '15620581120CA59123-7FE8-4F80-8CF4-2DFA123BE733.jpeg', 'active', '2019-07-02 14:31:52', '2019-07-02 14:31:52'),
(353, 191, '15620581121D266DCC-DDD8-49EF-A178-9FADE06013E7.jpeg', 'active', '2019-07-02 14:31:52', '2019-07-02 14:31:52'),
(354, 191, '1562058112B46895D2-E8EB-4507-9F90-856D869352DD.jpeg', 'active', '2019-07-02 14:31:52', '2019-07-02 14:31:52'),
(355, 191, '1562058112E47DF085-C9E7-474B-B749-4F475EB53FF0.jpeg', 'active', '2019-07-02 14:31:52', '2019-07-02 14:31:52'),
(356, 192, '15620596853F4C95E1-0137-40CA-9A3F-486AB6012A01.jpeg', 'active', '2019-07-02 14:58:05', '2019-07-02 14:58:05'),
(357, 193, '156206000649C9B536-5730-4D87-A0DA-278997965213.jpeg', 'active', '2019-07-02 15:03:26', '2019-07-02 15:03:26'),
(358, 196, '1562060659B0F820E0-71CB-4D5B-B996-53297CD806BF.jpeg', 'active', '2019-07-02 15:14:19', '2019-07-02 15:14:19'),
(359, 197, '15620671353F06358B-DD3C-4BF2-AFAE-E5E3DF2B2726.jpeg', 'active', '2019-07-02 17:02:15', '2019-07-02 17:02:15'),
(360, 197, '1562067135D5610495-9918-49B0-9D78-6C8CA9D0593C.jpeg', 'active', '2019-07-02 17:02:15', '2019-07-02 17:02:15'),
(361, 197, '15620671350692E477-EBBF-4F8C-8A70-BC50B32F459F.jpeg', 'active', '2019-07-02 17:02:15', '2019-07-02 17:02:15'),
(362, 197, '1562067135F505D8EF-A9DE-432F-9716-80C5FDC9AD6C.jpeg', 'active', '2019-07-02 17:02:15', '2019-07-02 17:02:15'),
(363, 198, '15620674044ACD33F2-D8B8-45F2-BAF1-BE991DDC8CFC.jpeg', 'active', '2019-07-02 17:06:44', '2019-07-02 17:06:44'),
(364, 198, '1562067404E3AE307A-F8A7-4977-BDA8-BDCBEB8636F1.jpeg', 'active', '2019-07-02 17:06:45', '2019-07-02 17:06:45'),
(365, 198, '1562067405B94C6D13-6844-4AF1-BA67-0BF2BCAC879B.jpeg', 'active', '2019-07-02 17:06:45', '2019-07-02 17:06:45'),
(366, 198, '15620674057ACF73B0-AA97-4A3F-82F4-FE911FE86527.jpeg', 'active', '2019-07-02 17:06:45', '2019-07-02 17:06:45'),
(367, 198, '1562067405B2E0C2EA-AE63-454F-924E-0BB76A703D13.jpeg', 'active', '2019-07-02 17:06:45', '2019-07-02 17:06:45'),
(368, 200, '1562068180D0590DAB-E86B-448F-9236-B8D39352EB19.jpeg', 'active', '2019-07-02 17:19:40', '2019-07-02 17:19:40'),
(369, 200, '1562068180D4DB491A-61AB-40D8-9229-AAC021B09C03.jpeg', 'active', '2019-07-02 17:19:40', '2019-07-02 17:19:40'),
(370, 200, '1562068180B6B45382-2A0B-44E6-8323-3B8B1721AC69.jpeg', 'active', '2019-07-02 17:19:40', '2019-07-02 17:19:40'),
(371, 201, '1562068440FA0B265D-50D7-4280-BDFA-4528B490CA63.jpeg', 'active', '2019-07-02 17:24:00', '2019-07-02 17:24:00'),
(372, 201, '1562068440C6A4D628-9B16-4F28-97AC-55CED676C852.jpeg', 'active', '2019-07-02 17:24:00', '2019-07-02 17:24:00'),
(373, 201, '1562068440F4CCDBB6-B7F0-4347-B429-8BC7B261F0CB.jpeg', 'active', '2019-07-02 17:24:00', '2019-07-02 17:24:00'),
(374, 201, '1562068440770AAF7D-9287-40D7-B473-D9F1B257A60D.jpeg', 'active', '2019-07-02 17:24:00', '2019-07-02 17:24:00'),
(375, 201, '15620684402250D889-7538-4228-B7F6-49534E31B36C.jpeg', 'active', '2019-07-02 17:24:00', '2019-07-02 17:24:00'),
(376, 202, '1562844053foxtail.jpg', 'active', '2019-07-11 16:50:53', '2019-07-11 16:50:53'),
(377, 203, '1562844157kodo.jpg', 'active', '2019-07-11 16:52:37', '2019-07-11 16:52:37'),
(378, 204, '1562844196Barnyard.jpg', 'active', '2019-07-11 16:53:16', '2019-07-11 16:53:16'),
(379, 205, '1562844237Little.jpg', 'active', '2019-07-11 16:53:57', '2019-07-11 16:53:57'),
(380, 206, '1562844294brown.jpg', 'active', '2019-07-11 16:54:54', '2019-07-11 16:54:54'),
(381, 207, '1563355041_5d2ee7a183b03.jpeg', 'active', '2019-07-17 14:47:23', '2019-07-17 14:47:23'),
(382, 207, '1563355043_5d2ee7a30efa0.jpeg', 'active', '2019-07-17 14:47:23', '2019-07-17 14:47:23'),
(383, 207, '1563355043_5d2ee7a3239a9.jpeg', 'active', '2019-07-17 14:47:23', '2019-07-17 14:47:23'),
(384, 207, '1563355043_5d2ee7a3347df.jpeg', 'active', '2019-07-17 14:47:23', '2019-07-17 14:47:23'),
(385, 207, '1563355043_5d2ee7a340fcb.jpeg', 'active', '2019-07-17 14:47:23', '2019-07-17 14:47:23'),
(386, 208, '1563355352_5d2ee8d83858d.jpeg', 'active', '2019-07-17 14:52:32', '2019-07-17 14:52:32'),
(387, 208, '1563355352_5d2ee8d848e9d.jpeg', 'active', '2019-07-17 14:52:32', '2019-07-17 14:52:32'),
(388, 208, '1563355352_5d2ee8d8581ca.jpeg', 'active', '2019-07-17 14:52:32', '2019-07-17 14:52:32'),
(389, 208, '1563355352_5d2ee8d864af4.jpeg', 'active', '2019-07-17 14:52:32', '2019-07-17 14:52:32'),
(390, 208, '1563355352_5d2ee8d87291e.jpeg', 'active', '2019-07-17 14:52:32', '2019-07-17 14:52:32'),
(391, 209, '1563355630_5d2ee9ee25eb0.jpeg', 'active', '2019-07-17 14:57:10', '2019-07-17 14:57:10'),
(392, 209, '1563355630_5d2ee9ee34b53.jpeg', 'active', '2019-07-17 14:57:10', '2019-07-17 14:57:10'),
(393, 209, '1563355630_5d2ee9ee4187d.jpeg', 'active', '2019-07-17 14:57:10', '2019-07-17 14:57:10'),
(394, 209, '1563355630_5d2ee9ee4e5c4.jpeg', 'active', '2019-07-17 14:57:10', '2019-07-17 14:57:10'),
(395, 209, '1563355630_5d2ee9ee5adb1.jpeg', 'active', '2019-07-17 14:57:10', '2019-07-17 14:57:10'),
(396, 210, '1563355940_5d2eeb24a771f.jpeg', 'active', '2019-07-17 15:02:20', '2019-07-17 15:02:20'),
(397, 211, '1563367591_5d2f18a7b913c.jpeg', 'active', '2019-07-17 18:16:31', '2019-07-17 18:16:31'),
(398, 211, '1563367591_5d2f18a7ee7f4.jpeg', 'active', '2019-07-17 18:16:32', '2019-07-17 18:16:32'),
(399, 211, '1563367592_5d2f18a8070af.jpeg', 'active', '2019-07-17 18:16:32', '2019-07-17 18:16:32'),
(400, 211, '1563367592_5d2f18a813621.jpeg', 'active', '2019-07-17 18:16:32', '2019-07-17 18:16:32'),
(401, 211, '1563367592_5d2f18a81fa1c.jpeg', 'active', '2019-07-17 18:16:32', '2019-07-17 18:16:32'),
(402, 212, '1563367991_5d2f1a376c5e3.jpeg', 'active', '2019-07-17 18:23:11', '2019-07-17 18:23:11'),
(403, 212, '1563367991_5d2f1a377b47e.jpeg', 'active', '2019-07-17 18:23:11', '2019-07-17 18:23:11'),
(404, 212, '1563367991_5d2f1a3788448.jpeg', 'active', '2019-07-17 18:23:11', '2019-07-17 18:23:11'),
(405, 212, '1563367991_5d2f1a379554f.jpeg', 'active', '2019-07-17 18:23:11', '2019-07-17 18:23:11'),
(406, 212, '1563367991_5d2f1a37a40de.jpeg', 'active', '2019-07-17 18:23:11', '2019-07-17 18:23:11'),
(407, 213, '1563373297_5d2f2ef1c416c.jpeg', 'active', '2019-07-17 19:51:37', '2019-07-17 19:51:37'),
(408, 213, '1563373297_5d2f2ef1d3050.jpeg', 'active', '2019-07-17 19:51:37', '2019-07-17 19:51:37'),
(409, 214, '1563373706_5d2f308a726be.jpeg', 'active', '2019-07-17 19:58:26', '2019-07-17 19:58:26'),
(410, 214, '1563373706_5d2f308a80f74.jpeg', 'active', '2019-07-17 19:58:26', '2019-07-17 19:58:26'),
(411, 214, '1563373706_5d2f308a8fe7e.jpeg', 'active', '2019-07-17 19:58:26', '2019-07-17 19:58:26'),
(412, 215, '1564235937_5d3c58a18e40a.jpeg', 'active', '2019-07-27 19:28:58', '2019-07-27 19:28:58'),
(413, 215, '1564235938_5d3c58a24a9d8.jpeg', 'active', '2019-07-27 19:28:58', '2019-07-27 19:28:58'),
(414, 216, '1564236202_5d3c59aa362e2.jpeg', 'active', '2019-07-27 19:33:22', '2019-07-27 19:33:22'),
(415, 217, '1564236567_5d3c5b17d4d81.jpeg', 'active', '2019-07-27 19:39:27', '2019-07-27 19:39:27'),
(416, 218, '1564237211_5d3c5d9b332b4.jpeg', 'active', '2019-07-27 19:50:11', '2019-07-27 19:50:11'),
(417, 219, '1564237476_5d3c5ea431d43.jpeg', 'active', '2019-07-27 19:54:36', '2019-07-27 19:54:36'),
(418, 219, '1564237476_5d3c5ea4423f2.jpeg', 'active', '2019-07-27 19:54:36', '2019-07-27 19:54:36'),
(419, 220, '1564237752_5d3c5fb8400c0.jpeg', 'active', '2019-07-27 19:59:12', '2019-07-27 19:59:12'),
(420, 220, '1564237752_5d3c5fb84ed37.jpeg', 'active', '2019-07-27 19:59:12', '2019-07-27 19:59:12'),
(421, 221, '1564238833_5d3c63f1166df.jpeg', 'active', '2019-07-27 20:17:13', '2019-07-27 20:17:13'),
(422, 221, '1564238833_5d3c63f1250bd.jpeg', 'active', '2019-07-27 20:17:13', '2019-07-27 20:17:13'),
(423, 222, '1564381273_5d3e9059a4983.jpeg', 'active', '2019-07-29 11:51:13', '2019-07-29 11:51:13'),
(424, 222, '1564381273_5d3e9059d29db.jpeg', 'active', '2019-07-29 11:51:13', '2019-07-29 11:51:13'),
(425, 222, '1564381273_5d3e9059dfd9c.jpeg', 'active', '2019-07-29 11:51:13', '2019-07-29 11:51:13'),
(426, 223, '1564381536_5d3e916026b0d.jpeg', 'active', '2019-07-29 11:55:36', '2019-07-29 11:55:36'),
(427, 223, '1564381536_5d3e916034d23.jpeg', 'active', '2019-07-29 11:55:36', '2019-07-29 11:55:36'),
(428, 223, '1564381536_5d3e916043ea6.jpeg', 'active', '2019-07-29 11:55:36', '2019-07-29 11:55:36'),
(429, 223, '1564381536_5d3e91604ff19.jpeg', 'active', '2019-07-29 11:55:36', '2019-07-29 11:55:36'),
(430, 223, '1564381536_5d3e91605b8d2.jpeg', 'active', '2019-07-29 11:55:36', '2019-07-29 11:55:36'),
(431, 224, '1564385767_5d3ea1e777aeb.jpeg', 'active', '2019-07-29 13:06:07', '2019-07-29 13:06:07'),
(432, 224, '1564385767_5d3ea1e785e7d.jpeg', 'active', '2019-07-29 13:06:07', '2019-07-29 13:06:07'),
(433, 224, '1564385767_5d3ea1e791a1b.jpeg', 'active', '2019-07-29 13:06:07', '2019-07-29 13:06:07'),
(434, 224, '1564385767_5d3ea1e79ea0c.jpeg', 'active', '2019-07-29 13:06:07', '2019-07-29 13:06:07'),
(435, 224, '1564385767_5d3ea1e7ad06c.jpeg', 'active', '2019-07-29 13:06:07', '2019-07-29 13:06:07'),
(436, 225, '1564391548_5d3eb87c26d07.jpeg', 'active', '2019-07-29 14:42:28', '2019-07-29 14:42:28'),
(437, 225, '1564391548_5d3eb87c35a0b.jpeg', 'active', '2019-07-29 14:42:28', '2019-07-29 14:42:28'),
(438, 225, '1564391548_5d3eb87c41e37.jpeg', 'active', '2019-07-29 14:42:28', '2019-07-29 14:42:28'),
(439, 225, '1564391548_5d3eb87c4e457.jpeg', 'active', '2019-07-29 14:42:28', '2019-07-29 14:42:28'),
(440, 225, '1564391548_5d3eb87c84537.jpeg', 'active', '2019-07-29 14:42:28', '2019-07-29 14:42:28'),
(441, 227, '1564392348_5d3ebb9c77185.jpeg', 'active', '2019-07-29 14:55:48', '2019-07-29 14:55:48'),
(442, 227, '1564392348_5d3ebb9c863c2.jpeg', 'active', '2019-07-29 14:55:48', '2019-07-29 14:55:48'),
(443, 227, '1564392348_5d3ebb9c950b7.jpeg', 'active', '2019-07-29 14:55:48', '2019-07-29 14:55:48'),
(444, 227, '1564392348_5d3ebb9ca1da8.jpeg', 'active', '2019-07-29 14:55:48', '2019-07-29 14:55:48'),
(445, 227, '1564392348_5d3ebb9caeb85.jpeg', 'active', '2019-07-29 14:55:48', '2019-07-29 14:55:48'),
(446, 228, '1564392816_5d3ebd708851a.jpeg', 'active', '2019-07-29 15:03:36', '2019-07-29 15:03:36'),
(447, 228, '1564392816_5d3ebd7096754.jpeg', 'active', '2019-07-29 15:03:36', '2019-07-29 15:03:36'),
(448, 228, '1564392816_5d3ebd70a265d.jpeg', 'active', '2019-07-29 15:03:36', '2019-07-29 15:03:36'),
(449, 228, '1564392816_5d3ebd70ae639.jpeg', 'active', '2019-07-29 15:03:36', '2019-07-29 15:03:36'),
(450, 228, '1564392816_5d3ebd70ba5f9.jpeg', 'active', '2019-07-29 15:03:36', '2019-07-29 15:03:36'),
(464, 230, '1564849706_5d45b62a2f934.jpg', 'active', '2019-08-03 21:58:26', '2019-08-03 21:58:26'),
(465, 230, '1564849706_5d45b62a3d053.jpg', 'active', '2019-08-03 21:58:26', '2019-08-03 21:58:26'),
(466, 230, '1564849706_5d45b62a4924b.jpg', 'active', '2019-08-03 21:58:26', '2019-08-03 21:58:26'),
(467, 231, '1564850079_5d45b79f0a6a8.jpg', 'active', '2019-08-03 22:04:39', '2019-08-03 22:04:39'),
(468, 231, '1564850079_5d45b79f19167.jpg', 'active', '2019-08-03 22:04:39', '2019-08-03 22:04:39'),
(469, 231, '1564850079_5d45b79f25de2.jpg', 'active', '2019-08-03 22:04:39', '2019-08-03 22:04:39'),
(470, 232, '1564850518_5d45b956dea48.jpg', 'active', '2019-08-03 22:11:58', '2019-08-03 22:11:58'),
(471, 232, '1564850518_5d45b956edb71.jpg', 'active', '2019-08-03 22:11:59', '2019-08-03 22:11:59'),
(594, 316, '1570767274_5da001aa1312f.png', 'active', '2019-10-11 09:44:34', '2019-10-11 09:44:34'),
(473, 233, '1564850979_5d45bb23889b1.jpg', 'active', '2019-08-03 22:19:39', '2019-08-03 22:19:39'),
(474, 233, '1564850979_5d45bb2397236.jpg', 'active', '2019-08-03 22:19:39', '2019-08-03 22:19:39'),
(475, 234, '1564851734_5d45be16601b1.jpg', 'active', '2019-08-03 22:32:14', '2019-08-03 22:32:14'),
(476, 234, '1564851734_5d45be166e4a6.jpg', 'active', '2019-08-03 22:32:14', '2019-08-03 22:32:14'),
(477, 234, '1564851734_5d45be167a395.jpg', 'active', '2019-08-03 22:32:14', '2019-08-03 22:32:14'),
(478, 235, '1564852014_5d45bf2e471a6.jpg', 'active', '2019-08-03 22:36:54', '2019-08-03 22:36:54'),
(479, 235, '1564852014_5d45bf2e57016.jpg', 'active', '2019-08-03 22:36:54', '2019-08-03 22:36:54'),
(480, 236, '1564852298_5d45c04a27de5.jpg', 'active', '2019-08-03 22:41:38', '2019-08-03 22:41:38'),
(481, 236, '1564852298_5d45c04a366b5.jpg', 'active', '2019-08-03 22:41:38', '2019-08-03 22:41:38'),
(482, 237, '1564934785_5d470281d1d89.jpg', 'active', '2019-08-04 21:36:25', '2019-08-04 21:36:25'),
(483, 238, '1564934878_5d4702deaf672.jpg', 'active', '2019-08-04 21:37:58', '2019-08-04 21:37:58'),
(484, 239, '1564934935_5d4703172ffbb.jpg', 'active', '2019-08-04 21:38:55', '2019-08-04 21:38:55'),
(485, 240, '1564935419_5d4704fb22872.jpg', 'active', '2019-08-04 21:46:59', '2019-08-04 21:46:59'),
(486, 241, '1564937266_5d470c32e6e40.jpg', 'active', '2019-08-04 22:17:47', '2019-08-04 22:17:47'),
(487, 242, '1564939979_5d4716cb44aa7.jpg', 'active', '2019-08-04 23:02:59', '2019-08-04 23:02:59'),
(488, 243, '1564940856_5d471a380d262.jpg', 'active', '2019-08-04 23:17:36', '2019-08-04 23:17:36'),
(558, 245, '1565710368_5d52d820a09b9.jpeg', 'active', '2019-08-13 21:02:49', '2019-08-13 21:02:49'),
(490, 244, '1565020517_5d4851659cd42.jpg', 'active', '2019-08-05 21:25:17', '2019-08-05 21:25:17'),
(496, 240, '1565060859_5d48eefb864ae.jpg', 'active', '2019-08-06 08:37:39', '2019-08-06 08:37:39'),
(492, 244, '1565020550_5d485186b8724.jpg', 'active', '2019-08-05 21:25:50', '2019-08-05 21:25:50'),
(495, 68, '1565023996_5d485efc241d0.jpg', 'active', '2019-08-05 22:23:16', '2019-08-05 22:23:16'),
(498, 245, '1565061409_5d48f121d9878.jpg', 'active', '2019-08-06 08:46:49', '2019-08-06 08:46:49'),
(499, 245, '1565061409_5d48f121e6026.jpg', 'active', '2019-08-06 08:46:50', '2019-08-06 08:46:50'),
(500, 245, '1565061410_5d48f12200a83.jpg', 'active', '2019-08-06 08:46:50', '2019-08-06 08:46:50'),
(501, 246, '1565068117_5d490b55f0625.jpg', 'active', '2019-08-06 10:38:38', '2019-08-06 10:38:38'),
(505, 248, '1565152063_5d4a533fb7b3a.jpg', 'active', '2019-08-07 09:57:43', '2019-08-07 09:57:43'),
(506, 247, '1565152118_5d4a5376b526b.jpg', 'active', '2019-08-07 09:58:38', '2019-08-07 09:58:38'),
(507, 249, '1565152434_5d4a54b257029.jpg', 'active', '2019-08-07 10:03:54', '2019-08-07 10:03:54'),
(508, 250, '1565152673_5d4a55a15a93e.jpg', 'active', '2019-08-07 10:07:53', '2019-08-07 10:07:53'),
(509, 251, '1565153724_5d4a59bcc6ee0.jpg', 'active', '2019-08-07 10:25:24', '2019-08-07 10:25:24'),
(510, 252, '1565154271_5d4a5bdf1a84e.jpg', 'active', '2019-08-07 10:34:31', '2019-08-07 10:34:31'),
(511, 253, '1565154789_5d4a5de5509c4.jpg', 'active', '2019-08-07 10:43:09', '2019-08-07 10:43:09'),
(512, 254, '1565154926_5d4a5e6e1bf7e.jpg', 'active', '2019-08-07 10:45:26', '2019-08-07 10:45:26'),
(513, 255, '1565174444_5d4aaaacc1854.jpeg', 'active', '2019-08-07 16:10:44', '2019-08-07 16:10:44'),
(514, 256, '1565175055_5d4aad0f15a73.jpeg', 'active', '2019-08-07 16:20:55', '2019-08-07 16:20:55'),
(515, 257, '1565175443_5d4aae936b690.jpeg', 'active', '2019-08-07 16:27:24', '2019-08-07 16:27:24'),
(516, 257, '1565175444_5d4aae942caf1.jpeg', 'active', '2019-08-07 16:27:24', '2019-08-07 16:27:24'),
(517, 257, '1565175444_5d4aae9443350.jpeg', 'active', '2019-08-07 16:27:24', '2019-08-07 16:27:24'),
(518, 257, '1565175444_5d4aae94597f3.jpeg', 'active', '2019-08-07 16:27:24', '2019-08-07 16:27:24'),
(519, 257, '1565175444_5d4aae946f5c6.jpeg', 'active', '2019-08-07 16:27:24', '2019-08-07 16:27:24'),
(520, 258, '1565175787_5d4aafeb3c983.jpeg', 'active', '2019-08-07 16:33:07', '2019-08-07 16:33:07'),
(521, 258, '1565175787_5d4aafeb58cd2.jpeg', 'active', '2019-08-07 16:33:07', '2019-08-07 16:33:07'),
(522, 258, '1565175787_5d4aafeb70582.jpeg', 'active', '2019-08-07 16:33:07', '2019-08-07 16:33:07'),
(523, 258, '1565175787_5d4aafeb8a44b.jpeg', 'active', '2019-08-07 16:33:07', '2019-08-07 16:33:07'),
(524, 258, '1565175787_5d4aafeba33ba.jpeg', 'active', '2019-08-07 16:33:07', '2019-08-07 16:33:07'),
(525, 259, '1565184123_5d4ad07bb7bf7.jpg', 'active', '2019-08-07 18:52:03', '2019-08-07 18:52:03'),
(527, 260, '1565188339_5d4ae0f3d5aa3.jpg', 'active', '2019-08-07 20:02:19', '2019-08-07 20:02:19'),
(528, 261, '1565189196_5d4ae44cb6099.jpg', 'active', '2019-08-07 20:16:36', '2019-08-07 20:16:36'),
(529, 262, '1565189538_5d4ae5a201170.jpg', 'active', '2019-08-07 20:22:18', '2019-08-07 20:22:18'),
(530, 263, '1565189748_5d4ae6743c991.jpg', 'active', '2019-08-07 20:25:48', '2019-08-07 20:25:48'),
(531, 264, '1565189945_5d4ae7398e99b.jpg', 'active', '2019-08-07 20:29:05', '2019-08-07 20:29:05'),
(532, 265, '1565190086_5d4ae7c60d94d.jpeg', 'active', '2019-08-07 20:31:26', '2019-08-07 20:31:26'),
(533, 266, '1565190175_5d4ae81ff2351.jpg', 'active', '2019-08-07 20:32:56', '2019-08-07 20:32:56'),
(534, 267, '1565190353_5d4ae8d114004.jpg', 'active', '2019-08-07 20:35:53', '2019-08-07 20:35:53'),
(535, 268, '1565194805_5d4afa3549750.jpg', 'active', '2019-08-07 21:50:05', '2019-08-07 21:50:05'),
(536, 268, '1565194805_5d4afa356b54c.jpg', 'active', '2019-08-07 21:50:05', '2019-08-07 21:50:05'),
(560, 290, '1566475177_5d5e83a9c17f5.jpg', 'active', '2019-08-22 17:29:37', '2019-08-22 17:29:37'),
(561, 290, '1566475177_5d5e83a9e7915.jpg', 'active', '2019-08-22 17:29:37', '2019-08-22 17:29:37'),
(539, 271, '1565263589_5d4c06e503c17.jpeg', 'active', '2019-08-08 16:56:29', '2019-08-08 16:56:29'),
(540, 272, '1565264308_5d4c09b478ca5.jpeg', 'active', '2019-08-08 17:08:28', '2019-08-08 17:08:28'),
(541, 273, '1565264947_5d4c0c337153c.jpeg', 'active', '2019-08-08 17:19:07', '2019-08-08 17:19:07'),
(542, 274, '1565265617_5d4c0ed1d9947.jpeg', 'active', '2019-08-08 17:30:17', '2019-08-08 17:30:17'),
(543, 275, '1565266574_5d4c128e83c8c.jpeg', 'active', '2019-08-08 17:46:14', '2019-08-08 17:46:14'),
(545, 277, '1565267868_5d4c179c96316.jpeg', 'active', '2019-08-08 18:07:48', '2019-08-08 18:07:48'),
(546, 278, '1565283134_5d4c533e5706b.jpeg', 'active', '2019-08-08 22:22:14', '2019-08-08 22:22:14'),
(548, 280, '1565284181_5d4c57554d3ab.jpeg', 'active', '2019-08-08 22:39:41', '2019-08-08 22:39:41'),
(549, 281, '1565313643_5d4cca6b53b0e.jpeg', 'active', '2019-08-09 06:50:43', '2019-08-09 06:50:43'),
(550, 282, '1565314281_5d4ccce9e15e3.jpeg', 'active', '2019-08-09 07:01:21', '2019-08-09 07:01:21'),
(551, 283, '1565314921_5d4ccf696e34e.jpeg', 'active', '2019-08-09 07:12:01', '2019-08-09 07:12:01'),
(552, 284, '1565315433_5d4cd1696b4ce.jpeg', 'active', '2019-08-09 07:20:33', '2019-08-09 07:20:33');
INSERT INTO `product_images` (`pi_id`, `pi_product_id`, `pi_image_name`, `pi_status`, `created_at`, `updated_at`) VALUES
(553, 285, '1565315976_5d4cd3881fdc5.jpeg', 'active', '2019-08-09 07:29:36', '2019-08-09 07:29:36'),
(557, 289, '1565317268_5d4cd8948c027.jpeg', 'active', '2019-08-09 07:51:08', '2019-08-09 07:51:08'),
(555, 287, '1565316610_5d4cd6025a433.jpeg', 'active', '2019-08-09 07:40:10', '2019-08-09 07:40:10'),
(562, 290, '1566475177_5d5e83a9f2251.jpg', 'active', '2019-08-22 17:29:38', '2019-08-22 17:29:38'),
(563, 290, '1566475178_5d5e83aa04f74.jpg', 'active', '2019-08-22 17:29:38', '2019-08-22 17:29:38'),
(564, 291, '1566475379_5d5e847361896.jpg', 'active', '2019-08-22 17:32:59', '2019-08-22 17:32:59'),
(565, 290, '1566475448_5d5e84b82edef.jpg', 'active', '2019-08-22 17:34:08', '2019-08-22 17:34:08'),
(566, 292, '1566476035_5d5e8703c9205.jpg', 'active', '2019-08-22 17:43:55', '2019-08-22 17:43:55'),
(567, 292, '1566476035_5d5e8703d1e89.jpg', 'active', '2019-08-22 17:43:55', '2019-08-22 17:43:55'),
(568, 292, '1566476035_5d5e8703d9008.jpg', 'active', '2019-08-22 17:43:55', '2019-08-22 17:43:55'),
(569, 292, '1566476035_5d5e8703e04e0.jpg', 'active', '2019-08-22 17:43:55', '2019-08-22 17:43:55'),
(570, 295, '1568636273_5d7f7d7182aad.jpg', 'active', '2019-09-16 17:47:53', '2019-09-16 17:47:53'),
(571, 299, '1568640272_5d7f8d105cfa1.jpg', 'active', '2019-09-16 18:54:32', '2019-09-16 18:54:32'),
(572, 300, '1568640699_5d7f8ebb5da26.jpg', 'active', '2019-09-16 19:01:39', '2019-09-16 19:01:39'),
(573, 301, '1568641109_5d7f905549968.jpg', 'active', '2019-09-16 19:08:29', '2019-09-16 19:08:29'),
(574, 302, '1568641586_5d7f923223ad3.jpg', 'active', '2019-09-16 19:16:26', '2019-09-16 19:16:26'),
(575, 303, '1569265918_5d8918feb0575.jpg', 'active', '2019-09-24 00:41:58', '2019-09-24 00:41:58'),
(576, 304, '1569266828_5d891c8cee35b.jpg', 'active', '2019-09-24 00:57:09', '2019-09-24 00:57:09'),
(577, 305, '1569267464_5d891f0847ba7.jpg', 'active', '2019-09-24 01:07:44', '2019-09-24 01:07:44'),
(578, 306, '1569268296_5d892248042a9.jpg', 'active', '2019-09-24 01:21:36', '2019-09-24 01:21:36'),
(579, 307, '1569268520_5d892328559c4.jpg', 'active', '2019-09-24 01:25:20', '2019-09-24 01:25:20'),
(580, 308, '1569269828_5d8928446dbaf.jpg', 'active', '2019-09-24 01:47:08', '2019-09-24 01:47:08'),
(581, 309, '1569270785_5d892c01a77c2.jpg', 'active', '2019-09-24 02:03:05', '2019-09-24 02:03:05'),
(582, 310, '1569271302_5d892e06495e4.jpg', 'active', '2019-09-24 02:11:42', '2019-09-24 02:11:42'),
(583, 311, '1569272679_5d893367d22bd.jpg', 'active', '2019-09-24 02:34:39', '2019-09-24 02:34:39'),
(589, 312, '1569907299_5d92e2633404e.jpg', 'active', '2019-10-01 10:51:39', '2019-10-01 10:51:39'),
(590, 312, '1569907299_5d92e26344c18.jpg', 'active', '2019-10-01 10:51:39', '2019-10-01 10:51:39'),
(591, 315, '1569929393_5d9338b1b21b6.jpg', 'active', '2019-10-01 16:59:53', '2019-10-01 16:59:53'),
(592, 312, '1569939392_5d935fc0088d5.jpg', 'active', '2019-10-01 19:46:32', '2019-10-01 19:46:32'),
(593, 312, '1569939408_5d935fd036dc3.jpg', 'active', '2019-10-01 19:46:48', '2019-10-01 19:46:48'),
(595, 317, '1570811238_5da0ad6609998.png', 'active', '2019-10-11 21:57:18', '2019-10-11 21:57:18'),
(596, 318, '1570811394_5da0ae02cbcd6.png', 'active', '2019-10-11 21:59:55', '2019-10-11 21:59:55'),
(597, 319, '1570868950_5da18ed65e46a.jpg', 'active', '2019-10-12 13:59:10', '2019-10-12 13:59:10'),
(598, 320, '1570902082_5da210423d850.jpg', 'active', '2019-10-12 23:11:22', '2019-10-12 23:11:22'),
(599, 321, '1571033761_5da412a10bb4c.jpg', 'active', '2019-10-14 11:46:01', '2019-10-14 11:46:01'),
(600, 326, '1571050929_5da455b1b6e0e.jpg', 'active', '2019-10-14 16:32:09', '2019-10-14 16:32:09'),
(601, 325, '1571050985_5da455e9a7e68.jpg', 'active', '2019-10-14 16:33:05', '2019-10-14 16:33:05'),
(602, 323, '1571051052_5da4562c8c8a1.jpg', 'active', '2019-10-14 16:34:12', '2019-10-14 16:34:12'),
(603, 327, '1571287108_5da7f04492d38.jpg', 'active', '2019-10-17 10:08:28', '2019-10-17 10:08:28'),
(604, 328, '1572204014_5db5edeeb7787.jpg', 'active', '2019-10-28 00:50:15', '2019-10-28 00:50:15'),
(605, 329, '1572205482_5db5f3aa43751.jpg', 'active', '2019-10-28 01:14:42', '2019-10-28 01:14:42'),
(606, 330, '1572205946_5db5f57a8b0fe.jpg', 'active', '2019-10-28 01:22:26', '2019-10-28 01:22:26'),
(607, 331, '1572717953_5dbdc5810aff3.jpg', 'active', '2019-11-02 23:35:53', '2019-11-02 23:35:53'),
(608, 337, '1572931943_5dc10967c83fc.jpg', 'active', '2019-11-05 11:02:23', '2019-11-05 11:02:23'),
(610, 335, '1572932072_5dc109e824710.jpg', 'active', '2019-11-05 11:04:32', '2019-11-05 11:04:32'),
(611, 338, '1574055683_5dd22f0350199.jpg', 'active', '2019-11-18 11:11:23', '2019-11-18 11:11:23'),
(612, 339, '1575648679_5dea7da78d2d9.jpg', 'active', '2019-12-06 21:41:19', '2019-12-06 21:41:19'),
(613, 340, '1575949977_5def16991b8b5.jpg', 'active', '2019-12-10 09:22:57', '2019-12-10 09:22:57'),
(614, 341, '1575950133_5def17356134a.jpg', 'active', '2019-12-10 09:25:33', '2019-12-10 09:25:33'),
(615, 342, '1578580885_5e173b959a6fc.jpeg', 'active', '2020-01-09 20:11:25', '2020-01-09 20:11:25'),
(616, 343, '1581306352_5e40d1f09eaa7.jpg', 'active', '2020-02-10 09:15:52', '2020-02-10 09:15:52'),
(617, 344, '1581311968_5e40e7e0993d0.jpg', 'active', '2020-02-10 10:49:28', '2020-02-10 10:49:28'),
(618, 345, '1581312445_5e40e9bd48534.jpg', 'active', '2020-02-10 10:57:25', '2020-02-10 10:57:25'),
(619, 346, '1581312692_5e40eab47de49.jpg', 'active', '2020-02-10 11:01:32', '2020-02-10 11:01:32'),
(620, 347, '1583374109_5e605f1d73cc8.jpeg', 'active', '2020-03-05 07:38:29', '2020-03-05 07:38:29'),
(621, 348, '1592561442_5eec8f223461d.jpg', 'active', '2020-06-19 22:40:42', '2020-06-19 22:40:42'),
(622, 349, '1592561601_5eec8fc19c466.jpg', 'active', '2020-06-19 22:43:21', '2020-06-19 22:43:21'),
(623, 350, '1592561763_5eec9063c604c.jpg', 'active', '2020-06-19 22:46:03', '2020-06-19 22:46:03'),
(624, 351, '1592561872_5eec90d06f706.jpg', 'active', '2020-06-19 22:47:52', '2020-06-19 22:47:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_skus`
--

CREATE TABLE `product_skus` (
  `sku_id` bigint(20) UNSIGNED NOT NULL,
  `sku_product_id` int(11) DEFAULT NULL,
  `sku_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'like weight,color,flowers',
  `sku_option` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'like qty ex: 2kg,3kg, 5flowers, 5ltrs',
  `sku_vendor_price` decimal(8,2) DEFAULT NULL COMMENT 'like vendor Price ',
  `sku_vdc_commission_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'like fixed',
  `sku_vdc_commission_value` decimal(8,2) DEFAULT NULL COMMENT 'like Price',
  `sku_vdc_final_price` decimal(8,2) DEFAULT NULL COMMENT 'like Price',
  `sku_store_discount_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'like fixed',
  `sku_store_discount_value` decimal(8,2) DEFAULT NULL COMMENT 'like Price',
  `sku_store_price` decimal(8,2) DEFAULT NULL COMMENT 'like Price',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_skus`
--

INSERT INTO `product_skus` (`sku_id`, `sku_product_id`, `sku_no`, `sku_type`, `sku_option`, `sku_vendor_price`, `sku_vdc_commission_type`, `sku_vdc_commission_value`, `sku_vdc_final_price`, `sku_store_discount_type`, `sku_store_discount_value`, `sku_store_price`, `created_at`, `updated_at`) VALUES
(1, 1, 'ajK2TA9GzlAjJZ6w', 'weight', '24', '1100.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-22 06:40:53', '2019-05-22 06:40:53'),
(2, 2, 'b7ORjTbEOtJDpX3d', 'weight', '24', '1100.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-22 06:41:16', '2019-05-22 06:41:16'),
(3, 3, 'ifPWv6Q9Av1T7TaF', 'weight', '0.0024', '1100.00', 'percentage', '40.00', '1540.00', 'percentage', '10.00', '1386.00', '2019-05-22 06:41:46', '2019-07-30 09:04:36'),
(5, 5, '4PaEsuuIrJXgZxJP', 'weight', '10.5', '550.00', 'percentage', '40.00', '770.00', 'percentage', '10.00', '693.00', '2019-05-22 06:50:42', '2019-06-29 08:34:16'),
(6, 6, 'S3i6i3VXaqZO0wgu', 'weight', '500gms', '500.00', 'fixed', '200.00', '700.00', 'fixed', '100.00', '600.00', '2019-05-22 10:02:56', '2019-05-28 18:06:39'),
(7, 7, 'Wxaz8eejV0Q3qMk4', 'quantity', '1', '700.00', 'percentage', '25.00', '875.00', 'percentage', '10.00', '788.00', '2019-05-22 13:34:00', '2019-06-08 20:03:48'),
(8, 7, 'JgTVZShi1rOFXy6X', 'quantity', '2', '1400.00', 'percentage', '25.00', '1750.00', 'percentage', '5.00', '1663.00', '2019-05-22 13:34:00', '2019-06-08 20:03:48'),
(9, 8, 'XppNnCMZQQkjQ70G', 'weight', '1kg', '1000.00', 'percentage', '40.00', '1400.00', 'percentage', '10.00', '1260.00', '2019-05-23 01:18:46', '2019-07-02 03:28:29'),
(10, 9, 'FQyr5umaBUyOnP1L', 'weight', '1kg', '900.00', 'percentage', '40.00', '1260.00', 'percentage', '10.00', '1094.00', '2019-05-23 01:21:10', '2019-07-02 03:28:17'),
(11, 10, '01MZw5Vpdp6kQwvQ', 'color', '1kg', '1200.00', 'percentage', '40.00', '1680.00', 'percentage', '10.00', '1458.00', '2019-05-23 01:23:03', '2019-07-02 03:27:59'),
(12, 11, 'mR8RtSYNOA55SfN8', 'weight', '1kg', '800.00', 'percentage', '40.00', '1120.00', 'percentage', '10.00', '1008.00', '2019-05-23 01:25:52', '2019-07-02 03:27:51'),
(13, 12, '0CnvpMzQf80TgFj5', 'weight', '1kg', '800.00', 'percentage', '40.00', '1120.00', 'percentage', '10.00', '972.00', '2019-05-23 01:27:01', '2019-07-02 03:27:41'),
(14, 13, 'pw9gaKkeMzwHDjht', 'weight', '1kg', '800.00', 'percentage', '40.00', '1120.00', 'percentage', '10.00', '972.00', '2019-05-23 01:27:51', '2019-07-02 03:24:56'),
(15, 14, 'WeLvh4vmCtewPYKD', 'weight', '1kg', '800.00', 'percentage', '35.00', '1080.00', 'percentage', '10.00', '972.00', '2019-05-23 01:29:13', '2019-05-30 19:22:31'),
(16, 15, 'h38S8DyALPMhaydL', 'weight', '1kg', '200.00', 'percentage', '40.00', '980.00', 'fixed', '900.00', '80.00', '2019-05-23 01:31:17', '2020-06-18 06:41:45'),
(17, 16, 'RfF5ds7xGfxnMb0C', 'weight', '1kg', '800.00', 'percentage', '40.00', '1120.00', 'percentage', '10.00', '936.00', '2019-05-23 01:33:09', '2019-07-02 03:23:35'),
(18, 17, 'G1siUrpOg3ccx1LD', 'weight', '1kg', '800.00', 'percentage', '40.00', '1120.00', 'percentage', '10.00', '1008.00', '2019-05-23 01:41:43', '2019-07-02 03:16:26'),
(19, 18, 'UsWY8mXf2DZb7Jmd', 'weight', '1kg', '800.00', 'percentage', '60.00', '1280.00', 'fixed', '10.00', '1270.00', '2019-05-23 01:43:05', '2019-07-24 01:05:40'),
(20, 19, 'n6G0mlpqVps0X0B9', 'weight', '1kg', '500.00', 'percentage', '35.00', '675.00', 'percentage', '10.00', '608.00', '2019-05-23 02:51:25', '2019-05-30 19:22:01'),
(21, 19, 'VNdoPRaGIsEnZgW8', 'weight', '2kg', '1200.00', 'percentage', '35.00', '1620.00', 'percentage', '10.00', '1458.00', '2019-05-23 02:51:25', '2019-05-30 19:22:01'),
(22, 20, 'vVDbMAjECvxhyJQ9', 'weight', '203 Grams', '8560.00', 'percentage', '40.00', '11984.00', 'percentage', '10.00', '10786.00', '2019-05-23 02:56:57', '2019-06-29 08:32:05'),
(23, 21, '1L5PaUXnNqS5FXJO', 'weight', '160 Grams', '6800.00', 'percentage', '40.00', '9520.00', 'percentage', '10.00', '8568.00', '2019-05-23 02:58:45', '2019-06-29 08:31:39'),
(24, 22, '1XKxNuRHkjD3Oh5w', 'weight', '128 Grams', '5450.00', 'percentage', '25.00', '6812.00', 'percentage', '5.00', '6472.00', '2019-05-23 03:05:09', '2019-05-24 15:43:25'),
(25, 23, 'BVuHVKXdfS2UdBZl', 'weight', '180.5 Grams', '7600.00', 'percentage', '40.00', '10640.00', 'percentage', '10.00', '9576.00', '2019-05-23 03:12:54', '2019-06-29 08:31:17'),
(26, 24, '9pMTpTHmEdEQyViC', 'weight', '160 Grams', '6750.00', 'percentage', '40.00', '9450.00', 'percentage', '10.00', '8505.00', '2019-05-23 03:15:46', '2019-06-29 08:30:55'),
(27, 25, '44yyZme2ExRhtPZD', 'weight', '1kg', '1200.00', 'percentage', '40.00', '1680.00', 'percentage', '10.00', '1512.00', '2019-05-23 03:33:48', '2019-07-02 03:15:40'),
(28, 26, 'kM4KQEm2e9Sqk0PQ', 'weight', '1kg', '1500.00', 'percentage', '40.00', '2100.00', 'percentage', '10.00', '1890.00', '2019-05-23 03:35:14', '2019-07-02 03:14:55'),
(29, 27, 'iH7n5nvzDoGe6R0Y', 'weight', '1kg', '1000.00', 'percentage', '40.00', '1400.00', 'percentage', '10.00', '1260.00', '2019-05-23 03:37:51', '2019-07-02 03:14:30'),
(30, 28, 'ug9NMUP9sAh2E0AM', 'weight', '2kg', '1800.00', 'percentage', '30.00', '2340.00', 'percentage', '10.00', '2106.00', '2019-05-23 03:39:47', '2019-05-25 12:03:22'),
(31, 29, 't1G6tJr0MzeS0q6o', 'weight', '2kg', '2200.00', 'percentage', '40.00', '3080.00', 'percentage', '10.00', '2772.00', '2019-05-23 03:41:47', '2019-07-02 03:13:58'),
(32, 30, 'wlke11W1uzQ2FHXo', 'weight', '2kg', '3000.00', 'percentage', '40.00', '4200.00', 'percentage', '10.00', '3780.00', '2019-05-23 03:44:29', '2019-07-02 03:13:39'),
(33, 31, 'AxKswIojVaOEcQGB', 'weight', '126 Grams', '5340.00', 'percentage', '40.00', '7476.00', 'percentage', '10.00', '6729.00', '2019-05-23 03:44:35', '2019-06-29 08:30:32'),
(34, 32, 'x8CYXoUarh6twcpz', 'weight', '2kg', '2000.00', 'percentage', '40.00', '2800.00', 'percentage', '10.00', '2520.00', '2019-05-23 03:46:18', '2019-07-02 03:13:15'),
(35, 33, '7Z6E88KjUk3cxTx2', 'weight', '1kg', '1100.00', 'percentage', '40.00', '1540.00', 'percentage', '10.00', '1386.00', '2019-05-23 03:51:51', '2019-07-02 03:12:58'),
(36, 34, 'XiI6GK65SmPS9QVw', 'weight', '1kg', '1400.00', 'percentage', '40.00', '1960.00', 'percentage', '10.00', '1764.00', '2019-05-23 03:53:50', '2019-07-02 03:12:37'),
(37, 35, 'IzytyftA5jN9WWJr', 'weight', '2kg', '3200.00', 'percentage', '40.00', '4480.00', 'percentage', '10.00', '4032.00', '2019-05-23 03:56:20', '2019-07-02 03:12:19'),
(38, 36, 'UvbTQ6wHESQPumxV', 'weight', '2kg', '3200.00', 'percentage', '40.00', '4480.00', 'percentage', '10.00', '4032.00', '2019-05-23 03:57:51', '2019-07-02 03:11:45'),
(39, 37, 'zbBYVzwEHEU8pOEd', 'weight', '2kg', '3000.00', 'percentage', '40.00', '4200.00', 'percentage', '10.00', '3780.00', '2019-05-23 03:58:30', '2019-07-02 03:11:25'),
(40, 38, 'rfgcTjOeKMTBJpaz', 'weight', '2kg', '3000.00', 'percentage', '40.00', '4200.00', 'percentage', '10.00', '3780.00', '2019-05-23 04:00:27', '2019-07-02 03:11:08'),
(41, 39, 'rgeVI1h8AyrdXxad', 'weight', '2kg', '3000.00', 'percentage', '40.00', '4200.00', 'percentage', '10.00', '3780.00', '2019-05-23 04:06:35', '2019-07-02 03:10:51'),
(42, 40, 'HJVrDzGsPwxY1Ucy', 'weight', '2kg', '3000.00', 'percentage', '40.00', '4200.00', 'percentage', '10.00', '3780.00', '2019-05-23 04:09:20', '2019-07-02 03:09:43'),
(43, 41, '3CNZZbSYSx3cWUnk', 'weight', '1', '1200.00', 'percentage', '40.00', '1680.00', 'percentage', '10.00', '1512.00', '2019-05-23 04:10:24', '2019-07-19 18:24:58'),
(44, 42, 'zQB7Yp81N7We0kwB', 'weight', '1kg', '1200.00', 'percentage', '40.00', '1680.00', 'percentage', '10.00', '1512.00', '2019-05-23 04:12:24', '2019-07-02 03:08:50'),
(45, 43, 'AxJTYHQf6IDP1bKZ', 'weight', '770 Grams', '32000.00', 'percentage', '40.00', '44800.00', 'percentage', '10.00', '40320.00', '2019-05-23 05:05:26', '2019-06-29 08:28:37'),
(46, 44, 'wTbUJa7nigqzSt02', 'weight', '84 Grams', '3600.00', 'percentage', '40.00', '5040.00', 'percentage', '10.00', '4536.00', '2019-05-23 05:13:35', '2019-06-29 08:27:31'),
(47, 45, 'Z3UJbfFoht4mT01K', 'weight', '44 Grams', '1950.00', 'percentage', '40.00', '2730.00', 'percentage', '10.00', '2457.00', '2019-05-23 05:18:48', '2019-06-29 08:26:58'),
(48, 46, 'UnzTTr8aLwdOldAH', 'weight', '44 Grams', '1950.00', 'percentage', '40.00', '2730.00', 'percentage', '10.00', '2457.00', '2019-05-23 05:22:12', '2019-06-29 08:26:02'),
(49, 47, 'P9VMKmnVHcnNf4ST', 'weight', '116 Grams', '4900.00', 'percentage', '40.00', '6860.00', 'percentage', '10.00', '6174.00', '2019-05-23 05:27:11', '2019-06-29 08:25:26'),
(50, 48, 'sekK5d6vCsJ40UPc', 'weight', '19 Grams', '1080.00', 'percentage', '40.00', '1512.00', 'percentage', '10.00', '1361.00', '2019-05-23 05:37:41', '2019-06-29 08:25:03'),
(51, 49, 'orHbm7V4zWO2aVHl', 'weight', '10 Grams', '550.00', 'percentage', '40.00', '770.00', 'percentage', '10.00', '693.00', '2019-05-23 05:44:08', '2019-06-29 08:24:41'),
(52, 50, 'Z18pSaqeEvbz4yDc', 'weight', '104 Grams', '4450.00', 'percentage', '40.00', '6230.00', 'percentage', '10.00', '5607.00', '2019-05-23 05:56:44', '2019-06-29 08:24:20'),
(53, 51, 'GcBCaT4Tki0f2Prw', 'weight', '32 Grams', '1550.00', 'percentage', '40.00', '2170.00', 'percentage', '10.00', '1953.00', '2019-05-23 06:04:40', '2019-06-29 08:23:52'),
(54, 52, 'ZCbpX02gB5WiODyV', 'quantity', '1', '1000.00', 'percentage', '40.00', '1400.00', 'percentage', '10.00', '1260.00', '2019-05-23 07:07:56', '2019-06-29 08:42:28'),
(55, 54, 'NffR3wXCXCURease', 'weight', '500gms', '510.00', 'percentage', '50.00', '764.00', 'percentage', '10.00', '688.00', '2019-05-23 13:06:45', '2019-08-04 22:33:28'),
(56, 55, '9cvmsztYdwQWZifE', 'weight', '500gms', '340.00', 'percentage', '50.00', '510.00', 'percentage', '10.00', '459.00', '2019-05-23 17:57:30', '2019-08-04 22:32:48'),
(57, 53, '4dGwQJyhVwPD0ETd', 'weight', '500gms', '3400.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-23 18:16:45', '2019-05-23 20:15:05'),
(58, 56, '1ICu30EXJdsdcfyE', 'weight', '45', '2250.00', 'percentage', '50.00', '3375.00', 'percentage', '10.00', '2835.00', '2019-05-23 18:23:13', '2019-08-27 07:42:56'),
(59, 57, '9it0rCIJ9icK5Rgy', 'weight', '500Gms', '400.00', 'percentage', '50.00', '600.00', 'percentage', '10.00', '540.00', '2019-05-23 18:34:29', '2019-08-04 22:32:01'),
(60, 58, '1FQQxG2VZnvF6Nau', 'weight', '52', '2550.00', 'percentage', '40.00', '3570.00', 'percentage', '10.00', '3213.00', '2019-05-23 18:47:40', '2019-06-29 08:22:46'),
(61, 59, '2usBhqIutN3tiEum', 'weight', '34 Grams', '1700.00', 'percentage', '40.00', '2380.00', 'percentage', '10.00', '2142.00', '2019-05-23 18:52:15', '2019-06-29 08:22:10'),
(62, 60, 'xW72FiYU7tNFLfGT', 'weight', '500gms', '370.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-23 18:52:23', '2019-05-23 18:52:23'),
(64, 61, 'liDJm3A3B6ZF2Fr6', 'weight', '1', '650.00', 'percentage', '50.00', '975.00', 'percentage', '10.00', '878.00', '2019-05-23 19:01:40', '2019-08-04 22:31:27'),
(65, 62, 'SJ7JgxzjtASTBz06', 'weight', '37 Grams', '1850.00', 'percentage', '40.00', '2590.00', 'percentage', '10.00', '2331.00', '2019-05-23 19:03:43', '2019-06-29 08:21:35'),
(66, 63, '5jUU9NVGkJ0DVFwx', 'weight', '1', '600.00', 'percentage', '50.00', '900.00', 'percentage', '10.00', '810.00', '2019-05-23 19:17:04', '2019-08-04 22:28:42'),
(67, 64, 'jqGziwJgevXAY42J', 'weight', '500gms', '270.00', 'percentage', '50.00', '1620.00', 'percentage', '10.00', '1458.00', '2019-05-23 19:25:23', '2019-08-05 22:21:49'),
(68, 65, 'lk6GXgT4bgqz9aXN', 'weight', '1', '640.00', 'percentage', '50.00', '960.00', 'percentage', '10.00', '864.00', '2019-05-23 19:38:05', '2019-08-04 22:20:16'),
(69, 66, 'fD5YFI03FCEjVlcM', 'weight', '1', '560.00', 'percentage', '50.00', '840.00', 'percentage', '10.00', '756.00', '2019-05-23 19:47:17', '2019-08-04 22:20:34'),
(70, 67, '9UBZDBOrzt90dUEy', 'weight', '1', '540.00', 'percentage', '50.00', '810.00', 'percentage', '10.00', '729.00', '2019-05-23 19:57:26', '2019-08-04 20:58:36'),
(71, 68, 'U92nBXqX0TloABXk', 'weight', '500gms', '260.00', 'percentage', '40.00', '364.00', 'percentage', '10.00', '328.00', '2019-05-23 20:00:23', '2019-06-29 08:52:05'),
(72, 69, 'C4fZgmOIaM0WyKDR', 'weight', '1', '440.00', 'percentage', '50.00', '660.00', 'percentage', '10.00', '594.00', '2019-05-23 20:11:46', '2019-08-08 10:31:22'),
(73, 72, 'rWY9XCFKT5EftSSy', 'weight', '33 Grams', '1750.00', 'percentage', '40.00', '2450.00', 'percentage', '10.00', '2205.00', '2019-05-24 05:25:05', '2019-06-29 08:20:54'),
(74, 71, 'wAXfKYqOWkOuNB7f', 'quantity', '1', '1800.00', 'percentage', '45.00', '2610.00', 'percentage', '5.00', '2480.00', '2019-05-24 05:29:34', '2019-05-24 08:00:24'),
(75, 70, 'HagX480TmtrBJSUp', 'quantity', '800', '800.00', 'percentage', '40.00', '1120.00', 'percentage', '10.00', '1008.00', '2019-05-24 05:34:35', '2019-06-29 08:42:03'),
(76, 74, 'c3BljnqtFUAAwA6d', 'weight', '503 Grams', '22400.00', 'percentage', '40.00', '31360.00', 'percentage', '10.00', '28224.00', '2019-05-24 05:35:56', '2019-06-29 08:19:48'),
(77, 75, 'Rf6UOPJMFCv0QsA5', 'Size', '20”', '1650.00', 'percentage', '25.00', '2062.00', 'percentage', '5.00', '1959.00', '2019-05-24 05:39:38', '2019-05-24 07:00:58'),
(78, 76, '6erQTqlZ4OINh5cC', 'weight', '45 Grams', '2350.00', 'percentage', '40.00', '3290.00', 'percentage', '10.00', '2961.00', '2019-05-24 05:42:14', '2019-06-29 08:18:47'),
(79, 77, 'BVKcYw1ITKmfqMlh', 'Size', '20 inch', '1650.00', 'percentage', '30.00', '2145.00', 'percentage', '10.00', '1931.00', '2019-05-24 05:45:13', '2019-06-11 10:36:20'),
(80, 78, 'YoQDpd6ONwJcJtlS', 'weight', '8 grams', '550.00', 'percentage', '50.00', '825.00', 'percentage', '10.00', '756.00', '2019-05-24 05:46:21', '2019-08-29 18:59:40'),
(81, 79, 'WfDdjtcxCXodR10f', 'weight', '8 grams', '550.00', 'percentage', '50.00', '750.00', 'percentage', '10.00', '693.00', '2019-05-24 05:49:22', '2019-10-11 22:50:32'),
(82, 73, 'NV1OFEm7wuUkM6Lm', 'quantity', '1', '1500.00', 'percentage', '30.00', '1950.00', 'percentage', '5.00', '1853.00', '2019-05-24 05:50:00', '2019-05-24 07:04:32'),
(83, 80, 'SEgOgFgMF0MAiIxv', 'weight', '6 Grams', '750.00', 'percentage', '30.00', '975.00', 'percentage', '10.00', '878.00', '2019-05-24 05:54:58', '2019-06-10 09:17:08'),
(84, 81, 'pvq3j3hJibqLZCuH', 'weight', '55 grams', '3000.00', 'percentage', '25.00', '3750.00', 'percentage', '5.00', '3563.00', '2019-05-24 05:59:08', '2019-09-03 10:49:32'),
(85, 82, 'WTA9VMElQZcfBSJ1', 'weight', '1 gram', '200.00', 'percentage', '50.00', '225.00', 'percentage', '10.00', '203.00', '2019-05-24 06:09:38', '2019-10-11 22:48:14'),
(86, 84, 'RnAefDB8zqpLN7ei', 'quantity', '1', '1650.00', 'percentage', '25.00', '2062.00', 'percentage', '0.00', '2062.00', '2019-05-24 17:02:05', '2019-07-22 10:24:28'),
(87, 83, 'pZLZGR4YfrioESdV', 'Size', 'Mini', '1650.00', 'percentage', '25.00', '2062.00', 'percentage', '0.00', '2062.00', '2019-05-24 21:52:32', '2019-07-22 10:24:57'),
(88, 85, 'ohzuwh4K4eO4ZSnv', 'color', '3 colours', '2000.00', 'percentage', '35.00', '2700.00', 'percentage', '5.00', '2565.00', '2019-05-24 22:00:34', '2019-05-25 12:05:07'),
(89, 86, 'yLJ0Z67R8rqSIpaJ', 'Size', 'Big', '1100.00', 'percentage', '25.00', '1375.00', 'percentage', '0.00', '1375.00', '2019-05-24 23:58:08', '2019-07-22 10:17:43'),
(90, 87, 'eKIx7t0SV9Rkv540', 'quantity', '1', '950.00', 'percentage', '25.00', '1187.00', 'percentage', '0.00', '1187.00', '2019-05-26 00:18:00', '2019-08-02 12:20:33'),
(91, 88, 'J6UBPdG9NeRDg03S', 'quantity', '1', '950.00', 'percentage', '25.00', '1187.00', 'percentage', '0.00', '1187.00', '2019-05-26 00:35:53', '2019-08-02 12:20:14'),
(93, 90, 'YeyBofyFmrgxeTmA', 'weight', '1', '840.00', 'fixed', '0.00', '840.00', 'fixed', '0.00', '840.00', '2019-05-26 07:50:20', '2019-05-26 07:52:33'),
(94, 91, '81yUiQLP6f5etdX6', 'color', 'Available', '12000.00', 'percentage', '35.00', '16200.00', 'percentage', '10.00', '14580.00', '2019-05-27 02:52:32', '2019-09-17 14:19:16'),
(95, 92, 'NtEfqNaRUwGwmLyX', 'color', 'Colours available', '9600.00', 'percentage', '35.00', '12960.00', 'percentage', '25.00', '9720.00', '2019-05-27 02:59:26', '2019-05-29 06:52:48'),
(96, 93, 'WunGrko4EJ4t1EVx', 'color', 'Designs available', '7800.00', 'percentage', '25.00', '9750.00', 'percentage', '10.00', '8775.00', '2019-05-27 03:15:49', '2019-06-11 02:12:53'),
(97, 94, 'DcqVb3LCibRiDld0', 'color', 'Designs and colours', '7000.00', 'percentage', '25.00', '8750.00', 'percentage', '10.00', '7875.00', '2019-05-27 03:23:26', '2019-06-11 02:12:12'),
(98, 95, 'qw1TauGbrXJmJPvh', 'quantity', '1', '110.00', 'percentage', '30.00', '2600.00', 'percentage', '10.00', '2340.00', '2019-05-27 05:39:07', '2019-05-27 05:54:59'),
(99, 96, '5zCBiXJt4qePMxiM', 'weight', '1kg', '250.00', 'percentage', '50.00', '375.00', 'percentage', '10.00', '338.00', '2019-05-27 21:41:21', '2019-05-27 22:01:20'),
(100, 97, 'F2wKpwxJOSorP0cB', 'weight', '1kg', '500.00', 'percentage', '25.00', '625.00', 'percentage', '15.00', '532.00', '2019-05-27 22:10:00', '2019-05-28 18:12:32'),
(101, 98, 'oQBR8jbhFb9No0pc', 'weight', '500gms', '50.00', 'percentage', '30.00', '130.00', 'fixed', '20.00', '110.00', '2019-05-28 14:32:19', '2019-05-28 15:21:01'),
(102, 97, '65DmUgnwRaIcx2vE', 'weight', '2kg', '700.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-28 19:30:44', '2019-05-28 19:30:44'),
(103, 99, '7IeRjNIarYBzlLMY', 'weight', '1kg', '875.00', 'percentage', '35.00', '1181.00', 'percentage', '25.00', '886.00', '2019-05-29 05:30:56', '2019-05-29 06:46:45'),
(104, 99, 'rIrVFO6nkYiQefda', 'weight', '2kg', '1750.00', 'percentage', '35.00', '2362.00', 'percentage', '25.00', '1772.00', '2019-05-29 05:30:56', '2019-05-29 06:46:45'),
(105, 100, '7hKlsIjyx460NTxR', 'weight', '1kg', '875.00', 'percentage', '35.00', '1181.00', 'percentage', '25.00', '886.00', '2019-05-29 05:32:47', '2019-05-29 06:47:27'),
(106, 101, 'tdcnhsALCvdDQ6qa', 'weight', '1kg', '875.00', 'percentage', '35.00', '1181.00', 'percentage', '25.00', '886.00', '2019-05-29 05:35:46', '2019-05-29 06:44:04'),
(107, 102, 'pCvz3ykIK2ggUK2y', 'weight', '1kg', '875.00', 'percentage', '35.00', '1181.00', 'percentage', '25.00', '886.00', '2019-05-29 05:40:21', '2019-05-29 06:44:34'),
(108, 103, 'Sr8Ij2Wni17ldnEu', 'quantity', '10', '450.00', 'percentage', '30.00', '780.00', 'percentage', '10.00', '702.00', '2019-05-29 05:48:26', '2019-06-07 18:34:57'),
(109, 104, '7rnbZJ8BZcIfSLos', 'quantity', '10 Flowers', '550.00', 'percentage', '30.00', '845.00', 'percentage', '10.00', '761.00', '2019-05-29 05:50:34', '2019-06-07 18:31:02'),
(110, 105, 'r7MyQCNM0gpXQo9k', 'quantity', '12', '1350.00', 'percentage', '30.00', '1207.00', 'percentage', '10.00', '1087.00', '2019-05-29 05:52:34', '2019-06-07 18:16:32'),
(111, 106, 'ULHBXd49iLpsBlVE', 'quantity', '10 Flowers', '550.00', 'percentage', '30.00', '715.00', 'percentage', '10.00', '644.00', '2019-05-29 05:54:26', '2019-06-07 18:21:54'),
(112, 107, '7eyBO1Ft2UVtHxHY', 'color', 'Colours available', '7800.00', 'percentage', '30.00', '10140.00', 'percentage', '10.00', '9126.00', '2019-05-30 07:10:22', '2019-05-30 19:20:09'),
(113, 108, 'ZCz5phXv3PvKvTGx', 'color', 'Only one', '15000.00', 'percentage', '25.00', '18750.00', 'percentage', '10.00', '18225.00', '2019-05-30 07:12:59', '2019-05-30 19:19:48'),
(114, 109, 'bZpN7hIpDnh3wsRi', 'color', 'More colours', '1650.00', 'percentage', '35.00', '2227.00', 'percentage', '10.00', '2005.00', '2019-05-30 07:15:52', '2019-05-30 19:19:05'),
(115, 111, 'jRfBhCc5Ypy2Y8cD', 'color', 'Different', '6500.00', 'percentage', '35.00', '8775.00', 'percentage', '10.00', '7898.00', '2019-05-30 07:23:54', '2019-05-30 19:17:41'),
(116, 112, 'JLMx1UIwSBbydgjW', 'color', 'More colours avay', '3800.00', 'percentage', '35.00', '5130.00', 'percentage', '10.00', '4617.00', '2019-05-31 02:32:04', '2019-06-02 08:10:43'),
(117, 110, 'dYJBEGxXZLoOdW0R', 'Size', '3.75”', '3000.00', 'percentage', '35.00', '4050.00', 'percentage', '25.00', '3038.00', '2019-05-31 02:33:16', '2019-06-02 08:11:04'),
(118, 113, '4BTfAzgl1ucepdtX', 'color', 'More colours available', '7600.00', 'percentage', '35.00', '10260.00', 'percentage', '10.00', '9234.00', '2019-05-31 02:43:59', '2019-06-02 07:50:53'),
(119, 114, 'rsmvjzK9BQRGriGe', 'color', 'More colours', '16300.00', 'percentage', '25.00', '20375.00', 'percentage', '10.00', '18338.00', '2019-06-01 16:13:31', '2019-06-02 07:43:26'),
(120, 115, '9xDOMwUupIgFJtu8', 'color', 'More available', '9900.00', 'percentage', '45.00', '14355.00', 'percentage', '10.00', '12920.00', '2019-06-01 16:29:14', '2019-06-02 07:50:23'),
(121, 116, '0kzEkSly50eglJ8G', 'color', 'More colours', '1450.00', 'percentage', '25.00', '1812.00', 'percentage', '10.00', '1631.00', '2019-06-02 02:13:17', '2019-06-02 07:49:19'),
(122, 117, 'UqMvDv0cnHJU9Kiq', 'Size', 'Long haram', '1850.00', 'percentage', '35.00', '2497.00', 'percentage', '10.00', '2248.00', '2019-06-02 02:28:18', '2019-06-02 07:49:58'),
(123, 118, 'due7Tf90puZjyC0C', 'color', '6colors', '1450.00', 'percentage', '25.00', '1812.00', 'percentage', '10.00', '1631.00', '2019-06-02 02:33:05', '2019-06-02 07:48:21'),
(124, 119, 'SPrjFknwQVNhD6di', 'color', '9 colours', '9000.00', 'percentage', '25.00', '11250.00', 'percentage', '10.00', '10125.00', '2019-06-02 02:38:21', '2019-06-02 07:46:22'),
(125, 120, '9HJ5L7hq7MfBPtQ4', 'color', 'One', '1450.00', 'percentage', '35.00', '1957.00', 'percentage', '10.00', '1762.00', '2019-06-02 02:41:18', '2019-06-02 07:45:02'),
(126, 121, 'PgDoMEbuTqqh1xZV', 'color', 'Customised to any colour', '4800.00', 'percentage', '25.00', '6000.00', 'percentage', '10.00', '5400.00', '2019-06-02 03:54:46', '2019-06-02 07:44:14'),
(127, 122, 'Dmw7iqxKbfnSBJwt', 'color', 'Available', '8500.00', 'percentage', '25.00', '10625.00', 'percentage', '10.00', '9563.00', '2019-06-02 04:03:20', '2019-06-02 07:43:46'),
(128, 123, 'Pi55VduzYlG3Ay1T', 'quantity', '10 flowers', '20.00', 'percentage', '10.00', '22.00', 'percentage', '50.00', '11.00', '2019-06-03 01:34:27', '2019-06-03 01:37:44'),
(129, 123, 'SqsEsgRTKeq62FII', 'quantity', '20', '40.00', 'percentage', '50.00', '60.00', 'percentage', '20.00', '48.00', '2019-06-03 01:34:27', '2019-06-03 01:37:44'),
(130, 124, 'o4oT8jN6tSZkbmFY', 'quantity', '500gms', '90.00', 'fixed', '20.00', '110.00', 'percentage', '0.80', '110.00', '2019-06-03 04:48:50', '2019-06-03 04:53:35'),
(131, 125, 'FQs05eXo6ZiV6ZMX', 'weight', '1kg', '100.00', 'fixed', '20.00', '120.00', 'fixed', '10.00', '110.00', '2019-06-03 06:40:14', '2019-06-03 06:49:36'),
(132, 125, 'Wh3ASJNz3n5wWmBW', 'weight', '2kg', '150.00', 'percentage', '200.00', '450.00', 'percentage', '10.00', '405.00', '2019-06-03 21:55:03', '2019-06-03 21:55:43'),
(133, 126, 'HVcUxCZIVmyseAmO', 'weight', '1kg', '1400.00', 'percentage', '30.00', '1820.00', 'percentage', '10.00', '1638.00', '2019-06-05 12:51:45', '2019-06-10 09:08:18'),
(134, 127, 'cZyIwR3FcDprzx92', 'weight', '1', '1400.00', 'percentage', '30.00', '1820.00', 'percentage', '10.00', '1638.00', '2019-06-05 12:53:06', '2019-06-10 09:01:12'),
(135, 128, 'ig4js7jpnKWrgWlk', 'weight', '1kg', '1400.00', 'percentage', '30.00', '1820.00', 'percentage', '10.00', '1638.00', '2019-06-05 12:54:57', '2019-06-10 08:58:48'),
(136, 129, 'LoKQ60fBcE4uBOe2', 'weight', '1kg', '1600.00', 'percentage', '30.00', '2080.00', 'percentage', '10.00', '1872.00', '2019-06-05 12:56:07', '2019-06-10 08:55:00'),
(137, 130, 'yNcWoywK8sPsfprs', 'weight', '1kg', '1400.00', 'percentage', '30.00', '1820.00', 'percentage', '10.00', '1638.00', '2019-06-05 12:58:19', '2019-06-10 08:41:00'),
(138, 131, '9IBejy3wE1mP0XNf', 'weight', '1kg', '1800.00', 'percentage', '30.00', '2080.00', 'percentage', '10.00', '1872.00', '2019-06-05 12:59:16', '2020-06-17 14:17:25'),
(139, 145, 'vxlsvzDsWi9m6Raa', 'color', 'More colours in pendant', '730.00', 'percentage', '25.00', '912.00', 'percentage', '10.00', '789.00', '2019-06-08 06:53:52', '2019-06-08 07:04:34'),
(140, 143, 'FJ7QkXyoJezlxNtp', 'quantity', '24', '1150.00', 'percentage', '30.00', '1495.00', 'percentage', '20.00', '1196.00', '2019-06-08 18:28:58', '2019-06-08 18:35:56'),
(141, 138, 'J5jWGT9aSK8qRfGh', 'quantity', '30', '1000.00', 'percentage', '30.00', '1300.00', 'percentage', '10.00', '1170.00', '2019-06-08 20:18:07', '2019-06-08 20:19:11'),
(142, 141, 'rZaGiCzp8TkZCuY0', 'quantity', '1', '800.00', 'percentage', '30.00', '1040.00', 'percentage', '20.00', '832.00', '2019-06-08 20:24:13', '2019-06-08 20:24:36'),
(143, 146, 'gtcnpW157hOKbfJa', 'color', 'More colours available', '4300.00', 'percentage', '40.00', '6020.00', 'percentage', '10.00', '5418.00', '2019-06-09 04:05:16', '2019-06-29 07:59:52'),
(144, 147, '3BbM9sR73JUOs4Kx', 'color', 'More colours available', '3000.00', 'percentage', '40.00', '4200.00', 'percentage', '10.00', '3510.00', '2019-06-09 04:21:00', '2019-06-10 07:24:23'),
(145, 148, 'XdrdcRzCRgJOY1zR', 'color', 'More colours', '6500.00', 'percentage', '30.00', '8450.00', 'percentage', '10.00', '7605.00', '2019-06-09 04:24:59', '2019-06-10 07:08:09'),
(146, 131, 'EnJeX2MCzE7fVVzx', 'weight', '2kg', '3600.00', 'percentage', '30.00', '4160.00', 'percentage', '10.00', '3744.00', '2019-06-10 06:52:04', '2020-06-17 14:17:25'),
(147, 150, 'yc91ZT7Ikm1w4mFT', 'quantity', '10', '1000.00', 'percentage', '30.00', '1300.00', 'percentage', '10.00', '1170.00', '2019-06-10 08:51:24', '2019-06-10 08:51:49'),
(148, 151, 'juvpjg1sXKLmaUyO', 'quantity', '10', '500.00', 'percentage', '30.00', '650.00', 'percentage', '10.00', '585.00', '2019-06-10 08:56:49', '2019-06-10 08:57:12'),
(149, 152, 'yzx289ziHMjZK62h', 'weight', '1kg', '12.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-10 17:23:11', '2019-06-10 17:23:11'),
(150, 152, 'F45AtgrpNxBpFrP7', 'weight', '2kg', '24.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-10 17:23:11', '2019-06-10 17:23:11'),
(151, 152, 'Urcb9qOwGAdFG3ZS', 'weight', '3kg', '36.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-10 17:23:11', '2019-06-10 17:23:11'),
(152, 153, 'IW2o2HIyYaOrcYKG', 'color', 'More colours Ava', '9500.00', 'percentage', '25.00', '11875.00', 'percentage', '10.00', '10688.00', '2019-06-10 17:49:05', '2019-06-11 02:11:22'),
(153, 154, '55snYJus0B930HpB', 'color', 'More', '550.00', 'percentage', '30.00', '715.00', 'percentage', '10.00', '644.00', '2019-06-10 19:01:27', '2019-06-11 10:27:22'),
(154, 156, '7lxBrrUdnfXrD3u7', 'weight', '1 KG', '1500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-10 22:14:03', '2019-06-10 22:14:03'),
(155, 157, 'LT9LHTEe4wr8nWSN', 'weight', '1 KG', '2000.00', 'fixed', '1500.00', '3500.00', 'fixed', '1000.00', '2500.00', '2019-06-10 22:20:31', '2019-06-10 22:22:53'),
(156, 158, 'ExPdS2lxegSddCLh', 'weight', '1 KG', '1500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-10 23:15:51', '2019-06-10 23:15:51'),
(157, 106, 'kTQSSgpMDIDH3LMM', 'quantity', '25 Flowers', '645.00', 'percentage', '50.00', '967.00', 'percentage', '20.00', '774.00', '2019-06-12 20:13:35', '2019-06-12 20:16:44'),
(158, 160, 'YRO1JyDpo0k8b3fz', 'weight', '1kg', '250.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-12 23:13:59', '2019-06-12 23:13:59'),
(159, 161, '9KmViJc4T41QudHn', 'weight', '1 KG', '1500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-13 00:01:40', '2019-06-13 00:01:40'),
(160, 162, 'lDghGJOxJV4yU9tr', 'weight', '1kg', '250.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-13 00:04:54', '2019-06-13 00:04:54'),
(161, 162, 'GkLJpCTlzxUlum3J', 'weight', '2kg', '350.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-13 00:04:54', '2019-06-13 00:04:54'),
(162, 162, 'GsvWANCfY7IzuoHE', 'weight', '3kg', '450.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-13 00:04:54', '2019-06-13 00:04:54'),
(163, 163, '4pBHpcP4FgtVJZg5', 'quantity', '5 Flowers', '250.00', 'percentage', '30.00', '325.00', 'percentage', '50.00', '163.00', '2019-06-13 00:15:11', '2019-06-13 00:16:04'),
(164, 163, 'WTConpsI7iUVCQP9', 'quantity', '25 flowers', '500.00', 'percentage', '50.00', '750.00', 'percentage', '50.00', '375.00', '2019-06-13 00:15:11', '2019-06-13 00:16:04'),
(165, 163, 'drW6DxaVPq1Q797n', 'quantity', '32 Flowers', '780.00', 'percentage', '50.00', '1170.00', 'percentage', '50.00', '585.00', '2019-06-13 00:15:11', '2019-06-13 00:16:04'),
(166, 164, 'YzRxczKt8yAfUR9G', 'weight', '1 KG', '1000.00', 'percentage', '50.00', '1500.00', 'percentage', '20.00', '1200.00', '2019-06-18 19:00:49', '2019-06-18 19:04:39'),
(167, 165, 'KpQndRxzAgG3TAoo', 'weight', '1 KG', '500.00', 'percentage', '50.00', '750.00', 'percentage', '25.00', '563.00', '2019-06-18 19:01:30', '2019-06-18 19:04:21'),
(168, 166, 'V0oAK0WdAutTfO6A', 'color', 'Red', '500.00', 'percentage', '50.00', '750.00', 'percentage', '25.00', '563.00', '2019-06-19 15:43:28', '2019-06-19 15:44:29'),
(169, 166, 'uj7h9bocKryOEbvV', 'color', 'Blue', '650.00', 'percentage', '50.00', '975.00', 'percentage', '25.00', '732.00', '2019-06-19 15:43:28', '2019-06-19 15:44:29'),
(170, 167, '9OgisPTYZD8vsXWl', 'weight', '1kg', '100.00', 'percentage', '20.00', '120.00', 'percentage', '10.00', '108.00', '2019-06-20 10:33:10', '2019-06-20 10:39:41'),
(171, 167, 'dj8VgCt75t5ZID6D', 'weight', '2kg', '1000.00', 'fixed', '100.00', '1100.00', 'fixed', '100.00', '1000.00', '2019-06-20 10:33:10', '2019-06-20 10:49:28'),
(172, 167, '8jSD8Z9cNyktOW5n', 'weight', '3kg', '1500.00', 'fixed', '0.00', '1500.00', 'fixed', '0.00', '1500.00', '2019-06-20 10:33:10', '2019-07-13 23:07:05'),
(173, 167, 'EAMKJGIe4r60gedc', 'weight', '4kg', '2000.00', 'fixed', '0.00', '2000.00', 'fixed', '0.00', '2000.00', '2019-06-20 10:33:10', '2019-07-13 23:07:05'),
(174, 167, 'S8Tf0jPmDWPgLGt1', 'weight', '5kg', '2500.00', 'fixed', '0.00', '2500.00', 'fixed', '0.00', '2500.00', '2019-06-20 10:33:10', '2019-07-13 23:07:05'),
(175, 168, 'WdX4ZXRDFP9GdOdc', 'weight', '1kg', '500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-21 18:47:29', '2019-06-21 18:47:29'),
(192, 183, 'uneh7PcE1JJPqctW', 'weight', '1kg', '120.00', 'fixed', '100.00', '600.00', 'fixed', '120.00', '480.00', '2019-06-23 09:58:57', '2019-07-21 10:39:33'),
(193, 183, 'GXm6TKkAcSBrVtTe', 'weight', '2kg', '1200.00', 'percentage', '40.00', '1680.00', 'percentage', '10.00', '1512.00', '2019-06-23 09:58:57', '2019-06-23 10:04:43'),
(178, 169, 's3RvT0TUsgJtm6FK', 'quantity', '10', '550.00', 'percentage', '40.00', '770.00', 'percentage', '10.00', '693.00', '2019-06-22 23:31:30', '2019-06-28 02:55:47'),
(179, 170, 'QpbqVEO9wmQyjn0m', 'quantity', '10', '550.00', 'percentage', '40.00', '770.00', 'percentage', '10.00', '693.00', '2019-06-22 23:32:36', '2019-06-28 02:55:29'),
(180, 171, '2oGDPPN35pXb1a2V', 'quantity', '10', '550.00', 'percentage', '40.00', '770.00', 'percentage', '10.00', '693.00', '2019-06-22 23:33:54', '2019-06-28 02:55:08'),
(181, 172, 'w3chsIUvwmyoKZGJ', 'quantity', '10', '1000.00', 'percentage', '40.00', '1400.00', 'percentage', '10.00', '1260.00', '2019-06-22 23:35:12', '2019-06-28 02:54:47'),
(182, 173, '8tMU1XyXynulHaDx', 'quantity', '10', '550.00', 'percentage', '40.00', '770.00', 'percentage', '10.00', '693.00', '2019-06-22 23:37:42', '2019-06-28 02:53:59'),
(183, 174, 'ToptBNbwicGdrTzC', 'quantity', '10', '450.00', 'percentage', '40.00', '630.00', 'percentage', '10.00', '567.00', '2019-06-22 23:38:41', '2019-06-28 02:53:42'),
(184, 175, 'pt1dhAt0318MhiUq', 'quantity', '10', '550.00', 'percentage', '40.00', '770.00', 'percentage', '10.00', '693.00', '2019-06-22 23:40:10', '2019-06-28 02:53:19'),
(185, 176, 'yxgt9xNhXkQZD2wb', 'quantity', '10', '1000.00', 'percentage', '40.00', '1400.00', 'percentage', '10.00', '1260.00', '2019-06-22 23:41:29', '2019-06-28 02:53:00'),
(186, 177, 'LIxarteqOQEQ5P3F', 'quantity', '10', '450.00', 'percentage', '40.00', '630.00', 'percentage', '10.00', '567.00', '2019-06-22 23:42:23', '2019-06-28 02:52:29'),
(187, 178, 'JrxmLTuWJpYabUVa', 'quantity', '10', '450.00', 'percentage', '40.00', '630.00', 'percentage', '10.00', '567.00', '2019-06-22 23:43:29', '2019-06-28 02:52:10'),
(188, 179, 'CGb6G5hzso43apxA', 'quantity', '12', '1350.00', 'percentage', '40.00', '1890.00', 'percentage', '10.00', '1701.00', '2019-06-22 23:44:32', '2019-06-28 02:51:51'),
(189, 180, '36jz1XN6rfRaSVBu', 'quantity', '12', '1350.00', 'percentage', '40.00', '1890.00', 'percentage', '10.00', '1701.00', '2019-06-22 23:45:49', '2019-06-28 02:51:29'),
(190, 181, 'tn3kSiO0otG7wnJD', 'quantity', '12', '1350.00', 'percentage', '40.00', '1890.00', 'percentage', '10.00', '1701.00', '2019-06-22 23:46:44', '2019-06-28 02:50:56'),
(191, 182, '9w94dz85p3YWjQeO', 'quantity', '12', '1350.00', 'percentage', '40.00', '1890.00', 'percentage', '10.00', '1701.00', '2019-06-22 23:47:32', '2019-06-24 01:54:09'),
(194, 183, 'NhIctG98agCVWvmY', 'weight', '3kg', '1500.00', 'percentage', '40.00', '2100.00', 'percentage', '10.00', '1890.00', '2019-06-23 09:58:57', '2019-06-23 10:04:43'),
(199, 184, 'u37xMeakpXggfbfn', 'weight', '1kg', '250.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-26 18:13:59', '2019-06-26 18:13:59'),
(198, 185, 'RO6oqjnNwiUSR90i', 'weight', '1kg', '25.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-26 16:56:00', '2019-06-26 16:56:00'),
(200, 186, 'DMAtYUMc1VGWUuZd', 'weight', '1kg', '2100.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-28 11:11:06', '2019-06-28 11:11:06'),
(201, 187, 'DyGSaX4Es6Q7nO27', 'weight', '1kg', '11000.00', 'percentage', '40.00', '15400.00', 'percentage', '10.00', '13860.00', '2019-06-29 21:48:18', '2019-07-02 03:08:08'),
(202, 188, 'soqDYZjctNiBRGJM', 'weight', '1kg', '12000.00', 'percentage', '50.00', '18000.00', 'percentage', '10.00', '16200.00', '2019-07-02 14:13:45', '2019-07-09 01:35:46'),
(203, 189, 'T2MBVHfFKOj1JiCv', 'weight', '1kg', '6800.00', 'percentage', '50.00', '10200.00', 'percentage', '10.00', '9180.00', '2019-07-02 14:17:45', '2019-07-09 01:35:29'),
(204, 190, 'ZKrbovNsyMFBRTnG', 'weight', '1kg', '10000.00', 'percentage', '50.00', '15000.00', 'percentage', '10.00', '13500.00', '2019-07-02 14:24:48', '2019-07-09 01:34:38'),
(205, 191, 'zQkamBLDk4ZAUbw0', 'weight', '1kg', '3400.00', 'percentage', '50.00', '5100.00', 'percentage', '10.00', '4590.00', '2019-07-02 14:31:52', '2019-07-09 01:33:54'),
(206, 192, 'qU68HxXz4UNHmu9t', 'weight', '1', '4500.00', 'percentage', '50.00', '6750.00', 'percentage', '10.00', '6075.00', '2019-07-02 14:58:05', '2019-07-21 08:15:07'),
(207, 193, 'srJKCGpilWG5bWSA', 'weight', '1', '5500.00', 'percentage', '50.00', '8250.00', 'percentage', '10.00', '7425.00', '2019-07-02 15:03:26', '2019-07-09 01:32:01'),
(208, 196, 'ssNBOGQN2GW3JNdI', 'weight', '1', '1650.00', 'percentage', '50.00', '2475.00', 'percentage', '10.00', '2228.00', '2019-07-02 15:14:19', '2019-07-09 01:30:44'),
(209, 197, 'L0uMBDT4Ubyx67mX', 'weight', '1', '2950.00', 'percentage', '40.00', '4130.00', 'percentage', '10.00', '3717.00', '2019-07-02 17:02:15', '2019-07-09 01:29:52'),
(210, 198, 'O73bo8XKmgXhk3UF', 'weight', '1', '4200.00', 'percentage', '40.00', '5880.00', 'percentage', '10.00', '5292.00', '2019-07-02 17:06:44', '2019-07-09 01:29:12'),
(211, 200, 'YYBK0prKtD4YpopM', 'weight', '2', '18000.00', 'percentage', '40.00', '25200.00', 'percentage', '10.00', '22680.00', '2019-07-02 17:19:40', '2019-07-09 01:28:13'),
(212, 201, 'sXSEai09NISoMpea', 'weight', '1', '8850.00', 'percentage', '40.00', '12390.00', 'percentage', '10.00', '11151.00', '2019-07-02 17:24:00', '2019-07-02 22:17:52'),
(213, 182, 'go5cwes72DJCCjnj', 'weight', '1', '0.00', 'fixed', '0.00', '0.00', 'fixed', '0.00', '0.00', '2019-07-03 10:58:05', '2019-07-03 10:59:03'),
(214, 202, 'oFCOB2pcEEHQqNAY', 'weight', '1', '100.00', 'fixed', '593.00', '693.00', 'fixed', '0.00', '693.00', '2019-07-11 16:50:53', '2019-08-15 01:15:27'),
(215, 203, 'EvGSD5HFlGDJPe99', 'weight', '1kg', '100.00', 'fixed', '593.00', '693.00', 'fixed', '0.00', '693.00', '2019-07-11 16:52:37', '2019-07-11 22:19:05'),
(216, 203, 'ZxzBZXJZ0ne9oYU2', 'weight', '2kg', '175.00', 'fixed', '1211.00', '1386.00', 'fixed', '0.00', '1386.00', '2019-07-11 16:52:37', '2019-07-11 22:19:05'),
(217, 204, '2ASHDhZ2EwYbd14P', 'weight', '1', '120.00', 'fixed', '573.00', '693.00', 'fixed', '0.00', '693.00', '2019-07-11 16:53:16', '2019-09-06 18:11:06'),
(218, 205, 'tk7dlcvD2CHiY3ds', 'weight', '1', '150.00', 'fixed', '543.00', '693.00', 'fixed', '0.00', '693.00', '2019-07-11 16:53:57', '2019-07-15 09:08:13'),
(219, 206, 'GHNLlmhasPnbODq0', 'weight', '1', '220.00', 'fixed', '473.00', '693.00', 'fixed', '0.00', '693.00', '2019-07-11 16:54:54', '2019-07-15 09:05:45'),
(220, 207, 'QG8xggbS2RXeBhYp', 'weight', '1', '7500.00', 'percentage', '50.00', '11250.00', 'percentage', '10.00', '10125.00', '2019-07-17 14:47:21', '2019-07-18 02:22:49'),
(221, 208, 'NSf2Qa6wSSF6P7dD', 'weight', '1', '7500.00', 'percentage', '50.00', '11250.00', 'percentage', '10.00', '10125.00', '2019-07-17 14:52:32', '2019-07-18 02:24:27'),
(222, 209, 'Nax4Eapy99EfTLzd', 'weight', '1', '5650.00', 'percentage', '50.00', '8475.00', 'percentage', '10.00', '7628.00', '2019-07-17 14:57:10', '2019-07-18 02:21:59'),
(223, 210, 'g0rDj8EJVypDWSHz', 'weight', '1', '2150.00', 'percentage', '50.00', '3225.00', 'percentage', '10.00', '2903.00', '2019-07-17 15:02:20', '2019-07-18 02:21:07'),
(224, 211, 'JCTxXn6ZrOFqpHAh', 'weight', '1', '10500.00', 'percentage', '40.00', '14700.00', 'percentage', '10.00', '13230.00', '2019-07-17 18:16:31', '2019-07-18 02:20:32'),
(225, 212, 'fkyVOwzlXeCJODWk', 'weight', '1', '2150.00', 'percentage', '50.00', '3225.00', 'percentage', '10.00', '2903.00', '2019-07-17 18:23:11', '2019-07-18 02:19:51'),
(226, 213, 'q017FCsJc2TlAi06', 'weight', '1', '1450.00', 'percentage', '50.00', '2175.00', 'percentage', '10.00', '1958.00', '2019-07-17 19:51:37', '2019-07-18 02:18:57'),
(227, 214, 'mMoiMMjB4k92QEd6', 'weight', '1', '1400.00', 'percentage', '50.00', '2100.00', 'percentage', '10.00', '1890.00', '2019-07-17 19:58:26', '2019-07-18 02:18:20'),
(228, 215, 'amUPrKTbCIMIJAB9', 'weight', '1', '1870.00', 'percentage', '50.00', '2805.00', 'percentage', '10.00', '2525.00', '2019-07-27 19:28:57', '2019-08-08 10:18:09'),
(229, 216, 'cnK9FR2yNeTNmDmH', 'weight', '1', '2230.00', 'percentage', '50.00', '3345.00', 'percentage', '10.00', '3011.00', '2019-07-27 19:33:22', '2019-08-08 10:23:41'),
(230, 217, '3ExaVQdBJTY0swYn', 'weight', '1', '1550.00', 'percentage', '50.00', '2325.00', 'percentage', '10.00', '2093.00', '2019-07-27 19:39:27', '2019-08-08 10:23:17'),
(231, 218, 'QVqrR0DGKSh1iSLF', 'weight', '1', '900.00', 'percentage', '50.00', '1350.00', 'percentage', '10.00', '1215.00', '2019-07-27 19:50:11', '2019-08-04 21:18:59'),
(232, 219, 'OflydGzJ1ToZqWII', 'weight', '1', '1400.00', 'percentage', '50.00', '2100.00', 'percentage', '10.00', '1890.00', '2019-07-27 19:54:36', '2019-08-08 10:22:50'),
(233, 220, 'dZmLX3wglxVKPHhr', 'weight', '1', '1350.00', 'percentage', '50.00', '2025.00', 'percentage', '10.00', '1823.00', '2019-07-27 19:59:12', '2019-08-04 21:18:30'),
(234, 221, '7yGCKfRHqGwH4jET', 'weight', '1', '2450.00', 'percentage', '40.00', '3430.00', 'percentage', '10.00', '3087.00', '2019-07-27 20:17:13', '2019-08-02 11:27:00'),
(235, 222, 'lQslJDUI4rhujRRI', 'weight', '1', '1600.00', 'percentage', '50.00', '2400.00', 'percentage', '10.00', '2160.00', '2019-07-29 11:51:13', '2019-08-08 10:20:31'),
(236, 223, 'krfMlNY4V7ILE1ov', 'weight', '1', '1600.00', 'percentage', '50.00', '2400.00', 'percentage', '40.00', '1440.00', '2019-07-29 11:55:36', '2019-08-04 21:17:55'),
(237, 224, 'w44a95f1rNiw7r0K', 'weight', '1', '6000.00', 'percentage', '50.00', '9000.00', 'percentage', '10.00', '8100.00', '2019-07-29 13:06:07', '2019-08-08 10:19:49'),
(238, 225, 'qZD9MG1rIZofxpGm', 'weight', '1', '6000.00', 'percentage', '50.00', '9000.00', 'percentage', '10.00', '8100.00', '2019-07-29 14:42:28', '2019-08-08 10:18:42'),
(239, 227, 'ujp3xMdMfTpjLZyY', 'weight', '1', '4000.00', 'percentage', '40.00', '5600.00', 'percentage', '10.00', '5040.00', '2019-07-29 14:55:48', '2019-08-02 11:21:13'),
(240, 228, '0lSIbNYelHTf05aQ', 'weight', '1', '7800.00', 'percentage', '40.00', '10920.00', 'percentage', '10.00', '9828.00', '2019-07-29 15:03:36', '2019-08-02 11:20:22'),
(259, 238, 'EbN5dnMNSGYETM8m', 'color', 'Orange', '300.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-04 21:37:58', '2019-08-04 21:37:58'),
(242, 54, 'rWnYl9cUZprKvIWQ', 'weight', '1000gms', '1020.00', 'percentage', '50.00', '1529.00', 'percentage', '10.00', '1377.00', '2019-08-01 09:32:29', '2019-08-04 22:33:28'),
(258, 237, '9vvbZGpn3Yv6l6JA', 'color', 'Red', '250.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-04 21:36:25', '2019-08-04 21:36:25'),
(246, 55, 'rFyK16FLkCCHCoJH', 'weight', '1000gms', '680.00', 'percentage', '50.00', '1020.00', 'percentage', '10.00', '918.00', '2019-08-01 10:06:25', '2019-08-04 22:32:48'),
(247, 53, 'dbjvFqvKAsJiFWND', 'weight', '1000gms', '680.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-01 10:10:39', '2019-08-01 10:10:39'),
(248, 57, 'O3J5NsPSiAimNLNQ', 'weight', '1000gms', '800.00', 'percentage', '50.00', '1200.00', 'percentage', '10.00', '1080.00', '2019-08-01 10:12:50', '2019-08-04 22:32:01'),
(249, 229, 'VKw7qRLrPg3nsTqE', 'weight', '500gms', '220.00', 'percentage', '50.00', '330.00', 'percentage', '10.00', '297.00', '2019-08-01 10:19:25', '2019-08-03 23:11:26'),
(250, 230, 'rCuXE6hsnAoR9UkH', 'quantity', '1', '700.00', 'percentage', '50.00', '1050.00', 'percentage', '10.00', '945.00', '2019-08-03 21:58:25', '2019-08-03 22:00:28'),
(251, 231, 'odvEzqQrKRmt1BEy', 'quantity', '1', '500.00', 'percentage', '50.00', '750.00', 'percentage', '10.00', '675.00', '2019-08-03 22:04:39', '2019-08-03 22:06:10'),
(252, 232, 'z32geaiCNesHXH9l', 'quantity', '1', '150.00', 'fixed', '200.00', '350.00', 'fixed', '50.00', '300.00', '2019-08-03 22:11:58', '2019-08-03 22:13:33'),
(253, 233, '4V1R6xvqnh9611Hj', 'quantity', '1', '750.00', 'percentage', '50.00', '1125.00', 'percentage', '10.00', '1013.00', '2019-08-03 22:19:39', '2019-08-03 22:20:55'),
(254, 234, 'kBhhvVC9A6hDEqvP', 'quantity', '1', '260.00', 'fixed', '340.00', '600.00', 'fixed', '100.00', '500.00', '2019-08-03 22:32:14', '2019-08-03 22:35:18'),
(255, 235, 'Ut6Xq2d3CpCZ8XJG', 'quantity', '1', '200.00', 'fixed', '400.00', '600.00', 'fixed', '100.00', '500.00', '2019-08-03 22:36:54', '2019-08-03 22:37:23'),
(256, 236, 'IU5nT4oipLuOOXn4', 'quantity', '1', '500.00', 'percentage', '50.00', '750.00', 'percentage', '10.00', '675.00', '2019-08-03 22:41:38', '2019-08-03 22:42:03'),
(257, 229, 'vbBRapBwq2vMfyRe', 'weight', '1kg', '440.00', 'percentage', '50.00', '660.00', 'percentage', '10.00', '594.00', '2019-08-03 23:09:15', '2019-08-03 23:11:26'),
(260, 239, '50Aaaemj77Xr6Rsw', 'color', 'Red', '300.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-04 21:38:55', '2019-08-04 21:38:55'),
(261, 240, '1hZW2x0AN7DgW3OQ', 'quantity', '1', '900.00', 'percentage', '50.00', '1350.00', 'percentage', '10.00', '1215.00', '2019-08-04 21:46:59', '2019-08-04 21:50:07'),
(262, 241, 'tIFigDhTejKK0Y1I', 'quantity', '1', '1100.00', 'percentage', '50.00', '1650.00', 'percentage', '10.00', '1485.00', '2019-08-04 22:17:46', '2019-08-04 22:45:16'),
(263, 242, '3UAj78cLBbjQuvei', 'quantity', '1', '1000.00', 'percentage', '50.00', '1500.00', 'percentage', '10.00', '1350.00', '2019-08-04 23:02:59', '2019-08-04 23:03:42'),
(264, 243, '5bgHo7FrTO7jlgGb', 'weight', '1', '720.00', 'percentage', '50.00', '1080.00', 'percentage', '5.00', '890.00', '2019-08-04 23:17:36', '2019-08-06 08:57:23'),
(266, 244, 'GtG4JqJL8BdcrVdr', 'weight', '0.5', '495.00', 'percentage', '50.00', '742.00', 'percentage', '5.00', '705.00', '2019-08-05 21:24:40', '2019-08-06 08:57:08'),
(267, 244, 'gquvuxnsGtJ6Zrrw', 'weight', '1', '990.00', 'percentage', '50.00', '1485.00', 'percentage', '5.00', '1411.00', '2019-08-05 21:24:40', '2019-08-06 08:57:08'),
(268, 68, 'YmDqx0mcmNHbETVE', 'weight', '1000gms', '520.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-05 22:12:30', '2019-08-05 22:12:30'),
(269, 64, 'eZ7kZ7LlxQKUH18y', 'weight', '1000gms', '540.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-05 22:21:49', '2019-08-05 22:21:49'),
(270, 245, 'ud1ASb1j3vQpLxZb', 'weight', '0.5', '495.00', 'percentage', '50.00', '742.00', 'percentage', '10.00', '668.00', '2019-08-06 08:46:49', '2019-08-06 08:48:40'),
(271, 245, 'ZtCNNP9Hpr6QDKoD', 'weight', '1', '990.00', 'percentage', '50.00', '1485.00', 'percentage', '10.00', '1337.00', '2019-08-06 08:46:49', '2019-08-06 08:48:40'),
(272, 246, 'GTTt774V2A75NfMj', 'weight', '1', '700.00', 'percentage', '30.00', '910.00', 'percentage', '10.00', '819.00', '2019-08-06 10:38:37', '2019-08-06 10:41:03'),
(273, 247, 'BWTaBjcbnbc8kpyS', 'quantity', '1', '300.00', 'percentage', '50.00', '450.00', 'percentage', '10.00', '378.00', '2019-08-07 09:34:29', '2019-08-07 09:36:22'),
(274, 248, 'k8veGycOvVShStUP', 'quantity', '1', '300.00', 'percentage', '50.00', '450.00', 'percentage', '10.00', '405.00', '2019-08-07 09:57:43', '2019-08-07 10:00:38'),
(275, 249, 'ButudnnmhwbXHTT0', 'quantity', '1', '300.00', 'percentage', '50.00', '450.00', 'percentage', '10.00', '405.00', '2019-08-07 10:03:54', '2019-08-07 10:04:46'),
(276, 250, 'EzSocwPnTzQRq0fI', 'weight', '1', '300.00', 'percentage', '50.00', '450.00', 'percentage', '10.00', '405.00', '2019-08-07 10:07:53', '2019-08-07 10:09:09'),
(277, 251, 'vJ7QkX2bZqRouJsS', 'weight', '1', '300.00', 'percentage', '50.00', '450.00', 'percentage', '10.00', '405.00', '2019-08-07 10:25:24', '2019-08-07 10:25:59'),
(278, 252, 'mbk0ZhmN6X9txWUB', 'quantity', '1', '400.00', 'percentage', '50.00', '600.00', 'percentage', '10.00', '540.00', '2019-08-07 10:34:31', '2019-08-07 10:50:37'),
(279, 253, 'VFRwLmhoMXIRDdLz', 'weight', '1', '400.00', 'percentage', '50.00', '600.00', 'percentage', '10.00', '540.00', '2019-08-07 10:43:09', '2019-08-07 10:49:58'),
(280, 254, 'DVaYkyXg0bCqpPmj', 'quantity', '1', '400.00', 'percentage', '50.00', '600.00', 'percentage', '10.00', '540.00', '2019-08-07 10:45:26', '2019-08-07 10:47:58'),
(281, 255, '6VsLjdCxTTD5Kxi4', 'weight', '1', '7400.00', 'percentage', '50.00', '11100.00', 'percentage', '10.00', '9990.00', '2019-08-07 16:10:44', '2019-08-08 10:08:41'),
(282, 256, 'v68B7pfXwUtDFqgz', 'weight', '1', '7800.00', 'percentage', '50.00', '11700.00', 'percentage', '10.00', '10530.00', '2019-08-07 16:20:55', '2019-08-08 10:08:10'),
(283, 257, 'skJgRvuXWnrN1xuN', 'weight', '1', '7400.00', 'percentage', '50.00', '11100.00', 'percentage', '10.00', '9990.00', '2019-08-07 16:27:23', '2019-08-08 10:00:28'),
(284, 258, 'NsBvI5kI7vnr5vCn', 'weight', '1', '8000.00', 'percentage', '50.00', '12000.00', 'percentage', '10.00', '10800.00', '2019-08-07 16:33:07', '2019-08-08 09:59:33'),
(285, 259, 'yG4oMoRg53LZFFwG', 'weight', '0.5', '200.00', 'percentage', '50.00', '300.00', 'percentage', '10.00', '270.00', '2019-08-07 18:52:03', '2019-08-07 20:04:15'),
(286, 260, 'Zp6DMzyMD5FOYkdk', 'weight', '0.5', '200.00', 'percentage', '50.00', '300.00', 'percentage', '10.00', '270.00', '2019-08-07 19:58:08', '2019-08-07 20:03:11'),
(287, 261, 'pAYmPGA8iRERxPFn', 'weight', '0.5', '210.00', 'percentage', '50.00', '315.00', 'percentage', '10.00', '284.00', '2019-08-07 20:16:36', '2019-08-07 20:20:22'),
(288, 262, 'eVK0JpLyAYJLTLFk', 'quantity', '0.5', '200.00', 'percentage', '50.00', '300.00', 'percentage', '10.00', '270.00', '2019-08-07 20:22:18', '2019-08-07 20:22:39'),
(289, 263, 'sNYOsGE9wlsYcIBV', 'weight', '0.5', '300.00', 'percentage', '50.00', '450.00', 'percentage', '10.00', '405.00', '2019-08-07 20:25:48', '2019-08-07 20:26:39'),
(290, 264, 'pBJhnGXcUkmhJmTO', 'weight', '0.5', '250.00', 'percentage', '50.00', '375.00', 'percentage', '10.00', '338.00', '2019-08-07 20:29:05', '2019-08-07 20:29:56'),
(291, 265, 'rYsxPOdgRWRxJ0a1', 'weight', '0.5', '400.00', 'percentage', '10.00', '440.00', 'percentage', '5.00', '418.00', '2019-08-07 20:31:26', '2019-08-07 20:59:43'),
(292, 266, '2kzTccRC9bfilt17', 'weight', '0.5', '250.00', 'percentage', '50.00', '375.00', 'percentage', '10.00', '338.00', '2019-08-07 20:32:55', '2019-08-07 20:33:37'),
(293, 267, 'OOFQTRlcIKUaLXvw', 'weight', '0.5', '250.00', 'percentage', '50.00', '375.00', 'percentage', '10.00', '338.00', '2019-08-07 20:35:53', '2019-08-07 20:37:16'),
(294, 268, 'msPfTNPBA7SEAz6N', 'quantity', '1', '400.00', 'percentage', '60.00', '640.00', 'percentage', '10.00', '576.00', '2019-08-07 21:50:05', '2019-08-07 21:51:21'),
(295, 269, 'bXmipGRdJaOEdyr0', 'weight', '0.5', '400.00', 'percentage', '50.00', '600.00', 'fixed', '10.00', '590.00', '2019-08-08 14:41:31', '2019-08-08 16:45:25'),
(296, 270, 'r2t6j4UI3ZYiZbpP', 'weight', '0.5', '500.00', 'percentage', '50.00', '750.00', 'percentage', '10.00', '675.00', '2019-08-08 16:34:10', '2019-08-08 16:40:54'),
(297, 271, 'uogxyFDHCGK0n1c0', 'weight', '0.5', '490.00', 'percentage', '50.00', '735.00', 'percentage', '10.00', '662.00', '2019-08-08 16:56:29', '2019-08-08 16:57:16'),
(298, 272, 'xyK9XiDBlJ4hy70x', 'weight', '0.5', '470.00', 'percentage', '50.00', '705.00', 'percentage', '10.00', '635.00', '2019-08-08 17:08:28', '2019-08-08 17:20:33'),
(299, 273, 'Ln9ITkbHlGHHD65j', 'weight', '0.5', '500.00', 'percentage', '50.00', '750.00', 'percentage', '10.00', '675.00', '2019-08-08 17:19:07', '2019-08-08 17:20:02'),
(300, 274, 'hzw72H55qsJSAt3W', 'weight', '0.5', '520.00', 'percentage', '50.00', '780.00', 'percentage', '10.00', '702.00', '2019-08-08 17:30:17', '2019-08-08 17:31:11'),
(301, 275, '41Z14wYUJugOKxAU', 'weight', '0.5', '850.00', 'percentage', '50.00', '1275.00', 'percentage', '10.00', '1148.00', '2019-08-08 17:46:14', '2019-08-08 17:48:54'),
(302, 276, 'xyldZKSVN41XBiO9', 'weight', '0.5', '850.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-08 17:47:55', '2019-08-08 17:47:55'),
(303, 277, 'iERnxhn5SuLZaT5r', 'weight', '0.5', '590.00', 'percentage', '50.00', '885.00', 'percentage', '10.00', '797.00', '2019-08-08 18:07:48', '2019-08-08 18:09:25'),
(304, 278, 'eCnB65JesGKXgYqu', 'weight', '0.5', '490.00', 'percentage', '50.00', '735.00', 'percentage', '10.00', '662.00', '2019-08-08 22:22:14', '2019-08-08 22:23:23'),
(305, 279, 'Y7eEWxGG0YSKZPaq', 'weight', '0.5', '510.00', 'percentage', '50.00', '764.00', 'percentage', '10.00', '688.00', '2019-08-08 22:38:08', '2019-08-08 22:40:24'),
(306, 280, 'G1gMkzsn1OKTDCNG', 'weight', '0.5', '510.00', 'percentage', '50.00', '764.00', 'percentage', '10.00', '688.00', '2019-08-08 22:39:41', '2019-08-08 22:41:16'),
(307, 281, 'BBC4Vjouj4KmPmlP', 'weight', '0.5', '380.00', 'percentage', '50.00', '570.00', 'percentage', '10.00', '513.00', '2019-08-09 06:50:43', '2019-08-09 06:51:32'),
(308, 282, 'NQ06Zmwe4BNaPoEA', 'weight', '0.5', '510.00', 'percentage', '50.00', '764.00', 'percentage', '10.00', '688.00', '2019-08-09 07:01:21', '2019-08-09 07:02:10');
INSERT INTO `product_skus` (`sku_id`, `sku_product_id`, `sku_no`, `sku_type`, `sku_option`, `sku_vendor_price`, `sku_vdc_commission_type`, `sku_vdc_commission_value`, `sku_vdc_final_price`, `sku_store_discount_type`, `sku_store_discount_value`, `sku_store_price`, `created_at`, `updated_at`) VALUES
(309, 283, '9PRvojLbaySvuLCr', 'weight', '0.5', '490.00', 'percentage', '50.00', '735.00', 'percentage', '10.00', '662.00', '2019-08-09 07:12:01', '2019-08-09 07:12:36'),
(310, 284, 'pNgRK3n7JOwcEkcP', 'weight', '0.5', '470.00', 'percentage', '50.00', '705.00', 'percentage', '10.00', '635.00', '2019-08-09 07:20:33', '2019-08-09 07:21:07'),
(311, 285, 'Y7eWLyzOX6gPddRC', 'weight', '0.5', '500.00', 'percentage', '50.00', '750.00', 'percentage', '10.00', '675.00', '2019-08-09 07:29:36', '2019-08-09 07:30:09'),
(312, 286, 'e90GoNwcYQxjv8Uo', 'weight', '0.5', '520.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-09 07:38:32', '2019-08-09 07:38:32'),
(313, 287, '6wBNr6qzMOsklYQx', 'weight', '0.5', '520.00', 'percentage', '50.00', '780.00', 'percentage', '10.00', '702.00', '2019-08-09 07:40:10', '2019-08-09 07:42:47'),
(314, 288, 'AwIliocV76Ruf2Pa', 'weight', '0.5', '520.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-09 07:41:24', '2019-08-09 07:41:24'),
(315, 289, 'BT6xys2ZyVfVJTOb', 'weight', '0.5', '850.00', 'percentage', '50.00', '1275.00', 'percentage', '10.00', '1148.00', '2019-08-09 07:51:08', '2019-08-09 07:52:08'),
(316, 56, 'hwxR1THhkL25Cmcs', 'weight', '10.6', '650.00', 'percentage', '50.00', '975.00', 'percentage', '10.00', '878.00', '2019-08-22 17:13:50', '2019-08-27 07:42:56'),
(317, 56, 'OHXH015OqCmwWbbc', 'weight', '20.3', '1080.00', 'percentage', '50.00', '1620.00', 'percentage', '10.00', '1458.00', '2019-08-22 17:13:50', '2019-08-27 07:42:56'),
(318, 56, 'anErZ4nQYk9TZkBJ', 'weight', '33.4', '1700.00', 'percentage', '50.00', '2550.00', 'percentage', '10.00', '2295.00', '2019-08-22 17:13:50', '2019-08-27 07:42:56'),
(319, 56, 'GNBChmnHDuHFSSBc', 'weight', '49', '2400.00', 'percentage', '50.00', '3600.00', 'percentage', '10.00', '3240.00', '2019-08-22 17:13:50', '2019-08-27 07:42:56'),
(320, 56, '1CTVN85cdG4ynokI', 'weight', '56.1', '2750.00', 'percentage', '50.00', '4125.00', 'percentage', '10.00', '3713.00', '2019-08-22 17:13:50', '2019-08-27 07:42:56'),
(321, 56, 'BZTcAfjMjS95KSLE', 'weight', '74.2', '3600.00', 'percentage', '50.00', '5400.00', 'percentage', '10.00', '4860.00', '2019-08-22 17:13:50', '2019-08-27 07:42:56'),
(322, 56, 'ty0bokJfk5LSDRVB', 'weight', '92.5', '4400.00', 'percentage', '50.00', '6600.00', 'percentage', '10.00', '5940.00', '2019-08-22 17:13:50', '2019-08-27 07:42:56'),
(323, 290, 'UkbautrPTnG3mmUE', 'weight', '60.9 grams', '3150.00', 'percentage', '50.00', '4425.00', 'percentage', '10.00', '3983.00', '2019-08-22 17:29:37', '2019-10-11 22:47:01'),
(324, 290, 'qB1bpKY3wtGZdodw', 'weight', '78.2 grams', '3950.00', 'percentage', '50.00', '5625.00', 'percentage', '10.00', '5063.00', '2019-08-22 17:29:37', '2019-10-11 22:47:01'),
(325, 291, 'VsF0x8cFxaNHLW2K', 'weight', '47.7 grams', '2550.00', 'percentage', '50.00', '3525.00', 'percentage', '10.00', '3173.00', '2019-08-22 17:32:59', '2019-10-11 22:45:40'),
(326, 291, '2a50Dk0iwj3xcca9', 'weight', '49.2 grams', '2650.00', 'percentage', '50.00', '3675.00', 'percentage', '10.00', '3308.00', '2019-08-22 17:32:59', '2019-10-11 22:45:40'),
(327, 291, 'nPQVBy3uhzY3fu3E', 'weight', '68.1grams', '3480.00', 'percentage', '50.00', '4919.00', 'percentage', '10.00', '4428.00', '2019-08-22 17:32:59', '2019-10-11 22:45:40'),
(328, 290, 'BejRVtWrapU7MWui', 'weight', '65.7 grams', '3400.00', 'percentage', '50.00', '4800.00', 'percentage', '10.00', '4580.00', '2019-08-22 17:35:02', '2019-10-11 22:47:01'),
(329, 290, 'iz5pH2b23p6YDxpj', 'weight', '79.2 grams', '4000.00', 'percentage', '50.00', '5700.00', 'percentage', '10.00', '5130.00', '2019-08-22 17:35:02', '2019-10-11 22:47:01'),
(330, 292, 'h8uGgYq8CZz1OQ69', 'Size', '2 inch', '800.00', 'percentage', '50.00', '1200.00', 'percentage', '10.00', '1080.00', '2019-08-22 17:43:55', '2019-08-27 07:00:58'),
(331, 292, 'WBsziJKFUykgFfPl', 'Size', '3 inch', '1100.00', 'percentage', '50.00', '1650.00', 'percentage', '10.00', '1485.00', '2019-08-22 17:43:55', '2019-08-27 07:00:58'),
(332, 292, '5c3FRMtsS3a7RQoW', 'Size', '4 inch', '1500.00', 'percentage', '50.00', '2250.00', 'percentage', '10.00', '2025.00', '2019-08-22 17:43:55', '2019-08-27 07:00:58'),
(333, 82, 'cTIL91KDiolKOzc6', 'weight', '2 grams', '250.00', 'percentage', '50.00', '300.00', 'percentage', '10.00', '270.00', '2019-08-22 17:47:04', '2019-10-11 22:48:14'),
(334, 82, 'QZ8EEC3IevDDWFqB', 'weight', '5 grams', '350.00', 'percentage', '50.00', '450.00', 'percentage', '10.00', '405.00', '2019-08-22 17:47:04', '2019-10-11 22:48:14'),
(335, 82, 'WZYegSvnw79BX1Oy', 'weight', '10 grams', '650.00', 'percentage', '50.00', '900.00', 'percentage', '10.00', '810.00', '2019-08-22 17:47:04', '2019-10-11 22:48:14'),
(336, 82, 'jSC4MYhGdQxXGM5G', 'weight', '20 grams', '1150.00', 'percentage', '50.00', '1650.00', 'percentage', '10.00', '1485.00', '2019-08-22 17:47:04', '2019-10-11 22:48:14'),
(337, 82, 'Ctf5TDdWagLeXGy2', 'weight', '50 grams', '2550.00', 'percentage', '50.00', '3750.00', 'percentage', '10.00', '3375.00', '2019-08-22 17:47:04', '2019-10-11 22:48:14'),
(338, 79, 'DjDB9MqFT2ioEVGV', 'weight', '11 grams', '700.00', 'percentage', '50.00', '975.00', 'percentage', '10.00', '878.00', '2019-08-22 17:52:44', '2019-10-11 22:50:32'),
(339, 79, 'rsgpSVFZLQfoKh3h', 'weight', '18 grams', '1000.00', 'percentage', '50.00', '1425.00', 'percentage', '10.00', '1283.00', '2019-08-22 17:52:44', '2019-10-11 22:50:32'),
(340, 78, 'ADGnbJzEswhPHXSV', 'weight', '11.5 grams', '680.00', 'percentage', '50.00', '1020.00', 'percentage', '10.00', '918.00', '2019-08-22 17:53:37', '2019-08-29 18:59:40'),
(341, 235, 'XmINas4pLTgX9ak1', 'weight', '300 grams', '0.00', 'percentage', '0.00', '0.00', 'percentage', '0.00', '0.00', '2019-09-06 18:15:32', '2019-09-06 18:18:05'),
(342, 293, '4In2OkK1180Z5rWZ', 'quantity', '1', '1750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-16 13:12:07', '2019-09-16 13:12:07'),
(343, 294, 'EDvtPPT7g5q47wnR', 'quantity', '1', '1750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-16 13:15:19', '2019-09-16 13:15:19'),
(344, 295, 'nKb8cjecjgHawDtN', 'quantity', '1', '1750.00', 'percentage', '35.00', '2362.00', 'percentage', '10.00', '2126.00', '2019-09-16 17:47:53', '2019-09-17 14:23:12'),
(345, 296, 'OIJ2JiTHNpUASsiU', 'quantity', '1', '1750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-16 17:50:10', '2019-09-16 17:50:10'),
(346, 297, 'zRrPqTE8Z0qm6C6i', 'weight', '1', '1750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-16 17:52:07', '2019-09-16 17:52:07'),
(347, 298, 'HFL459cVKnJ5cXm6', 'weight', '1', '1750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-16 17:52:41', '2019-09-16 17:52:41'),
(348, 299, 'jMrblaDL9PsseG4b', 'weight', '1', '3650.00', 'percentage', '35.00', '4927.00', 'percentage', '10.00', '4435.00', '2019-09-16 18:54:32', '2019-09-17 14:22:24'),
(349, 300, 'ZEdzYpWlWkenQUh0', 'weight', '1', '3650.00', 'percentage', '35.00', '4927.00', 'percentage', '10.00', '4435.00', '2019-09-16 19:01:39', '2019-09-17 14:21:39'),
(350, 301, 'SV6awjCAv0kTU1Xx', 'weight', '1', '1350.00', 'percentage', '45.00', '1957.00', 'percentage', '10.00', '1762.00', '2019-09-16 19:08:29', '2019-09-17 14:27:05'),
(351, 302, 'pGP2wZiTdBiwVE3K', 'weight', '1', '1780.00', 'percentage', '35.00', '2403.00', 'percentage', '25.00', '1803.00', '2019-09-16 19:16:26', '2019-09-17 14:20:04'),
(352, 303, 'BDIY8DO9hqbtrRbW', 'weight', '0.5', '400.00', 'fixed', '50.00', '450.00', 'fixed', '0.00', '450.00', '2019-09-24 00:41:58', '2019-09-24 00:52:18'),
(353, 303, 'nBWBB6xe51DUufas', 'weight', '1', '800.00', 'fixed', '50.00', '850.00', 'fixed', '0.00', '850.00', '2019-09-24 00:41:58', '2019-09-24 00:52:18'),
(354, 304, '5MukFADWxQWlJR3b', 'weight', '0.5', '400.00', 'fixed', '50.00', '450.00', 'fixed', '0.00', '450.00', '2019-09-24 00:57:08', '2019-09-24 00:58:24'),
(355, 304, 'Y0KOJj4DXtXtMPAn', 'weight', '1', '800.00', 'fixed', '50.00', '850.00', 'fixed', '0.00', '850.00', '2019-09-24 00:57:08', '2019-09-24 00:58:24'),
(356, 305, 'AZR0KwgcXzJKCLtR', 'weight', '0.5', '400.00', 'fixed', '50.00', '450.00', 'fixed', '0.00', '450.00', '2019-09-24 01:07:44', '2019-09-24 01:08:23'),
(357, 305, '6ks3J4VOGZTp47P0', 'weight', '1', '800.00', 'fixed', '50.00', '850.00', 'fixed', '0.00', '850.00', '2019-09-24 01:07:44', '2019-09-24 01:08:23'),
(358, 306, 'XqLyH5acRbpW1Glh', 'weight', '0.5', '400.00', 'fixed', '50.00', '450.00', 'fixed', '0.00', '450.00', '2019-09-24 01:21:36', '2019-09-24 01:22:12'),
(359, 306, 'YYuoZ6M2YH0EV8hu', 'weight', '1', '800.00', 'fixed', '50.00', '850.00', 'fixed', '0.00', '850.00', '2019-09-24 01:21:36', '2019-09-24 01:22:12'),
(360, 307, 'zy3kA47N1rZbDPWS', 'weight', '0.5', '400.00', 'fixed', '50.00', '450.00', 'fixed', '0.00', '450.00', '2019-09-24 01:25:20', '2019-09-24 01:25:46'),
(361, 307, 'V1ujVy8typodQ6nC', 'weight', '1', '800.00', 'fixed', '50.00', '850.00', 'fixed', '0.00', '850.00', '2019-09-24 01:25:20', '2019-09-24 01:25:46'),
(362, 308, 'idBctKcDDn09ZnZT', 'weight', '0.5', '400.00', 'fixed', '50.00', '450.00', 'fixed', '0.00', '450.00', '2019-09-24 01:47:08', '2019-09-24 01:47:39'),
(363, 308, 'W3loQcFrQlzdjwkw', 'weight', '1', '800.00', 'fixed', '50.00', '850.00', 'fixed', '0.00', '850.00', '2019-09-24 01:47:08', '2019-09-24 01:47:39'),
(364, 309, 'aAN5ZMHAsdvPIBTm', 'weight', '0.5', '750.00', 'fixed', '50.00', '800.00', 'fixed', '0.00', '800.00', '2019-09-24 02:03:05', '2019-09-24 02:03:37'),
(365, 309, 'g19VHMJQ0JHhLOdw', 'weight', '1', '1500.00', 'fixed', '50.00', '1550.00', 'fixed', '0.00', '1550.00', '2019-09-24 02:03:05', '2019-09-24 02:03:37'),
(366, 310, 'GvwPYr5qt00Blujc', 'weight', '0.5', '500.00', 'fixed', '50.00', '550.00', 'fixed', '0.00', '550.00', '2019-09-24 02:11:42', '2019-09-24 02:12:08'),
(367, 310, 'dZO5wh3LmBYRyXtI', 'weight', '1', '1000.00', 'fixed', '50.00', '1050.00', 'fixed', '0.00', '1050.00', '2019-09-24 02:11:42', '2019-09-24 02:12:08'),
(368, 311, '4NmO1SNgujt8vGgG', 'weight', '0.5', '1000.00', 'fixed', '50.00', '1050.00', 'fixed', '0.00', '1050.00', '2019-09-24 02:34:39', '2019-09-24 02:35:07'),
(369, 311, 'jHEbZwv6gIbBh8MP', 'weight', '1', '2000.00', 'fixed', '50.00', '2050.00', 'fixed', '0.00', '2050.00', '2019-09-24 02:34:39', '2019-09-24 02:35:07'),
(370, 312, 'RoIXYe3nkHYZVKuH', 'weight', '1', '1100.00', 'fixed', '400.00', '1500.00', 'fixed', '50.00', '1450.00', '2019-10-01 09:19:19', '2019-10-01 20:17:35'),
(371, 313, 'LJFF4PVvQxq7vO6v', 'weight', '1 KG', '1500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-01 10:15:35', '2019-10-01 10:15:35'),
(372, 314, 'LrtjF97xT4DUuw3B', 'weight', '5', '1000.00', 'percentage', '2.00', '1020.00', 'percentage', '10.00', '918.00', '2019-10-01 10:17:22', '2019-10-01 10:35:23'),
(373, 315, 'F2YygMjDCj0rNX2U', 'weight', '1', '3150.00', 'percentage', '40.00', '4410.00', 'percentage', '5.00', '4190.00', '2019-10-01 16:59:53', '2019-10-01 20:15:50'),
(374, 316, 'dZRyP3GLMlwMqsSi', 'weight', '1', '2850.00', 'fixed', '2000.00', '4850.00', 'fixed', '1000.00', '3850.00', '2019-10-11 09:44:34', '2019-10-11 23:42:10'),
(375, 317, 'x7TOXVEvRX7zylk2', 'weight', '1', '4600.00', 'fixed', '1500.00', '6100.00', 'fixed', '800.00', '5300.00', '2019-10-11 21:57:18', '2019-10-11 22:56:24'),
(376, 318, '8QiWWSMO9jWn7YkL', 'weight', '1', '2750.00', 'fixed', '1500.00', '4250.00', 'fixed', '900.00', '3350.00', '2019-10-11 21:59:54', '2019-10-11 22:57:34'),
(377, 319, '2T1mnC9AE0cO9dyl', 'weight', '1', '1850.00', 'fixed', '1200.00', '3050.00', 'fixed', '600.00', '2450.00', '2019-10-12 13:59:10', '2019-10-12 20:01:18'),
(378, 320, 'jvb5ivzLcMiPA9gb', 'weight', '1', '2480.00', 'fixed', '1500.00', '3980.00', 'fixed', '900.00', '3080.00', '2019-10-12 21:41:31', '2019-10-12 23:19:10'),
(379, 321, 'XsJwhFsnaryF0mSD', 'weight', '1', '1600.00', 'fixed', '1050.00', '2650.00', 'fixed', '600.00', '2050.00', '2019-10-14 11:46:01', '2019-10-14 20:38:00'),
(380, 322, 'tZYsnB14n8wEN5FS', 'weight', '1', '1600.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-14 12:15:49', '2019-10-14 12:15:49'),
(381, 323, 'hS7ZAg1s3JHtvHyc', 'weight', '1', '1600.00', 'fixed', '1050.00', '2650.00', 'fixed', '600.00', '2050.00', '2019-10-14 12:16:43', '2019-10-14 20:34:27'),
(382, 324, '1cgzjmnveqplCn1T', 'weight', '1', '1600.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-14 12:17:24', '2019-10-14 12:17:24'),
(383, 325, 'B7sgIoj9jOUqo3Sa', 'weight', '1', '1020.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-14 16:15:25', '2019-10-14 16:15:25'),
(384, 326, 'JgDxVcN7vluQSP7A', 'weight', '1', '1020.00', 'fixed', '380.00', '1400.00', 'fixed', '0.00', '1400.00', '2019-10-14 16:30:11', '2019-10-15 03:26:37'),
(385, 327, 'EaL3mFmjwgl4EIN3', 'weight', '1', '2050.00', 'fixed', '1500.00', '3550.00', 'fixed', '700.00', '2850.00', '2019-10-17 10:08:28', '2019-10-17 10:11:40'),
(386, 328, 'ruWJ3zWuKOQgkjUD', 'weight', '1', '7650.00', 'fixed', '2500.00', '10150.00', 'fixed', '1000.00', '8650.00', '2019-10-28 00:50:14', '2019-10-28 01:28:28'),
(387, 329, 'D7Ho1vay8BZiDi6f', 'weight', '1', '11250.00', 'fixed', '3000.00', '14250.00', 'fixed', '1800.00', '12450.00', '2019-10-28 01:14:42', '2019-10-28 01:31:43'),
(388, 330, 'HCDrZYObOkRqEFvb', 'weight', '1', '2500.00', 'fixed', '2000.00', '4500.00', 'fixed', '1250.00', '3250.00', '2019-10-28 01:22:26', '2019-10-28 01:35:29'),
(389, 331, '008ENHOXIVrOW9kb', 'weight', '1', '2900.00', 'fixed', '1500.00', '4400.00', 'fixed', '800.00', '3600.00', '2019-11-02 23:35:53', '2019-11-05 09:19:45'),
(390, 332, 'Ts464jgZFojqG8LL', 'weight', '1', '950.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-02 23:39:54', '2019-11-02 23:39:54'),
(391, 333, 'CuaKgnAp51X1C8iT', 'weight', '1', '950.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-02 23:41:46', '2019-11-02 23:41:46'),
(392, 334, 'OmpyZyXF2pc56UsC', 'weight', '1', '950.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-02 23:42:22', '2019-11-02 23:42:22'),
(393, 335, '5SNEQg9VK5ZWyyfi', 'weight', '1', '950.00', 'fixed', '1000.00', '1950.00', 'fixed', '500.00', '1450.00', '2019-11-03 17:28:25', '2020-06-18 06:37:43'),
(394, 336, 'OedJFzfJGoqjvVpn', 'weight', '1', '950.00', NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 10:58:29', '2019-11-05 10:58:29'),
(395, 337, 'RhwWKCQG65TxpxSZ', 'weight', '1', '1850.00', 'fixed', '750.00', '2600.00', 'fixed', '150.00', '2450.00', '2019-11-05 11:02:23', '2019-11-05 12:04:02'),
(396, 338, 'bheCJYdNllSmYSmn', 'weight', '1', '2150.00', 'fixed', '1500.00', '3650.00', 'fixed', '900.00', '2750.00', '2019-11-18 11:11:23', '2019-11-18 19:29:22'),
(397, 339, 'O2BxmtKMj6CYL5rK', 'weight', '1', '7700.00', 'fixed', '1500.00', '9200.00', 'fixed', '650.00', '8550.00', '2019-12-06 21:41:19', '2019-12-06 22:00:08'),
(398, 340, 'znv6bK1SBI8NWR5X', 'weight', '1', '3900.00', 'fixed', '1000.00', '4900.00', 'fixed', '250.00', '4650.00', '2019-12-10 09:22:57', '2019-12-10 10:38:11'),
(399, 341, 'sv1pApnVFxrM8lwy', 'weight', '1', '1900.00', 'fixed', '800.00', '2700.00', 'fixed', '200.00', '2500.00', '2019-12-10 09:25:33', '2019-12-10 10:37:03'),
(400, 342, 'Thu9wSPqVKAC2zqf', 'Size', 'L', '500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-09 20:11:25', '2020-01-09 20:11:25'),
(401, 343, 'EFMzq4Llo15nkHeg', 'weight', '0.5', '1400.00', 'fixed', '350.00', '1750.00', 'fixed', '350.00', '1400.00', '2020-02-10 09:15:52', '2020-02-10 09:16:39'),
(402, 344, 'ffB0G57l9IFLrAhW', 'weight', '0.5', '1750.00', 'fixed', '500.00', '2250.00', 'fixed', '450.00', '1800.00', '2020-02-10 10:49:28', '2020-02-10 10:53:35'),
(403, 345, 'dsZcJaCLMgYhoQRO', 'weight', '1', '2100.00', 'fixed', '400.00', '2500.00', 'fixed', '400.00', '2100.00', '2020-02-10 10:57:25', '2020-02-10 11:02:12'),
(404, 346, 'ncCvayrLoIMJH6UR', 'weight', '0.5', '1050.00', 'fixed', '250.00', '1300.00', 'fixed', '250.00', '1050.00', '2020-02-10 11:01:32', '2020-02-10 11:02:36'),
(405, 347, 'FowlLBeix6p2jimt', 'quantity', '100', '7700.00', 'fixed', '0.00', '7700.00', 'fixed', '0.00', '7700.00', '2020-03-05 07:38:29', '2020-03-07 02:48:28'),
(406, 348, '6MApEuYXi63NxERr', 'weight', '1', '2052.00', 'fixed', '700.00', '2752.00', 'fixed', '200.00', '2552.00', '2020-06-19 22:40:42', '2020-06-20 16:27:11'),
(407, 349, 'XbQjdrtnamEi1GqV', 'weight', '1', '10647.00', 'fixed', '700.00', '11347.00', 'fixed', '200.00', '11147.00', '2020-06-19 22:43:21', '2020-06-20 16:26:24'),
(408, 350, 'xvvN7QcieVrdJxuN', 'weight', '1', '980.00', 'fixed', '700.00', '1680.00', 'fixed', '200.00', '1480.00', '2020-06-19 22:46:03', '2020-06-20 16:24:15'),
(409, 351, 'ZHE0DEzOEr9c3sJG', 'weight', '1', '980.00', 'fixed', '700.00', '1680.00', 'fixed', '200.00', '1480.00', '2020-06-19 22:47:52', '2020-06-20 03:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

CREATE TABLE `product_variations` (
  `pv_id` int(10) UNSIGNED NOT NULL,
  `pv_prod_id` int(11) NOT NULL,
  `pv_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv_weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv_mrp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pv_selling_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pv_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `publications`
--

CREATE TABLE `publications` (
  `b_id` bigint(20) UNSIGNED NOT NULL,
  `publication_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pages` int(11) NOT NULL,
  `author` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher_id` int(11) NOT NULL,
  `sku_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dis_price` double(8,2) DEFAULT NULL,
  `publication_slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `meta_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publications_status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pdf_file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_page` int(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE `publishers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pub_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pub_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pub_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pub_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pub_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `publishers`
--

INSERT INTO `publishers` (`id`, `pub_name`, `pub_slug`, `pub_image`, `pub_description`, `pub_status`, `created_at`, `updated_at`) VALUES
(1, 'Sister Niveditha', NULL, NULL, NULL, 1, '2020-10-09 16:20:17', '2020-10-09 16:20:17'),
(2, 'Jayanthi', NULL, NULL, NULL, 1, '2020-10-09 16:20:35', '2020-10-09 20:31:39');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `rt_id` bigint(20) UNSIGNED NOT NULL,
  `rt_user_id` int(11) NOT NULL,
  `rt_product_id` int(11) NOT NULL,
  `rt_rating` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rt_comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`rt_id`, `rt_user_id`, `rt_product_id`, `rt_rating`, `rt_comment`, `created_at`, `updated_at`) VALUES
(1, 36, 196, '5', 'test modifiy', '2019-07-11 10:23:19', '2019-07-11 10:23:19'),
(2, 41, 8, '4', 'Very good product for Cakes Lovers', '2019-07-31 21:46:43', '2019-07-31 21:46:43'),
(3, 41, 8, '4', 'nice', '2019-07-31 21:47:00', '2019-07-31 21:47:00'),
(4, 41, 8, '2', 'Very good products', '2019-07-31 21:48:11', '2019-07-31 21:48:11'),
(5, 41, 12, '4', 'Very good product for Cakes lovers', '2019-07-31 21:50:14', '2019-07-31 21:50:14'),
(6, 41, 179, '4', 'Very nice product', '2019-08-02 11:06:42', '2019-08-02 11:06:42'),
(7, 41, 232, '5', 'Nice product', '2019-08-09 18:19:32', '2019-08-09 18:19:32'),
(8, 41, 292, '4', 'nice one', '2019-08-28 15:32:04', '2019-08-28 15:32:04'),
(9, 41, 205, '4', 'good product', '2019-08-28 15:35:50', '2019-08-28 15:35:50'),
(10, 41, 256, '4', 'good', '2019-09-07 15:20:48', '2019-09-07 15:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `send_enquiries`
--

CREATE TABLE `send_enquiries` (
  `se_id` bigint(20) UNSIGNED NOT NULL,
  `se_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `se_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `se_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `se_message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `se_product_id` int(11) NOT NULL,
  `se_user_id` int(11) DEFAULT NULL,
  `se_otp` int(11) DEFAULT NULL,
  `se_status` int(11) DEFAULT NULL,
  `se_ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `se_reply_msg` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `s_id` int(10) UNSIGNED NOT NULL,
  `s_contact_person` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_address_line_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_address_line_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_options` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_msg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'medical,tutor,property,visa,insurance',
  `s_duration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_duedate` date DEFAULT NULL,
  `s_currency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_no_of_days` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_unit_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_unit_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_sub_total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_tax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'null,pending,paid',
  `s_payment_invoice_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_payment_transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_payment_payer` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_payment_transactions` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`s_id`, `s_contact_person`, `s_address_line_1`, `s_address_line_2`, `s_country`, `s_state`, `s_city`, `s_options`, `s_date`, `s_time`, `s_email`, `s_phone`, `s_msg`, `s_user_id`, `s_type`, `s_duration`, `s_duedate`, `s_currency`, `s_no_of_days`, `s_unit_price`, `s_unit_type`, `s_sub_total`, `s_tax`, `s_total`, `s_status`, `s_payment_invoice_num`, `s_payment_transaction_id`, `s_payment_payer`, `s_payment_transactions`, `created_at`, `updated_at`) VALUES
(20001, 'Nyalakanti Raja Gonda', '3-127/39/2 MANIKANTA NAGAR', 'BODUPPAL', '1', '52', 'Hyderabad', 'Diagnostics Services', '2019-07-31', '9 AM TO 12 PM', 'rajagonda@gmail.com', '91-9848090537', 'test message', '36', 'medical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-05 10:10:41', '2019-07-05 10:10:41'),
(20002, 'Saritha', 'near kondapur', 'opposite to India bank', '2', '52', 'Hyderabad', 'Wellness Check ups', '2019-07-17', '6 PM TO 9 PM', 'sbandaru@kitchell.com', '1-8628127929', 'Have  to get a wellness check up for my Father in law.', '23', 'medical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-11 00:17:48', '2019-07-11 00:17:48'),
(20003, 'Praveen Nandipati', 'Plot No:91, Kamala Prasanna Nagar', 'Allwyn Colony, Phase1, Kukatpally', '1', '52', 'Hyderabad', 'Wellness Check ups', '2019-07-27', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-7995165789', 'Required Eleder Parents complete Body checkups', '41', 'medical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-23 11:33:19', '2019-07-23 11:33:19'),
(20004, 'Praveen guptha', '91, allwyn colony, phase 1, kukatpally', 'Hyderaabad', '1', '52', 'Hyderabad', 'Diagnostics Services', '2019-08-31', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Test message from praveen of tab testing', '41', 'medical', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-25 19:29:21', '2019-08-25 19:29:21'),
(20005, 'Praveen Test', 'test', 'test', '2', '2', 'Hyd', 'Wellness Check ups', '2019-09-11', '6 AM TO 9 AM', 'itzursai@gmail.com', '91-78787545454', 'sdgsdgsdg', '41', 'medical-assistance', 'From  2019-04-04 to 2019-04-14', '2019-09-11', NULL, '10', '500', 'Day', NULL, '5', NULL, 'pending', NULL, NULL, NULL, NULL, '2019-09-04 17:29:10', '2019-09-04 17:29:28'),
(20006, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'Wellness Check ups', '2019-09-21', '12 PM TO 3 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Test message from praveen', '41', 'medical-assistance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-06 17:21:31', '2019-09-06 17:21:31'),
(20007, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'Music Vocal', '2019-09-20', '3 PM TO 6 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Test message from praveen', '41', 'online-tutor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-06 17:22:02', '2019-09-06 17:22:02'),
(20008, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'India to USA', '2019-09-21', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-9642123254', 'International courier test enquiry service', '41', 'international-courier', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 15:54:35', '2019-09-09 15:54:35'),
(20009, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'Birthday', '2019-09-21', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Event organization testing service', '41', 'event-organization', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 15:55:12', '2019-09-09 15:55:12'),
(20010, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'SEO', '2019-09-11', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Web Development Service', '41', 'web-developement', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 15:55:53', '2019-09-09 15:55:53'),
(20011, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'Dance Classical (Kuchipudi or Bharatanatyam or Bollywood)', '2019-09-20', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Summer Enrichment Testing Service', '41', 'summer-enrichment', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 15:56:30', '2019-09-09 15:56:30'),
(20012, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'New Apartment', '2019-09-21', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Real Estate Services', '41', 'real-estate', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 15:57:05', '2019-09-09 15:57:05'),
(20013, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'UK Insurance', '2019-09-13', '12 PM TO 3 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Visitors Insurance Service testing', '41', 'visitors-insurance', '45', '2019-09-13', NULL, '21', '100', 'Month', NULL, '15', NULL, 'pending', NULL, NULL, NULL, NULL, '2019-09-09 15:57:44', '2019-09-10 08:24:06'),
(20014, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'Other Visas', '2019-09-21', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-9642123254', 'Visa Support Service', '41', 'visa-support-services', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 15:58:20', '2019-09-09 15:58:20'),
(20015, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'Vacate/Cleaning', '2019-09-13', '6 AM TO 9 AM', 'praveennandipati@gmail.com', '91-9642123254', 'Property management', '41', 'property-management', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 15:59:38', '2019-09-09 15:59:38'),
(20016, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '52', 'Hyderabad', 'Maths', '2019-09-18', '6 AM TO 9 AM', 'praveennandipati@gmail.com', '91-9642123254', 'Online tutors Services providing', '41', 'online-tutor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 16:00:19', '2019-09-09 16:00:19'),
(20017, 'Praveen guptha', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', '1', '63', 'Hyderabad', 'Diagnostics Services', '2019-09-26', '6 AM TO 9 AM', 'praveennandipati@gmail.com', '91-9642123254', 'Medical Service', '41', 'medical-assistance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 16:00:58', '2019-09-09 16:00:58'),
(20018, 'Srini', 'Karmanghat', 'Karmanghat', '1', '52', 'Hyderabad', 'Wellness Check ups', '2019-09-25', '12 PM TO 3 PM', 'chvas4@gmail.com', '1-6462413704', 'This is a Test Message', '37', 'medical-assistance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-20 09:20:39', '2019-09-20 09:20:39'),
(20019, 'Hari Priya K', '3754 Deedham Dr', '3754 Deedham Dr', '2', '6', 'San Jose', 'India to USA', '2019-10-28', '9 AM TO 12 PM', 'aharipriya@gmail.com', '1-4086368309', 'Need quote for 11 kgs shipping from India to USA', '93', 'international-courier', '27-10-2019', '2019-10-31', NULL, '1', '92', 'Day', NULL, '0', NULL, 'approved', '5db7168335721_20019', 'PAYID-LW3RNBA3PY8073255553053N', '{}', '[{}]', '2019-10-27 00:05:34', '2019-10-28 21:57:20'),
(20020, 'Srinivas Chenna', '3514 DEL AMO BLVD', '48', '2', '6', 'TORRANCE', 'India to USA', '2019-10-30', '9 AM TO 12 PM', 'srinivas_chenna@yahoo.com', '1-6462413704', 'This is just a test enquiry to try to send an invoice for the international courier service by Srinivas Chenna.', '37', 'international-courier', '2019-30-10-2019-31-10', '1970-01-01', NULL, '1', '1', 'Day', NULL, NULL, NULL, 'pending', NULL, NULL, NULL, NULL, '2019-10-27 04:10:48', '2019-10-28 21:55:45'),
(20021, 'praveennandipati', 'hydeabd', 'hydeabad', '1', '52', 'Hyderabad', 'Music - Guitar', '2020-07-17', '9 AM TO 12 PM', 'praveennandipati@gmail.com', '91-919642123254', 'test message from praveen', '41', 'online-tutor', 'JUN 06 20 - JULY 16 20', '2020-07-21', NULL, '4', '15', 'Day', NULL, '0', NULL, 'pending', NULL, NULL, NULL, NULL, '2020-07-16 20:47:31', '2020-07-16 20:55:24'),
(20022, 'Ramesh Kiran', '91', 'Pragathi Nagar, Kukatpally, Hyderabad', '1', '65', 'Tirunavanatapuram', 'Music - Guitar', '2020-07-24', '12 PM TO 3 PM', 'praveennandipati@gmail.com', '91-7995165789', 'message', '41', 'online-tutor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-19 04:49:25', '2020-07-19 04:49:25');

-- --------------------------------------------------------

--
-- Table structure for table `service_banners`
--

CREATE TABLE `service_banners` (
  `banner_id` bigint(20) UNSIGNED NOT NULL,
  `banner_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_banners`
--

INSERT INTO `service_banners` (`banner_id`, `banner_title`, `banner_image`, `banner_link`, `banner_status`, `created_at`, `updated_at`) VALUES
(1, 'We are open to medical requests', '1558514967servicecarousel02.jpg', 'http://velchala.com/service/medical', 'active', '2019-05-22 21:19:27', '2019-05-31 19:37:31'),
(2, 'Get the best online tutoring with accredited tutors', '1558515083servicecarousel01.jpg', 'http://velchala.com/service/online-tutor', 'active', '2019-05-22 21:21:23', '2019-05-31 19:37:23'),
(3, 'Take hold of our gratifying services in property management', '1558515131servicecarousel04.jpg', 'http://velchala.com/service/property-management', 'active', '2019-05-22 21:22:11', '2019-05-31 19:37:14'),
(4, 'One-stop solution for your real estate services', '1558515173servicecarousel05.jpg', 'http://velchala.com/service/real-estate', 'active', '2019-05-22 21:22:53', '2019-05-31 19:37:05'),
(5, 'Let all your visa support services handled by us', '1558515219servicecarousel03.jpg', 'http://velchala.com/service/visa-support-services', 'active', '2019-05-22 21:23:39', '2019-05-31 19:36:54'),
(6, 'Secured future with velchala Insurance services', '1558515255servicecarousel05.jpg', 'http://velchala.com/service/visitors-insurance', 'active', '2019-05-22 21:24:15', '2019-05-31 19:36:42'),
(7, 'We are passionate about  web experiences.', '1561102489webdevelopment.jpg', 'https://www.velchala.com/service/web-developement', 'active', '2019-06-21 13:04:49', '2019-06-21 13:07:58');

-- --------------------------------------------------------

--
-- Table structure for table `service_documents`
--

CREATE TABLE `service_documents` (
  `sd_id` bigint(20) UNSIGNED NOT NULL,
  `sd_service_id` int(11) NOT NULL,
  `sd_doc_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sd_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sd_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_documents`
--

INSERT INTO `service_documents` (`sd_id`, `sd_service_id`, `sd_doc_name`, `sd_title`, `sd_status`, `created_at`, `updated_at`) VALUES
(2, 20005, '156845345115682726701568269463channelsReports.xlsx', 'Blood Samples', '1', '2019-09-14 15:00:51', '2019-09-14 15:00:51'),
(3, 20018, '1568951690COMPREHENSIVE HEART PACKAGE.docx', 'Test File-123', '1', '2019-09-20 09:24:50', '2019-09-20 09:24:50');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `s_id` int(10) UNSIGNED NOT NULL,
  `site_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tolfree_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_fb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_tw` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`s_id`, `site_logo`, `admin_logo`, `admin_email`, `admin_name`, `address`, `tolfree_email`, `social_fb`, `social_tw`, `social_in`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'info@velchala.com1', 'velchala.com1', 'Bajanara hill1', '9573302201', NULL, NULL, NULL, '2019-05-24 02:13:12', '2020-10-10 03:14:47');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` bigint(20) UNSIGNED NOT NULL,
  `state_country_id` int(11) NOT NULL,
  `state_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_country_id`, `state_name`, `state_status`, `created_at`, `updated_at`) VALUES
(2, 2, 'Alabama', 'active', '2019-06-10 05:15:42', '2019-06-10 05:15:42'),
(3, 2, 'Alaska', 'active', '2019-06-10 05:16:03', '2019-06-10 05:16:03'),
(4, 2, 'Arizona', 'active', '2019-06-10 05:16:18', '2019-07-25 22:10:23'),
(5, 2, 'Arkansas', 'active', '2019-06-10 05:16:32', '2019-06-10 05:16:32'),
(6, 2, 'California', 'active', '2019-06-10 05:16:45', '2019-07-25 22:10:05'),
(7, 2, 'Colorado', 'active', '2019-06-10 05:16:57', '2019-06-10 05:16:57'),
(8, 2, 'Connecticut', 'active', '2019-06-10 05:17:08', '2019-06-10 05:17:08'),
(9, 2, 'Delaware', 'active', '2019-06-10 05:17:18', '2019-06-10 05:17:18'),
(10, 2, 'Florida', 'active', '2019-06-10 05:17:28', '2019-06-10 05:17:28'),
(11, 2, 'Georgia', 'active', '2019-06-10 05:17:36', '2019-06-10 05:17:36'),
(12, 2, 'Hawaii', 'active', '2019-06-10 05:17:45', '2019-06-10 05:17:45'),
(13, 2, 'Idaho', 'active', '2019-06-10 05:17:57', '2019-06-10 05:17:57'),
(14, 2, 'Ilinois', 'active', '2019-06-10 05:18:06', '2019-07-25 22:06:06'),
(15, 2, 'Indiana', 'active', '2019-06-10 05:18:18', '2019-06-10 05:18:18'),
(16, 2, 'Iowa', 'active', '2019-06-10 05:18:27', '2019-06-10 05:18:27'),
(17, 2, 'Kansas', 'active', '2019-06-10 05:18:38', '2019-06-10 05:18:38'),
(18, 2, 'Kentucky', 'active', '2019-06-10 05:18:49', '2019-06-10 05:18:49'),
(19, 2, 'Louisiana', 'active', '2019-06-10 05:18:58', '2019-06-10 05:18:58'),
(20, 2, 'Maine', 'active', '2019-06-10 05:19:08', '2019-06-10 05:19:08'),
(21, 2, 'Maryland', 'active', '2019-06-10 05:19:19', '2019-06-10 05:19:19'),
(22, 2, 'Massachusetts', 'active', '2019-06-10 05:19:27', '2019-06-10 05:19:27'),
(23, 2, 'Michigan', 'active', '2019-06-10 05:19:34', '2019-06-10 05:19:34'),
(24, 2, 'Minnesota', 'active', '2019-06-10 05:19:45', '2019-06-10 05:19:45'),
(25, 2, 'Mississippi', 'active', '2019-06-10 05:19:54', '2019-06-10 05:19:54'),
(26, 2, 'Missouri', 'active', '2019-06-10 05:20:03', '2019-06-10 05:20:03'),
(27, 2, 'Montana', 'active', '2019-06-10 05:20:13', '2019-06-10 05:20:13'),
(28, 2, 'Nebraska', 'active', '2019-06-10 05:20:20', '2019-06-10 05:20:20'),
(29, 2, 'Nevada', 'active', '2019-06-10 05:20:29', '2019-06-10 05:20:29'),
(30, 2, 'New Hampshire', 'active', '2019-06-10 05:20:38', '2019-06-10 05:20:38'),
(31, 2, 'New Jersey', 'active', '2019-06-10 05:20:48', '2019-06-10 05:20:48'),
(32, 2, 'New Mexico', 'active', '2019-06-10 05:20:56', '2019-06-10 05:20:56'),
(33, 2, 'New York', 'active', '2019-06-10 05:21:05', '2019-06-10 05:21:05'),
(34, 2, 'North Carolina', 'active', '2019-06-10 05:21:14', '2019-06-10 05:21:14'),
(35, 2, 'North Dakota', 'active', '2019-06-10 05:21:28', '2019-06-10 05:21:28'),
(36, 2, 'Ohio', 'active', '2019-06-10 05:21:37', '2019-06-10 05:21:37'),
(37, 2, 'Oklahoma', 'active', '2019-06-10 05:21:47', '2019-06-10 05:21:47'),
(38, 2, 'Oregon', 'active', '2019-06-10 05:21:56', '2019-06-10 05:21:56'),
(39, 2, 'Pennsylvania', 'active', '2019-06-10 05:22:05', '2019-06-10 05:22:05'),
(40, 2, 'Rhode Island', 'active', '2019-06-10 05:22:14', '2019-06-10 05:22:14'),
(41, 2, 'South Carolina', 'active', '2019-06-10 05:22:23', '2019-06-10 05:22:23'),
(42, 2, 'South Dakota', 'active', '2019-06-10 05:22:33', '2019-06-10 05:22:33'),
(43, 2, 'Tennessee', 'active', '2019-06-10 05:22:42', '2019-06-10 05:22:42'),
(44, 2, 'Texas', 'active', '2019-06-10 05:22:52', '2019-06-10 05:22:52'),
(45, 2, 'Utah', 'active', '2019-06-10 05:23:01', '2019-06-10 05:23:01'),
(46, 2, 'Vermont', 'active', '2019-06-10 05:23:08', '2019-06-10 05:23:08'),
(47, 2, 'Virginia', 'active', '2019-06-10 05:23:17', '2019-07-25 22:08:12'),
(48, 2, 'Washington', 'active', '2019-06-10 05:23:26', '2019-07-25 22:08:01'),
(49, 2, 'West Virginia', 'active', '2019-06-10 05:23:35', '2019-07-25 22:07:50'),
(50, 2, 'Wisconsin', 'active', '2019-06-10 05:23:44', '2019-07-25 22:07:35'),
(51, 2, 'Wyoming', 'active', '2019-06-10 05:23:55', '2019-07-25 22:07:21'),
(52, 1, 'Telangana', 'active', '2019-06-10 09:17:34', '2019-07-10 22:36:10'),
(54, 1, 'Andhra Pradesh', 'active', '2019-08-08 19:58:28', '2019-08-08 19:58:28'),
(55, 1, 'Arunachal Pradesh', 'active', '2019-08-08 19:58:54', '2019-08-08 19:58:54'),
(56, 1, 'Assam', 'active', '2019-08-08 19:59:19', '2019-08-08 19:59:19'),
(57, 1, 'Bihar', 'active', '2019-08-08 19:59:35', '2019-08-08 19:59:35'),
(58, 1, 'Chhattisgarh', 'active', '2019-08-08 20:00:00', '2019-08-08 20:00:00'),
(59, 1, 'Goa', 'active', '2019-08-08 20:00:31', '2019-08-08 20:00:31'),
(60, 1, 'Gujarat', 'active', '2019-08-08 20:01:08', '2019-08-08 20:01:08'),
(61, 1, 'Haryana', 'active', '2019-08-08 20:01:20', '2019-08-08 20:01:20'),
(62, 1, 'Himachal Pradesh', 'active', '2019-08-08 20:02:01', '2019-08-08 20:02:01'),
(63, 1, 'Jharkhand', 'active', '2019-08-08 20:02:16', '2019-08-08 20:02:16'),
(64, 1, 'Karnataka', 'active', '2019-08-08 20:02:38', '2019-08-08 20:02:38'),
(65, 1, 'Kerala', 'active', '2019-08-08 20:04:36', '2019-08-08 20:04:36'),
(66, 1, 'Madhya Pradesh', 'active', '2019-08-08 20:04:57', '2019-08-08 20:04:57'),
(67, 1, 'Maharashtra', 'active', '2019-08-08 20:06:13', '2019-08-08 20:06:13'),
(68, 1, 'Manipur', 'active', '2019-08-08 20:06:35', '2019-08-08 20:06:35'),
(69, 1, 'Meghalaya', 'active', '2019-08-08 20:06:58', '2019-08-08 20:06:58'),
(70, 1, 'Mizoram', 'active', '2019-08-08 20:07:12', '2019-08-08 20:07:12'),
(71, 1, 'Nagaland', 'active', '2019-08-08 20:07:37', '2019-08-08 20:07:37'),
(72, 1, 'Odisha', 'active', '2019-08-08 20:07:53', '2019-08-08 20:07:53'),
(73, 1, 'Punjab', 'active', '2019-08-08 20:08:10', '2019-08-08 20:08:10'),
(74, 1, 'Rajasthan', 'active', '2019-08-08 20:09:07', '2019-08-08 20:09:07'),
(75, 1, 'Sikkim', 'active', '2019-08-08 20:09:24', '2019-08-08 20:09:24'),
(76, 1, 'Tamil Nadu', 'active', '2019-08-08 20:09:36', '2019-08-08 20:09:36'),
(77, 1, 'Tripura', 'active', '2019-08-08 20:09:52', '2019-08-08 20:09:52'),
(78, 1, 'Uttar Pradesh', 'active', '2019-08-08 20:10:15', '2019-08-08 20:10:15'),
(79, 1, 'Uttarakhand', 'active', '2019-08-08 20:10:59', '2019-08-08 20:10:59'),
(80, 1, 'West Bengal', 'active', '2019-08-08 20:11:12', '2019-08-08 20:11:12');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'itzursai@gmail.com', '2019-05-14 18:59:46', '2019-05-14 18:59:46'),
(2, 'srinivas_chenna@yahoo.com', '2019-05-28 18:29:09', '2019-05-28 18:29:09'),
(3, 'praveennandipati@gmail.com', '2019-06-03 01:25:18', '2019-06-03 01:25:18'),
(4, 'admin@gmail.com', '2019-06-22 16:26:41', '2019-06-22 16:26:41'),
(5, 'srishannusuri@gmail', '2019-07-15 10:15:22', '2019-07-15 10:15:22'),
(6, 'srishannusuri@gmail.com', '2019-07-15 10:15:44', '2019-07-15 10:15:44'),
(7, 'saimahi@gmail.com', '2019-08-16 00:36:46', '2019-08-16 00:36:46'),
(8, 'sudheer41in@gmail.com', '2019-09-10 01:49:04', '2019-09-10 01:49:04'),
(9, 'saitest@test.com', '2019-10-10 12:04:26', '2019-10-10 12:04:26'),
(10, 'marilynnjones4518@gmail.com', '2019-10-10 16:52:30', '2019-10-10 16:52:30'),
(11, 'mrbush530@gmail.com', '2019-10-10 22:47:33', '2019-10-10 22:47:33'),
(12, 'leitch.hamil@yahoo.com', '2019-10-10 23:50:26', '2019-10-10 23:50:26'),
(13, 'althebakerboy@aol.com', '2019-10-11 00:07:52', '2019-10-11 00:07:52'),
(14, 'ldlombardi@aol.com', '2019-10-11 00:22:43', '2019-10-11 00:22:43'),
(15, 'riobard_kevan@yahoo.com', '2019-10-11 00:26:42', '2019-10-11 00:26:42'),
(16, 'alcorn.robert@gmail.com', '2019-10-11 00:45:23', '2019-10-11 00:45:23'),
(17, 'bettyprincess@aol.com', '2019-10-11 00:47:15', '2019-10-11 00:47:15'),
(18, 'syco42020@hotmail.com', '2019-10-11 00:52:55', '2019-10-11 00:52:55'),
(19, 'rpark9242@aol.com', '2019-10-11 00:54:35', '2019-10-11 00:54:35'),
(20, 'noel_16v@hotmail.com', '2019-10-11 01:29:40', '2019-10-11 01:29:40'),
(21, 'laura@commercialsigncompany.com', '2019-10-11 01:54:33', '2019-10-11 01:54:33'),
(22, 'werrh@aol.com', '2019-10-11 02:27:16', '2019-10-11 02:27:16'),
(23, 'dabber455@hotmail.com', '2019-10-11 02:49:05', '2019-10-11 02:49:05'),
(24, 'rossyferguson@aol.com', '2019-10-11 03:24:04', '2019-10-11 03:24:04'),
(25, 'dorthy.milroy@aol.com', '2019-10-11 04:13:31', '2019-10-11 04:13:31'),
(26, 'garryh@bigvisionimaging.com', '2019-10-11 04:30:59', '2019-10-11 04:30:59'),
(27, 'j.wilson@midwestconstruct.com', '2019-10-11 04:56:16', '2019-10-11 04:56:16'),
(28, 'eloi.eynard@hotmail.com', '2019-10-11 05:02:24', '2019-10-11 05:02:24'),
(29, 'dennisgoughnour@yahoo.com', '2019-10-11 06:07:21', '2019-10-11 06:07:21'),
(30, 'wildbird@bellsouth.net', '2019-10-11 06:14:53', '2019-10-11 06:14:53'),
(31, 'kenlauraine@bellsouth.net', '2019-10-11 07:05:34', '2019-10-11 07:05:34'),
(32, 'ran314@aol.com', '2019-10-11 07:44:59', '2019-10-11 07:44:59'),
(33, 'berton_hare@yahoo.com', '2019-10-11 12:03:36', '2019-10-11 12:03:36'),
(34, 'donelanmb@yahoo.com', '2019-10-11 12:27:38', '2019-10-11 12:27:38'),
(35, 'commonscalhoun@yahoo.com', '2019-10-11 17:56:19', '2019-10-11 17:56:19'),
(36, 'robin.peretzian@worcesteracademy.org', '2019-10-11 20:36:30', '2019-10-11 20:36:30'),
(37, 'ktaloza@csufresno.edu', '2019-10-11 22:32:55', '2019-10-11 22:32:55'),
(38, 'bschnei3@gmail.com', '2019-10-12 01:22:50', '2019-10-12 01:22:50'),
(39, 'bobspamphlet@hotmail.com', '2019-10-12 01:55:16', '2019-10-12 01:55:16'),
(40, 'electrical.innovations94@gmail.com', '2019-10-12 10:52:28', '2019-10-12 10:52:28'),
(41, 'carlos.olivares@hdita.com', '2019-10-12 14:14:34', '2019-10-12 14:14:34'),
(42, 'chanelletrammell@yahoo.com', '2019-10-12 18:48:25', '2019-10-12 18:48:25'),
(43, 'chancykiara@yahoo.com', '2019-10-12 19:17:41', '2019-10-12 19:17:41'),
(44, 'hymiedingsdale@yahoo.com', '2019-10-13 10:42:38', '2019-10-13 10:42:38'),
(45, 'curtis_keena@yahoo.com', '2019-10-13 20:13:13', '2019-10-13 20:13:13'),
(46, 'gizi_j@hotmail.com', '2019-10-13 21:07:22', '2019-10-13 21:07:22'),
(47, 'artemas.court@yahoo.com', '2019-10-14 16:41:49', '2019-10-14 16:41:49'),
(48, 'btaucher@itcet.com', '2019-10-14 21:34:31', '2019-10-14 21:34:31'),
(49, 'sergiom10@bellsouth.net', '2019-10-14 21:48:27', '2019-10-14 21:48:27'),
(50, 'kenneth_burns@bellsouth.net', '2019-10-14 21:50:30', '2019-10-14 21:50:30'),
(51, 'sportjunkie@bellsouth.net', '2019-10-14 21:59:26', '2019-10-14 21:59:26'),
(52, 'jarl_halbostad@hotmail.com', '2019-10-14 23:51:50', '2019-10-14 23:51:50'),
(53, 'ollygeff@hotmail.com', '2019-10-15 03:30:22', '2019-10-15 03:30:22'),
(54, 'pkogler@scwcompanies.com', '2019-10-15 03:48:24', '2019-10-15 03:48:24'),
(55, 'zzhan142@gmail.com', '2019-10-15 04:33:17', '2019-10-15 04:33:17'),
(56, 'hereiam133@gmail.com', '2019-10-15 08:33:29', '2019-10-15 08:33:29'),
(57, 'jamesd@aaaasc.com', '2019-10-15 09:16:05', '2019-10-15 09:16:05'),
(58, 'gracia_mattinson@yahoo.com', '2019-10-15 15:26:21', '2019-10-15 15:26:21'),
(59, 'sweenes202@embarqmail.com', '2019-10-15 17:32:37', '2019-10-15 17:32:37'),
(60, 'kingme2012@gmail.com', '2019-10-15 17:35:41', '2019-10-15 17:35:41'),
(61, 'nothwoodguthrie@yahoo.com', '2019-10-15 18:39:01', '2019-10-15 18:39:01'),
(62, 'mord.stapletoncotton@yahoo.com', '2019-10-15 18:42:21', '2019-10-15 18:42:21'),
(63, 'mase94@bellsouth.net', '2019-10-15 22:21:09', '2019-10-15 22:21:09'),
(64, 'awtechwriter@gmail.com', '2019-10-16 01:56:42', '2019-10-16 01:56:42'),
(65, 'alex-lmn@hotmail.com', '2019-10-16 02:35:48', '2019-10-16 02:35:48'),
(66, 'drieledelanira@hotmail.com', '2019-10-16 03:57:18', '2019-10-16 03:57:18'),
(67, 'kmcgrann@att.net', '2019-10-16 04:08:00', '2019-10-16 04:08:00'),
(68, 'peacedoula8@yahoo.com', '2019-10-16 04:27:54', '2019-10-16 04:27:54'),
(69, 'asta.balaikiene@yahoo.com', '2019-10-16 17:22:04', '2019-10-16 17:22:04'),
(70, 'info@gmstimber.co.uk', '2019-10-16 17:37:18', '2019-10-16 17:37:18'),
(71, 'andertoneli@yahoo.com', '2019-10-16 19:31:50', '2019-10-16 19:31:50'),
(72, 'orlosky.daniel@foerstergroup.com', '2019-10-17 17:21:56', '2019-10-17 17:21:56'),
(73, 'joey_214@live.com', '2019-10-17 20:45:18', '2019-10-17 20:45:18'),
(74, 'attitudecheck@comcast.net', '2019-10-17 22:00:16', '2019-10-17 22:00:16'),
(75, 'treece.williams@gmail.com', '2019-10-18 05:06:36', '2019-10-18 05:06:36'),
(76, 'paulmcdaniel9738@gmail.com', '2019-10-18 05:44:16', '2019-10-18 05:44:16'),
(77, 'rambaut.ethel@yahoo.com', '2019-10-18 06:02:57', '2019-10-18 06:02:57'),
(78, 'michaellucas3497@gmail.com', '2019-10-18 06:44:19', '2019-10-18 06:44:19'),
(79, 'mosswordsworth@yahoo.com', '2019-10-20 20:00:57', '2019-10-20 20:00:57'),
(80, 'alzbeta.polackova@centrum.cz', '2019-10-22 08:29:22', '2019-10-22 08:29:22'),
(81, 'allistirsarginson@yahoo.com', '2019-10-22 09:28:28', '2019-10-22 09:28:28'),
(82, 'tysker_mike@hotmail.com', '2019-10-23 04:36:38', '2019-10-23 04:36:38'),
(83, 'nery_abel@hotmail.com', '2019-10-23 09:57:30', '2019-10-23 09:57:30'),
(84, 'dsortino@amfam.com', '2019-10-23 17:48:32', '2019-10-23 17:48:32'),
(85, 'herve_grascom@yahoo.com', '2019-10-24 06:14:47', '2019-10-24 06:14:47'),
(86, 'dillonh1996@gmail.com', '2019-10-24 06:23:00', '2019-10-24 06:23:00'),
(87, 'luison1991@gmail.com', '2019-10-24 15:35:05', '2019-10-24 15:35:05'),
(88, 'dreamerdyc@gmail.com', '2019-10-24 18:51:21', '2019-10-24 18:51:21'),
(89, 'r.crysler@sbcglobal.net', '2019-10-24 18:52:19', '2019-10-24 18:52:19'),
(90, 'emmadamsgaard@hotmail.com', '2019-10-24 19:40:21', '2019-10-24 19:40:21'),
(91, 'kleistrj@sbcglobal.net', '2019-10-24 21:02:48', '2019-10-24 21:02:48'),
(92, 'lee_kittygirl@yahoo.com', '2019-10-25 00:19:36', '2019-10-25 00:19:36'),
(93, 'teri.adcox@idealpoultry.com', '2019-10-25 02:09:14', '2019-10-25 02:09:14'),
(94, 'rhch774@att.net', '2019-10-25 02:56:55', '2019-10-25 02:56:55'),
(95, 'stormy.jardon@yahoo.com', '2019-10-25 07:27:19', '2019-10-25 07:27:19'),
(96, 'evs.freeman@yahoo.com', '2019-10-25 08:03:57', '2019-10-25 08:03:57'),
(97, 'stephen.roccia@vzw.com', '2019-10-25 10:29:34', '2019-10-25 10:29:34'),
(98, 'pbradley@glps.net', '2019-10-25 14:46:15', '2019-10-25 14:46:15'),
(99, 'josh@lowcountryaudiosc.com', '2019-10-25 16:20:52', '2019-10-25 16:20:52'),
(100, 'gibby1050@gmail.com', '2019-10-25 18:45:43', '2019-10-25 18:45:43'),
(101, 'cbonhage@att.net', '2019-10-25 19:27:12', '2019-10-25 19:27:12'),
(102, 'emindarrius@yahoo.com', '2019-10-25 23:02:45', '2019-10-25 23:02:45'),
(103, 'candeharris@att.net', '2019-10-25 23:50:10', '2019-10-25 23:50:10'),
(104, 'leon.walter@willistonschools.org', '2019-10-26 03:24:23', '2019-10-26 03:24:23'),
(105, 'crjunebug@aol.com', '2019-10-26 05:10:30', '2019-10-26 05:10:30'),
(106, 'kelly-hamilton80@att.net', '2019-10-26 05:14:04', '2019-10-26 05:14:04'),
(107, 'dunlop.javier@yahoo.com', '2019-10-26 08:43:12', '2019-10-26 08:43:12'),
(108, 'tijuanagarner@yahoo.com', '2019-10-26 08:47:17', '2019-10-26 08:47:17'),
(109, 'busfleet@aol.com', '2019-10-26 11:56:11', '2019-10-26 11:56:11'),
(110, 'lisco1@optonline.net', '2019-10-26 12:09:16', '2019-10-26 12:09:16'),
(111, 'dweingart@zoominternet.net', '2019-10-26 12:44:01', '2019-10-26 12:44:01'),
(112, 'johnyoung8360@gmail.com', '2019-10-26 19:09:07', '2019-10-26 19:09:07'),
(113, 'brockyacklam@yahoo.com', '2019-10-26 21:26:09', '2019-10-26 21:26:09'),
(114, 'potusmonkey1972@gmail.com', '2019-10-26 21:40:33', '2019-10-26 21:40:33'),
(115, 'jmonte1226@aol.com', '2019-10-27 01:34:32', '2019-10-27 01:34:32'),
(116, 'ronda.masanotti@stpete.org', '2019-10-27 07:57:28', '2019-10-27 07:57:28'),
(117, 'hebe_4@hotmail.com', '2019-10-27 14:47:38', '2019-10-27 14:47:38'),
(118, 'bmoorefield@gmail.com', '2019-10-27 22:21:09', '2019-10-27 22:21:09'),
(119, 'bowers.jessey@yahoo.com', '2019-10-28 07:00:49', '2019-10-28 07:00:49'),
(120, 'amykeel@akeeldesigns.com', '2019-10-28 15:23:50', '2019-10-28 15:23:50'),
(121, 'charlesgallagher@bellsouth.net', '2019-10-28 22:17:59', '2019-10-28 22:17:59'),
(122, 'mfrances52491@verizon.net', '2019-10-29 01:27:08', '2019-10-29 01:27:08'),
(123, 'jordy.roberts@me.com', '2019-10-29 03:02:15', '2019-10-29 03:02:15'),
(124, 'autumnkoolman@att.net', '2019-10-29 03:06:41', '2019-10-29 03:06:41'),
(125, 'alexander.velasquez@dechert.com', '2019-10-29 03:18:29', '2019-10-29 03:18:29'),
(126, 'david@stalmic.com', '2019-10-29 04:08:23', '2019-10-29 04:08:23'),
(127, 'dianlyn05@yahoo.com', '2019-10-29 04:31:21', '2019-10-29 04:31:21'),
(128, 'ajgoldberg00@yahoo.com', '2019-10-29 05:45:02', '2019-10-29 05:45:02'),
(129, 'boothe.willock@yahoo.com', '2019-10-29 08:51:09', '2019-10-29 08:51:09'),
(130, 'dmattingly@nationalworkforce.com', '2019-10-29 09:27:29', '2019-10-29 09:27:29'),
(131, 'laurab@aidantfire.com', '2019-10-29 10:21:58', '2019-10-29 10:21:58'),
(132, 'mdriscoll@organo.com', '2019-10-29 15:35:15', '2019-10-29 15:35:15'),
(133, 'nelson.molina@blount.com', '2019-10-29 20:40:48', '2019-10-29 20:40:48'),
(134, 'mmckeon93@gmail.com', '2019-10-30 15:46:58', '2019-10-30 15:46:58'),
(135, 'aznsteven_92@hotmail.com', '2019-10-30 20:07:26', '2019-10-30 20:07:26'),
(136, 'anca@vertusproperties.com', '2019-10-31 09:09:40', '2019-10-31 09:09:40'),
(137, 'darrell.vanhorne@yahoo.com', '2019-10-31 14:31:55', '2019-10-31 14:31:55'),
(138, 'mike@fitlogistix.com', '2019-11-01 01:44:03', '2019-11-01 01:44:03'),
(139, 'tonyramirez10494@gmail.com', '2019-11-01 02:56:16', '2019-11-01 02:56:16'),
(140, 'amandamegyesi@northcoastdesignbuild.com', '2019-11-01 10:28:43', '2019-11-01 10:28:43'),
(141, 'corbie_norwood@yahoo.com', '2019-11-01 12:22:43', '2019-11-01 12:22:43'),
(142, 'd.rahden@gmail.com', '2019-11-01 14:03:02', '2019-11-01 14:03:02'),
(143, 'alexcdunlop@gmail.com', '2019-11-01 20:44:55', '2019-11-01 20:44:55'),
(144, 'logan@nwss.us', '2019-11-01 21:14:09', '2019-11-01 21:14:09'),
(145, 'rsoles@myokaloosa.com', '2019-11-02 15:11:49', '2019-11-02 15:11:49'),
(146, 'tkhoury@mdausa.org', '2019-11-02 17:05:21', '2019-11-02 17:05:21'),
(147, 'jfox9793@gmail.com', '2019-11-03 03:00:57', '2019-11-03 03:00:57'),
(148, 'reynoldbaldwin73@gmail.com', '2019-11-03 03:44:43', '2019-11-03 03:44:43'),
(149, 'joanarnau88@gmail.com', '2019-11-04 17:45:02', '2019-11-04 17:45:02'),
(150, 'ruthwc85@ripsewstitch.com', '2019-11-05 03:00:47', '2019-11-05 03:00:47'),
(151, 'mrdb123@gmail.com', '2019-11-05 11:06:00', '2019-11-05 11:06:00'),
(152, 'heathhermann@yahoo.com', '2019-11-05 21:00:11', '2019-11-05 21:00:11'),
(153, 'kgonzalez1019@gmail.com', '2019-11-05 22:17:20', '2019-11-05 22:17:20'),
(154, 'mvang54@yahoo.com', '2019-11-05 22:19:16', '2019-11-05 22:19:16'),
(155, 'sknadev@gmail.com', '2019-11-05 22:51:24', '2019-11-05 22:51:24'),
(156, 'bjustice@uniontransfer.com', '2019-11-06 03:35:12', '2019-11-06 03:35:12'),
(157, 'gretchen.rogers@ncmedicalsupply.com', '2019-11-06 06:09:08', '2019-11-06 06:09:08'),
(158, 'ldaacrook@msn.com', '2019-11-06 10:17:45', '2019-11-06 10:17:45'),
(159, 'mclachl2@msu.edu', '2019-11-06 11:23:02', '2019-11-06 11:23:02'),
(160, 'eekblade@yahoo.com', '2019-11-06 14:08:54', '2019-11-06 14:08:54'),
(161, 'elayfox@yahoo.com', '2019-11-06 21:10:26', '2019-11-06 21:10:26'),
(162, 'hiltonworkman@yahoo.com', '2019-11-06 22:42:33', '2019-11-06 22:42:33'),
(163, 'dlapiana@remax440.com', '2019-11-07 05:51:51', '2019-11-07 05:51:51'),
(164, 'peelonrazo@gmail.com', '2019-11-07 07:33:50', '2019-11-07 07:33:50'),
(165, 'eaw3@westchestergov.com', '2019-11-07 20:22:24', '2019-11-07 20:22:24'),
(166, 'brie.hernandez@dpd.ci.dallas.tx.us', '2019-11-09 00:19:39', '2019-11-09 00:19:39'),
(167, 'gpmarshall57@gmail.com', '2019-11-09 11:39:05', '2019-11-09 11:39:05'),
(168, 'devonhandy@gmail.com', '2019-11-09 13:30:11', '2019-11-09 13:30:11'),
(169, 'mr.jjcabrera@gmail.com', '2019-11-09 19:50:18', '2019-11-09 19:50:18'),
(170, 'dvalencia@ci.visalia.ca.us', '2019-11-09 23:22:16', '2019-11-09 23:22:16'),
(171, 'petersp186@gmail.com', '2019-11-11 04:34:28', '2019-11-11 04:34:28'),
(172, 'jclinch13@gmail.com', '2019-11-12 08:22:07', '2019-11-12 08:22:07'),
(173, 'tom@congressionalseafood.com', '2019-11-13 15:25:37', '2019-11-13 15:25:37'),
(174, 'mmeriage@hotmail.com', '2019-11-13 22:56:58', '2019-11-13 22:56:58'),
(175, 'travis.kennedy@oncor.com', '2019-11-14 03:02:35', '2019-11-14 03:02:35'),
(176, 'noahwbeal04@gmail.com', '2019-11-14 22:23:53', '2019-11-14 22:23:53'),
(177, 'billdipretoro@gmail.com', '2019-11-15 21:47:47', '2019-11-15 21:47:47'),
(178, 'feuerhelm.lindy@yahoo.com', '2019-11-16 11:02:10', '2019-11-16 11:02:10'),
(179, 'redy077@gmail.com', '2019-11-17 00:49:35', '2019-11-17 00:49:35'),
(180, 'malcolsoncurt@yahoo.com', '2019-11-17 15:15:23', '2019-11-17 15:15:23'),
(181, 'renato.alden@yahoo.com', '2019-11-17 15:20:04', '2019-11-17 15:20:04'),
(182, 'kirkma81@gmail.com', '2019-11-19 01:01:01', '2019-11-19 01:01:01'),
(183, 'mitton.lyle@yahoo.com', '2019-11-19 21:03:15', '2019-11-19 21:03:15'),
(184, 'everettlightburn@yahoo.com', '2019-11-20 20:40:42', '2019-11-20 20:40:42'),
(185, 'realestatebyks@gmail.com', '2019-11-20 21:02:37', '2019-11-20 21:02:37'),
(186, 'breckinridgesmith@yahoo.com', '2019-11-21 05:06:35', '2019-11-21 05:06:35'),
(187, 'ljcole25@yahoo.com', '2019-11-21 22:06:11', '2019-11-21 22:06:11'),
(188, 'crane.woody@yahoo.com', '2019-11-21 22:55:05', '2019-11-21 22:55:05'),
(189, 'djohn1733@yahoo.com', '2019-11-22 07:31:44', '2019-11-22 07:31:44'),
(190, 'hhdaryani@sbcglobal.net', '2019-11-22 13:51:02', '2019-11-22 13:51:02'),
(191, 'tangiralapriya@gmail.com', '2019-12-10 19:15:00', '2019-12-10 19:15:00'),
(192, 'nirbhayasaaya@gmail.com', '2020-01-08 22:53:46', '2020-01-08 22:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL COMMENT '404=admin,1=user,405=vendor',
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `mobile`, `role`, `gender`, `dob`, `image`, `email_verified_at`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(128, 'venkatesh', 'venkatp686@gmail.com', '$2y$10$1EswlDDS8rf0K0kQbn5hm./JT6X2sb3pTBf.gb6gc.XdG3h6RSYaC', '9573302201', 1, NULL, NULL, NULL, '2020-10-09 22:59:27', 'active', NULL, '2020-10-09 22:39:06', '2020-10-09 22:59:27'),
(127, 'N Praveen guptha', 'Praveennandipati@gmail.com', '$2y$10$Yww5aZqbkO9e4WCIf8yf5uF69ruzcY6G5CyyPikpNfC5LdUhJDB9C', '9642123254', 1, 'male', NULL, '1602215268signupimg.jpg', '2020-10-09 16:18:04', 'active', NULL, '2020-10-09 16:13:35', '2020-10-09 16:18:04'),
(126, 'venkatesh', 'admin@gmail.com', '$2y$10$C0H4amkMeVqle64PZiA1S.RJbAKicJFG03EFKXIABT8WOoV50W50y', '9573302201', 404, NULL, NULL, NULL, '2020-10-09 09:23:42', 'active', NULL, '2020-10-09 09:23:42', '2020-10-09 09:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `ua_id` int(10) UNSIGNED NOT NULL,
  `ua_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_landmark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ua_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ua_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ua_defult` int(11) NOT NULL DEFAULT 0 COMMENT '1=default address',
  `ua_user_id` int(11) NOT NULL,
  `ua_country` int(11) NOT NULL,
  `ua_pincode` int(11) DEFAULT NULL,
  `ua_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`ua_id`, `ua_name`, `ua_address`, `ua_landmark`, `ua_city`, `ua_state`, `ua_email`, `ua_phone`, `ua_defult`, `ua_user_id`, `ua_country`, `ua_pincode`, `ua_status`, `created_at`, `updated_at`) VALUES
(55, 'srini', '20405 anza', NULL, NULL, '6', 'test@test.com', '1-6462413704', 0, 37, 2, 90503, 'active', '2019-10-01 10:34:52', '2019-10-01 10:34:52'),
(56, 'S.Ramachandra', '6-1-832 Kovoor Nagar, Anantapur', NULL, NULL, '54', 'madimula@gmail.com', '91-9295752379', 1, 65, 1, 515004, 'active', '2019-10-11 07:19:57', '2019-10-11 07:20:17'),
(11, 'Aruna', '3-6-498 St no #7 Himayath Nagar Hyderabad', 'St opposite Hotel Minerva', 'hyderabad', 'telangana', 'Sireeshabandaru29@gmail.com', '', 1, 22, 1, 500029, 'active', '2019-06-02 12:12:27', '2019-06-02 12:12:27'),
(38, 'sreenath kadiyala', '221 concordia woods dr, Morrisville NC', NULL, NULL, '34', 'ksnath25@gmail.com', '1-2012539714', 1, 39, 2, 27560, 'active', '2019-07-17 07:37:08', '2019-07-17 07:37:08'),
(16, 'Saritha', '1234 east grand canyon Drive', 'Near Temple', NULL, '8', 'sbandaru@kitchell.com', '1-8628127929', 1, 23, 2, 85249, 'active', '2019-06-10 07:47:32', '2019-07-12 03:18:28'),
(17, 'Goverdhan Bandaru', 'Plot No.82, Flat No.206, Swarna Palace, Road No.4, Green Hills colony, Kothapet,', 'Near Ozone Hospital', '3', '52', 'sbandaru@kitchell.com', '+91-9885299333', 0, 23, 1, 500035, 'active', '2019-06-10 09:26:41', '2019-07-10 23:09:31'),
(18, 'JALENDHAR REDDY', 'Ananda Sai Old Age Home, Plot no, 8 H.No, 9-114/12, Vijayapuri Colony, Nagaram (v) Keesara mdl ; Medchal; Malkajgiri', 'Near Vijaya Hospital', '3', '52', 'sbandaru@kitchell.com', '+91-9908820582', 0, 23, 1, 501218, 'active', '2019-06-10 10:03:57', '2019-07-10 23:09:31'),
(27, 'Swathy Chenna', '3514 Del Amo Blvd, Torrance, CA', 'Near Del Throne Park', NULL, '6', 'cswathy@gmail.com', '1-3102567069', 1, 8, 2, 90503, 'active', '2019-06-23 09:18:29', '2019-06-23 09:18:29'),
(35, 'Nyalakanti Raja Gonda', '3-127/39/2 MANIKANTA NAGAR\r\nBODUPPAL', 'Telangana', '3', '52', 'rajagonda@gmail.com', '91-09848090537', 1, 36, 1, 500092, 'active', '2019-07-12 10:27:45', '2019-07-12 10:27:45'),
(39, 'raghu', '2-3-603/49/124/2 Newpatelnagar amberpet hyd', 'svr function hall', '3', '52', 'sharma7972@gmail.com', '91-8897594442', 1, 40, 1, 500013, 'active', '2019-07-23 04:39:46', '2019-07-23 04:39:46'),
(29, 'Kiran Chenna', '101 sri venkat nivas', 'Sathavahanagar', '3', '52', 'kiran.chenna123@gmail.com', '91-9985855206', 1, 31, 1, 500072, 'active', '2019-06-27 00:14:37', '2019-06-27 00:14:37'),
(30, 'Sri G.R.K Sastry', 'Flat number 201, Creative Avenue, Gokul Nagar, Tarnaka, Hyderabad', 'lane beside Vijaya Diary', '3', '52', 'prakum02@gmail.com', '91-9618671555', 1, 32, 1, 500017, 'active', '2019-06-27 11:05:07', '2019-06-27 11:05:07'),
(36, 'Nyalakanti Raja Gonda', 'test US adresss', 'test lanmark', NULL, '2', 'rajagonda@gmail.com', '1-848090537', 0, 36, 2, 124545, 'active', '2019-07-12 10:28:25', '2019-07-12 10:28:48'),
(57, 'Mani parepally', 'Flat no 201,Laxmi Durga residency, Miryalaguda', 'Near S.R.Digi school, Reddy colony', '6', '52', 'Sudharani.parepalli@yahoo.com', '91-7331137875', 1, 66, 1, 508207, 'active', '2019-10-11 23:41:43', '2019-10-11 23:41:43'),
(54, 'Ramesh samudrala', '7 Bhagyanagar Colony, karman ghat', 'Near Apollo Pharmacy', '3', '52', 'saimahi30@gmail.com', '91-9866610121', 1, 63, 1, 500079, 'active', '2019-09-30 07:05:55', '2019-09-30 07:05:55'),
(52, 'Praveen guptha', 'Plot No:915\r\nAllwyn Colony, Phase 1, Kukatpally, Hyderabad', 'Telangana', NULL, '18', 'praveennandipati@gmail.com', '1-9642123254', 0, 41, 2, 522265, 'active', '2019-09-06 18:14:47', '2019-09-06 18:14:47'),
(42, 'SRINIVAS CHENNA', '3514 DEL AMO BLVD', 'CA', NULL, '6', 'srinivas_chenna@yahoo.com', '1-6462413704', 1, 37, 2, 90503, 'active', '2019-07-25 22:12:43', '2019-10-01 10:12:29'),
(43, 'Test', 'H.No:5-19\r\nDurgaNagar', 'Telangana', '3', '52', 'sai96030@gmail.com', '91-9603036519', 1, 42, 1, 500072, 'active', '2019-07-29 18:55:53', '2019-07-29 18:55:53'),
(44, 'Ramesh Samudrala', 'Plot # 7, Road # 3, Bhagya Nagar Colony,  Karmanghat, Hyderabad-500079', 'Near Apollo Pharmacy', '3', '52', 'cswathy@gmail.com', '91-9866610121', 0, 37, 1, 500079, 'active', '2019-08-04 21:54:06', '2019-10-01 10:12:03'),
(45, 'Ram Reddy', '14 104 16G, shanthinagar MLG, 508207', NULL, '3', '52', 'KISHORE.KARNA@GMAIL.COM', '91-9849733685', 1, 47, 1, 508207, 'active', '2019-08-08 01:34:44', '2019-08-08 01:34:44'),
(46, 'Nageswara Rao Devarasetty', 'Plot No 52, H.No 11-4-150/10, Road No 1, Sri Venkateswara Colony, Saroor Nagar', 'Near GD Goenka La Petite Montessori Pre School', '3', '52', 'laxmiramesh2002@yahoo.com', '91-9908177475', 1, 48, 1, 500035, 'active', '2019-08-10 18:33:03', '2019-08-10 18:33:03'),
(47, 'Kiran Kalavala', '1770 92nd St, Apt #: 13102', 'West Des Moines', NULL, '16', 'kiran.kalavala@gmail.com', '1-9547023658', 1, 51, 2, 50266, 'active', '2019-08-12 00:03:47', '2019-08-12 00:07:19'),
(48, 'Suresh Dakoju', 'SURESH DAKOJU\r\nMATRIX BLUE BELLS\r\nFLAT NO:401,SRI RAM NAGAR BLOCK-C NEAR BARFI GHAR,\r\nKONDAPUR VILLAGE\r\n+918886594848', 'Near Barfi Ghar', '3', '52', 'sureshdakoju@gmail.com', '91-8886594848', 1, 52, 1, 500084, 'active', '2019-08-12 04:56:34', '2019-08-12 04:56:34'),
(49, 'KIRAN TALLAM', 'DELOITTE TAX SERVICES INDIA PVT LTD,\r\nDELOITTE TOWERS 1, SURVEY#39, GACHIBOWLI, HYDERABAD', NULL, '3', '52', 'tallamkiran@gmail.com', '91-9885654666', 1, 53, 1, 500081, 'active', '2019-08-12 08:22:51', '2019-08-12 08:26:52'),
(50, 'Avanthika', '22121 Erwin st M112 Woodland Hills', NULL, NULL, '6', 'Srinivas.gajing@yahoo.com', '1-4696476591', 1, 54, 2, 91367, 'active', '2019-08-13 00:03:49', '2019-08-13 00:03:49'),
(51, 'Praveen guptha nandipati', '91, hyderabad', 'Near temple', '3', '52', 'Praveennandipati@gmail.com', '91-9642123254', 1, 41, 1, 500072, 'active', '2019-08-25 19:15:48', '2019-08-25 19:15:48'),
(53, 'Praveen guptha', 'Plot No:915\r\nAllwyn Colony, Phase 1, Kukatpally, Hyderabad', 'temple', '3', '52', 'praveennandipati@gmail.com', '91-9642123254', 0, 41, 1, 500072, 'active', '2019-09-07 15:33:23', '2019-09-07 15:33:23'),
(58, 'Leela Ganesna', 'Is the same address. \r\n\r\nSwetha address \r\nL.Padma\r\nH.No.1-8-12\r\nSubhash Road\r\nOpp: VYSYA BANK\r\nKamareddy-503111\r\nNizamabad district\r\nTelangaana state\r\nMobile:9346449285', NULL, '6', '52', 'ganesnar@gmail.com', '91-9346449285', 1, 82, 1, 50311, 'active', '2019-10-18 09:33:10', '2019-12-07 22:10:50'),
(59, 'Vijayalakshmi Battula', 'V nageswararao', 'Vidyanagar colony', '6', '52', 'vijaya2076@gmail.com', '91-9849991312', 1, 95, 1, 507101, 'active', '2019-10-29 00:42:58', '2019-10-29 00:42:58'),
(60, 'Manoj kumar Pabbathi', 'Chappidi Adivi Reddy Gardens,\r\nAlmasguda,Hyderabad', 'Besides Sri Sri Homes', '3', '52', 'manojchanti.kumar@yahoo.com', '91-9000397174', 1, 110, 1, 500058, 'active', '2019-12-09 10:07:55', '2019-12-09 10:07:55'),
(61, 'Sridhar', '1216 hidden ridge apt #2064 irving tx', NULL, NULL, '44', 'sri.nyala@gmail.com', '1-7703782942', 1, 111, 2, 75038, 'active', '2020-02-05 02:40:55', '2020-02-05 02:40:55'),
(62, 'Sridhar Nyalamadugula', '1216 Hidden ridge \r\napt 2064', NULL, NULL, '44', 'sri.nyala@gmail.com', '1-7703782942', 0, 111, 2, 75038, 'active', '2020-02-05 02:43:39', '2020-02-05 02:43:39'),
(63, 'Swathy', 'Road 3, house No#7', 'Near Apollo hosipital', '3', '52', 'Cswathy@gmail.com', '91-9866610121', 1, 102, 1, 500087, 'active', '2020-06-18 07:12:26', '2020-06-18 07:12:26'),
(64, 'Govardhan Bandaru', 'Flat no 206', 'Near ozone hospital', '3', '52', 'Jayasrimerugu89@gmail.com', '91-9032615531', 1, 114, 1, 500082, 'active', '2020-06-21 04:52:18', '2020-06-21 04:52:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_reviews`
--

CREATE TABLE `user_reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pub_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `rating` int(11) NOT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendorbusinessdetails`
--

CREATE TABLE `vendorbusinessdetails` (
  `vb_id` int(10) UNSIGNED NOT NULL,
  `vb_vendor_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_tan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_gst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_address1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_address2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_pincode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_account_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_ifsc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_bank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_branch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_bank_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_bank_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_pan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_address_proof` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_cheque` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vb_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'pending,approved,reject',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendorbusinessdetails`
--

INSERT INTO `vendorbusinessdetails` (`vb_id`, `vb_vendor_id`, `vb_name`, `vb_tan`, `vb_gst`, `vb_address1`, `vb_address2`, `vb_state`, `vb_city`, `vb_pincode`, `vb_account_name`, `vb_account_number`, `vb_ifsc`, `vb_bank`, `vb_branch`, `vb_bank_state`, `vb_bank_city`, `vb_type`, `vb_pan`, `vb_address_proof`, `vb_cheque`, `vb_status`, `created_at`, `updated_at`) VALUES
(1, '4', 'R C JEWELLERS', NULL, '37AREPP8135C1ZS', 'Shop No: 26, JCS Complex', 'Near Spencer\'s, Opp New RTC Busstand, Kurnool', 'Andhra Pradesh', 'Hyderabad', '518003', 'R C Jewellers', '50200009088412', 'HDFC0000742', 'HDFC BANK', 'KURNOOL BRANCH', 'Andhra Pradesh', 'Hyderabad', 'Proprietor', 'AREPP8135C', '1559134416VC.jpg', '155869890620190519_184035.jpg', 'approved', '2019-05-22 06:09:56', '2019-06-09 17:32:50'),
(2, '15', 'Sri LG Foods', NULL, NULL, NULL, NULL, 'Telangana', 'Hyderabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'approved', '2019-05-23 17:03:24', '2019-07-31 08:49:15'),
(3, '9', 'Trendy Woman', NULL, NULL, NULL, NULL, 'Telangana', 'Hyderabad', NULL, 'Rangaraju Sridevi', '20191270000375', 'HDFC0002019', 'HDFC', 'KONDAPUR', 'Telangana', 'Hyderabad', 'Proprietor', NULL, NULL, NULL, 'approved', '2019-05-25 11:53:04', '2019-05-27 05:02:25'),
(4, '5', 'Praveen Textiles', 'AFDPN8700J', 'AFDPN8700J', '91', 'Allwyn Colony, 1', 'Telangana', 'Hyderabad', '500072', 'Praveen Guptha N', '123456789', 'SBIIFSC', 'SBI', 'Kukatpally Branch', 'Telangana', 'Hyderabad', 'Proprietor', 'AFDPN8700J', NULL, NULL, 'approved', '2019-05-25 19:19:27', '2019-05-27 19:48:01'),
(5, '3', 'JK', 'TAn', 'sdg', 'sadg', 'sdg', 'Telangana', 'Hyderabad', '500060', 'sdgsdg', '32652356', 'sgdsagd', 'gsd', 'sgdasdg', 'Telangana', 'Hyderabad', 'Private Limited Company', 'sdgsdgasg', NULL, NULL, 'approved', '2019-05-28 18:35:58', '2019-05-28 18:36:24'),
(6, '11', 'Cakes & Hugs', '123456', 'GST12345', 'Plot No:123', 'Allwyn', 'Telangana', 'Hyderabad', NULL, 'Chaitanya', '12345678', 'ICICI0202', 'ICICI Bank', 'Kukatpally Branch', 'Telangana', 'Hyderabad', 'Private Limited Company', '123456556', '1561262586PicsArt_10-11-12.49.37.jpg', '1561263725chequeman-cheque2.jpg', 'approved', '2019-06-23 09:30:50', '2019-06-24 18:36:41'),
(7, '11', 'Cakes & Hugs', '123456', 'GST12345', 'Plot No:123', 'Allwyn colony, Kukatpally, Hyderabad', 'Telangana', 'Hyderabad', '500072', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2019-06-23 09:31:11', '2019-06-23 09:31:11'),
(8, '38', 'Test Business', 'AFDPN8700J', 'AFDPN8700J', 'Plot No:91, Allwyn Colony', 'Phase 1, Kukatpally', 'Telangana', 'Hyderabad', '500072', 'Praveen Guptha N', '12345678910', 'ICICI0009', 'ICICI Bank', 'Kukatpally', 'Telangana', 'Hyderabad', 'Private Limited Company', NULL, NULL, NULL, 'approved', '2019-07-11 16:38:12', '2019-07-11 16:39:51'),
(9, '44', 'Wonder Photos', '123456789', '123456789', 'Test Address Need To Update with correct address', 'Test Address', 'Telangana', 'Hyderabad', '123456', 'Wonder Photos Bank', '123456789', '123456789', 'Test Bank', 'Test Branch', 'Telangana', 'Hyderabad', 'Private Limited Company', '123456789', NULL, NULL, 'approved', '2019-08-03 09:52:36', '2019-08-03 10:30:37'),
(10, '43', 'Rakhi’s', '12345', '12345', 'A1 block, 5th floor, zore complex', 'Punjagutta, Hyderabad.', 'Telangana', 'Hyderabad', '504251', 'Jayasri Merugu', '62104652288', 'SBIN0020120', 'SBI BANK', 'Bellampelly', 'Telangana', 'Hyderabad', 'Private Limited Company', NULL, NULL, NULL, 'approved', '2019-08-04 21:08:37', '2019-08-04 21:10:27');

-- --------------------------------------------------------

--
-- Table structure for table `vendorsaccount`
--

CREATE TABLE `vendorsaccount` (
  `va_id` int(10) UNSIGNED NOT NULL,
  `va_vendor_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_business_desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_address_1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_address_2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_state` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_city` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_pincode` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_mobile` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_time_from` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_time_to` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `va_status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'pending,approved,reject',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendorsaccount`
--

INSERT INTO `vendorsaccount` (`va_id`, `va_vendor_id`, `va_display_name`, `va_business_desc`, `va_address_1`, `va_address_2`, `va_state`, `va_city`, `va_pincode`, `va_name`, `va_mobile`, `va_email`, `va_time_from`, `va_time_to`, `va_status`, `created_at`, `updated_at`) VALUES
(1, '4', 'R C JEWELLERS', 'R C Jewelers was established in 2015. We believe in \"TRUST - QUALITY - TRADITION\". We are into bullion desk, manufacturing, distribution, retail and after sale services. This enables the retailer to offer its customers products and services that unique and offer unmatched value and quality.\r\n\r\nWe exclusively deals with Silver Articles such as Pure Silver(999), 925 Sterling Silver Jewelry(Imported), Silver Articles, Antique Silver Articles(92% Purity) and Coins.\r\n\r\nWe also deals with Gold Ornaments(916 KDM Hallmark Jewelry) on order basis. We deals with Pure Gold(999) and Coins.', 'Shop No: 26, JCS Complex', 'Near Spencer\'s, Opp New RTC Busstand, Kurnool', 'Andhra Pradesh', 'Hyderabad', '518003', 'Praveen Shankar Paluru', '+91 8919843791, +91 9985901310, 08518 259952', 'vdc_rcjknl@yahoo.com', '09:00 hrs', '09:00 hrs', 'approved', '2019-05-22 05:25:58', '2019-06-25 10:15:03'),
(2, '15', 'Sri Lakshmi Ganapathi Ladoo\'s', 'No Sugar, Pure Ghee, Healthy & Tasty Home Made Dry Fruit Laddoo\'s.', 'G.1, Plot No.39, Sai Kririshna Apts', 'Nayakranti Nagar , Kapra', 'Telangana', 'Hyderabad', '5000062', 'Y Homesh', '09642150005', 'homeshy@gmail.com', '09:00 hrs', '09:00 hrs', 'approved', '2019-05-23 16:51:06', '2019-07-31 08:44:39'),
(3, '9', 'Trendy Woman', 'Artifical Jewelry, Sarees', 'Bank Colony Road', 'Hyderabad', 'Telangana', 'Hyderabad', NULL, 'Rangaraju Sridevi', '917702778177', 'srishannusuri@gmail.com', '09:00 hrs', '09:00 hrs', 'approved', '2019-05-25 11:49:46', '2019-05-27 05:03:04'),
(4, '5', 'Praveen Guptha N', 'Flower Boutuqe Business', '91,', 'Allwyn Colony Phase 1', 'Telangana', 'Hyderabad', '500072', 'Praveen Guptha Nandipati', '9642123254', 'praveennandipati@gmail.com', '09:00 hrs', '09:00 hrs', 'approved', '2019-05-25 19:27:38', '2019-05-27 21:29:22'),
(5, '3', 'Jk Enterprises', 'Testing', 'H.No:5-19', 'DurgaNagar', 'Telangana', 'Hyderabad', '500072', 'Sairam', '9603036519', 'itzursai@gmail.com', '09:00 hrs', '09:00 hrs', 'approved', '2019-05-28 18:35:00', '2019-05-28 18:35:35'),
(6, '11', 'Chaitanya Cake and Hugs', 'Cakes Business', 'Plot No:915', 'Allwyn Colony, Phase 1, Kukatpally, Hyderabad', 'Telangana', 'Hyderabad', '522265', 'Chaitu', '9849747939', 'vdc_cnh@yahoo.com', '09:00 hrs', '11:00 hrs', 'approved', '2019-06-23 09:24:59', '2019-06-23 09:53:24'),
(7, '11', 'Chaitanya Cake and Hugs', 'Cakes Business', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', '2019-06-23 09:25:11', '2019-06-23 09:25:11'),
(8, '11', NULL, NULL, 'H.no: 123', 'Gandhi Hills, Hyderabad', 'Telangana', 'Hyderabad', '522265', NULL, NULL, NULL, NULL, NULL, 'pending', '2019-06-23 09:25:57', '2019-06-23 09:25:57'),
(9, '38', 'Test Vemdpr', 'Test Business Description', '91, allwyn Colony Phase 1', 'Kukatpally', 'Andhra Pradesh', 'Secunderabad', '500072', NULL, NULL, NULL, NULL, NULL, 'approved', '2019-07-11 16:36:44', '2019-08-25 19:43:13'),
(10, '44', 'Wonder Photos', 'Wonder Photos\r\nCustomized Photo Frames', 'Wonder Photos', 'Hyderabad', 'Telangana', 'Hyderabad', '123456', NULL, NULL, NULL, NULL, NULL, 'approved', '2019-08-03 09:48:01', '2019-08-03 10:01:44'),
(11, '43', 'Home Vendor', 'Internal Vendor id for VDC to upoad products on our own', 'A1 block, 5th floor, zore Complex, Punjagutta.', 'Hyderabad', 'Telangana', 'Hyderabad', '500082', 'Jayasri', '+1-9032615531', 'jayasrimerugu1312@gmail.com', '09:00 hrs', '09:00 hrs', 'approved', '2019-08-04 20:08:50', '2019-08-04 20:56:36');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `poem_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cat_type` int(11) NOT NULL COMMENT '1-gallery,2-books,3-jayanthi-events,4-blog events,5-interviews',
  `item_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `video_url`, `poem_name`, `created_at`, `updated_at`, `cat_type`, `item_id`, `status`) VALUES
(39, 'jeY6igPL_ho', 'interview', '2020-10-09 09:27:51', '2020-10-09 09:27:51', 3, 4, 1),
(40, 'sadsadsd', 'sadas', '2020-10-09 09:54:50', '2020-10-09 09:54:50', 3, 5, 1),
(41, 'xRr-Nn73UHs', 'interview', '2020-10-09 15:35:43', '2020-10-09 15:35:43', 2, 27, 1),
(52, 'lLhadiHjiRE&t', 'New', '2020-10-09 18:59:28', '2020-10-09 18:59:28', 4, 3, 1),
(53, 'lLhadiHjiRE', 'New', '2020-10-09 18:59:28', '2020-10-09 18:59:28', 4, 3, 1),
(54, 'lLhadiHjiRE', 'Jayanthi Events', '2020-10-09 19:01:36', '2020-10-09 19:01:36', 3, 1, 1),
(55, 'lLhadiHjiRE', 'Jayanthi Events  2', '2020-10-09 19:01:36', '2020-10-09 19:01:36', 3, 1, 1),
(56, 'lLhadiHjiRE', 'New Blog Event 02', '2020-10-09 19:19:50', '2020-10-09 19:19:50', 4, 4, 1),
(57, 'lLhadiHjiRE', 'New Blog Event 02', '2020-10-09 19:21:11', '2020-10-09 19:21:11', 4, 4, 1),
(58, '_QOf0FB9PBs', 'HMTV Special Focus On Sahitya Sangamam | Dr Velchala Kondal Rao | Part 2', '2020-10-10 02:28:14', '2020-10-10 02:28:14', 3, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `publi_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `user_id`, `publi_id`, `created_at`, `updated_at`) VALUES
(2, 16, 17, '2020-09-30 11:51:23', NULL),
(3, 16, 19, '2020-09-30 12:20:10', NULL),
(4, 18, 18, '2020-09-30 13:42:03', NULL),
(5, 18, 19, '2020-09-30 13:42:04', NULL),
(14, 124, 22, '2020-10-08 20:15:12', NULL),
(15, 124, 21, '2020-10-08 20:15:13', NULL),
(16, 124, 20, '2020-10-08 20:15:16', NULL),
(27, 125, 25, '2020-10-09 02:35:18', NULL),
(30, 124, 23, '2020-10-09 06:03:46', NULL),
(31, 127, 1, '2020-10-09 16:42:38', NULL),
(32, 127, 20, '2020-10-10 17:54:31', NULL),
(33, 127, 22, '2020-10-10 18:06:10', NULL),
(34, 127, 25, '2020-10-10 18:25:28', NULL),
(35, 127, 23, '2020-10-10 18:26:35', NULL),
(36, 127, 21, '2020-10-10 18:29:01', NULL),
(38, 127, 24, '2020-10-10 18:29:23', NULL),
(40, 127, 24, '2020-10-10 18:30:29', NULL),
(41, 127, 30, '2020-10-10 19:16:09', NULL),
(44, 127, 34, '2020-10-10 19:47:23', NULL),
(46, 127, 35, '2020-10-10 19:48:40', NULL),
(47, 127, 37, '2020-10-10 19:49:18', NULL),
(48, 127, 36, '2020-10-10 19:49:50', NULL),
(52, 127, 32, '2020-10-10 20:02:02', NULL),
(53, 127, 31, '2020-10-10 21:34:24', NULL),
(54, 127, 31, '2020-10-10 21:34:24', NULL),
(58, 127, 26, '2020-10-10 21:45:35', NULL),
(78, 128, 36, '2020-10-11 00:12:06', NULL),
(79, 128, 35, '2020-10-11 00:12:31', NULL),
(80, 128, 34, '2020-10-11 00:12:41', NULL),
(81, 127, 29, '2020-10-11 00:14:10', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `blog_articles`
--
ALTER TABLE `blog_articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_news`
--
ALTER TABLE `blog_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_coupons_usage`
--
ALTER TABLE `cart_coupons_usage`
  ADD PRIMARY KEY (`cu_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`cu_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `earnings`
--
ALTER TABLE `earnings`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `magazines`
--
ALTER TABLE `magazines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `occassion_reminder`
--
ALTER TABLE `occassion_reminder`
  ADD PRIMARY KEY (`or_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`oitem_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_info`
--
ALTER TABLE `payment_info`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poems`
--
ALTER TABLE `poems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `poems_gal_cat_id_foreign` (`gal_cat_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`pi_id`);

--
-- Indexes for table `product_skus`
--
ALTER TABLE `product_skus`
  ADD PRIMARY KEY (`sku_id`);

--
-- Indexes for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD PRIMARY KEY (`pv_id`);

--
-- Indexes for table `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`rt_id`);

--
-- Indexes for table `send_enquiries`
--
ALTER TABLE `send_enquiries`
  ADD PRIMARY KEY (`se_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `service_banners`
--
ALTER TABLE `service_banners`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `service_documents`
--
ALTER TABLE `service_documents`
  ADD PRIMARY KEY (`sd_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`ua_id`);

--
-- Indexes for table `user_reviews`
--
ALTER TABLE `user_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendorbusinessdetails`
--
ALTER TABLE `vendorbusinessdetails`
  ADD PRIMARY KEY (`vb_id`);

--
-- Indexes for table `vendorsaccount`
--
ALTER TABLE `vendorsaccount`
  ADD PRIMARY KEY (`va_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blog_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog_articles`
--
ALTER TABLE `blog_articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog_news`
--
ALTER TABLE `blog_news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cart_coupons_usage`
--
ALTER TABLE `cart_coupons_usage`
  MODIFY `cu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `cu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=458;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupon_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `earnings`
--
ALTER TABLE `earnings`
  MODIFY `e_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `magazines`
--
ALTER TABLE `magazines`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `occassion_reminder`
--
ALTER TABLE `occassion_reminder`
  MODIFY `or_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5079;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `oitem_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `payment_info`
--
ALTER TABLE `payment_info`
  MODIFY `pi_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `poems`
--
ALTER TABLE `poems`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `p_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `pi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=625;

--
-- AUTO_INCREMENT for table `product_skus`
--
ALTER TABLE `product_skus`
  MODIFY `sku_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=410;

--
-- AUTO_INCREMENT for table `product_variations`
--
ALTER TABLE `product_variations`
  MODIFY `pv_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `publications`
--
ALTER TABLE `publications`
  MODIFY `b_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `publishers`
--
ALTER TABLE `publishers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `rt_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `send_enquiries`
--
ALTER TABLE `send_enquiries`
  MODIFY `se_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=371;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `s_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20023;

--
-- AUTO_INCREMENT for table `service_banners`
--
ALTER TABLE `service_banners`
  MODIFY `banner_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `service_documents`
--
ALTER TABLE `service_documents`
  MODIFY `sd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `s_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `ua_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `user_reviews`
--
ALTER TABLE `user_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vendorbusinessdetails`
--
ALTER TABLE `vendorbusinessdetails`
  MODIFY `vb_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vendorsaccount`
--
ALTER TABLE `vendorsaccount`
  MODIFY `va_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
