<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes(['verify' => true]);

Route::get('/telugu', 'Frontend\FrontendController@index')->name('hometelugu');
Route::get('/english', 'Frontend\FrontendController@index')->name('homeenglish');

Route::get('/', 'Frontend\FrontendController@index')->name('home');

Route::get('/set/currency', 'Controller@setCurrency')->name('setCurrency');

// Registration Routes...

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');

Route::post('email/resend', 'Auth\ResendVerificationController@resend')->name('verification.resend');


//Route::get('/category/{category}', 'Frontend\FrontendController@categoriesPage')->name('categoriesPage');


Route::get('/faq', 'Controller@faq')->name('page_faq');
Route::get('/citieswedeliver', 'Controller@citieswedeliver')->name('page_citieswedeliver');
Route::match(array('GET', 'POST'), '/contact-us', 'HomeController@Contactus')->name('page_conatct');

Route::get('/about-us', 'Controller@about')->name('page_about');
Route::get('/privacy', 'Controller@privacy')->name('page_privacy');
Route::get('/returnpolicy', 'Controller@returnpolicy')->name('page_returnpolicy');
Route::get('/termsofuse', 'Controller@termsofuse')->name('page_termsofuse');

Route::get('/corevalues', 'Controller@corevalues')->name('page_corevalues');
Route::get('/career', 'Controller@career')->name('page_career');
Route::get('/sitemap', 'Controller@sitemap')->name('sitemap');
// end pages
Route::post('/subscription/save', 'Frontend\FrontendController@subscriptionSave')->name('subscriptionSave');

Route::prefix('blog')->group(function () {

    Route::get('/', 'Controller@bloglist')->name('bloglist');
    Route::get('/view/{id}', 'Controller@blogView')->name('blogView');

});

Route::match(array('GET', 'POST'), 'cart/thank-you', 'Controller@cartThankYouPage')->name('cartThankYouPage');


Route::group(['prefix' => 'location-{locale}'], function() {

    Route::get('/{category}/{type?}', 'Frontend\FrontendController@categoriesPage')->name('categoriesPage');
    Route::get('/{category}/{foritem}/filter/', 'Frontend\FrontendController@categoriesSplPage')->name('categoriesSplPage');
});


Route::get('/product/{category}/{product}/{sku?}/{otp?}', 'Frontend\FrontendController@productPage')->name('productPage');
//Route::get('/service/{service}', 'Frontend\FrontendController@servicePage')->name('servicePage');

Route::match(array('GET', 'POST'), '/service/{service?}', 'Frontend\FrontendController@servicePage')->name('servicePage');


Route::match(array('GET', 'POST'), '/autocomplete_course/', 'Frontend\FrontendController@autocomplete_course')->name('autocomplete_course');
Route::match(array('GET', 'POST'), '/ajax_wish_list/', 'Frontend\FrontendController@ajax_wish_list')->name('ajax_wish_list');
Route::match(array('GET', 'POST'), '/get_langauage_ajax_data/', 'Frontend\FrontendController@ajax_language_filter')->name('get_langauage_ajax_data');
Route::match(array('GET', 'POST'), '/get_gallery_load_ajax_data/', 'Frontend\FrontendController@ajax_gallery_load')->name('ajax_gallery_load');
Route::match(array('GET', 'POST'), '/get_videos_load_ajax_data/', 'Frontend\FrontendController@ajax_videos_load')->name('ajax_videos_load');


Route::get('/cart', 'Frontend\FrontendController@cartPage')->name('cartPage');
Route::get('/cart/address', 'Frontend\UserController@cartAdressPage')->name('cartAdressPage');

// Cart operation

// Add to Cart
Route::match(array('GET', 'POST'), '/ajax/addToCart', 'Frontend\FrontendController@addToCart')->name('addToCart');
// Add to Cart for only one product
Route::match(array('GET', 'POST'), '/product/addTocart', 'Frontend\FrontendController@buyProduct')->name('buyProduct');


Route::match(array('GET', 'POST'), '/checkout', 'Frontend\UserController@checkoutPage')->name('checkoutPage');

Route::get('/payment/success', 'Frontend\UserController@paymentSuccessPage')->name('paymentSuccessPage');


// Send Enquiry
Route::match(array('GET', 'POST'), '/product/send-enquiry', 'Frontend\FrontendController@productSendEnquiry')->name('productSendEnquiry');
Route::match(array('GET', 'POST'), '/product/send-enquiry/verify', 'Frontend\FrontendController@productSendEnquiryVerify')->name('productSendEnquiryVerify');
// End Send Enquiry

Route::prefix('cart')->group(function () {

});


Route::prefix('user')->group(function () {

   
   
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('userlogin');
    Route::post('/login', 'Auth\LoginController@login');
    Route::get('/logout', 'Auth\LoginController@logout')->name('userlogout');


    // Add to wish list
    Route::match(array('GET', 'POST'), '/profile', 'Frontend\UserController@index')->name('userprofile');
    // Delete User Image

    Route::match(array('GET', 'POST'), '/manage/password', 'Frontend\UserController@changePassword')->name('userChangePassword');
    Route::match(array('GET', 'POST'), '/orders/{status?}', 'Frontend\UserController@orders')->name('orders');
    Route::match(array('GET', 'POST'), '/order/details/{order}', 'Frontend\UserController@orderDetails')->name('orderDetails');
//    Route::match(array('GET', 'POST'), '/order/invoice/{order}', 'Frontend\UserController@orderInvoice')->name('orderInvoice');
    Route::match(array('GET', 'POST'), '/unprocessed/orders', 'Frontend\UserController@unProcessedOrders')->name('unProcessedOrders');
     Route::match(array('GET', 'POST'), '/pdf_view/{id}', 'Frontend\UserController@pdf_view')->name('pdf_view');

    Route::match(array('GET', 'POST'), '/addres-book', 'Frontend\UserController@userAddressBook')->name('userAddressBook');
//    Route::match(array('GET', 'POST'), '/addres-book/remove', 'Frontend\UserController@userAddressBookRemove')->name('userAddressBookRemove');

    Route::match(array('GET', 'POST'), '/manage/addres-book', 'Frontend\UserController@userAddAddressBook')->name('userAddAddressBook');

    Route::match(array('GET', 'POST'), '/wish-list', 'Frontend\UserController@wishList')->name('wishList');
     Route::match(array('GET', 'POST'), '/pdf_view/{id}', 'Frontend\UserController@pdf_view')->name('pdf_view');

    Route::match(array('GET', 'POST'), '/payment/confirmation', 'Frontend\UserController@paymentConfirmation')->name('paymentConfirmation');

    Route::match(array('GET', 'POST'), '/cart/orders/save', 'Frontend\UserController@saveOrders')->name('saveOrders');



    Route::prefix('payment')->group(function () {

        Route::match(array('GET', 'POST'), 'services/pay/{order}', 'PaymentController@servicePayWithpaypal')->name('payWithpaypal');
        Route::match(array('GET', 'POST'), '/status', 'PaymentController@getPaymentStatus')->name('getPaymentStatus');


    });


    Route::match(array('GET', 'POST'), '/addres-book/default-addres', 'Frontend\UserController@makeDefaultAddress')->name('makeDefaultAddress');
    Route::match(array('GET', 'POST'), '/service/invoices', 'Frontend\UserController@ServiceInvoices')->name('ServiceInvoices');
    Route::match(array('GET', 'POST'), '/service/invoice/{id?}', 'Frontend\UserController@showServiceInvoice')->name('showServiceInvoice');

    Route::match(array('GET', 'POST'), '/occassion-reminder', 'Frontend\UserController@occassionReminder')->name('occassionReminder');

    Route::match(array('GET', 'POST'), '/cart/service/paypal/{ref_id}', 'PaymentController@payWithpaypalCart')->name('payWithpaypalCart');
    Route::match(array('GET', 'POST'), '/cart/payment/status/save/', 'PaymentController@getPaymentCartStatus')->name('getPaymentCartStatus');

    Route::match(array('GET', 'POST'), '/order/invoice/{id?}', 'Frontend\UserController@showOrderInvoice')->name('showOrderInvoice');



    Route::prefix('cart')->group(function () {

    });


    


    Route::match(array('GET', 'POST'), '/rating/add', 'Frontend\FrontendController@addNewRating')->name('addNewRating');

});




Route::prefix('admin')->group(function () {

    Route::match(array('GET', 'POST'), '/settings', 'Admin\SettingsController@index')->name('admin_settings');

    Route::match(array('GET', 'POST'), '/product/enquiries', 'Admin\AdminController@product_enquiries')->name('product_enquiries');

    Route::post('/enquiry/delete', 'Admin\AdminController@enquiryDelete')->name('enquiryDelete');

    /* login module routes */
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('adminLogin');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    /* Dashboard routes */
    Route::get('/dashboard', 'Admin\AdminController@index')->name('dashboard');

    /* Change Password */
    Route::match(array('GET', 'POST'), '/change/password', 'Admin\AdminController@changePassword')->name('changePassword');

    /* update Profile */
    Route::match(array('GET', 'POST'), '/update/profile', 'Admin\AdminController@profile')->name('profile');


    /* Countries module routes */
    Route::match(array('GET', 'POST'), '/publishers', 'Admin\CountriesController@index')->name('admin_publishers');


  

    /* Blogs module routes */
    Route::match(array('GET', 'POST'), '/blogs', 'Admin\BlogsController@index')->name('blogs');
    Route::match(array('GET', 'POST'), '/blogs/manage/{id?}', 'Admin\BlogsController@addNewBlogs')->name('addNewBlogs');
    Route::post('ajax/deleteblogimage', 'Admin\BlogsController@deleteBlogimage')->name('deleteBlogimage');
    
      /* Poems module routes */
    Route::match(array('GET', 'POST'), '/admin_poems', 'Admin\PoemsController@index')->name('admin_poems');
    Route::match(array('GET', 'POST'), '/addNewPoems/manage/{id?}', 'Admin\PoemsController@addNewPoems')->name('addNewPoems');
    
      /* Interviews module routes */
    Route::match(array('GET', 'POST'), '/admin_interviews', 'Admin\InterviewsController@index')->name('admin_interviews');
    Route::match(array('GET', 'POST'), '/addNewInterviews/manage/{id?}', 'Admin\InterviewsController@addNewPoems')->name('addNewInterviews');
    
      /* admin pincodes module routes */
    Route::match(array('GET', 'POST'), '/admin_pincodes', 'Admin\PincodesController@index')->name('admin_pincodes');
    Route::match(array('GET', 'POST'), '/admin_pincodes/manage/{id?}', 'Admin\PincodesController@addNewPincodes')->name('addNewPincodes');
   

    /* Events module routes */
    Route::match(array('GET', 'POST'), '/events', 'Admin\EventsController@index')->name('events');
    Route::match(array('GET', 'POST'), '/events/manage/{id?}', 'Admin\EventsController@addNewEvents')->name('addNewEvents');
    Route::post('/gallery_delete_img', 'Admin\EventsController@delete_img')->name('delete_img');
    Route::post('ajax/deleteblogimage', 'Admin\EventsController@deleteEventimage')->name('deleteBlogimage');

    /*magazines modules routes */
    Route::match(array('GET', 'POST'), '/magazines', 'Admin\MagazinesController@index')->name('adminmagazines');
    Route::match(array('GET', 'POST'), '/magazines/manage/{id?}', 'Admin\MagazinesController@addNewMagazines')->name('addNewMagazines');
    Route::post('ajax/deleteblogimage', 'Admin\MagazinesController@deleteMagazinesimage')->name('deleteMagazinesimage');
    /*blogNews modules routes */
    Route::match(array('GET', 'POST'), '/admin_blog_news', 'Admin\BlogNewsController@index')->name('admin_blog_news');
    Route::match(array('GET', 'POST'), '/admin_blog_news/manage/{id?}', 'Admin\BlogNewsController@addNewBlogNews')->name('addNewBlogNews');
    Route::post('ajax/deleteblogimage', 'Admin\EventsController@deleteBlogNewsimage')->name('deleteBlogNewsimage');

    /*blogArticales modules routes */
    Route::match(array('GET', 'POST'), '/admin_blog_articales', 'Admin\BlogArticalesController@index')->name('admin_blog_articales');
    Route::match(array('GET', 'POST'), '/admin_blog_articales/manage/{id?}', 'Admin\BlogArticalesController@addNewBlogArticales')->name('addNewBlogArticales');
    Route::post('ajax/deleteblogimage', 'Admin\BlogArticalesController@deleteBlogArticalesimage')->name('deleteBlogArticlesimage');

    /* Categories module routes */

    Route::match(array('GET', 'POST'), '/publishers', 'Admin\CategoriesController@index')->name('admin_publishers');

    Route::match(array('GET', 'POST'), '/categories/manage/{id?}', 'Admin\CategoriesController@addNewCategories')->name('addNewCategories');
    Route::post('ajax/deletecategoryimage', 'Admin\CategoriesController@deleteCategoryimage')->name('deleteCategoryimage');
    Route::post('ajax/checkNewCategoryAlias', 'Admin\CategoriesController@checkNewCategoryAlias')->name('checkNewCategoryAlias');


    Route::post('ajax/checkCouponcode', 'Admin\CouponsController@checkCouponcode')->name('checkCouponcode');
    /* Books module routes */
    Route::match(array('GET', 'POST'), '/books', 'Admin\ProductsController@index')->name('admin_books');
    Route::match(array('GET', 'POST'), '/products/manage/{id?}', 'Admin\ProductsController@addNewProducts')->name('addNewProducts');
    Route::match(array('GET', 'POST'), '/products/ProductApprove/{id?}', 'Admin\ProductsController@delete')->name('deleteBooks');
    Route::match(array('GET', 'POST'), '/products/view/{id?}', 'Admin\ProductsController@AdminProductView')->name('AdminProductView');
    Route::post('ajax/deleteproductimage', 'Admin\ProductsController@deleteproductimage')->name('deleteproductimage');

    Route::post('ajax/checkProductAlias', 'Admin\ProductsController@checkProductAlias')->name('checkProductAlias');

    Route::match(array('GET', 'POST'), '/products/extraInfo/{id?}', 'Admin\ProductsController@productExtraInfo')->name('productExtraInfo');

    Route::match(array('GET', 'POST'), '/products/import/{id?}', 'Admin\ProductsController@sellerImportListings')->name('sellerImportListings');


    Route::match(array('GET', 'POST'), '/products/extraInfo/manage/{id?}/{extraid?}', 'Admin\ProductsController@addProductExtraInfo')->name('addProductExtraInfo');

    Route::post('ajax/deleteproductextraimage', 'Admin\ProductsController@deleteProductExtraimage')->name('deleteProductExtraimage');
    /* Users module routes */
    Route::match(array('GET', 'POST'), '/users', 'Admin\UsersController@index')->name('users');
    /* Users Address module routes */

    Route::match(array('GET', 'POST'), '/user/Address/{id}', 'Admin\UsersController@userAddress')->name('userAddress');
    /* Users Orders module routes */
    Route::match(array('GET', 'POST'), '/user/orders/{id}', 'Admin\UsersController@userOrders')->name('userAdminOrders');

    Route::match(array('GET', 'POST'), '/user/ordersDetails/{id}', 'Admin\UsersController@userOrdersDetails')->name('userOrdersDetails');
    Route::match(array('GET', 'POST'), '/user/ordersStatus/{id}', 'Admin\UsersController@orderStatusUpdate')->name('orderStatusUpdate');
    /* Users Orders module routes */
    Route::match(array('GET', 'POST'), '/user/wishlist/{id}', 'Admin\UsersController@userWishlist')->name('userWishlist');
    /* Orders module routes */
    Route::match(array('GET', 'POST'), '/orders', 'Admin\UsersController@orders')->name('adminorders');
    Route::match(array('GET', 'POST'), '/reports', 'Admin\UsersController@reports')->name('adminreports');
    Route::match(array('GET', 'POST'), '/reviews', 'Admin\UsersController@Reviews')->name('Reviews');
    Route::get('subscriptions', 'Admin\AdminController@subscriptions')->name('subscriptions');


});




Route::prefix('ajax')->group(function () {

    Route::post('deleteuserimage', 'frontend\UserController@deleteUserimage')->name('deleteUserimage');

    Route::post('ajax/checkProductAlias', 'AjaxController@checkProductAlias')->name('checkProductAlias');

    Route::match(array('GET', 'POST'), 'get-types', 'AjaxController@showCategoryTypes')->name('showCategoryTypes');

    Route::match(array('GET', 'POST'), 'get-states', 'AjaxController@showStates')->name('showStates');
    Route::match(array('GET', 'POST'), 'get-cities', 'AjaxController@showCities')->name('showCities');

    Route::match(array('GET', 'POST'), '/seller-listings/options/remove', 'AjaxController@removeProductOptions')->name('seller_remove_options');


    // update From Cart
    Route::match(array('GET', 'POST'), '/ajax/update/item/', 'Frontend\FrontendController@updateItemFromCart')->name('updateItemFromCart');
// Remove From Cart
    Route::match(array('GET', 'POST'), '/ajax/remove/item/', 'Frontend\FrontendController@removeItemFromCart')->name('removeItemFromCart');
//Add to wishlist
    Route::match(array('GET', 'POST'), '/ajax/addToWishlist', 'Frontend\UserController@addToWishList')->name('addToWishList');

    Route::match(array('GET', 'POST'), '/api/ajax/editor/imageupload', 'Controller@editorimageupload')->name('editorImageupload');
//Ajax
    Route::match(array('GET', 'POST'), 'ajax/product/image/remove', 'AjaxController@productImageRemove')->name('VendordeleteProductimage');

    Route::match(array('GET', 'POST'), 'ajax/search/products', 'AjaxController@search')->name('searchProductInfo');


    Route::match(array('GET', 'POST'), '/getammachethivantaoptions/', 'AjaxController@getammachethivantaoptions')->name('getammachethivantaoptions');

    Route::match(array('GET', 'POST'), '/curencyConvert', 'AjaxController@ajaxCurencyConvert')->name('ajaxCurencyConvert');




    Route::get('ammchathivanta_wightbased_price','AjaxController@ajax_ammchathivanta_wightbased_price')->name('ajax_ammchathivanta_wightbased_price');






});


Route::get('/velchala', 'Frontend\FrontendController@velchala')->name('velchala');
Route::get('/vsp', 'Frontend\FrontendController@vsp')->name('vsp');
Route::get('/publications', 'Frontend\FrontendController@publications')->name('publications');
Route::get('/vsp', 'Frontend\FrontendController@vsp')->name('vsp');
Route::get('/magazines', 'Frontend\FrontendController@magazines')->name('magazines');
Route::post('/ajax_magazines', 'Frontend\FrontendController@ajax_magazines')->name('ajax_magazines');
Route::get('/jayanthi-events', 'Frontend\FrontendController@jayanthi_events')->name('jayanthi-events');
Route::get('/jayanthi-events-details/{id}', 'Frontend\FrontendController@jayanthi_events_details')->name('jayanthi-events-details');
Route::get('/blog-article-detail/{id}', 'Frontend\FrontendController@blog_article_detail')->name('blog-article-detail');
Route::get('/blog-events-details/{id}', 'Frontend\FrontendController@blog_events_details')->name('blog-events-details');
Route::get('/photo-albums', 'Frontend\FrontendController@photo_albums')->name('photo-albums');
Route::get('/gallery-videos', 'Frontend\FrontendController@gallery_videos')->name('gallery-videos');
Route::get('/gallery-poems', 'Frontend\FrontendController@gallery_poems')->name('gallery-poems');
Route::get('/jayanthi-events-photos/{id}', 'Frontend\FrontendController@jayanthi_events_photos')->name('jayanthi-events-photos');
Route::get('/blog-events-photos/{id}', 'Frontend\FrontendController@blog_events_photos')->name('blog-events-photos');
Route::get('/details/{id}', 'Frontend\FrontendController@book_details')->name('book-details');
Route::get('/blog-events', 'Frontend\FrontendController@blog_events')->name('blog-events');
Route::get('/blog_interviews', 'Frontend\FrontendController@blog_interviews')->name('blog_interviews');
Route::get('/blog-article', 'Frontend\FrontendController@blog_article')->name('blog-article');
Route::get('/blog_news', 'Frontend\FrontendController@blog_news')->name('blog_news');
Route::post('/addtocart', 'Frontend\FrontendController@add_to_cart_data_ajax');
Route::post('/add_to_cart_data_ajax', 'Frontend\FrontendController@add_to_cart_data_ajax');
Route::get('/cart', 'Frontend\FrontendController@cart');
Route::post('/remove_cart', 'Frontend\FrontendController@remove_cart');



//Route::get('/home', 'HomeController@index')->name('home');
