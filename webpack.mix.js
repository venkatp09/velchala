const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.sass', 'public/css');


mix.copyDirectory('resources/assets/backend/images', 'public/admin/images');
mix.copyDirectory('public/theme/img/', 'public/frontend/images');



// Admin
mix.js('resources/assets/backend/js/adminScripts.js', 'public/admin/js');
mix.sass('resources/assets/backend/sass/style_default.scss', 'public/admin/css');

// frontend
mix.js('public/theme/js/custom.js', 'public/frontend/js');
mix.sass('public/theme/css/style.scss', 'public/frontend/css');






