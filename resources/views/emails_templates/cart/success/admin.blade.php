<!DOCTYPE html>
<html>

<head>
    <title>Thanks from velchala</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            padding: 5px;
        }

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>

    <style type="text/css">


        ol, ul {
            list-style: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }

        q, blockquote {
            quotes: none;
        }

        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none;
        }

        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
            display: block;
        }

        body {
            font-family: 'Poppins', sans-serif;
            font-weight: 300;
            font-size: 13px;
            margin: 0;
            padding: 0;
        }

        body a {
            text-decoration: none;
            color: inherit;
        }

        body a:hover {
            color: inherit;
            opacity: 0.7;
        }

        body .container {
            /*width: 900px;*/
            margin: 0 auto;
            padding: 0 20px;
        }

        body .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        body .left {
            float: left;
        }

        body .right {
            float: right;
        }

        body .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        body .no-break {
            page-break-inside: avoid;
        }

        header {
            margin-top: 20px;
            margin-bottom: 50px;
        }

        header figure {
            float: left;
            width: 60px;
            height: 60px;
            margin-right: 15px;
            text-align: center;
        }

        header .company-address {
            float: left;
            max-width: 150px;
            line-height: 1.7em;
        }

        header .company-address .title {
            color: #8BC34A;
            font-weight: 400;
            font-size: 1.5em;
            text-transform: uppercase;
        }

        header .company-contact {
            float: right;
            height: 60px;
            padding: 0 10px;
            background-color: #8BC34A;
            color: white;
        }

        header .company-contact span {
            display: inline-block;
            vertical-align: middle;
        }

        header .company-contact .circle {
            width: 20px;
            height: 20px;
            background-color: white;
            border-radius: 50%;
            text-align: center;
        }

        header .company-contact .circle img {
            vertical-align: middle;
        }

        header .company-contact .phone {
            height: 100%;
            margin-right: 20px;
        }

        header .company-contact .email {
            height: 100%;
            min-width: 100px;
            text-align: right;
        }

        .date {
            line-height: 20px;
        }

        section .details {
            margin-bottom: 55px;
        }

        section .details .client {
            width: 50%;
            line-height: 20px;
        }

        section .details .client .name {
            color: #8BC34A;
        }

        section .details .data {
            width: 50%;
            text-align: right;
        }

        section .details .title {
            margin-bottom: 15px;
            color: #8BC34A;
            font-size: 3em;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;

        }

        section table .qty, section table .unit, section table .total {
            width: 15%;
        }

        section table .desc {
            width: 55%;
        }

        section table thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }

        section table thead th {
            padding: 5px 10px;
            background: #8BC34A;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #FFFFFF;
            text-align: right;
            color: white;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table thead th:last-child {
            border-right: none;
        }

        section table thead .desc {
            text-align: left;
        }

        section table thead .qty {
            text-align: center;
        }

        section table tbody td {
            padding: 10px;
            background: #E8F3DB;
            color: #000;
            text-align: right;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #E8F3DB;
        }

        section table tbody td:last-child {
            border-right: none;
        }

        section table tbody h3 {
            margin-bottom: 5px;
            color: #8BC34A;
            font-weight: 600;
            font-size: 15px;
        }

        section table tbody .desc {
            text-align: left;
            line-height: 20px;
            font-size: 13px;
        }

        section table tbody .qty {
            text-align: center;
        }

        section table.grand-total {
            margin-bottom: 45px;
        }

        section table.grand-total td {
            padding: 5px 10px;
            border: none;
            color: #777777;
            text-align: right;
            line-height: 25px;
        }

        section table.grand-total .desc {
            background-color: transparent;
        }

        section table.grand-total tr:last-child td {
            font-weight: 600;
            color: #8BC34A;
            font-size: 1.18181818181818em;
        }

        footer {
            margin-bottom: 20px;
        }

        footer .thanks {
            margin-bottom: 40px;
            color: #8BC34A;
            font-size: 1.16666666666667em;
            font-weight: 600;
        }

        footer .notice {
            margin-bottom: 25px;
            line-height: 22px;
        }

        footer .end {
            padding-top: 5px;
            border-top: 2px solid #8BC34A;
            text-align: center;
        }

    </style>

    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet"/>
    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet"/>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; padding:25px; background:#eaeff2;">

    <?php
    $userAddress = unserialize($orders->order_delivery_address);

    //                                                            dump($userAddress);
    ?>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <a href="https://www.velchala.com/"><img src="https://www.velchala.com/logo.png"></a>
            </td>
        </tr>
        <tr>
            <td align="center">
                <h1 style="font-weight: normal; margin:0;">Hi <strong style="color:#0098eb">Admin</strong></h1>
                <h3 style="padding:10px 50px; line-height: 30px; margin: 0;">{{$userAddress['ua_name']}} placed new order</h3>
            </td>
        </tr>



    </table>
        <?php
        $order_currency = $orders->order_currency;
        ?>


    <section>
        <div class="container">
            <div class="details clearfix">
                <div class="client left">


                    <p>Shipping TO:</p>
                    <p class="name">
                        <strong style="font-size:16px;">
                            {{--{!!  !!}--}}
                            {{$userAddress['ua_name']}},
                        </strong>
                    </p>
                    <p>
                        {{--Plot No:91, Ganapathi Nivas, Allwyn colony 1, Hyderabad--}}



                        {{$userAddress['ua_address']}},
                        {{$userAddress['ua_landmark']}},
                        {{$userAddress['ua_city']}},
                        {{$userAddress['ua_state']}},
                        {{$userAddress['ua_country']}},
                        {{$userAddress['ua_pincode']}},
                        {{$userAddress['ua_phone']}},

                    </p>
                    <a href="mailto:{{$userAddress['ua_email']}}">
                        {{$userAddress['ua_email']}}
                    </a>
                </div>
                <div class="data right">
                    {{--<div class="title">Invoice</div>--}}
                    <div class="date">
                        Date of Order: {{$orders->created_at}}<br>
                        {{--Due Date: 30/06/2014--}}
                    </div>
                </div>
            </div>

            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th class="desc">Image</th>
                    <th class="desc">Product Name</th>
                    <th class="qty">Quantity</th>
                    <th class="unit">Price</th>
                    <th class="unit">Delevery Charge</th>
                    <th class="total">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $subtotal = array();

                ?>

                @if (count($orders->orderItems)> 0)
                    @foreach($orders->orderItems AS $items)

                        <?php
                        //                                                                dump($items);
                        ?>

                        <tr>
                            <td class="desc"> <?php
                                if (isset($items->getProduct->productImages[0])) {
                                    $item_img = url('/uploads/products/' . $items->getProduct->productImages[0]->pi_image_name);

                                } else {
                                    $item_img = '/frontend/images/no-image.png';
                                }


                                ?>
                                <img src="{{$item_img}}" alt="" title="" class="img-fluid"
                                     width="150px">
                            </td>
                            <td class="desc">
                                <h6>
                                    {{ $items->oitem_product_name  }}

                                    {{--Service of Medical Supporting--}}
                                </h6>

                            </td>
                            <td class="qty">
                                {{ $items->oitem_qty  }}</td>
                            <td class="unit">

                                {!! currencySymbol($order_currency) !!} {{  $items->oitem_product_price }}

                            </td>

                            <td class="no-break">

                                {!! currencySymbol($order_currency) !!} {{ $items->oitem_delivery_charge  }}


                            </td>

                            <td class="no-break">


                                <?php
                                $subtotalItem = $items->oitem_sub_total + $items->oitem_delivery_charge;

                                $subtotal[] = $subtotalItem;

                                ?>

                                    {!! currencySymbol($order_currency) !!} {{ $subtotalItem  }}


                            </td>
                        </tr>
                    @endforeach
                @endif


                </tbody>
            </table>



            <div class="no-break">
                <table class="grand-total">
                    <tbody>
                    <tr>
                        <td class="" width="40%"></td>
                        <td class="" colspan="2"> SUBTOTAL:</td>
                        <td class="">
                            {!! currencySymbol($order_currency) !!}

                    {{  array_sum($subtotal)   }}



                        </td>
                    </tr>
                    <tr>
                        <td class=""></td>

                        <td class="" colspan="2">
                            Shipping :
                        </td>
                        <td class="">

                            {!! currencySymbol($order_currency) !!}

                            {{  $orders->order_shipping_price   }}
                        </td>
                    </tr>
                    <tr>
                        <td class=""></td>
                        <td class="" colspan="2">
                            TAX {{$orders->s_tax}}%:
                        </td>
                        <td class="">
                            <?php
                            $taxAmount = array_sum($subtotal) / 100 * $orders->s_tax;
                            ?>
                            {!! currencySymbol($order_currency) !!}

                            {{  $taxAmount   }}
                        </td>
                    </tr>


                    <tr>
                        <td class=""></td>
                        <td class="" colspan="2">
                            Total:
                        </td>
                        <td class="">
                            <?php
                            $taxotalAmount = $taxAmount + $orders->order_shipping_price + $orders->order_sub_total_price;
                            ?>
                            {!! currencySymbol($order_currency) !!}

                            {{  $taxotalAmount   }}
                        </td>
                    </tr>

                    <tr>
                        <td class=""></td>
                        <td class="" colspan="2">
                            Coupon Discount :
                        </td>
                        <td class="">

                            - {!! currencySymbol($order_currency) !!}

                            {{  $orders->order_coupon_discount_amount   }}
                        </td>
                    </tr>

                    <tr>
                        <td class=""></td>
                        <td class="" colspan="2">GRAND TOTAL:</td>
                        <td class="">
                            <?php
                            $total = array_sum($subtotal) + $taxAmount;
                            ?>
                            {!! currencySymbol($order_currency) !!}

                            {{  $orders->order_total_price  }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </section>


</div>
</body>

</html>
