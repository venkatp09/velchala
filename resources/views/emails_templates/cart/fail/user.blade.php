<!DOCTYPE html>
<html>

<head>
    <title>Thanks from velchala</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            padding: 5px;
        }

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; padding:25px; background:#eaeff2;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <a href="https://www.velchala.com/"><img src="https://www.velchala.com/logo.png"></a>
            </td>
        </tr>
        <tr>
            <td align="center">
                <h1 style="font-weight: normal; margin:0;">Hi <strong style="color:#0098eb">{{$userAddress['ua_name']}}</strong></h1>
                <h3 style="padding:10px 50px; line-height: 30px; margin: 0;">Your Order has been failed, once again try</h3>
            </td>
        </tr>



    </table>
</div>
</body>

</html>
