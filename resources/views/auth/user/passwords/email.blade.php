<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
 <link rel="icon" type="image/png" sizes="32x32" href="{{ url('theme/img/fav.png')}}">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- animation components-->
<link rel="stylesheet" href="{{ url('theme/css/animate-4.0.css')}}">

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">

<!-- Fade loading -->
<link rel="stylesheet" href="{{ url('theme/css/animsition.css')}}">
</head>
<body>
    <main>
        <!-- div login -->
        <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">

                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="{{url('/')}}" class="brand-login">
                                <img src="{{url('theme/img/logo.svg')}}" alt="Velchala">
                            </a>
                            <h1 class="text-center flight pb-0">Reset Your Account</h1>
                            <p class="text-center">This is a secure system and you will need to provide your login details to access the site.</p>
                        </div>
                        
                        @if(session()->has('flash_message'))
                        <strong style="color: green;"> {{ session()->get('flash_message') }}</strong>
                        @endif
                <form class="formsign" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Enter your Registered Email Address<span class="mand">*</span></label>
                        <input id="email" type="text" placeholder="Email Address"
                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                    </div>
                    <input type="submit"  value="Submit" class="btn orange-btn w-100 mt-2">
                    <p class="text-center">Back to <a href="{{ route('userlogin') }}" class="fgreen">Login ?</a></p>

                </form>

            </div>
        </div>
        <!--/ div login -->
    </main>
