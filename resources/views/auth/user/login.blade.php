<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Velchala Kondal Rao</title>
 <link rel="icon" type="image/png" sizes="32x32" href="{{ url('theme/img/fav.png')}}">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- animation components-->
<link rel="stylesheet" href="{{ url('theme/css/animate-4.0.css')}}">

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">

<!-- Fade loading -->
<link rel="stylesheet" href="{{ url('theme/css/animsition.css')}}">
</head>
<body>
    <main>
        <!-- div login -->
        <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">

                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="{{url('/')}}" class="brand-login">
                                <img src="{{url('theme/img/logo.svg')}}" alt="Velchala">
                            </a>
                            <h1 class="text-center flight pb-0">Login Account</h1>
                            <p class="text-center">This is a secure system and you will need to provide your login details to access the site.</p>
                        </div>
                        
                         @if (Session::has('flash_message'))
                    <br/>
                   
                        <strong>{!! Session::get('flash_message' ) !!}</strong>
                   
                @endif
        
                <form method="POST" action="{{ route('userlogin') }}" aria-label="{{ __('Login') }}" class="pt-4"
                      id="login">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Email<span class="mand">*</span></label>
                        <input id="email" required type="text" placeholder="Email Address"
                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span id="email_error"  class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                    </div>

                    <div class="form-group mb-0">
                        <label>Password<span class="mand">*</span></label>
                        <input required id="password" type="password" placeholder="Enter Your Password"
                               class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password">
                        @if ($errors->has('password'))
                            <span id="password_error" class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <p class="text-right"><a href="{{ route('password.reset') }}">Forgot Password?</a></p>
                    <input type="submit" value="login" class="btn orange-btn w-100 mt-2">
                    <p class="text-center">
                                Don't have an account? <a class="forange" href="{{url('register')}}">Register now</a>
                            </p>

                </form>

            </div>
        </div>
        <!--/ div login -->
    </main>
    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="resenactivationlink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Send Activation link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('verification.resend') }}">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" value="1" name="role">
                        <div class="form-group">
                            <label>Enter your Registered Email Address<span class="mand">*</span></label>
                            <input id="email" type="text" placeholder="Email Address"
                                   class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback"
                                      role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send link</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#email").focus(function(){
      $("#email_error").hide();
  });
   $("#password").focus(function(){
      $("#password_error").hide();
  });
});
</script>
