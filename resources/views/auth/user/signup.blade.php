<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
<link rel="icon" type="image/png" sizes="32x32" href="http://velchala.corewebpro.com/img/fav.png">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- animation components-->
<link rel="stylesheet" href="{{ url('theme/css/animate-4.0.css')}}">

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">

<!-- Fade loading -->
<link rel="stylesheet" href="{{ url('theme/css/animsition.css')}}">
</head>
<body>
    <main>
        <!-- div login -->
        <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">

                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="{{url('/')}}" class="brand-login">
                                <img src="{{url('theme/img/logo.svg')}}" alt="Velchala">
                            </a>
                            <h1 class="text-center flight pb-0">Login Account</h1>
                            <p class="text-center">This is a secure system and you will need to provide your login details to access the site.</p>
                        </div>
                        
                        @if(session()->has('flash_message'))
                        <strong style="color: green;"> {{ session()->get('flash_message') }}</strong>
                        @endif
                <form method="POST" action="{{ route('register') }}" class="pt-4" id="register" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="role" value="1">
                    <input type="hidden" name="status" value="active">
                    <div class="form-group">
                        <label>Name<span class="mand">*</span></label>
                        <input id="name" required type="text" placeholder="Name"
                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                               value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span id="name_error" class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Email Address<span class="mand">*</span></label>
                        <input required id="email" type="text" placeholder="Email Address"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span id="email_error" class="invalid-feedback" role="alert">
                            <strong>{!! $errors->first('email') !!}</strong>
                       </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="mobileNo">Mobile Number<span class="mand">*</span></label>


                        <div class="input-group">
                            
                            <input required id="mobileNo" type="text" placeholder="Mobile"
                                   class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile"
                                   value="{{ old('mobile') }}">
                            @if ($errors->has('mobile'))
                                <span id="mobileNo_error" class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobile') }}</strong>
                             </span>
                            @endif
                        </div>


                    </div>

                    <div class="form-group position-relative">
                        <label>Password<span class="mand">*</span></label>
                        <input id="password" required type="password" name="password" placeholder="Enter Your Password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} ">
                        @if ($errors->has('password'))
                            <span id="password_error" class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <!-- popover-->
                    {{--<a href="#" class="infodiv" data-toggle="popover" title="Password must contain"><i--}}
                    {{--class="fas fa-info-circle"></i></a>--}}
                    <!--/ popover -->
                        <!-- show password icon -->
                    {{--<span class="showpw"><i class="far fa-eye"></i></span>--}}
                    <!--/ show password  icon -->

                        <!-- loaded popover content -->
                        <div id="popover-content" style="display: none">
                            <ul class="list-group custom-popover">
                                <li class="list-group-item"><span>At least 6 Characters</span></li>
                                <li class="list-group-item"><span>At least 1 Upper case letter (A - Z)</span></li>
                                <li class="list-group-item"><span>At least 1 Lower case Letter (a - z)</span></li>
                                <li class="list-group-item"><span>At least 1 Number (0 - 9)</span></li>
                                <li class="list-group-item"><span>One specl character like !, $, # ....</span></li>
                            </ul>
                        </div>
                        <!--/ loaded popover content -->
                    </div>
                    <div class="form-group position-relative">
                        <label>Confirm Password<span class="mand">*</span></label>
                        <input id="password-confirm" required type="password" placeholder="Enter Your Confirm Password"
                               class="form-control" name="password_confirmation">
                    {{--<!-- show password icon --><span class="showpw"><i class="far fa-eye"></i></span>--}}
                    <!--/ show password  icon -->
                    </div>

                    
                    <div class="form-group text-center my-1">
                        <input class="btn orange-btn w-100 mt-2" type="submit" value="SIGN UP"></div>
                    <p class="text-center">Already have an account ? <a href="{{route('userlogin')}}" class="fgreen">Sign
                            in</a></p>
                </form>


                {{--<form class="pt-4">--}}
                {{--<div class="form-group">--}}
                {{--<label>First Name <span class="mand">*</span></label>--}}
                {{--<input type="text" placeholder="Enter Your First Name" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label>Last Name</label>--}}
                {{--<input type="text" placeholder="Enter Your Last Name" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label>Email Address<span class="mand">*</span></label>--}}
                {{--<input type="text" placeholder="Enter Your Email Address" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label>Enter Password<span class="mand">*</span></label>--}}
                {{--<input type="password" placeholder="Create Password" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group mb-0">--}}
                {{--<label>Confirm Password<span class="mand">*</span></label>--}}
                {{--<input type="password" placeholder="Confirm Password" class="form-control">--}}
                {{--</div>--}}
                {{--<input type="submit" value="Register With us" class="btn w-100 my-3">--}}
                {{--<p class="text-center">Already have an account ? <a href="login.php" class="fgreen">Sign in</a></p>--}}
                {{--</form>--}}
            </div>
        </div>
        <!--/ div login -->
    </main>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#email").focus(function(){
      $("#email_error").hide();
  });
   $("#password").focus(function(){
      $("#password_error").hide();
  });
  $("#mobileNo").focus(function(){
      $("#mobileNo_error").hide();
  });
  $("#name").focus(function(){
      $("#name_error").hide();
  });
});
</script>
