@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'blog_news'=>'Blogs News',
               ''=>'Add New'
               ),'Add New Blog News'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="blog_articales" action="{{ route('addNewBlogArticales') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $id }}">


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="blog_article_name" type="text"
                                               placeholder="Title"
                                               name="blog_article_name"
                                               value="{{ !empty(old('blog_article_name')) ? old('blog_article_name') : ((($blog_articales) && ($blog_articales->blog_article_name)) ? $blog_articales->blog_article_name : '') }}">
                                        @if ($errors->has('blog_article_name'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_article_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Download Pdf</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($blog_articales) && ($blog_articales->blog_article_pdf)) ? '' : 'blog_article_pdf'}}"
                                               id="blogimage" type="file" name="blog_article_pdf"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('blog_article_pdf'))
                                            <span class="text-danger">{{ $errors->first('blog_article_pdf') }}</span>
                                        @endif

                                        @if(($blog_articales) && ($blog_articales->blog_article_pdf))
                                            <div class="imagebox imagediv{{ $blog_articales->id }}">
                                                <a width="150" target="__blank" href="{{ url('theme/uploads/blogs_articles/').'/'.$blog_articales->blog_article_pdf }}"/>
                                                View Pdf
                                                </a>
                                                
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea required name="blog_article_desc" id="blog_article_desc" rows="9"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{{ !empty(old('blog_article_desc')) ? old('blog_news_des') : ((($blog_articales) && ($blog_articales->blog_article_desc)) ? $blog_articales->blog_article_desc : '') }}</textarea>
                                        @if ($errors->has('blog_article_desc'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_article_desc') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="meta_title" type="text"
                                               placeholder="Title"
                                               name="meta_title"
                                               value="{{ !empty(old('meta_title')) ? old('meta_title') : ((($blog_articales) && ($blog_articales->meta_title)) ? $blog_articales->meta_title : '') }}">
                                        @if ($errors->has('meta_title'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_title') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Keywords</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="meta_keywords" type="text"
                                               placeholder="Title"
                                               name="meta_keywords"
                                               value="{{ !empty(old('meta_keywords')) ? old('meta_keywords') : ((($blog_articales) && ($blog_articales->meta_keywords)) ? $blog_articales->meta_keywords : '') }}">
                                        @if ($errors->has('meta_keywords'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_keywords') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="meta_description" type="text"
                                               placeholder="Title"
                                               name="meta_description"
                                               value="{{ !empty(old('meta_description')) ? old('meta_description') : ((($blog_articales) && ($blog_articales->meta_description)) ? $blog_articales->meta_description : '') }}">
                                        @if ($errors->has('meta_description'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_description') }}</span>
                                        @endif
                                    </div>
                                </div>
                          
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.blog_news.script')

@endsection