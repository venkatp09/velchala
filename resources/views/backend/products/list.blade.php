@extends('backend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')
    @if(app('request')->input('search_category')!='')
        <?php
//        dd(array_first($products)->getCategory->pub_name);
?>
        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin_books')}}">Books</a>
            </li>
            <li class="breadcrumb-item">
               {{array_first($products)->getCategory->pub_name}}
            </li>
        </ul>
    @else
    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Books'
              ),'Books'
           ) !!}
    @endif
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <form method="GET" action="{{ route('admin_books') }}" accept-charset="UTF-8"
                          class="navbar-form navbar-right" role="search" autocomplete="off">
                        <input type="hidden" name="search" value="search">

                        <div class="row">
                            &nbsp;&nbsp;
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="book_name"
                                       placeholder="Enter Book Name to Search..."
                                       value="{{ request('book_name') }}">
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" name="publications_status" id="publications_status">
                                    <option value="">Select Status</option>
                                    <option value="0" {{ (app('request')->input('publications_status')=='0')  ? 'selected' : ''}}>
                                        Inactive
                                    </option>
                                    <option value="1" {{ (app('request')->input('publications_status')=='1')  ? 'selected' : ''}}>
                                        Active
                                    </option>
                                    
                                </select>
                            </div>
                            <div class="col-md-1">
                                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>

                    </form>

                    <br/>
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>
                            <a href="{{route('addNewProducts')}}" class="btn btn-primary" style="float:right;">+ Add
                                Products
                            </a>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Sku  Id</th>
                                    <th>Book Picture</th>
                                    <th>Book Name</th>
                                    <th>Publisher Name</th>
                                    <th>Approval Status</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=0 ?>
                                @if(count($products) > 0) 
                                    @foreach($products AS $product_item)
                                        <tr>
                                           
                                            <th scope="row">{{ $loop->iteration+$s_no }}</th>

                                            <td>
                                                VLC{{ $product_item->b_id }}
                                            </td>
                                            
                                            <td>

                                                <a href="{{route('AdminProductView',['id'=>$product_item->b_id])}}">
                                                  @if($product_item->img=="")
                                                    <img width="50"
                                                         src="https://dummyimage.com/600x400/f2f2f2/000000"
                                                         alt=""
                                                         class="img-thumb">
                                                    @else
                                                    <img width="50" class="img-thumb" src="{{url('theme/uploads/publications').'/'.$product_item->img}}">
                                                    @endif
                                                </a>
                                            </td>

                                            <td>
                                                <a href="{{route('AdminProductView',['id'=>$product_item->b_id])}}">
                                                    {{$product_item->publication_name}}
                                                </a>
                                            </td>
                                            <td>

                                                <?php
                                                $categorieData = getCategory($product_item->publisher_id);
                                                ?>

                                                {{$categorieData->pub_name}}
                                            </td>
                                            <td>
                                                @if($product_item->publications_status==1)
                                                Active
                                                @else
                                                Inactive
                                                @endif
                                            </td>
                                            
                                            <td class="options">
                                               <a href="{{route('AdminProductView',['id'=>$product_item->b_id])}}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-eye"></i> View
                                                </a>
                                <a class="dropdown-item"
                                    href="{{ route('addNewProducts',['id'=>$product_item->b_id])}}"><i
                                    class="fa fa-pencil"></i> Edit</a>
                                                {{--<a class="dropdown-item"--}}
                                                {{--href="">--}}
                                                {{--<i class="fa fa-eye"></i>--}}
                                                {{--View--}}
                                                {{--</a>--}}
                                                <form method="POST" name="change_status" id="productbox_form_{{ $product_item->b_id }}"
                                                      action="{{ route('admin_books') }}"
                                                      accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="b_id"
                                                           value="{{ $product_item->b_id }}"/>
                                                    <input type="hidden" name="status"
                                                           value="{{ $product_item->publications_status }}"/>
                                                    <button type="submit" name="Publish" class="dropdown-item"
                                                            title="Publish Book"
                                                            onclick="return confirm(&quot;Confirm Publish?&quot;)">
                                                      <i class="fa fa-eye"></i> @if($product_item->publications_status==1)
                                                Un Publish
                                                @else
                                                Publish
                                                @endif
                                                    </button>

                                                </form>
                                                <form method="POST" name="home_pageq" id="productbox_form_{{ $product_item->b_id }}"
                                                      action="{{ route('admin_books') }}"
                                                      accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="b_id"
                                                           value="{{ $product_item->b_id }}"/>
                                                    <input type="hidden" name="home_page"
                                                           value="{{ $product_item->home_page }}"/>
                                                    <button type="submit" name="favorite" class="dropdown-item"
                                                            title="Add in Home Page"
                                                            onclick="return confirm(&quot;Confirm Add in Home Page?&quot;)">
                                                      <i class="fa fa-eye"></i>
                                               @if($product_item->home_page==1)
                                                Remove Book in Home Page
                                                @else
                                                Added Book in Home Page
                                                @endif
                                                    </button>

                                                </form>
                                                <div id="productDetails{{ $product_item->b_id }}"
                                                     class="modal fade productDetails"
                                                     role="dialog">
                                                    <div class="modal-dialog modal-xl">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">

                                                                    <a href="{{route('AdminProductView',['id'=>$product_item->b_id])}}">
                                                                         @if($product_item->img=="")
                                                    <img width="50"
                                                         src="https://dummyimage.com/600x400/f2f2f2/000000"
                                                         alt=""
                                                         class="img-thumb">
                                                    @else
                                                    <img width="50" class="img-thumb" src="{{url('theme/uploads/publications').'/'.$product_item->img}}">
                                                    @endif
                                                                    </a>

                                                                    {{ $product_item->publication_name     }}

                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">


                                                                <form method="POST"
                                                                      id="productdetails_box_{{ $product_item->b_id }}"
                                                                      action=""
                                                                      accept-charset="UTF-8"
                                                                      class="form-horizontal productApprove"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="p_id"
                                                                           value="{{$product_item->p_id}}"/>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <table class="table table-bordered">

                                                                                <tbody>

                                                                                <?php
                                                                                dump($product_item->productSKUs)
                                                                                ?>
                                                                                </tbody>
                                                                            </table>


                                                                        </div>


                                                                        
                                                                        <div class="col-md-6">


                                                                            <div class="card">
                                                                                <div class="card-header">
                                                                                    Product Status
                                                                                </div>
                                                                                <div class="card-body">
                                                                                    <div class="form-group">

                                                                                        <div class="input-group">
                                                                                            <select class="p_status form-control"
                                                                                                    required
                                                                                                    name="p_status">
                                                                                                <option value="">
                                                                                                    --Select
                                                                                                    Status--
                                                                                                </option>
                                                                                                <option value="1" {{ (isset($product_item->publications_status) && $product_item->publications_status == '1' ) ? 'selected' : ''}}>
                                                                                                   Active
                                                                                                </option>
                                                                                                <option value="2" {{ (isset($product_item->publications_status) && $product_item->publications_status == '2' ) ? 'selected' : ''}}>
                                                                                                    Inactive
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </div>

                                                                    <div class="row text-center">
                                                                        <div class="col-md-12">
                                                                            <button type="submit"
                                                                                    class="btn btn-success">
                                                                                Submit
                                                                            </button>
                                                                        </div>

                                                                    </div>


                                                                </form>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                                <form method="POST" id="productbox_form_{{ $product_item->b_id }}"
                                                      action="{{ route('admin_books') }}"
                                                      accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="b_id"
                                                           value="{{ $product_item->b_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Product"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>


                                            </td>
                                        </tr>



                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="9" align="center">

                                            No results

                                        </td>

                                    </tr>

                                @endif

                                </tbody>
                            </table>
                             <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $products->firstItem() }}
                        to {{ $products->lastItem() }} of {{ $products->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $products->appends(['search' => Request::get('search'),'product_name' => Request::get('product_name'),'search_category' => Request::get('search_category')])->render() !!}
                    </div>
                </div>
            </div>
                        </div>
                    </div>
                </div>

            </div>
            
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')
    {!! javascriptFunctions() !!}
    <script>
        $(function () {

// Vendor calc
            $('.vdc_vendor_price, .vdc_commission').on("input", function () {

                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_vendor_price', '.vdc_commission_type', '.vdc_commission', '.vdc_store_price', 'add');
            });

            $('.vdc_commission_type').on('change', function () {

                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_vendor_price', '.vdc_commission_type', '.vdc_commission', '.vdc_store_price', 'add');
            });

            // store calc

            $('.vdc_store_price, .vdc_store_discount').on("input", function () {
                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_store_price', '.vdc_store_discount_type', '.vdc_store_discount', '.vdc_store_discount_price');
            });

            $('.vdc_store_discount_type').on('change', function () {
                var productDetails = $(this).closest('tr').attr('id');
                discountCalcWithParent('#' + productDetails, '.vdc_store_price', '.vdc_store_discount_type', '.vdc_store_discount', '.vdc_store_discount_price');
            });


            $('.productdetails_box_form tr').each(function () {

                var productDetails = $(this).attr('id');

                discountCalcWithParent('#' + productDetails, ' .vdc_vendor_price', ' .vdc_commission_type', ' .vdc_commission', ' .vdc_store_price', 'add');

                discountCalcWithParent('#' + productDetails, ' .vdc_store_price', ' .vdc_store_discount_type', ' .vdc_store_discount', ' .vdc_store_discount_price');
            });


            $('.productApprove').each(function () {

                $(this).validate({
                    ignore: [],
                    errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.text-danger').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.text-danger').remove();
                    },
                });
            })


            function validateSKUApruve() {

                $('.vdc_commission_type').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Select commission type",
                        }
                    });
                });

                $('.vdc_commission').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Enter commission Value",
                        }
                    });
                });
                $('.vdc_store_discount_type').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Select discount type ",
                        }
                    });
                });
                $('.vdc_store_discount').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Enter discount Value",
                        }
                    });
                });


            }

            validateSKUApruve()

        });
    </script>


@endsection