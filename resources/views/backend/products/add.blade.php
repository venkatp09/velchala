@extends('backend.layout')
@section('title', $title)

@section('headerStyles')
@endsection

@section('content')

    @if ($product_id!='')
        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin_books')}}">Products</a>
            </li>
            @if(isset($product->getCategory->pub_name))
                <li class="breadcrumb-item">
                    <a href="{{url('admin/products?search=search&product_name=&search_category='.$product->getCategory->category_id.'&search_vendor=&p_status=')}}">{{$product->getCategory->pub_name}}</a>
                </li>
            @endif
            <li class="breadcrumb-item">
                <a href="javascript:void(0)">{{$product->publication_name}}</a>
            </li>


        </ul>
    @else
        {!! getBreadcrumbs(
                     array(
                     'dashboard'=>'Home',
                     'admin_books'=>'Products',
                     ''=>'Add New'
                     ),'Add New Product'
                  ) !!}
    @endif
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    {{--                    @include('backend.products.nav')--}}
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="card">


                                <div class="card-body card-block">

                                    <?php
                                    //                                    dump($product);
                                    ?>

                                    <form method="POST" id="adProducts" action="{{ route('addNewProducts') }}"
                                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="b_id" value="{{ !empty(old('b_id')) ? old('b_id') : ((($product) && ($product->b_id)) ? $product->b_id : '') }}">


                                        {{--<input type="hidden"--}}
                                        {{--class="edit_current_product_alias"--}}
                                        {{--value="{{ (isset($product->b_id)) ? $product->b_id : old('b_id')}}"/>--}}

                                        <div class="row">
                                            <!-- col -->
                                            <div class="col-lg-6 col-md-6 ">
                                                <div class="card">
                                                    <div class="card-header">
                                                        Product Details
                                                    </div>
                                                    <div class="card-body">
                                                     <div class="row">

                                                            <div class="col-lg-4">

                                                                <div class="form-group">

                                                                    <label for="hf-email"
                                                                           class="form-control-label">Category</label>


                                        <select name="publisher_id" id="publisher_id"
                                                class="form-control">

                                            <option  value="">Select</option>
                                            @foreach($categories as $val)
                                            <option option  {{ (!empty(old('publisher_id')) &&old('publisher_id')==$val->id)  ? 'selected' : ((($product) && ($product->publisher_id == $val->id)) ? 'selected' : '') }} value="{{$val->id}}">{{$val->pub_name}}</option>
                                            @endforeach
                                            
                                        </select>
                                        @if ($errors->has('p_cat_id'))
                                            <span class="text-danger help-block">{{ $errors->first('p_cat_id') }}</span>
                                        @endif

                                                                </div>
                                                            </div>

                                                         
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Product
                                                                        name</label>
                                                                    <input class="form-control titleCreateAlias"
                                                                           id="publication_name"
                                                                           type="text"
                                                                           placeholder="Product name"
                                                                           name="publication_name"
                                                                           value="{{ !empty(old('publication_name')) ? old('publication_name') : ((($product) && ($product->publication_name)) ? $product->publication_name : '') }}">
                                                                    @if ($errors->has('publication_name'))
                                                                        <span class="text-danger help-block">{{ $errors->first('publication_name') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Book Pages</label>

                                                                    <input onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control titleCreateAlias"
                                                                           id="pages"
                                                                           type="text"
                                                                           placeholder="Book Pages"
                                                                           name="pages"
                                                                           value="{{ !empty(old('pages')) ? old('pages') : ((($product) && ($product->pages)) ? $product->pages : '') }}">
                                                                    @if ($errors->has('pages'))
                                                                        <span class="text-danger help-block">{{ $errors->first('pages') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Author Name</label>

                                                                    <input class="form-control titleCreateAlias"
                                                                           id="author"
                                                                           type="text"
                                                                           placeholder="author Name"
                                                                           name="author"
                                                                           value="{{ !empty(old('author')) ? old('author') : ((($product) && ($product->author)) ? $product->author : '') }}">
                                                                    @if ($errors->has('author'))
                                                                        <span class="text-danger help-block">{{ $errors->first('author') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Price</label>

                                                                    <input onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control titleCreateAlias"
                                                                           id="price"
                                                                           type="text"
                                                                           placeholder="Price"
                                                                           name="price"
                                                                           value="{{ !empty(old('price')) ? old('price') : ((($product) && ($product->price)) ? $product->price : '') }}">
                                                                    @if ($errors->has('price'))
                                                                        <span class="text-danger help-block">{{ $errors->first('price') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Discount Price</label>

                                                                    <input onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" class="form-control titleCreateAlias"
                                                                           id="dis_price"
                                                                           type="text"
                                                                           placeholder="Discount Price"
                                                                           name="dis_price"
                                                                           value="{{ !empty(old('dis_price')) ? old('dis_price') : ((($product) && ($product->dis_price)) ? $product->dis_price : '') }}">
                                                                    @if ($errors->has('dis_price'))
                                                                        <span class="text-danger help-block">{{ $errors->first('dis_price') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                         <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                @if($product)
                                                                   <img width="50" class="img-thumb" src="{{url('theme/uploads/publications').'/'.$product->img}}">
                                                                   <input type="hidden" name="old_img" value="{{$product->img}}">
                                                                   @endif
                                                                    <label for="hf-email" class=" form-control-label">
                                                                        Book Profile Image</label>
                                                                    
                                                                    <input class="form-control titleCreateAlias"
                                                                           id="img"
                                                                           type="file"
                                                                           placeholder="Image name"
                                                                           name="img"
                                                                           value="{{ !empty(old('img')) ? old('img') : ((($product) && ($product->img)) ? $product->img : '') }}">
                                                                    @if ($errors->has('img'))
                                                                        <span class="text-danger help-block">{{ $errors->first('img') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                          <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                @if($product)
                                                                   <a width="50" target="__blank" class="img-thumb" href="{{url('theme/uploads/publications').'/'.$product->pdf_file}}">View Book</a>
                                                                   <input type="hidden" name="old_pdf_file" value="{{$product->pdf_file}}">
                                                                   @endif
                                                                    <label for="hf-email" class=" form-control-label">
                                                                        Book Pdf</label>
                                                                    
                                                                    <input class="form-control titleCreateAlias"
                                                                           id="pdf_file"
                                                                           type="file"
                                                                           placeholder="Image name"
                                                                           name="pdf_file"
                                                                           value="{{ !empty(old('pdf_file')) ? old('pdf_file') : ((($product) && ($product->pdf_file)) ? $product->pdf_file : '') }}">
                                                                    @if ($errors->has('pdf_file'))
                                                                        <span class="text-danger help-block">{{ $errors->first('pdf_file') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">

                                                                <div class="form-group">

                                                                    <label for="hf-email"
                                                                           class="form-control-label">Language</label>


                                                                    <select name="language" id="language"
                                                                            class="form-control">
                                                                        <option value="">Select</option>
                                                                        <option {{ (!empty(old('language')) &&old('language')=='Telugu')  ? 'selected' : ((($product) && ($product->language =='Telugu')) ? 'selected' : '') }} value="Telugu">Telugu</option>
                                                                        <option {{ (!empty(old('language')) &&old('language')=='English')  ? 'selected' : ((($product) && ($product->language =='English')) ? 'selected' : '') }} value="English">English</option>
                                                                        <option {{ (!empty(old('language')) &&old('language')=='Urdu')  ? 'selected' : ((($product) && ($product->language =='Urdu')) ? 'selected' : '') }} value="Urdu">Urdu</option>
                                                                       
                                                                    </select>
                                                                    @if ($errors->has('language'))
                                                                        <span class="text-danger help-block">{{ $errors->first('language') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                       
                                                        <!-- row -->
                                                        <div class="row">

                                                            <div class="col-lg-6">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Product
                                                                        Availability</label>


                                                                    <select name="stock" id="stock"
                                                                            class="form-control">
                                                                        <?php
                                                                        $availability = availability(); ?>
                                                                        @foreach($availability as $ks=>$s)
                                                                            <option value="{{ $ks }}" {{ (!empty(old('stock')) && old('stock')==$ks)  ? 'selected' : ((($product) && ($product->stock == $ks)) ? 'selected' : '') }}>{{ $s }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('stock'))
                                                                        <span class="text-danger help-block">{{ $errors->first('stock') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                            <!--/ col -->


                                                        </div>


                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="form-control-label">Product Overview</label>--}}
                                                        {{--<!-- editor-->--}}
                                                        {{--<textarea id="txtEditor"></textarea>--}}
                                                        {{--<!-- editor-->--}}
                                                        {{--</div>--}}


                                                    </div>
                                                </div>


                                                <div class="card">
                                                    <div class="card-header">
                                                        Video Url
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">

                                                            <table class="table table-bordered" id="dynamicTable">
                                                                <tr>

                                                                    <th>Video Name</th>
                                                                    
                                                                </tr>

                                                                <?php
                                                                $j = 0;
                                                                ?>
                                                            
                                                            @if(count($product_videos)>0)
                                                                @foreach($product_videos as $val)
                                                                    <?php
                                                                    $j = $loop->iteration;
                                                                    ?>

                                                                    <tr id="id{{ $j }}">
                                                                        
                                                                        <td class="form-group">
                                                                            <input type="text"
                                                                                    name="book_videos[]"
                                                                                    placeholder="Enter Video"
                                                                                    class="form-control sku_option"
                                                                                    value="{{$val->video_url}}"/>
                                                                         <iframe width="200" height="200" src="https://www.youtube.com/embed/{{$value->video_url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>   
                                                                        </td>
                                                                         <td class="form-group">
                                                                            <input type="text"
                                                                                    name="book_videos_name[]"
                                                                                    placeholder="Enter Video"
                                                                                    class="form-control sku_option"
                                                                                    value="{{$val->poem_name}}"/>
                                                                            
                                                                        </td>

                                                                        <td>
                                                                            @if($j==1)
                                                                                *
                                                                            @else
                                                                                <button type="button"
                                                                                        class="btn btn-danger remove-tr">Remove
                                                                                </button>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                

                                                            </table>

                                                            <div class="col-md-12 text-right">
                                                                <button type="button" name="add" id="add"
                                                                        class="btn btn-success">Add New
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-6 col-md-6">
                                                <!-- card -->
                                                <div class="card">
                                                    <!-- card body -->
                                                    <div class="card-header">
                                                        Product Options
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product
                                                                Specifications</label>

                                                            <div class="input-group">
                                                <textarea name="description" id="description" rows="5"
                                                          placeholder="Product Specifications..."
                                                          class="form-control ckeditor">{!! !empty(old('description')) ? old('description') : ((($product) && ($product->description)) ? $product->description : '') !!}</textarea>
                                                                @if ($errors->has('description'))
                                                                    <span class="text-danger help-block">{{ $errors->first('description') }}</span>
                                                                @endif

                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product Meta Title</label>


                                                            <input class="form-control"
                                                                   id="meta_title"
                                                                   type="text"
                                                                   placeholder="Product Meta Title"
                                                                   name="meta_title"
                                                                   value="{{ !empty(old('meta_title')) ? old('meta_title') : ((($product) && ($product->meta_title)) ? $product->meta_title : '') }}">


                                                            @if ($errors->has('meta_title'))
                                                                <span class="text-danger help-block">{{ $errors->first('meta_title') }}</span>
                                                            @endif

                                                        </div>


                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product Meta Keywords</label>


                                                            <input class="form-control"
                                                                   id="meta_keywords"
                                                                   type="text"
                                                                   placeholder="Product Meta Keywords"
                                                                   name="meta_keywords"
                                                                   value="{{ !empty(old('meta_keywords')) ? old('meta_keywords') : ((($product) && ($product->meta_keywords)) ? $product->meta_keywords : '') }}">


                                                            @if ($errors->has('meta_keywords'))
                                                                <span class="text-danger help-block">{{ $errors->first('meta_keywords') }}</span>
                                                            @endif

                                                        </div>
                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product Meta Description</label>


                                                            <input class="form-control"
                                                                   id="meta_description"
                                                                   type="text"
                                                                   placeholder="Product Meta Description"
                                                                   name="meta_description"
                                                                   value="{{ !empty(old('meta_description')) ? old('meta_description') : ((($product) && ($product->meta_description)) ? $product->meta_description : '') }}">


                                                            @if ($errors->has('meta_description'))
                                                                <span class="text-danger help-block">{{ $errors->first('meta_description') }}</span>
                                                            @endif

                                                        </div>
                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Book Publish Date</label>


                                                            <input class="form-control"
                                                                   id="published_date"
                                                                   type="date"
                                                                  
                                                                   name="published_date"
                                                                   value="{{ !empty(old('published_date')) ? old('published_date') : ((($product) && ($product->published_date)) ? $product->published_date : '') }}">


                                                            @if ($errors->has('published_date'))
                                                                <span class="text-danger help-block">{{ $errors->first('published_date') }}</span>
                                                            @endif

                                                        </div>

                                 


                                                        <div clas="col-lg-12 py-4">


                                                            <div class="row">


                                                                <div class="col-lg-4">

                                                                </div>

                                                                <div class="col-lg-8 text-right">
                                                                    <br/>
                                                                    <button type="submit" class="btn btn-success">
                                                                        @if ($product_id!='')
                                                                            <i class="fa fa-dot-circle-o"></i>
                                                                            Update  Now
                                                                        @else
                                                                            <i class="fa fa-dot-circle-o"></i> Add
                                                                            Product
                                                                            Now
                                                                        @endif
                                                                    </button>
                                                                </div>
                                                            </div>


                                                        </div>


                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </form>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    {{--    <script src="{{url('backend/modules/products/js/products.js')}}"></script>--}}



    @include('backend.products.scripts')






@endsection