@extends('backend.layout')
@section('title', $title)


@section('content')


    <div class="content-wrapper">

        <div class="col-lg-12 col-md-2">
            <div class="white-box">

                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">


                    <div class="col-lg-9">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Manage Subscriptions</h4>

                                <table id="bannerslist" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Emails</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($subscriptions)>0)
                                    @foreach($subscriptions as $subscriptionsInfo)
                                        <tr>
                                            <td>{{ $subscriptionsInfo->id }}</td>
                                            <td>{{ $subscriptionsInfo->email }}</td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr style="text-align: center">
                                            <td colspan="10">No Result Found</td>
                                        </tr>
                                    @endif
                                    </tbody>

                                </table>
                                <div class="pagination-wrapper"
                                     style="float: right;"> {!! $subscriptions->appends(['search' => Request::get('search')])->render() !!} </div>




                            </div>

                        </div>
                    </div>



                </div>


            </div>
        </div>


    </div>



@endsection

@section('footerScripts')

@endsection





