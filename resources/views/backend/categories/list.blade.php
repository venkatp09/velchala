@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')


    

        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="">Categories</a>
            </li>


        </ul>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif



                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Categories</strong>
                            <a href="{{route('addNewCategories')}}" class="btn btn-primary" style="float:right;">+ Add
                                Categories
                            </a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Publisher Name</th>
                                    
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($categories)>0)
                                    @foreach($categories as $item)
                        <tr>
                            <th scope="row">{{ $item->id }}</th>
                            <td>{{ $item->pub_name }}</td>
<td> <a class="dropdown-item"
               href="{{ route('addNewCategories',['id'=>$item->id]) }}"><i
                        class="fa fa-pencil"></i> Edit</a><button type="submit" class="dropdown-item"
                                title="Delete Category"
onclick="return confirm(&quot;Confirm delete?&quot;)">
                           
                            </button></td>

                            </div>
                                </div>
                            </td>
                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" align="center">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $categories->firstItem() }}
                        to {{ $categories->lastItem() }} of {{ $categories->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $categories->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('category_status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

@endsection