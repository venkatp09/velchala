@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    {{--    //@if($category->category_parent_id!=0)--}}
    @if(isset($category_id))


        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin_publishers')}}">Publishers</a>
            </li>
        </ul>


    @else
        {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'categories'=>'Categories',
               ''=>'Add New'
               ),'Add New Category'
            ) !!}

    @endif



    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="categories" action="{{ route('addNewCategories') }}"
                                  accept-charset="UTF-8" class="form-horizontal categoriesValid"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="category_id" value="{{ $category_id }}">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="hf-email" class=" form-control-label">Publisher Name</label>
                    </div>
                    <div class="col-12 col-md-9">

                        <input class="form-control titleCreateAlias" id="pub_name" type="text"
                               placeholder="Publishers Name"
                               name="pub_name"
                               value="{{ !empty(old('pub_name')) ? old('pub_name') : ((($category) && ($category->pub_name)) ? $category->pub_name : '') }}">
                        @if ($errors->has('pub_name'))
                            <span class="text-danger help-block">{{ $errors->first('pub_name') }}</span>
                        @endif
                    </div>
                </div>

               


                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="hf-email" class=" form-control-label">Status</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="pub_status" id="pub_status" class="form-control">
                          <?php   $status = allStatuses('general'); ?>
                            @foreach($status as $ks=>$s)
                            <option value="{{ $ks }}" {{ (!empty(old('pub_status')) &&old('pub_status')==$ks)  ? 'selected' : ((($category) && ($category->pub_status == $ks)) ? 'selected' : '') }}
                            >
                                {{ $s }}
                            </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('pub_status'))
                                            <span class="text-danger help-block">{{ $errors->first('category_status') }}</span>
                                        @endif
                            
                        </select>
                        @if ($errors->has('pub_status'))
                            <span class="text-danger help-block">{{ $errors->first('pub_status') }}</span>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Submit
                    </button>
                    <button type="reset" class="btn btn-danger">
                        Reset
                    </button>
                </div>
            </form>
        </div>

                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

 

@endsection