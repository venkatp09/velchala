@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'blogs'=>'Blogs',
               ''=>'Add New'
               ),'Add New Blog'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="blogs" action="{{ route('addNewBlogs') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="blog_id" value="{{ $blog_id }}">


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="blog_title" type="text"
                                               placeholder="Event Title"
                                               name="blog_title"
                                               value="{{ !empty(old('blog_title')) ? old('blog_title') : ((($blog) && ($blog->blog_title)) ? $blog->blog_title : '') }}">
                                        @if ($errors->has('blog_title'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_title') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($blog) && ($blog->blog_image)) ? '' : 'blogimage'}}"
                                               id="blogimage" type="file" name="blog_image"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('blog_image'))
                                            <span class="text-danger">{{ $errors->first('blog_image') }}</span>
                                        @endif

                                        @if(($blog) && ($blog->blog_image))
                                            <div class="imagebox imagediv{{ $blog->blog_id }}">
                                                <img width="150" src="{{ url('theme/uploads/blogs/').'/'.$blog->blog_image }}"/>
                                                
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Blog Location</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required required class="form-control" id="blog_location" type="text"
                                               placeholder="Blog location"
                                               name="blog_location"
                                               value="{{ !empty(old('blog_location')) ? old('blog_location') : ((($blog) && ($blog->blog_location)) ? $blog->blog_location : '') }}">
                                        @if ($errors->has('blog_location'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_location') }}</span>
                                        @endif
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Blog Date</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="event_date" type="date"
                                               placeholder="Date"
                                               name="event_date"
                                               value="{{ !empty(old('event_date')) ? old('event_date') : ((($blog) && ($blog->event_date)) ? $blog->event_date : '') }}">
                                        @if ($errors->has('event_date'))
                                            <span class="text-danger help-block">{{ $errors->first('event_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="blog_description" id="blog_description" rows="9"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{{ !empty(old('blog_description')) ? old('blog_description') : ((($blog) && ($blog->blog_description)) ? $blog->blog_description : '') }}</textarea>
                                        @if ($errors->has('blog_description'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_description') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="meta_title" type="text"
                                               placeholder="Meta Title"
                                               name="meta_title"
                                               value="{{ !empty(old('meta_title')) ? old('meta_title') : ((($blog) && ($blog->meta_title)) ? $blog->meta_title : '') }}">
                                        @if ($errors->has('meta_title'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_title') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Keywords</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="meta_keywords" type="text"
                                               placeholder="Meta Keywords"
                                               name="meta_keywords"
                                               value="{{ !empty(old('meta_keywords')) ? old('meta_keywords') : ((($blog) && ($blog->meta_keywords)) ? $blog->meta_keywords : '') }}">
                                        @if ($errors->has('meta_keywords'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_keywords') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="meta_description" type="text"
                                               placeholder="Meta Description"
                                               name="meta_description"
                                               value="{{ !empty(old('meta_description')) ? old('meta_description') : ((($blog) && ($blog->meta_description)) ? $blog->meta_description : '') }}">
                                        @if ($errors->has('meta_description'))
                                            <span class="text-danger help-block">{{ $errors->first('meta_description') }}</span>
                                        @endif
                                    </div>
                                </div>
                               
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Gallery</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                         <input  type="file" name="gallery_images[]" multiple class="form-control" id="gallery_images" 
                                               placeholder="Title">
                                        @if ($errors->has('gallery_images'))
                                            <span class="text-danger help-block">{{ $errors->first('gallery_images') }}</span>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="imagebox imagediv">
                                 @if($event_images)  
                                 @foreach($event_images as $val)

                                           
                                                <img width="150" src="{{ url('theme/uploads/photos/').'/'.$val->img }}"/>
                                                <input type="hidden" name="gallery_images_old[]" value="{{$val->img }}"/>
                                                <a  data-id="{{$val->id}}" class="btn btn-danger remove_img">Remove</a>
                                     @endforeach @endif</div>
                                <div class="card">
                                                    <div class="card-header">
                                                        Video Url
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">

                                                            <table class="table table-bordered" id="dynamicTable">
                                                                <tr>

                                                                    <th>Video</th>
                                                                    <th>Video Name</th>
                                                                    
                                                                </tr>

                                                                <?php
                                                                $j = 0;
                                                                ?>
                                                            
                                                            @if($event_videos)
                                                                @foreach($event_videos as $val)
                                                                    <?php
                                                                    $j = $loop->iteration;
                                                                    ?>

                                                                    <tr id="id{{ $j }}">
                                                                        
                                                                        <td class="form-group">
                                                                            <input type="text"
                                                                                    name="book_videos[]"
                                                                                    placeholder="Enter Video"
                                                                                    class="form-control sku_option"
                                                                                    value="{{$val->video_url}}"/>
                                                                         <iframe width="200" height="200" src="https://www.youtube.com/embed/{{$val->video_url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>   
                                                                        </td>
                                                                         <td class="form-group">
                                                                            <input type="text"
                                                                                    name="book_videos_name[]"
                                                                                    placeholder="Enter Video"
                                                                                    class="form-control sku_option"
                                                                                    value="{{$val->poem_name}}"/>
                                                                            
                                                                        </td>

                                                                        <td>
                                                                            @if($j==0)
                                                                                *
                                                                            @else
                                                                                <button type="button"
                                                                                        class="btn btn-danger remove-tr">Remove
                                                                                </button>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                

                                                            </table>

                                                            <div class="col-md-12 text-right">
                                                                <button type="button" name="add" id="add"
                                                                        class="btn btn-success">Add New
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.blogs.script')

@endsection