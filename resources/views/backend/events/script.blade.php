{!! javascriptFunctions() !!}
<script>
     
     i=0;

    $("#add").click(function () {


        
        ++i;

        $("#dynamicTable").append('<tr>' +
            
            '<td class="form-group">' +
            '<input required type="text" name="book_videos[]" placeholder="Ex : https://www.youtube.com/embed/dWFvZl3D3us" class="form-control sku_option" />' +
            '</td>' +

            '<td class="form-group">' +
            '<input required type="text" name="book_videos_name[]" placeholder="Ex : Interviews" class="form-control sku_option" />' +
            '</td>' +
            
            '<td>' +
            '<button type="button" class="btn btn-danger remove-tr">Remove</button>' +
            '</td>' +
            '</tr>');

        
    });
     $(document).on('click', '.remove-tr', function () {

        var confirmMsg = confirm("Are you sure want to remove?");
        if (confirmMsg == true) {

            $(this).parents('tr').remove();
            var id = $(this).closest('tr').prop('id');
            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('seller_remove_options') }}',
                    type: 'POST',
                    data: {'id': id},
                    success: function (response) {
                    }
                });
            }


        } else {
            // txt = "You pressed Cancel!";
        }


    });
    $(".remove_img").click(function() {

        event_img_id = $(this).attr("data-id");
        data = 'event_img_id='+event_img_id+"&_token=<?php echo csrf_token() ?>";
        
    $.ajax({
        type:'POST',
        url:"{{route('delete_img')}}",
        data:data,
        success:function(results) {
            
        alert(results);
            location.reload();
        
        }
     });
  });

   
</script>