@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'admin_interviews'=>'Interviews',
               ''=>'Add New'
               ),'Add New Interviews'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="Interviews" action="{{ route('addNewInterviews') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                               


                                
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Video Url</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                         <input  type="text" name="videos[]" multiple class="form-control" id="videos" 
                                               placeholder="Ex: _QOf0FB9PBs">
                                        @if ($errors->has('videos'))
                                            <span class="text-danger help-block">{{ $errors->first('videos') }}</span>
                                        @endif
                                        
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Video Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                         <input  type="text" name="videos_name[]" multiple class="form-control" id="videos_name" 
                                               placeholder="Ex : HMTV Special Focus On Sahitya Sangamam | Dr Velchala Kondal Rao | Part 3">
                                        @if ($errors->has('videos_name'))
                                            <span class="text-danger help-block">{{ $errors->first('videos_name') }}</span>
                                        @endif
                                        
                                    </div>
                                </div>
                              
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.blogs.script')

@endsection