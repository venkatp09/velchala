<div class="btn-group" role="group" aria-label="Basic example">
    <a href="{{ route('userAddress',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='users-address') active @endif">Address</a>
    <a href="{{ route('userAdminOrders',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='users-orders') active @endif">Orders</a>
    <a href="{{ route('userWishlist',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='users-wishlist') active @endif">Wishlist</a>
</div>



