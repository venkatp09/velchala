@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'adminmagazines'=>'Magazines',
               ''=>'Add New'
               ),'Add New Magazines'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="events" action="{{ route('addNewMagazines') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $id }}">


                                

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Date</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="mag_date" type="text"
                                               placeholder="Ex: Jan-2020 - Mar-2020"
                                               name="mag_date"
                                               value="{{ !empty(old('mag_date')) ? old('mag_date') : ((($magazines) && ($magazines->mag_date)) ? $magazines->mag_date : '') }}">
                                        @if ($errors->has('mag_date'))
                                            <span class="text-danger help-block">{{ $errors->first('mag_date') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Magazine Cover Pic</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input  class="form-control {{ (($magazines) && ($magazines->mag_profile_pic)) ? '' : 'magazinesimage'}}"
                                               id="mag_profile_pic" type="file" name="mag_profile_pic"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('mag_profile_pic'))
                                            <span class="text-danger">{{ $errors->first('mag_profile_pic') }}</span>
                                        @endif

                                        @if(($magazines) && ($magazines->mag_profile_pic))
                                            <div class="imagebox imagediv{{ $magazines->id }}">
                                                <img width="150" src="{{ url('theme/uploads/magazines/').'/'.$magazines->mag_profile_pic }}"/>
                                                
                                            </div>
                                        @endif

                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Magazine</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input  class="form-control {{ (($magazines) && ($magazines->mag_pdf)) ? '' : 'magazinesimage'}}"
                                               id="mag_pdf" type="file" name="mag_pdf"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('mag_pdf'))
                                            <span class="text-danger">{{ $errors->first('mag_pdf') }}</span>
                                        @endif

                                        @if(($magazines) && ($magazines->mag_pdf))
                                            <div class="imagebox imagediv{{ $magazines->id }}">
                                                <a width="150" target="__blank" href="{{ url('theme/uploads/magazines/').'/'.$magazines->mag_pdf }}"/>
                                                View
                                                </a>
                                                
                                            </div>
                                        @endif

                                    </div>
                                </div>
                               
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.events.script')

@endsection