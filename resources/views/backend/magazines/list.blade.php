@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Magazines'
              ),'Magazines'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Events</strong>
                            <a href="{{route('addNewMagazines')}}" class="btn btn-primary" style="float:right;">+ Add
                                Magazines
                            </a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($adminmagazines)>0)
                                    @foreach($adminmagazines as $item)
                                        <tr>
                                            <td>
                                                <img src="{{ url('theme/uploads/magazines/').'/'.$item->mag_profile_pic }}"
                                                     width="100"/>
                                            </td>
                                            <td>{{ $item->mag_date }}</td>
                                            <td>
                                                <a target="__blank" href="{{ url('theme/uploads/magazines/').'/'.$item->mag_pdf }}"
                                                     width="100"/>View Magazine</a>
                                            </td>
                                           
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-outline-primary dropdown-toggle" href="#"
                                                       role="button"
                                                       data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item"
                                                           href="{{ route('addNewMagazines',['id'=>$item->id]) }}"><i
                                                                    class="fa fa-pencil"></i> Edit</a>
                                                        <form method="POST" id="events"
                                                              action="{{ route('adminmagazines') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="id"
                                                                   value="{{ $item->id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Magazine"
                                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i
                                                                        class="fa fa-trash"></i> Delete
                                                            </button>

                                                        </form>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $adminmagazines->firstItem() }}
                        to {{ $adminmagazines->lastItem() }} of {{ $adminmagazines->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $adminmagazines->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

@endsection