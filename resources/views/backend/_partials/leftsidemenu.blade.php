<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                {{--<img src="/admin/images/avatar-7.jpg" alt="person" class="img-fluid rounded-circle"/>--}}
                <a class="navbar-brand" href="{{route('dashboard')}}"><img
                            src="{{ url('theme/img/logo.svg')}}" alt=""></a>
                <h2 class="h5">{!! Auth::guard('admin')->user()->name !!}</h2>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo">
                <a href="{{route('dashboard')}}" class="brand-small text-center">
                    <strong>B</strong>
                    <strong class="text-primary">D</strong>
                </a>
            </div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li>
                    <a href="{{route('home')}}" target="_blank">
                        <i class="icon-screen"> </i>
                        <span> Visit Site </span>
                    </a>
                </li>
                <li class="{{  $active_menu=='dashboard'?'active':'' }}">
                    <a href="{{route('dashboard')}}"> <i class="icon-home"></i> Dashboard </a>
                </li>
                
                <li class="{{  $active_menu=='categories'?'active':'' }}">
                    <a href="{{route('admin_publishers')}}"> 
                    <i class="icon-home"></i>Publishers</a>
                </li>
                <li class="{{  $active_menu=='products'?'active':'' }}">
                    <a href="{{route('admin_books')}}"> <i class="icon-home"></i>Publications</a>
                </li>

                

                <li class="{{  $active_menu=='blogs'?'active':'' }}">
                    <a href="{{route('blogs')}}"> <i class="icon-home"></i>Blogs  Events</a>
                </li>

                <li class="{{  $active_menu=='events'?'active':'' }}">
                    <a href="{{route('events')}}"> <i class="icon-home"></i>Jayanthi Events</a>
                </li>

                <li class="{{  $active_menu=='magazines'?'active':'' }}">
                    <a href="{{route('adminmagazines')}}"> <i class="icon-home"></i> Magazines </a>
                </li>

                <li class="{{  $active_menu=='blog_news'?'active':'' }}">
                    <a href="{{route('admin_blog_news')}}"> <i class="icon-home"></i> Blog News </a>
                </li>

                <li class="{{  $active_menu=='blog_articales'?'active':'' }}">
                    <a href="{{route('admin_blog_articales')}}"> <i class="icon-home"></i> Blog Article </a>
                </li>
                 <li class="{{  $active_menu=='poems'?'active':'' }}">
                    <a href="{{route('admin_poems')}}"> <i class="icon-home"></i>Poems</a>
                </li>
                <li class="{{  $active_menu=='admin_interviews'?'active':'' }}">
                    <a href="{{route('admin_interviews')}}"> <i class="icon-home"></i>Interviews</a>
                </li>
                 <li class="{{  $active_menu=='admin_pincodes'?'active':'' }}">
                    <a href="{{route('admin_pincodes')}}"> <i class="icon-home"></i>Pincodes</a>
                </li>


                <!--<li class="{{  $active_menu=='users'?'active':'' }}"><a href="{{route('users')}}"> <i
                                class="icon-home"></i> Users </a></li>
               
                <li class="{{  $active_menu=='orders'?'active':'' }}"><a href="{{route('adminorders')}}"> <i
                                class="icon-home"></i> Orders </a></li>
               <li class="{{  $active_menu=='reports'?'active':'' }}"><a href="{{route('adminreports')}}"> <i
                                class="icon-home"></i> Reports </a></li>
                <li class="{{  $active_menu=='subscriptions'?'active':'' }}"><a href="{{route('subscriptions')}}"> <i
                                class="icon-home"></i> Subscribers </a></li>
                <li class="{{  $active_menu=='product_enquiries'?'active':'' }}"><a
                            href="{{route('product_enquiries')}}"> <i
                                class="icon-home"></i> Product Enquiries </a></li>
                <li class="{{  $active_menu=='reviews'?'active':'' }}"><a
                            href="{{route('Reviews')}}"> <i
                                class="icon-home"></i> Reviews & Ratings </a></li>-->
                {{--<li>--}}
                {{--<a href="login.html">--}}
                {{--<i class="icon-interface-windows"></i>--}}
                {{--Login page--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="#">--}}
                {{--<i class="icon-mail"></i> Demo--}}
                {{--<div class="badge badge-warning">6 New</div>--}}
                {{--</a>--}}
                {{--</li>--}}
            </ul>
        </div>
        {{--<div class="admin-menu">--}}
        {{--<h5 class="sidenav-heading">Second menu</h5>--}}
        {{--<ul id="side-admin-menu" class="side-menu list-unstyled">--}}
        {{--<li><a href="#"> <i class="icon-screen"> </i>Demo</a></li>--}}
        {{--<li><a href="#"> <i class="icon-flask"> </i>Demo--}}
        {{--<div class="badge badge-info">Special</div>--}}
        {{--</a></li>--}}
        {{--<li><a href=""> <i class="icon-flask"> </i>Demo</a></li>--}}
        {{--<li><a href=""> <i class="icon-picture"> </i>Demo</a></li>--}}
        {{--</ul>--}}
        {{--</div>--}}
    </div>
</nav>