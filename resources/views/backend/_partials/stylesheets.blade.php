<link rel="apple-touch-icon" href="/backend/apple-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="{{ url('theme/img/fav.png')}}">

<link rel="stylesheet" href="/admin/css/style_default.css" id="theme-stylesheet">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css">--}}