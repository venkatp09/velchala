@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'admin_pincodes'=>'Pincodes',
               ''=>'Add New'
               ),'Add New Pincodes'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="pincodes" action="{{ route('addNewPincodes') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $pin_id }}">


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Pincode</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="pincode" type="text"
                                               placeholder="Pincode"
                                               name="pincode"
                                               value="{{ !empty(old('pincode')) ? old('pincode') : ((($pincodes) && ($pincodes->pincode)) ? $pincodes->pincode : '') }}">
                                        @if ($errors->has('pincode'))
                                            <span class="text-danger help-block">{{ $errors->first('pincode') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Location</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input required class="form-control" id="address" type="text"
                                               placeholder="Location"
                                               name="address"
                                               value="{{ !empty(old('address')) ? old('address') : ((($pincodes) && ($pincodes->address)) ? $pincodes->address : '') }}">
                                        @if ($errors->has('address'))
                                            <span class="text-danger help-block">{{ $errors->first('address') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Shipping Price</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" required class="form-control" id="price" type="text"
                                               placeholder="Shipping Price"
                                               name="price"
                                               value="{{ !empty(old('price')) ? old('price') : ((($pincodes) && ($pincodes->price)) ? $pincodes->price : '') }}">
                                        @if ($errors->has('price'))
                                            <span class="text-danger help-block">{{ $errors->first('price') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

@include('backend.pincodes.script')

@endsection