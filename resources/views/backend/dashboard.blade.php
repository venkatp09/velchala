@extends('backend.layout')
@section('title', $title)

@section('headerStyles')
@endsection

@section('footerScripts')


@endsection


@section('content')

    <!-- Counts Section -->
    <section class="dashboard-counts section-padding">
        <div class="container-fluid">
            <div class="row">
                <!-- Count item widget-->

                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-user"></i></div>
                        <div class="name"><strong class="text-uppercase">Active Users</strong>
                            <div class="count-number"><a href="#">{{$userscount}}</a></div>
                        </div>
                    </div>
                </div>

                <!-- Count item widget-->
               
                <!-- Count item widget-->
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-check"></i></div>
                        <div class="name"><strong class="text-uppercase">Active Books</strong>
                            <div class="count-number"><a href="{{route('admin_books')}}">{{$productscount}}</a></div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-bill"></i></div>
                        <div class="name"><strong class="text-uppercase">Total Orders</strong>
                            <div class="count-number"><a href="{{route('adminorders')}}">{{$orderscount}}</a></div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-list"></i></div>
                        <div class="name"><strong class="text-uppercase">Product Enquiries</strong>
                            <div class="count-number"><a href="{{route('product_enquiries')}}">{{$productenquiries}}</a></div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget-->
                
            </div>
        </div>
    </section>



  



@endsection


