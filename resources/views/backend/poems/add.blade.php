@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'blogs'=>'Blogs',
               ''=>'Add New'
               ),'Add New Blog'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="blogs" action="{{ route('addNewPoems') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="blog_id" value="{{ $blog_id }}">


                                
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Gallery</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                         <input  type="file" name="poem_image[]" multiple class="form-control" id="poem_image" 
                                               placeholder="Title">
                                        @if ($errors->has('poem_image'))
                                            <span class="text-danger help-block">{{ $errors->first('poem_image') }}</span>
                                        @endif
                                        
                                    </div>
                                </div>
                              
                                
                                
                              
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.blogs.script')

@endsection