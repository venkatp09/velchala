<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <title>Velchala Kondal Rao</title>
     @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Jayanthi Events</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Jayanthi Events</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

        <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            <!-- col -->
            @if(count($events)!=0)
            @foreach($events as $value)
                <div class="col-sm-6  col-lg-4 wow animate__animated animate__fadeInDown">
                    <div class="card blogcard">
                        <a href="{{ route('jayanthi-events-details',['id'=>$value->event_url]) }}">
                             @if($value->img!="")
                            <img class="card-img-top img-fluid" src="{{ url('theme/uploads/events').'/'.$value->img }}">
                             @else
                       
                        <img class="card-img-top img-fluid" src="theme/uploads/blog_news/dummyblog_news.jpeg">
                        @endif
                        </a>
                        <div class="card-body">
                            <h5 class="card-title ptregular">{{substr(strip_tags($value->event_name),0,40)}} @if(strlen($value->event_name)>40)...@endif </h5>
                           <!-- <p class="card-text pb-3">{!! substr($value->event_des,0,100) !!} @if(strlen($value->event_des)>100)...@endif</p>-->
                            <p>{{$value->event_location}}<span class="d-inline-block px-3 small pb-3">|</span>{{date("d-m-Y",strtotime($value->event_date))}}</p>
                            <a href="{{ route('jayanthi-events-details',['id'=>$value->event_url]) }}" class="btn orange-btn">Read More</a>
                        </div>
                    </div>
                </div>
           @endforeach
           @else 
           <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                <p>Thank you for visit us</p>
            </div>
            
            @endif
            <!--/ col -->  
         </div>
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
           
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
   @stop 
