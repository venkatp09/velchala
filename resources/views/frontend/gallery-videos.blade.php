<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main" >
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Video Gallery</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Gallery</a></li>                    
                        <li class="breadcrumb-item active" aria-current="page"><span>Videos</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body" id="result_video_ajax">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">
                  @if(count($video)>0)
                  @foreach($video as $value) 
                 <!-- col -->
                  <div class="col-sm-6 col-md-4 text-center video-col wow animate__animated animate__fadeInDown">
                    <iframe width="100%"  src="https://www.youtube.com/embed/{{$value->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <p class="text-left">{{$value->poem_name}}</p>
                </div>
                 @endforeach
                 @else
                 <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p class="text-center">Currently We dont have any data you are looking, We will update you Soon, </p>
                <p class="text-center">Thank you for visit us</p>
            </div>
            </div>
            @endif     </div>
                <!--/ col -->
                <?php  $videostotal = \App\Models\Videos::where('cat_type','!=',5)->get(); $videostotal = count($videostotal) ?>
               @if(count($video)>0)
                @if(count($video)<$videostotal)
                <div class="row justify-content-center pb-4">
                <div class="col-lg-4 text-center">
                <a class="orange-btn" data-type="more" data-count-value="{{ count($video) }}" id="loadmore_details" href="javascript:void(0)">Load More</a>
                </div>
                </div>
                @else
                 <div class="row justify-content-center pb-4">
                <div class="col-lg-4 text-center">
                <a class="orange-btn" data-type="less" data-count-value="{{ count($video) }}" id="loadmore_details" href="javascript:void(0)">Load Less</a>
                </div>
                </div>
                @endif
                @endif

               
                <!--/ row -->
            </div>
            <!--/ container -->
       </div>
       <!--/ sub page body -->
       
    </main> 
    <!--/ main-->
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
     $("#loadmore_details").click(function() {

             type1 = $(this).attr("data-type");
             count=$(this).attr("data-count-value");
             data = 'type1='+type1+'&count='+count+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
               type:'POST',
               url:'/get_videos_load_ajax_data',
               data:data,
               success:function(results) {
               
                  //$("#result_publications_ajax").html(results);
                  $("#result_video_ajax").html(results.html);
                 // $("#result_gellery_ajax_count").html(results.count);
               }
            });
         });
     </script>
   @stop   
