<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Velchala Kondal Rao</title>
@extends('frontend.includes.layout')
@section('content')
    <!--main-->   
   <div id="alertAddWishlist-Detail" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
  <strong><span class="icon-check"></span> Success!</strong>  Added Successfully to your Wishlist.  <a class="d-block" href="{{route('wishList')}}"><strong>View Now</strong></a>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span></button></div>
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container wow animate__animated animate__fadeInDown">
                <h1>Publications</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Publications</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       @if(count($publications)>0)
       <div class="subpage-body">
            <!-- publications -->
            <div class="publications-list">
                <!-- sort -->
                <div class="sort">
                   <!-- continainer-->
                   <div class="container">
                       <!-- row -->
                       <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-md-4 align-self-center">
                                <!--<p class="pb-0">1 – 40 of 75 results</p>-->
                            <p id="result_publications_ajax_count" class="pb-0">{{ count($publications)}} results</p>
                            </div>
                            <!--/col -->
                             <!-- col -->
                             <div class="col-md-8 publications-filters">
                                <div class="form-group">
                                     <select data-id="lang" id="filterlang" class="form-control filter">  
                                        <option value="All">All</option>                                      
                                         <option value="Telugu">Telugu </option>
                                         <option value="English">English </option>                                                                     
                                         <option value="Urdu">Urdu </option>                                                                     
                                     </select>
                                 </div>
                                <div class="form-group">
                                     <select data-id="publi" id="filterpubli" class="form-control filter">  
                                         <option value="All">All</option>
                                         @foreach($publishers as $val)                                      
                                         <option value="{{$val->pub_name}} ">{{ucfirst($val->pub_name)}} </option>                            
                                         @endforeach                                                                    
                                     </select>
                                 </div>
                                
                             </div>
                            <!--/col -->
                       </div>
                       <!--/row -->
                   </div>
                   <!--/ container --> 
                </div>
                <!--/ sort -->

                <!-- publications list items -->
                <div  class="publications-items">
                    <!-- container -->
                    <div class="container" id="result_publications_ajax">                   
                    <!-- alert wishlist-->
                    <div id="alertAddWishlist" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                        <strong><span class="icon-check-circle"></span> Success!</strong> Your Book  Added Successfully to your Wishlist.  <a href="{{route('wishList')}}" class="d-block"><strong>View Now</strong></a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!--/ alert wishlist -->
                        <!-- row -->
            <div id="result_publications_ajax2" class="row py-3">
                <!-- col -->

               @foreach($publications as $value)
               <?php  if(isset(auth()->user()->id)){ $userid=auth()->user()->id; }else{ $userid=""; } ?>
                <?php  $wishlist = \App\Models\Wishlist::where(['publi_id'=>$value->b_id,'user_id'=>$userid])->get(); $wishlist= count($wishlist) ?>
                <div class="col-6 col-sm-4 col-md-3">
                    <div class="book-item">
                        <figure class="bookcover">
                            <a href="{{ url('details',['catalias'=>$value->publication_slug]) }}">
                                <img src="theme/uploads/publications/{{$value->img}}" alt="" class="img-fluid">
                            </a>
                            @if(Auth::user())
                           
                            <div id="wishlist_{{$value->b_id}}" data-publ-id="{{$value->b_id}}" @if($wishlist>0) style="background-color:#007aff" class="wishlist-icon" @else class="wishlist-icon  wishlist-add"  @endif   data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                <span   class="icon-heart-o icomoon "></span>
                            </div>
                            @else
                            
                            <a href="{{url('login')}}"><div class="wishlist-icon"  data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                <span class="icon-heart-o icomoon"></span>
                            </div> </a>
                            @endif
                            <span class="badge badge-pill badge-success">{{$value->pub_name}}</span>
                        </figure>
                        <article class="text-center">
                            <a href="{{ url('details',['publications_name_url'=>$value->publication_slug]) }}">{{$value->publication_name}}</a>
                            
                        </article>
                    </div>
                </div>
                @endforeach
                    
                    <!--/ col -->                           
                </div>
                <?php  $publicationsTotal = \App\Models\Products::where('publications_status',1)->get(); $publicationsTotal = count($publicationsTotal) ?>
               
                @if(count($publications)<$publicationsTotal)
                <div class="row justify-content-center pb-4">
                <div class="col-lg-4 text-center">
                <a class="orange-btn" data-id="loadmore" data-count-value="{{ count($publications) }}" id="loadmore_details" href="javascript:void(0)">Load More</a>
                </div>
                </div>
                @endif

                <!-- row -->
               
                <!--/ row -->
            </div>
            <!--/ container -->
            </div>
            <!--/ publications list items -->
        </div>
        <!--/ publications -->

   </div>
   @else
   <div class="col-md-6 text-center no-data ">
                <h2 class="h2">No Data Available Now</h2>
                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                <p>Thank you for visit us</p>
            </div>
            </div>
    @endif
   <!--/ sub page body -->



</main> 
    <!--/ main-->
     <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         $(".filter").change(function() {

             filter = $(this).val();
             filterType = $(this).attr("data-id");
             count=$(this).attr("data-count-value");
             filterlang = $('#filterlang').val();
             filterpubli = $('#filterpubli').val();
             data = 'filter='+filter+'&filterpubli='+filterpubli+'&filterlang='+filterlang+'&filterType='+filterType+"&_token=<?php echo csrf_token() ?>";
            $.ajax({
               type:'POST',
               url:'/get_langauage_ajax_data',
               data:data,
               success:function(results) {
                  
                  //$("#result_publications_ajax").html(results);
                  $("#result_publications_ajax").html(results.html);
                  $("#result_publications_ajax_count").html(results.count);
               }
            });
         });
         
          $("#loadmore_details").click(function() {

             filterlang = $('#filterlang').val();
             filterpubli = $('#filterpubli').val();
             filterType = $(this).attr("data-id");
             filter='10';
             count=$(this).attr("data-count-value");
             data = 'filter='+filter+'&filterpubli='+filterpubli+'&filterlang='+filterlang+'&filterType='+filterType+'&count='+count+"&_token=<?php echo csrf_token() ?>";
              
             $.ajax({
               type:'POST',
               url:'/get_langauage_ajax_data',
               data:data,
               success:function(results) {
                
                  //$("#result_publications_ajax").html(results);
                  $("#result_publications_ajax").html(results.html);
                  $("#result_publications_ajax_count").html(results.count);
               }
            });
         });
      </script>
<script>
         $(".wishlist-add").click(function() {

             publicationId = $(this).attr("data-publ-id");
             publi_id ="#wishlist_"+publicationId;
             data = 'publicationId='+publicationId+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/ajax_wish_list',
               data:data,
               success:function(results) {

                   $(publi_id).css('background-color','#007aff');
                   $(publi_id).removeClass('wishlist-add');
                   $("#alertAddWishlist-Detail").show();
               }
            });
         });
   </script>
  

   <script>
    $(document).ready(function(){        
        $('.wishlist-icon').click(function(){
            $('#alertAddWishlist').show();
        }) 
    });
   </script>
    </body>
</html>
 @stop