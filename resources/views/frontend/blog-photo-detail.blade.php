<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                 <?php   $BlogEvents = \App\Models\Blogs::where(['blog_id'=>$photos['0']->event_name])->get()->first();  ?>
                <h1>{{ $BlogEvents->blog_title }}</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item"><a href="{{route('photo-albums')}}">Photo Album</a></li>  
                       
                        <li class="breadcrumb-item active" aria-current="page"><span></span>{{ $BlogEvents->blog_title }}</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->
        <div class="subpage-body">
           <!-- container -->
           <div class="container gallery-block grid-gallery" id="result_gallery_ajax">
                <!-- row -->
                <div class="row">
                    <!-- item -->
                     @foreach($photos as $value) 
                    <div class="col-6 col-sm-6 col-md-3 item wow animate__animated animate__fadeInUp">
                        <a class="lightbox" href="{{ url('theme/uploads/photos').'/'.$value->img }}">
                           <img src="{{ url('theme/uploads/photos').'/'.$value->img }}" alt="" class="img-fluid image scale-on-hover">
                        </a>
                    </div>
                    @endforeach
                    <!-- item -->                   
               </div>
                <?php  $Photostotal = \App\Models\Photos::where('cat_type',"4")->where('event_name',$photos['0']->event_name)->get(); $Photostotal = count($Photostotal) ?>
               
                @if(count($photos)<$Photostotal)
                <div class="row justify-content-center pb-4">
                <div class="col-lg-4 text-center">
                <a class="orange-btn" data-type-photo="blog" data-type="more" data-id="{{$photos['1']->event_name}}" data-count-value="{{ count($photos) }}" id="loadmore_details" href="javascript:void(0)">Load More</a>
                </div>
                </div>
                @endif
               <!--/ row -->
           </div>
           <!--/ container -->
       </div>
       <!-- sub page body -->
       
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
     $("#loadmore_details").click(function() {
             
             type1 = $(this).attr("data-type");
             gallery_id = $(this).attr("data-id");
             count=$(this).attr("data-count-value");
             dataTypePhoto = $(this).attr("data-type-photo");
             data = 'gallery_id='+gallery_id+'&dataTypePhoto='+dataTypePhoto+'&type1='+type1+'&count='+count+"&_token=<?php echo csrf_token() ?>";
             
            $.ajax({
               type:'POST',
               url:'/get_gallery_load_ajax_data',
               data:data,
               success:function(results) {
             
                  //$("#result_publications_ajax").html(results);
                  $("#result_gallery_ajax").html(results.html);
                  $("#result_gellery_ajax_count").html(results.count);
               }
            });
         });
     </script>
    @stop
