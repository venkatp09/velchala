<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('includes.layout')

  @section('content')
    <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">
                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="index.php" class="brand-login">
                                <img src="img/logo.svg" alt="">
                            </a>
                            <h1 class="text-center flight pb-0">Login Account</h1>
                            <p class="text-center">This is a secure system and you will need to provide your login details to access the site.</p>
                        </div>
                        <!-- form -->
                        <form class="form py-3">
                            <div class="form-group">
                                <label for="userNameInput">Enter your Username</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="userNameInput" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Enter your Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" id="userNameInput" placeholder="Password">
                                </div>
                            </div>
                            <p class="text-right"><a href="forgotpassword.php" class="fblue">Forgot Password?</a></p>
                            <input type="button" onclick="pageRedirect()" class="btn orange-btn w-100 mt-2" value="Sign in">
                            <p class="text-center">
                                Don't have an account? <a class="forange" href="register.php">Register now</a>
                            </p>

                            <script>
                                function pageRedirect() {
                                    window.location.href = "user-profile-information.php";
                                    }   
                                </script>
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ login section -->
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6 loginrt d-none d-lg-block"></div>
                <!--/ col -->
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>   
   
   @stop
     
    </body>
</html>