<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
 <link rel="icon" type="image/png" sizes="32x32" href="{{ url('theme/img/fav.png')}}">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- animation components-->
<link rel="stylesheet" href="{{ url('theme/css/animate-4.0.css')}}">

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">

<!-- Fade loading -->
<link rel="stylesheet" href="{{ url('theme/css/animsition.css')}}">
</head>
    <div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">
                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="{{url('/')}}" class="brand-login">
                                <img src="{{url('theme/img/logo.svg')}}" alt="">
                            </a>
                            <h1 class="text-center flight pb-0">Verify Account</h1>
                            <p class="text-center">We will sent otp to your register mobile </p>
                        </div>
                         @if($msg) 
                        <strong style="color: red;">{{ $msg }}</strong>
                        @endif
                        <!-- form -->
                        <form action="{{url('verify_account_otp')}}" method="post" class="form py-3">
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>"> 
                            <div class="form-group">
                                <label for="RegisteredEmail">Please enter Otp to verify your account </label>
                                <div class="input-group">
                                    <input type="text" name="account_otp" class="form-control" id="account_otp" placeholder="Otp">
                                </div>
                            </div>                                                       
                            <input type="submit" class="btn orange-btn w-100 mt-2" value="Verify Account">
                           
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ login section -->
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6 loginrt d-none d-lg-block"></div>
                <!--/ col -->
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>

    </body>
</html>