<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
 <link rel="icon" type="image/png" sizes="32x32" href="{{ url('theme/img/fav.png')}}">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- animation components-->
<link rel="stylesheet" href="{{ url('theme/css/animate-4.0.css')}}">

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">

<!-- Fade loading -->
<link rel="stylesheet" href="{{ url('theme/css/animsition.css')}}">
</head>
<body>
<div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">

                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="{{url('/')}}" class="brand-login">
                                <img src="{{url('theme/img/logo.svg')}}" alt="Velchala">
                            </a>
                            <h1 class="text-center flight pb-0">Login Account</h1>
                            <p class="text-center">This is a secure system and you will need to provide your login details to access the site.</p>
                        </div>
                        
                       @if (Session::has('flash_message'))
                    <br/>
                   
                        <strong>hi</strong>
                  
                @endif
        
                         
                    <form method="POST" class="form py-3" action="{{ route('login') }}">
                        @csrf

                       <div class="form-group">
                                <label for="userNameInput">Enter your Username1</label>
                                <div class="input-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group">
                                <label for="userNameInput">Enter your Password</label>
                                <div class="input-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                                @if (Route::has('password.request'))
                                <p class="text-right"><a  href="{{ url('forgot-password') }}" class="fblue">Forgot Password?</a></p>
                                </a>
                                @endif
                                <button type="submit" class="btn orange-btn w-100 mt-2">
                                    {{ __('Login') }}
                                </button>

                               
                             <p class="text-center">
                                Don't have an account? <a class="forange" href="{{url('register')}}">Register now</a>
                            </p>
                    </form>
                 </div>
                    <!--/ login section -->
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6 loginrt d-none d-lg-block"></div>
                <!--/ col -->
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>   
</body>
</html>

