<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>News</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                                         
                        <li class="breadcrumb-item active" aria-current="page"><span>News</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            @if(count($blog_news))
            @foreach($blog_news as $value)
            <!-- col -->
            <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInDown">
                <div class="card blogcard">
                    <a href="" target="_blank">
                        @if($value->blog_news_img!="")
                        <img class="card-img-top img-fluid" src="theme/uploads/blog_news/{{$value->blog_news_img}}">
                        @else
                        <img class="card-img-top img-fluid" src="theme/uploads/blog_news/dummyblog_news.jpeg">
                        @endif
                    </a>
                    <div class="card-body">
                        <h5 class="card-title ptregular">{{ucfirst(substr($value->blog_news_name,0,40))}} @if(strlen($value->blog_news_name)>40)...@endif</h5>
                        <p class="card-text pb-3"><?php echo substr(strip_tags($value->blog_news_des),0,100) ?> @if(strlen($value->blog_news_des)>100)...@endif</p>
                        <p>{{ucfirst($value->blog_news_paper_name)}}	 <span class="d-inline-block px-3 small pb-3">|</span>{{date("d-m-Y",strtotime($value->blog_news_date))}}</p>
                        <a href="{{ucfirst($value->blog_news_url)}}" class="btn orange-btn" target="_blank">Read More</a>
                    </div>
                </div>
            </div>
            <!--/ col -->
            @endforeach
            @else
            <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                <p>Thank you for visit us</p>
            </div>
            </div>
            @endif  
           
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

  @stop