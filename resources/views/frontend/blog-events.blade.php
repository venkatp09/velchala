<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
     <main class="subpage-main">
            
            <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Events</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item active" aria-current="page"><span>Events</span></li>                   
                                            
                        
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row py-3">
            <!-- col -->
              @if(count($blogs)>0)
              @foreach($blogs as $value)
            <div class="col-md-6 col-lg-4 wow animate__animated animate__fadeInDown">
                    <div class="card blogcard">
                        <a href="{{ url('blog-events-details',['url'=>$value->blog_url]) }}">
                            <img class="card-img-top img-fluid" src="{{ url('theme/uploads/blogs').'/'.$value->blog_image }}">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title ptregular">{{substr(strip_tags($value->blog_title),0,40)}} @if(strlen($value->blog_title)>40)...@endif</h5>
                            <!--<p class="card-text pb-3">{{substr(strip_tags($value->blog_description),0,100)}}..</p>-->
                            <p>{{ $value->blog_location }} <span class="d-inline-block px-3 small pb-3">|</span>{{ date("d-M-Y",strtotime($value->event_date)) }}</p>
                            <a href="{{ url('blog-events-details',['url'=>$value->blog_url]) }}" class="btn orange-btn">Read More</a>
                        </div>
                    </div>
                </div>
                 @endforeach
                 @else 
           <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p class="text-center">Currently We dont have any data you are looking, We will update you Soon, </p>
                <p class="text-center">Thank you for visit us</p>
            </div>
            </div>
            @endif
                   
            <!--/ col -->                  
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

     @stop
