<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Photo Gallery Albums</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Gallery</a></li>                    
                        <li class="breadcrumb-item active" aria-current="page"><span>Photo Gallery Albums</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-5">
                     <!-- col -->
                       @if(count($events))
                       @foreach($events as $value)
                        <?php  $photos = \App\Models\Photos::where(['cat_type'=>3,'event_name'=>$value->id])->get(); $countPhotos= count($photos) ?> 
                     <div class="col-sm-6 col-md-6 col-lg-4 wow animate__animated animate__fadeInUp">
                        <div class="book-item albumitem">
                            <figure class="bookcover">
                                <a href="{{ url('jayanthi-events-photos',['id'=>$value->id]) }}">
                                    <img src="{{ url('theme/uploads/events').'/'.$value->img }}" alt="" class="img-fluid">
                                </a>                              
                                <span class="badge badge-pill photosnumber">{{ $countPhotos }} Photos</span>
                            </figure>
                            <article>
                                <h2 class="h5">
                                    <a href="{{ url('jayanthi-events-photos',['id'=>$value->id]) }}">{{ $value->event_name }}</a>
                                </h2>
                                <div class="item-deails d-flex flex-wrap">
                                    <p class="small"><span class="icon-pin icomoon"></span>{{ $value->event_location }}</p>
                                    <p class="small pl-4"><span class="icon-calendar icomoon pr-1"></span>{{ date("d-m-Y",strtotime($value->event_date)) }}</p>
                                </div> 
                            </article>
                        </div>
                    </div>
                     @endforeach
                     @endif
                      @if(count($events))
                       @foreach($blogs as $value)
                        <?php  $photos = \App\Models\Photos::where(['cat_type'=>4,'event_name'=>$value->blog_id])->get(); $countPhotos= count($photos) ?> 
                     <div class="col-sm-6 col-md-6 col-lg-4 wow animate__animated animate__fadeInUp">
                        <div class="book-item albumitem">
                            <figure class="bookcover">
                                <a href="{{ url('blog-events-photos',['id'=>$value->blog_id]) }}">
                                    <img src="{{ url('theme/uploads/blogs').'/'.$value->blog_image }}" alt="" class="img-fluid">
                                </a>                              
                                <span class="badge badge-pill photosnumber">{{ $countPhotos }} Photos</span>
                            </figure>
                            <article>
                                <h2 class="h5">
                                    <a href="{{ url('blog-events-photos',['id'=>$value->blog_id]) }}">{{ $value->blog_title }}</a>
                                </h2>
                                <div class="item-deails d-flex flex-wrap">
                                    <p class="small"><span class="icon-pin icomoon"></span>{{ $value->blog_location }}</p>
                                    <p class="small pl-4"><span class="icon-calendar icomoon pr-1"></span>{{ date("d-m-Y",strtotime($value->event_date)) }}</p>
                                </div> 
                            </article>
                        </div>
                    </div>
                     @endforeach
                     @endif
                    <!--/ col -->                 


                </div>
                <!--/ row -->
            </div>
            <!--/ container -->

       </div>
       <!--/ sub page body -->



    </main> 
    <!--/ main-->
   @stop
