<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{{ ucfirst($publications->meta_title) }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{{  ucfirst($publications->meta_description)}} "/> 
<meta name="keywords" content="{{  ucfirst($publications->meta_keywords)}}" />
<meta name="robots" content="index, follow"/>
<meta name="copyright" content="Velchala" />
<meta name="author" content="Velchala" />
<meta name="language" content="en_US" />

<meta property="og:url" content="{{ url('details',['catalias'=>$publications->publication_slug]) }}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{  ucfirst($publications->meta_title)}}" /> 
<meta property="og:description" content="{{  ucfirst($publications->meta_description)}}" /> 
<meta property="og:image" content="{{ url('theme/uploads/publications').'/'.$publications->img }}" />


 @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
       <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- <h1>Publications</h1> -->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('/publications')}}">Publications</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>{{  ucfirst($publications->publication_name)}}</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

            <!-- top publication detail -->
            <div class="book-top-detail">
                <!-- container -->
                <div class="container">
                   
                    <!-- card-->
                    <div class="card p-2 p-sm-5">
                        <!-- alert wishlist-->
                        <div id="alertAddWishlist-Detail" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                            <strong><span class="icon-check"></span> Success!</strong>  Added Successfully to your Wishlist.  <a class="d-block" href="{{route('wishList')}}"><strong>View Now</strong></a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!--/ alert wishlist -->

                         <!-- alert add to cart-->
                         <div id="alertAddtocart" class="alert alert-success alert-dismissible wow animate__animated" role="alert">
                            <strong><span class="icon-check"></span> Success!</strong>  </strong> Added Successfully to your cartlist.  <a class="d-block" href="{{url('cart')}}"><strong>View Now</strong></a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!--/ alert add to cart -->


                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6">
                                <figure class="figure-detail">
                                    <img src="{{ url('theme/uploads/publications').'/'.$publications->img }}" alt="{{  ucfirst($publications->publication_name)}}" class="img-fluid">
                                </figure>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                                <?php  if(isset(auth()->user()->id)){ $userid=auth()->user()->id; }else{ $userid=""; } ?>
                                 <?php  $wishlist = \App\Models\Wishlist::where(['publi_id'=>$publications->b_id,'user_id'=>$userid])->get(); $wishlist= count($wishlist) ?>
                             <div class="col-lg-6 book-detail-rt align-self-center">
                                 <!-- border -->
                                <div class="px-4 py-3 border">
                                    <!-- row -->
                                    <div class="row position-relative">
                                        <div class="col-md-9 col-12">
                                            <h1 class="h2 ptregular">{{  ucfirst($publications->publication_name)}}</h1>
                                           
                                            <p class="price pl-1">
                                              <span class="offer">{{  $publications->price-$publications->dis_price}}</span>
                                                <span class="oldprice">{{  $publications->price}}</span>
                                            </p>
                                        </div>
                                        <div class="col-md-3 col-12 text-right ">

                                          @if(Auth::user())
                                            @if($wishlist!="")
                                            
                                             <div class="wishlist-icon" data-toggle="tooltip" data-placement="top" id="wishlist-icon" 
                                            style="background-color:#007aff" data-publ-id="{{ $publications->b_id }}" title="Added  Wishlist">
                                                <span class="icon-heart-o icomoon"></span>
                                            </div>
                                            @else
                                           <div class="wishlist-icon add-wishlist" data-toggle="tooltip" data-placement="top" id="wishlist-icon" data-publ-id="{{ $publications->b_id }}" title="Add to Wishlist">
                                                <span class="icon-heart-o icomoon"></span>
                                            </div>
                                            
                                            @endif

                                          @else
                                           <a href="{{url('login')}}"><div class="wishlist-icon"  data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                                <span class="icon-heart-o icomoon"></span>
                                            </div> </a>
                                          @endif  
                                        </div>
                                    </div>
                                    <!--/ row -->
                                    <p class="basic-description">{{  substr(strip_tags($publications->description),0,150)}} </p>
                                </div>
                                <!-- border/-->

                                <!-- border -->
                              
                                <div class="px-4 border mt-2 wow animate__animated animate__fadeInDown">
                                    <!-- specifications -->
                                    <div class="book-specs">
                                     <!-- row -->
                                     <div class="row">
                                         <!--col-->
                                         <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Availability</dt>
                                                    @if($publications->sku_code="in_stock")
                                                    <dd class="fgreen fsbold">In Stock</dd>
                                                    @else
                                                    <dd class="fgreen fsbold">Out Stock</dd>
                                                    @endif
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>SKU Code</dt>
                                                    <dd>{{  $publications->sku_code }}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Pages</dt>
                                                    <dd>{{  $publications->pages }}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Author</dt>
                                                    <dd>{{ ucfirst($publications->author )}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Language</dt>
                                                    <dd>{{ ucfirst($publications->language )}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Publisher</dt>
                                                    <dd>{{ ucfirst($publications->getCategory->pub_name )}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Published Date</dt>
                                                    <dd>{{ date("d-m-Y",strtotime($publications->published_date ))}}</dd>
                                                </dl>
                                            </div>
                                         </div>
                                        
                                     </div>
                                     <!--/ row -->
                                 </div>
                                 
                                </div>
                                <p class="py-3">
                                    <span>QTY</span>
                                    <span>
                                        <input id="cart_quanity" oninput="validity.valid||(value='');" min='1' value="1" class="text-center qty" type="number" placeholder="1">
                                    </span>
                                </p>
                                <a id="addtokcart-icon" href="javascript:void(0)"  data-publ-id="{{ $publications->b_id }}"  class="orange-btn-border text-uppercase">Add to Cart</a>
                                @if(Auth::user())
                               <!-- <a href="{{ route('pdf_view',['id'=>$publications->publication_slug] )}}" class="orange-btn text-uppercase" target="_blank">Read Online</a>-->
                                  <a href="{{ url('theme/uploads/publications').'/'.$publications->pdf_file }}" class="orange-btn text-uppercase" target="_blank">Read Online</a>
                                  @else
                                   <a href="{{url('login')}}" class="orange-btn text-uppercase"  data-toggle="tooltip" data-placement="top" >
                                      Read Online</a>
                                  @endif  

                                <!-- social share -->
                                <div class="social-share pt-5">
                                    <span class="pt-1 span-share">Share this product</span>
                                    <a target="__blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url('details',['catalias'=>$publications->publication_slug]) }}" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                                    <a target="__blank" href="https://twitter.com/home?status={{ url('details',['catalias'=>$publications->publication_slug]) }}" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                                    <a target="__blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url('details',['catalias'=>$publications->publication_slug]) }}&source={{ url('theme/uploads/publications').'/'.$publications->img }}" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                                    <a target="__blank" href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                                    <a target="__blank" href="https://api.whatsapp.com/send?text={{ url('details',['catalias'=>$publications->publication_slug]) }}" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                                    <a target="__blank" href="https://pinterest.com/pin/create/button/?url={{ url('details',['catalias'=>$publications->publication_slug]) }}&media={{ url('theme/uploads/publications').'/'.$publications->img }}&description=" class="sharesocial"><span class="icon-pinterest icomoon"></span></a>
                                </div>
                                <!--/ social share -->
                                
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/card ends -->

                    <!-- responsive tab -->
                    <div class="custom-tab">
                        <!-- tab -->
                        <div class="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1">
                                <li>Description</li>
                               <!-- <li>Reviews</li>-->
                                <li>Videos</li>
                            </ul>
                            <div class="resp-tabs-container hor_1">
                                <!-- description -->
                                <div>
                                    <p>{!! $publications->description !!}
                                </div>
                                <!--/ description -->

                                <!-- reviews -->
                              
                                <!--/ reviews -->

                                <!-- related videos -->
                                <!-- row -->
                                    <div>
                              @if(count($videos)!="")
                                    <h3 class="h4 ptregular pb-3 border-bottom">Related videos <span>({{count($videos)}})</span></h3>
                               @else 
                            <div class="row justify-content-center no-data">
                                <div class="col-md-6 text-center">
                                    <h2 class="h2">No Data Available Now</h2>
                                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                    <p>Thank you for visit us</p>
                                </div>
                            </div>
                              @endif
                                 

                                     <!-- row -->
                                     <div class="row justify-content-center py-3">
                                        @foreach($videos as $value)
                                         <!-- col -->
                                         <div class="col-md-4 text-center video-col">
                                         <iframe width="100%" src="https://www.youtube.com/embed/{{$value->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p class="text-left">{{$value->poem_name}}</p>
                                        </div>
                                        <!--/ col -->
                                        @endforeach  
                                    </div>
                                    <!--/row -->


                                </div>
                                    
                                </div>
                                <!--/ related videos -->
                            </div>
                        </div>
                        <!--/ tab -->
                    </div>
                    <!--/ responsive tab -->

                    <!-- you may also interested in -->
                   
                    <!--/ you may also interested in -->
                </div>
                <!--/ container-->
            </div>
       </div>
    </main> 
    <!--/ main-->

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         $("#addtokcart-icon").click(function() {

             publicationId = $(this).attr("data-publ-id");
             
             cart_quanity = $("#cart_quanity").val();
             data = 'publicationId='+publicationId+'&cart_quanity='+cart_quanity+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/add_to_cart_data_ajax',
               data:data,
               success:function(results) {
                   $('#alertAddtocart').show();
                   location.reload();
               }
            });
         });

      </script>

      <script>
         $(".add-wishlist").click(function() {

             publicationId = $(this).attr("data-publ-id");
             
             data = 'publicationId='+publicationId+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/ajax_wish_list',
               data:data,
               success:function(results) {
                  
                  $("#wishlist-icon").css('background-color','#007aff');
                  $("#wishlist-icon").removeClass('add-wishlist');
                  $('#alertAddWishlist-Detail').show();
               }
            });
         });

      </script>
    @stop
 