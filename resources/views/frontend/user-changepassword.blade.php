<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
      @extends('frontend.includes.user_layout')

  @section('content')
     <!-- alert wishlist-->
     <div id="alertAddrtoCart" class="alert alert-success alert-dismissible" role="alert">
        <strong><span class="icon-check-circle"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Cart List.  <a href="javascript:void(0)" class="d-block"><strong>View Cart Items</strong></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <!--/ alert wishlist -->
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Change Password</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                       <li class="breadcrumb-item"><a href="{{url('profile')}}">{{ ucfirst(Auth::user()->name) }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Change Password</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                

             @stop
             @section('content2')
             
                <div class="col-md-8 col-sm-8">
                    <!-- right profile detail -->
                    <div class="user-profile-rt">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6 wow animate__animated animate__fadeInDown">

                              
                    @if(session('flash_message_error'))
                    
                     <strong style="color: red;">{{session('flash_message_error')}}</strong>
                   
                    @endif
                     @if(session('flash_message'))
                    
                     <strong style="color: green;">{{session('flash_message')}}</strong>
                   
                    @endif

                        <form method="POST" id="changePassword"
                            action="{{ route('userChangePassword') }}"
                            accept-charset="UTF-8" >
                                {{ csrf_field() }}
                                <!-- row -->
                                   
                                    <div class="form-group">
                                        <label>Current Password</label>
                                          <div class="input-group">
                                        <input type="password" id="password" name="password"
                                                placeholder="Enter Current Password"
                                                value="{{ !empty(old('password')) ? old('password') : '' }}"
                                                class="form-control">
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="text-danger help-block">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>


                                    <div class="form-group">
                                        <label>New Password</label>
                                        <input type="password" id="new_password"
                                                name="new_password"
                                                value="{{ !empty(old('new_password')) ? old('new_password') : '' }}"
                                                placeholder="Enter New password"
                                                class="form-control">
                                        @if ($errors->has('new_password'))
                                            <span class="text-danger help-block">{{ $errors->first('new_password') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label>Confirm New Password</label>
                                        <input type="password" class="form-control"
                                                id="confirm_password"
                                                name="confirm_password"
                                                placeholder="Confirm Password"
                                                value="{{ !empty(old('confirm_password')) ? old('confirm_password') : '' }}">
                                        @if ($errors->has('confirm_password'))
                                            <span class="text-danger help-block">{{ $errors->first('confirm_password') }}</span>
                                        @endif
                                    </div>

                    <div class="form-group">
                        <input type="submit" value="Submit" class="orange-btn">
                    </div>
                                       
                                        <!--/ col -->

                                        <!-- col -->

                                        <!--/ col -->
                                   

                                </form>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
       <script>
        $(function () {

            $('#changePassword').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    password: {
                        required: true
                    },
                    new_password: {
                        required: true,
                        maxlength: 12,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password",
                        maxlength: 12,
                        minlength: 6
                    }
                },
                messages: {
                    password: {
                        required: 'Please enter password'
                    },
                    new_password: {
                        required: 'Please enter new password'
                    },
                    confirm_password: {
                        required: 'Please confirm password',
                        equalTo: "The confirm password and new password must match."
                    }
                },
            });
        });

    </script>
  @stop
   <script>
    $(document).ready(function(){        
        $('.addtoCart-icon').click(function(){
            $('#alertAddrtoCart').show();
        }) 
    });
   </script>    
   
    </body>
</html>