<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
   
     @extends('includes.user_layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>My Orders History</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('user_profile_information')}}">{{ ucfirst(Auth::user()->name) }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>My Orders History</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                  @stop
             @section('content2')
                <div class="col-md-8 col-sm-8">
                    <!-- right profile detail -->
                    <div class="user-profile-rt">                  

                    <!-- orders list item -->
                   

                    
                    <!-- orders list item -->
                  

                    
                    

                    <div class="row justify-content-center">
                <div class="col-md-6 text-center">
                    <h2 class="h2">No Data Available Now</h2>
                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                    <p>Thank you for visit us</p>
                </div>
            </div>
                     
                      
                   

                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
   @stop
   
    </body>
</html>