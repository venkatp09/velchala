<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
      @extends('frontend.includes.user_layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Profile Information</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('user_profile_information')}}">{{ ucfirst(Auth::user()->name) }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>User Profile Information</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
             @stop
             @section('content2')
                <div class="col-lg-8 col-sm-8 wow animate__animated animate__fadeInUp">
                    <!-- right profile detail -->
                    @if(session('success'))
                    <div class="alert alert-warning alert-dismissible" id="error-alert">
                     <strong style="color: green;">{{session('success')}}</strong>
                    </div>
                    @endif
                    @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                    <div class="user-profile-rt">
                    <form  enctype="multipart/form-data"  action ="" method = "post">
                        <!-- row -->
                        <div class="row justify-content-center">
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="figure-user mx-auto position-relative">
                                    @if($users->image!="")
                                    <img src="{{ url('theme/uploads/user_profile').'/'.$users->image}}" alt="">
                                    @else
                                    <img src="{{ url('theme/uploads/user_profile/dummy.jpg') }}" alt="">
                                    @endif

                                    

                                </div>

                                <!-- form -->
                                
                                    
                                    <!-- form group -->
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>"> 
                                    <div class="form-group">
                                        <label for="userName">upload profile</label>
                                        <div class="input-group">
                                            <input  class="form-control" type="file" name="image" placeholder="Image" id="example-text-input">
                                            <input type="hidden" class="form-control" id="userName" aria-describedby="emailHelp" name="old_img" value="{{ ucfirst($users->image_url)}}">
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <label for="userName">Write Your Name</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="userName" aria-describedby="emailHelp" placeholder="Enter email" name="name" value="{{ ucfirst($users->name)}}">
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="userName">Gender Male / Female</label>
                                        <div class="d-flex">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="male" value="male"  {{ ($users->gender=="male")? "checked" : "" }}>
                                                <label class="form-check-label" for="male">
                                                    Male
                                                </label>
                                            </div>
                                            <div class="form-check ml-4 form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="female" value="female" {{ ($users->gender=="female")? "checked" : "" }}>
                                                <label class="form-check-label" for="female">
                                                Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                     <!-- form group -->
                                     <div class="form-group">
                                        <label for="mNumber">Mobile Number</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="mNumber" aria-describedby="emailHelp" placeholder="Enter Phone Number" name="mobile" value="{{ ucfirst($users->mobile)}}">
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                     <!-- form group -->
                                     <div class="form-group">
                                        <label for="email">Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address" name="email" value="{{ ucfirst($users->email)}}">
                                        </div>
                                    </div>
                                    <!-- /form group -->

                                      <!-- form group -->
                                      <div class="form-group">
                                       <button class="btn  orange-btn w-100">Submit</button>
                                    </div>
                                    <!-- /form group -->
                                   
                                </form>
                                <!--/ form -->
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
     <!--image upload popup -->
   <!-- Modal -->

    <div class="modal fade" id="update-profile-picture" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Change Your Profile Picture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <!-- file input -->
            <form>
                    <input type="file" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple>
                </form>
            <!--/ file input -->
            </div>
            <div class="modal-footer">       
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('input[type="file"]').imageuploadify();
            })
        </script>
    </div>

    <!--/image upload popup -->
    @stop 
