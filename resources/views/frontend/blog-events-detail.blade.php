<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ ucfirst($blogs->meta_title) }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{{  ucfirst($blogs->meta_description)}} "/> 
<meta name="keywords" content="{{  ucfirst($blogs->meta_keywords)}}" />
<meta name="robots" content="index, follow"/>
<meta name="copyright" content="Velchala" />
<meta name="author" content="Velchala" />
<meta name="language" content="en_US" />
<meta property="og:url" content="{{ url('blog-events-details',['url'=>$blogs->blog_url]) }}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{ $blogs->blog_title }}" /> 
<meta property="og:description" content="{{ $blogs->blog_title }}" /> 
<meta property="og:image" content="{{ url('theme/uploads/blogs').'/'.$blogs->blog_image }}" />
     @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
     <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Blog Events Details</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item"><a href="{{url('/blog-events')}}">Events</a></li>                    
                        <li class="breadcrumb-item active" aria-current="page">{{ $blogs->blog_title }}<span></span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>

    <!-- container -->
    <div class="container">
        <!-- card -->
        <div class="card my-3 blog-detail">
            <!-- card header -->
            <div class="card-header">
                <!-- row -->
                <div class="row justify-content-center wow animate__animated animate__fadeInUp">
                    <div class="col-lg-8">
                        <a href="{{ url('blog-events') }}" class="fblue"><span class="icon-arrows"></span> Back to Events</a>
                        <h1 class="h1 pb-3">{{ $blogs->blog_title }}</h1>
                        <p class="pb-0 mb-0">{{ $blogs->blog_location }} <span class="d-inline-block px-3 small pb-3">|</span>{{ date("d-m-Y",strtotime($blogs->event_date)) }}</p>

                         <!-- social share -->
                        <div class="social-share py-3 d-flex">
                            <span class="pt-1 span-share">Share</span>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url('blog-events-details',['url'=>$blogs->blog_url]) }}" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                            <a target="_blank" href="https://twitter.com/home?status={{ url('blog-events-details',['url'=>$blogs->blog_url]) }}" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url('blog-events-details',['catalias'=>$blogs->blog_url]) }}&source={{ url('theme/uploads/blogs').'/'.$blogs->blog_image }}" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                            <a target="_blank" href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                            <a target="_blank" href="https://api.whatsapp.com/send?text={{ url('blog-events-details',['url'=>$blogs->blog_url]) }}" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                            <a target="_blank" href="https://pinterest.com/pin/create/button/?url={{ url('details',['url'=>$blogs->blog_url]) }}&media={{ url('theme/uploads/blogs').'/'.$blogs->blog_image }}&description=" class="sharesocial"><span class="icon-pinterest icomoon"></span></a>
                        </div>
                        <!--/ social share -->
                    </div>
                    
                </div>
                <!--/ row -->
            </div>
            <!-- card header -->

             <!-- card body -->
             <div class="card-body">
                <img class="card-img-top img-fluid mb-3" src="{{ url('theme/uploads/blogs').'/'.$blogs->blog_image }}">

                <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <!-- responsive tab -->
                    <div class="custom-tab">
                        <!-- tab -->
                        <div class="parentHorizontalTab wow animate__animated animate__fadeInUp">
                            <ul class="resp-tabs-list hor_1">
                                <li>Description</li>
                                <li>Photos</li>
                                <li>Videos</li>
                            </ul>
                            <div class="resp-tabs-container hor_1">
                                <!-- description -->
                                <div>
                                <p>{!! $blogs->blog_description !!}</p>
                                </div>
                                <!--/ description -->

                                <!-- reviews -->
                                <div>
                                     @if(count($photos)!="")
                                    <h2 class="h4 ptregular pb-3 border-bottom">Event Photo Gallery <span>({{count($photos)}})
                                    </span></h2>
                                 
                            
                              @endif
                                    <div class="gallery-block grid-gallery">
                                        <!-- row -->
                                        <div class="row">
                                            <!-- item -->
                                          @if(count($photos)>0)
                                          @foreach($photos as $value)
                                            <div class="col-6 col-sm-6 col-md-3 item">
                                                <a class="lightbox" href="{{ url('theme/uploads/photos').'/'.$value->img }}">
                                                    <img class="img-fluid image scale-on-hover" src="{{ url('theme/uploads/photos').'/'.$value->img }}">
                                                </a>
                                            </div>
                                           @endforeach
                                             @else 
                                    <div class="row justify-content-center no-data">
                                <div class="col-md-6 text-center">
                                    <h2 class="h2">No Data Available Now</h2>
                                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                    <p>Thank you for visit us</p>
                                </div>
                            </div>
                              @endif  
                                            <!-- item -->
                                            
                                            <!-- item -->                                          
                                    </div>
                                    <!--/ row -->
                                </div>
                                   

                                    

                                </div>
                                <!--/ reviews -->

                                <!-- related videos -->
                                <div>
                              @if(count($video)!="")
                                    <h3 class="h4 ptregular pb-3 border-bottom">Related videos <span>({{count($video)}})</span></h3>
                               @else 
                            <div class="row justify-content-center no-data">
                                <div class="col-md-6 text-center">
                                    <h2 class="h2">No Data Available Now</h2>
                                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                    <p>Thank you for visit us</p>
                                </div>
                            </div>
                              @endif
                                 

                                     <!-- row -->
                                     <div class="row justify-content-center py-3">
                                        @foreach($video as $value)
                                         <!-- col -->
                                         <div class="col-md-4 text-center video-col">
                                         <iframe width="100%" src="https://www.youtube.com/embed/{{$value->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <p class="text-left">{{$value->poem_name}}</p>
                                        </div>
                                        <!--/ col -->
                                        @endforeach  
                                    </div>
                                    <!--/row -->


                                </div>
                                <!--/ related videos -->
                            </div>
                        </div>
                        <!--/ tab -->
                    </div>
                    <!--/ responsive tab -->
                       
                    </div>
                </div>
                <!--/ row -->
             </div>
            <!-- card body -->


        </div>
        <!--/card -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

     @stop
   