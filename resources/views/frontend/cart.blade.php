<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Cart Items ({{ $countCartSum }})</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Cart Items</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body py-4">

            <!--container -->
            @if(count($cart_detailes)>0){
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-8 wow animate__animated animate__fadeInUp">
                        <!-- row -->
                        <div class="table-headerrow row">
                            <!-- col -->
                            <div class="col-md-6">
                               <p>Product Details</p>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2">
                                <p>Qty</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2">
                                <p>Price</p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2">
                                <p>Total</p>
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <?php $totalPrice=0;?>
                        @foreach($cart_detailes as $value)

                        <div class="row cartlist-itemrow py-2">
                            <!-- col -->
                            <div class="col-md-6 col-12">
                               <div class="d-flex">
                                    <a href="publication-detail.php" class="d-block cartimteimg">
                                        <img src="{{ url('theme/uploads/publications').'/'.$value->img }}" alt="" class="w-100">
                                    </a>
                                    <div class="cart-item-details">
                                        <h6 class="h6 pb-2 fsbold">
                                            <a href="publication-detail" class="fgray">{{ $value->publication_name }}</a>
                                        </h6>
                                        <p class="pb-0"> Language: <span>{{ $value->language }}</span></p>
                                        <p> Product Code: <span>{{ $value->sku_code }}</span></p>

                                        <p>
                                            <a  class="cart_remove_item" data-cart-id="{{ $value->cart_id }}"  data-cartitem-id="{{ $value->id }}" href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                               </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2 col-4">
                                <p>
                                    <input class="addtokcart-icon" oninput="validity.valid||(value='');"  min='1' data-publ-id="{{ $value->b_id }}" type="number" class="form-control" value="{{ $value->quanity }}">
                                </p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single"> <?php  if($value->dis_price!='0') { $price=$value->dis_price; } else $price=$value->price; ?>
                                 <?php echo  $price ?>  </p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single fsbold forange"> {{  $price*$value->quanity }}  </p>
                             </div>
                            <!--/ col -->
                        </div>
                        <?php $totalPrice += $price*$value->quanity ?>
                        @endforeach
                        <!--/ row -->

                        <p class="pt-2">
                            <a href="{{ url('publications')}}" class="fblue"><span class="icon-arrows"></span> Continue Shopping</a>
                        </p>

                    </div>
                    <!--/ left col -->

                     <!-- left col -->
                     <div class="col-lg-4 wow animate__animated animate__fadeInDown">
                         <!-- card -->
                         <div class="card cartcard">
                             <div class="card-header bggray">
                                 <h4 class="h6 fsbold">Order Summary</h4>
                             </div>
                             <div class="card-body">
                                 <p class="h6 d-flex justify-content-between  pb-4">
                                    <span>ITEMS {{ $countCartSum }}</span>
                                    <span class="price-single">{{ $totalPrice }}</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between  pb-4 border-bottom">
                                    <span>Shipping Charges</span>
                                    <span class="price-single">150</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between fsbold py-4">
                                    <span>Total Cost</span>
                                    <span class="price-single fblue">{{ $totalPrice }}</span>
                                 </p>
                                 <p>
                                     <a href="{{ url('checkout')}}" class="orange-btn w-100 text-center">Checkout</a>
                                 </p>
                             </div>
                         </div>
                         <!--/ card -->
                     </div>
                    <!--/ left col -->


                </div>
                <!--/ row -->
            </div>
            @else
            <div class="row justify-content-center no-data">
            <div class="col-md-6 text-center">
                <h2 class="h2">No Data Available Now</h2>
                <p>Empty Cart</p>
                <p>Thank you for visit us</p>
            </div>
            </div>
            @endif

            <!--/ container -->           
       </div>
       <!--/ sub page body -->
        <div id="alertAddtocart" class="alert alert-success alert-dismissible wow animate__animated alertAddtocart" role="alert">
        <strong><span class="icon-check"></span> Success!</strong> Your Book  Added Successfully to your cartlist.  <a class="d-block" href="{{url('cart')}}"><strong>View Now</strong></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="alertDeletetocart" class="alert alert-success alert-dismissible wow animate__animated" role="alert">
        <strong><span class="icon-check"></span> Success!</strong> Your Book  Removed Successfully to your cartlist.  <a class="d-block" href="{{url('cart')}}"><strong>View Now</strong></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    </main> 
    <!--/ main-->
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
    <script>
         $(".addtokcart-icon").click(function() {
           
             publicationId = $(this).attr("data-publ-id");
             cart_quanity = $(this).val();
             data = 'publicationId='+publicationId+'&cart_quanity='+cart_quanity+"&_token=<?php echo csrf_token() ?>";
            $.ajax({
              
               type:'POST',
               url:'/addtocart',
               data:data,
               success:function(results) {
                   $('.alertAddtocart').show();
                   location.reload();
               }
            });
         });

           $(".cart_remove_item").click(function() {
             
            if (confirm("Are you sure Delete Book From Cart?")){ 
             cartItemId = $(this).attr("data-cartitem-id");
             cartId = $(this).attr("data-cart-id");
             data = 'cartItemId='+cartItemId+'cartId='+cartId+"&_token=<?php echo csrf_token() ?>";
             $('#alertDeletetocart').show();
             $.ajax({
              
               type:'POST',
               url:'/remove_cart',
               data:data,
               success:function(results) {
                  
                  

                  location.reload();
               }
            });
          
         
         }else{
             return false;
         }
     });
      </script>
          </body>
</html>
    @stop
