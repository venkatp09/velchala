<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
   
    @extends('frontend.includes.layout')

  @section('content')
    <main class="subpage-main">
        <!-- figure -->
        <img src="theme/img/video-bg.jpg" class="img-fluid vspimg" alt="">
        <!--/ figure -->

        <!-- top vsp description -->
        <div class="top-vsp">
            <!-- container -->
            <div class="container whitebox">
                <!-- row -->
                <div class="row py-5 wow animate__animated animate__fadeInDown">                  
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <article class="pl-md-5">
                            <p class="fblue"><i>"Art is to Discover a Flash Extraordinary  in an Object Ordinary"</i></p>
                        </article>
                    </div>
                    <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 border-left">
                            <h1 class="ptregular pl-3">Welcome to Vishwanatha Sahithya Peetam</h1>
                        </div>
                    <!--/ col -->   
                    <!-- col -->
                    <div class="col-lg-12 py-4">
                        <h5 class="ptregular h5 fblue text-center">
                        <i>A literary cum cultural organization namely “Viswanadha Sahitya Peetam” was established by “Sister Nivedita Society” at Red Hills, Hyderabad to function with effect from the month of July 2003 with the following objectives..</i>
                        </h5>
                    </div>
                    <!--/ col -->                
                </div>
                <!--/ row -->
                
            </div>
            <!--/ container -->
        </div>
        <!--/ top vsp description -->

        <!-- orange section -->
        <div class="orangebg py-3 my-3">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6 text-center vspban wow animate__animated animate__fadeInDown">
                        <img src="theme/img/kavi_samraat.jpg" alt="">
                        <h2 class="h1 ptregular fwhite">A Vision and a Projection</h2>
                        <p class="fwhite">
                            <i>"I am convinced also of this, the heart that must rule humanity is replaced by the mind.  It is a bane"</i>
                        </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ orange section -->

        <div class="vspmaster">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-4">
                    <!-- col -->
                    <div class="col-lg-6">
                      <ul class="list-items">
                            <li>The primary objective of the “Peetam” is to acquaint more people with Viswanadha’s genius and Works by organizing such literary and cultural meets and activities as may be needed.</li>
                            <li>To establish a library with books by and on Viswanadha, including audio video material, letters, radio talks and the like to facilitate the research work of scholars doing research on Viswanadha, and also to arrange such guidance and counselling as may be needed by the research scholars.</li>
                            <li>To encourage translations of Viswanadha’s works into other languages.</li>
                            <li>To institute an award namely “Viswanadha Award” to be given to an eminent writer in Telugu adjudged as such by the jury constituted for the purpose.</li>
                            <li>To start a quarterly by name “JAYANTHI“ to publish literary views and reviews.</li>
                            <li>To conduct cultural and literary programs once in a year.</li>
                        </ul>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6">                       
                        <ul class="list-items">
                            <li>To hold a seminar once a year on one of the literary aspects of Viswanadha.</li>
                            <li>To hold an essay writing competition once a year on one of Viswanadha’s literary aspects and to award prizes.</li>
                            <li>To hold a meeting once a month at Hyderabad or at any other place to discuss about one of the books of Viswanadha.</li>
                            <li>To help to establish a Deemed University namely “Kavisamrat Viswanadha Satyanarayana
Deemed University for Literary and Cultural Studies” at Hyderabad.</li>
                            <li>To arrange a Viswanadha Memorial Lecture by an eminent scholar every year on his birthday.</li>                            
                        </ul>
                        <p>For the purpose of achieving the above objectives a literary and cultural organization namely “Viswanatha Sahitya Peetam” for the promotion of Viswanadha’s Thoughts.</p>
                       
                    </div>
                    <!--/ col -->
                    
                </div>
                <!--/ row -->
            </div>
            <!--/container -->
        </div>
    </main> 
    <!--/ main-->
   @stop
  