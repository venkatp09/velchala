 <div class="container">
                        <!-- row -->
                        <div class="row py-3">
                            <!-- col -->
                         @if(count($magazines)>0)
                         @foreach($magazines as $val)
                          <div class="col-6 col-sm-4 col-md-3 wow animate__animated animate__fadeInUp">
                                <div class="book-item">
                                    <figure class="bookcover">
                                        <a href="theme/uploads/magazines/{{ $val->mag_pdf }}" target="_blank">
                                            <img src="theme/uploads/magazines/{{ $val->mag_profile_pic }}" alt="" class="img-fluid">
                                        </a>  
                                    </figure>
                                    <article class="text-center">
                                        <a href="theme/magazines/{{ $val->mag_pdf }}" target="_blank">{{ $val->mag_date}}</a>
                                    </article>
                                </div>
                            </div>
                           @endforeach
                           @else
                           <div class="row justify-content-center no-data">
                            <div class="col-md-6 text-center">
                                <h2 class="h2">No Data Available Now</h2>
                                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                <p>Thank you for visit us</p>
                            </div>
                            </div>
                           @endif
                            <!--/ col -->                                                   
                        </div>
                        <!--/ row -->
                    </div>