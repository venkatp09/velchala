  <!-- alert wishlist-->
                    <div id="alertAddWishlist" class="alert alert-success alert-dismissible wow animate__animated animate__fadeInUp" role="alert">
                        <strong><span class="icon-check-circle"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Wishlist.  <a href="javascript:void(0)" class="d-block"><strong>View Now</strong></a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!--/ alert wishlist -->
                        <!-- row -->
            <div id="result_publications_ajax2" class="row py-3">
@if(count($ajax_language))
@foreach($ajax_language as $value)
                  <?php  if(isset(auth()->user()->id)){ $userid=auth()->user()->id; }else { $userid=""; } ?>
                <?php  $wishlist = \App\Models\Wishlist::where(['publi_id'=>$value->b_id,'user_id'=>$userid])->get(); $wishlist= count($wishlist) ?>
                 <div class="col-6 col-sm-4 col-md-3">
                    <div class="book-item">
                        <figure class="bookcover">
                            <a href="{{ url('details',['catalias'=>$value->publication_slug]) }}">
                                <img src="theme/uploads/publications/{{$value->img}}" alt="" class="img-fluid">
                            </a>
                            @if(Auth::user())
                           
                            <div id="wishlist_{{$value->b_id}}" data-publ-id="{{$value->b_id}}" @if($wishlist>0) style="background-color:#007aff" class="wishlist-icon" @else class="wishlist-icon  wishlist-add"  @endif   data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                <span   class="icon-heart-o icomoon "></span>
                            </div>
                            @else
                            
                            <a href="{{url('login')}}"><div class="wishlist-icon"  data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                <span class="icon-heart-o icomoon"></span>
                            </div> </a>
                            @endif
                            
                        </figure>
                        <article class="text-center">
                            <a href="{{ url('details',['publications_name_url'=>$value->publication_slug]) }}">{{$value->publication_name}}</a>
                            
                        </article>
                    </div>
                </div>

      <!--/ col -->                           
              

@endforeach
@else
 
<div class="col-md-6 text-center no-data">
<h2 class="h2">No Data Available Now</h2>
<p>Currently We dont have any data you are looking, We will update you Soon, </p>
<p>Thank you for visit us</p>
</div>
</div>
@endif  
  </div>
               
               
                @if(count($ajax_language)<$publicationsTotal)
               
                <div class="row justify-content-center pb-4">
                <div class="col-lg-4 text-center">
                <a class="orange-btn" data-id="loadmore" data-count-value="{{ count($ajax_language) }}" id="loadmore_details" href="javascript:void(0)">Load More</a>
                </div>
                </div>
                
                @else
                @if($data_type!="lang" and $data_type!="publi")
                 <div class="row justify-content-center pb-4">
                <div class="col-lg-4 text-center">
                <a class="orange-btn" data-id="loadless" data-count-value="{{ count($ajax_language) }}" id="loadmore_details" href="javascript:void(0)">Load Less</a>
                </div>
                </div>
                @endif
                @endif

<script>
         
             $(".filter").change(function() {

            filter = $(this).val();
           
             filterType = $(this).attr("data-id");
             count=$(this).attr("data-count-value");
             filterlang = $('#filterlang').val();
             filterpubli = $('#filterpubli').val();
             data = 'filter='+filter+'&filterpubli='+filterpubli+'&filterlang='+filterlang+'&filterType='+filterType+"&_token=<?php echo csrf_token() ?>";
          
            $.ajax({
               type:'POST',
               url:'/get_langauage_ajax_data',
               data:data,
               success:function(results) {
                  
                  //$("#result_publications_ajax").html(results);
                  $("#result_publications_ajax").html(results.html);
                  $("#result_publications_ajax_count").html(results.count);
               }
            });
         });
         

         $(".wishlist-add").click(function() {

             publicationId = $(this).attr("data-publ-id");
             publi_id ="#wishlist_"+publicationId;
             data = 'publicationId='+publicationId+"&_token=<?php echo csrf_token() ?>";
           
            $.ajax({
              
               type:'POST',
               url:'/ajax_wish_list',
               data:data,
               success:function(results) {

                   $(publi_id).css('background-color','#007aff');
                   $(publi_id).removeClass('wishlist-add');
                   $("#alertAddWishlist-Detail").show();
               }
            });
         });
  
      $("#loadmore_details").click(function() {

             filterlang = $('#filterlang').val();
             filterpubli = $('#filterpubli').val();
             filterType = $(this).attr("data-id");
             filter='10';
             count=$(this).attr("data-count-value");
             data = 'filter='+filter+'&filterpubli='+filterpubli+'&filterlang='+filterlang+'&filterType='+filterType+'&count='+count+"&_token=<?php echo csrf_token() ?>";
            
             $.ajax({
               type:'POST',
               url:'/get_langauage_ajax_data',
               data:data,
               success:function(results) {
                 
                  //$("#result_publications_ajax").html(results);
                  $("#result_publications_ajax").html(results.html);
                  $("#result_publications_ajax_count").html(results.count);
               }
            });
         });
      </script>