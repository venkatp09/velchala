<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Viswanadha Jayanti Quarterly Magazine</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>                      
                        <li class="breadcrumb-item active" aria-current="page"><span>Magazines</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
            <!-- publications -->
            <div class="publications-list">
                <!-- sort -->
                <div class="sort">
                   <!-- continainer-->
                   <div class="container">
                       <!-- row -->
                       <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-sm-6 align-self-center">
                                <p id="count_mag" class="pb-0">{{ count($magazines) }} results</p>
                            </div>
                          
                             <div class="col-sm-6 justify-content-end">
                                 <div class="form-group mb-0 float-right">
                                     <select name="change_year" id="change_year"  class="form-control">
                                        <option>Choose Year</option>
                                        <option value="All">All</option>
                                        <option value="2004">2004</option>
                                        <option value="2005">2005</option>
                                        <option value="2006">2006</option>
                                        <option value="2007">2007</option>
                                        <option value="2008">2008</option>
                                        <option value="2009">2009</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                     </select>
                                 </div>
                             </div>
                            <!--/col -->
                       </div>
                       <!--/row -->
                   </div>
                   <!--/ container --> 
                </div>
                <!--/ sort -->

                <!-- publications list items -->
                <div id="result_publication" class="publications-items">
                    <!-- container -->
                    <div class="container">
                        <!-- row -->
                        <div class="row py-3">
                            <!-- col -->
                         @if(count($magazines)>0)
                         @foreach($magazines as $val)
                          <div class="col-6 col-sm-4 col-md-3 wow animate__animated animate__fadeInUp">
                                <div class="book-item">
                                    <figure class="bookcover">
                                        <a href="theme/uploads/magazines/{{ $val->mag_pdf }}" target="_blank">
                                            <img src="theme/uploads/magazines/{{ $val->mag_profile_pic }}" alt="" class="img-fluid">
                                        </a>  
                                    </figure>
                                    <article class="text-center">
                                        <a href="theme/magazines/{{ $val->mag_pdf }}" target="_blank">{{ $val->mag_date}}</a>
                                    </article>
                                </div>
                            </div>
                           @endforeach
                           @else
                           <div class="row justify-content-center no-data">
                            <div class="col-md-6 text-center">
                                <h2 class="h2">No Data Available Now</h2>
                                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                                <p>Thank you for visit us</p>
                            </div>
                            </div>
                           @endif
                            <!--/ col -->                                                   
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container -->
                </div>
                <!--/ publications list items -->
            </div>
            <!--/ publications -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
     <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
     </script>
    <script>
     $("#change_year").change(function() {

             
              change_year=$("#change_year").val();
            
              data = 'change_year='+change_year+"&_token=<?php echo csrf_token() ?>";

            
            $.ajax({
               type:'POST',
               url:'/ajax_magazines',
               data:data,
               success:function(results) {
                  
                  //$("#result_publications_ajax").html(results);
                  $("#result_publication").html(results.html);
                  $("#count_mag").html(results.count);
               }
            });
         });
         </script>
    @stop
  