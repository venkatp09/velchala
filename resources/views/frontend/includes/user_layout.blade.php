<!-- //style -->
<link rel="icon" type="image/png" sizes="32x32" href="{{ url('theme/img/fav.png') }}">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- animation components-->
<!--<link rel="stylesheet" href="{{ url('theme/css/animate-4.0.css')}}">-->

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Fade loading -->
<!--<link rel="stylesheet" href="{{ url('theme/css/animsition.css')}}">-->

<!-- Fade loading 
<link rel="stylesheet" href="theme/css/animsition.css">
 //style -->


  <!-- header -->
 <header class="fixed-top">
        <!-- gif animation slider -->
        <div class="gif-flag">
        <img src="theme/img/indian-flag-waving-gif-animation-8.gif">
        </div>
    <!--/ gif animation slider -->        
       
    <!-- cust container -->
    @if(Session::get('lang')=="")
    <div class="container position-relative">
        <div class="navbar navbar-expand-lg bsnav px-0">
          <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{ url('theme/img/logo.svg') }}" alt="" title="Velchala Kondal Rao">
          </a>
          <li class="nav-item search-icon"><a class="nav-link" id="searchIcon" href="#"><span class="icon-search icomoon"></span></a></li>  
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-md-end">
              <ul class="navbar-nav navbar-mobile mr-0">
                <li class="nav-item active"><a class="nav-link" href="{{url('/')}}">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('velchala')}}">Velchala</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('vsp')}}" data-toggle="tooltip" data-placement="top" title="Viswanatha Sahitya Peetham">VSP..</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('publications')}}">Publications</a></li>              
                <li class="nav-item dropdown zoom"><a class="nav-link" href="#">Jayanthi <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="{{route('magazines')}}">Magazine</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('jayanthi-events')}}">Jayanthi Events</a></li>                                           
                  </ul>
                </li>
                <li class="nav-item dropdown zoom"><a class="nav-link" >Gallery <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="{{route('gallery-poems')}}">Poems</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('photo-albums')}}">Photos</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('gallery-videos')}}">Videos</a></li>                        
                  </ul>
                </li>
                  <li class="nav-item dropdown zoom"><a class="nav-link" >Blogs <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog-article')}}">Articles</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog_news')}}">News</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog_interviews')}}">Interviews</a></li>                        
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog-events')}}">Events</a></li>                        
                  </ul>
                </li>
                                 
                <li class="nav-item"><a class="nav-link t-link animate__animated animate__headShake animate__infinite" href="{{route('hometelugu')}}"> <span>తెలుగు</span></a></li>  
                <li class="nav-item">
                    <a class="nav-link cart-link" href="{{url('cart')}}">
                        <span class="icon-shopping-cart icomoon"></span> 
                        <span class="value">0</span>
                    </a>
                </li>
                @if(Auth::user())
                <li class="nav-item dropdown zoom">
                     <a class="nav-link" href="#">
                          @if(Auth::user()->image!="")
                                    <img class="postlogin-namethumb" src="{{ url('theme/uploads/user_profile').'/'.Auth::user()->image}}" alt="{{ Auth::user()->name }}">
                                    @else
                                    <img class="postlogin-namethumb" src="{{ url('theme/uploads/user_profile/dummy.jpg') }}" alt="{{ Auth::user()->name }}">
                                    @endif  
                                    {{ substr(ucfirst(Auth::user()->name),0,8) }} <i class="caret"></i>
                       </a>
                        <ul class="navbar-nav">
                          <li class="nav-item"><a class="nav-link" href="{{route('userprofile')}}">My profile</a></li>
                         <!-- <li class="nav-item"><a class="nav-link" href="{{url('/user_orders')}}">My Orders</a></li>-->
                          <li class="nav-item"><a class="nav-link" href="{{route('wishList')}}">My Wishlist</a></li>
                          <li class="nav-item"><a class="nav-link" href="{{route('userChangePassword')}}">Change Password</a></li>      
                          <li class="nav-item"><a class="nav-link" href="{{route('userlogout')}}">Logout</a></li>                    
                        </ul>
                    </li>
                @else
                  <li class="nav-item"><a class="nav-link" href="{{route('userlogin')}}"><span class="icon-user-o"></span> Login</a></li>
                @endif
              </ul>
            </div> 
          </div>   
          <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
        </div>

          <!-- search section -->
          <div class="search-visible">
              <form class="d-flex justify-content-between m-0">
                  <div class="form-group">
                    <input type="text" id="autocomplete_search" placeholder="Search Your Fvourite Book" class="form-control">
                    <ul id="searchResult"></ul>
                  </div>
                  <div class="closeicon align-self-center">
                    <a href="javascript:void(0)" id="closeSearchIcon"><span class="icon-close icomoon"></span></a>
                  </div>
              </form>
          </div>
          <!--/ search section -->

        </div>
        <!--/ cust container -->
    @else
      
       <div class="container position-relative">
        <div class="navbar navbar-expand-lg bsnav px-0">
          <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{ url('theme/img/logo.svg') }}" alt="" title="Velchala Kondal Rao">
          </a>
          <li class="nav-item search-icon"><a class="nav-link" id="searchIcon" href="#"><span class="icon-search icomoon"></span></a></li>  
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-md-end">
              <ul class="navbar-nav navbar-mobile mr-0">
                <li class="nav-item active"><a class="nav-link" href="{{url('/')}}">సూచిక</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('velchala')}}">వెల్చాల</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('vsp')}}" data-toggle="tooltip" data-placement="top" title="విశ్వనాథ సాహిత్య పీఠం">VSP..</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('publications')}}">ప్రచురణలు</a></li>              
                <li class="nav-item dropdown zoom"><a class="nav-link" href="#">జయంతి  <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="{{route('magazines')}}">పత్రిక</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('jayanthi-events')}}">జయంతి ఈవెంట్స్</a></li>                                           
                  </ul>
                </li>
                <li class="nav-item dropdown zoom"><a class="nav-link" >గ్యాలరీ<i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="{{route('gallery-poems')}}">కవితలు</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('photo-albums')}}">ఫోటోలు</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('gallery-videos')}}">వీడియోలు</a></li>                        
                  </ul>
                </li>
                  <li class="nav-item dropdown zoom"><a class="nav-link" >బ్లాగ్<i class="caret"></i></a>
                  <ul class="navbar-nav">
                    
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog-article')}}">వ్యాసాలు</a></li
                     <li class="nav-item"><a class="nav-link" href="{{url('/blog_news')}}">వార్తలు</a></li> >
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog_interviews')}}">ఇంటర్వ్యూలు</a></li>    
                    <li class="nav-item"><a class="nav-link" href="{{url('/blog-events')}}">సంఘటనలు</a></li>

                                        
                                          
                  </ul>
                </li>
                                 
                <li class="nav-item"><a class="nav-link t-link animate__animated animate__headShake animate__infinite" href="{{route('homeenglish')}}"> <span>English</span></a></li>  
               
                @if(Auth::user())
                <li class="nav-item dropdown zoom">
                     <a class="nav-link" href="#">
                          @if(Auth::user()->image!="")
                                    <img class="postlogin-namethumb" src="{{ url('theme/uploads/user_profile').'/'.Auth::user()->image}}" alt="{{ Auth::user()->name }}">
                                    @else
                                    <img class="postlogin-namethumb" src="{{ url('theme/uploads/user_profile/dummy.jpg') }}" alt="{{ Auth::user()->name }}">
                                    @endif  
                                    {{ substr(ucfirst(Auth::user()->name),0,8) }} <i class="caret"></i>
                       </a>
                        <ul class="navbar-nav">
                          <li class="nav-item"><a class="nav-link" href="{{route('userprofile')}}">My profile</a></li>
                         <!-- <li class="nav-item"><a class="nav-link" href="{{url('/user_orders')}}">My Orders</a></li>-->
                          <li class="nav-item"><a class="nav-link" href="{{route('wishList')}}">My Wishlist</a></li>
                          <li class="nav-item"><a class="nav-link" href="{{route('userChangePassword')}}">Change Password</a></li>      
                          <li class="nav-item"><a class="nav-link" href="{{route('userlogout')}}">Logout</a></li>                    
                        </ul>
                    </li>
                @else
                  <li class="nav-item"><a class="nav-link" href="{{route('userlogin')}}"><span class="icon-user-o"></span> Login</a></li>
                @endif
              </ul>
            </div> 
          </div>   
          <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
        </div>

          <!-- search section -->
          <div class="search-visible">
              <form class="d-flex justify-content-between m-0">
                  <div class="form-group">
                    <input type="text" id="autocomplete_search" placeholder="Search Your Fvourite Book" class="form-control">
                    <ul id="searchResult"></ul>
                  </div>
                  <div class="closeicon align-self-center">
                    <a href="javascript:void(0)" id="closeSearchIcon"><span class="icon-close icomoon"></span></a>
                  </div>
              </form>
          </div>
          <!--/ search section -->

        </div>
    
    @endif
</header>
    <!--/ header -->


     


     

@yield('content')

<!-- left col -->
                <div class="col-md-4 col-sm-4">
                     <!-- user left nav -->
                    <div class="user-leftNav whitebox">
                        <h3 class="h5 text-uppercase fwhite bgorange p-2">{{ ucfirst(Auth::user()->name) }}</h3>
                        <ul>
                            <li>
                                <a href="{{route('userprofile')}}">Profile</a>
                            </li>
                            <!-- <li>
                                <a href="{{url('/user_manage_address')}}">Manage Address</a>
                            </li>
                           <li>
                                <a href="{{url('/user_orders')}}">Orders History</a>
                            </li>-->
                            <li>
                                <a href="{{route('wishList')}}">Wishlist Items</a>
                            </li>
                            <li>
                                <a href="{{route('userChangePassword')}}">Change Password</a>
                            </li>
                            <li>
                                <a href="{{route('userlogout')}}">Logout</a>
                            </li>
                        </ul>
                    </div>
                    <!--/ user left nav -->
                </div>


@yield('content2')



 @if(Session::get('lang')=="")
    <!--  footer -->
      <footer>
        <!-- container -->
        <div class="container">
            <!-- nav -->
            <ul class="nav justify-content-center aos-item" >
                <li class="nav-item">
                    <a href="{{url('/')}}" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}" class="nav-link">Velchala</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/vsp')}}" class="nav-link">VSP</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/publications')}}" class="nav-link">Publications</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}" target="_blank" class="nav-link">Blog</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/photo_albums')}}" class="nav-link">Photo</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/gallery_videos')}}" class="nav-link">Videos</a>
                </li>                
                <li class="nav-item">
                    <a href="{{url('/contact')}}" class="nav-link">Contact</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}" class="nav-link">Faq's</a>
                </li>
            </ul>
            <!--/ nav -->
            <p class="text-center"><i>© 2020 Velchala Kondal Rao. All rights reserved. </i></p>
            <ul class="nav justify-content-center termsnav aos-item">
                <li class="nav-item">
                    <a href="{{url('/velchala')}}" class="nav-link">Terms of Services</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}"  class="nav-link">Return Policy</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}"  class="nav-link">Privacy Policy</a>
                </li>
            </ul>
            <ul class="nav justify-content-center socialnav aos-item">
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-facebook icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-twitter icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-linkedin icomoon"></span></a>
                </li>
            </ul>            
        </div>
        <!--/container -->
        <a id="movetop" href="#" class="movetop"><span class="icon-arrow-up icomoon"></span></a>
    </footer>
    @else
    <!--  footer -->
      <footer>
        <!-- container -->
        <div class="container">
            <!-- nav -->
            <ul class="nav justify-content-center aos-item" >
                <li class="nav-item">
                    <a href="{{url('/')}}" class="nav-link">సూచిక</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}" class="nav-link">వెల్చాల</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/vsp')}}" class="nav-link">VSP</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/publications')}}" class="nav-link">ప్రచురణలు</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}" target="_blank" class="nav-link">బ్లాగ్</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/photo_albums')}}" class="nav-link">ఫోటోలు</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/gallery_videos')}}" class="nav-link">వీడియోలు</a>
                </li>                
                <li class="nav-item">
                    <a href="{{url('/contact')}}" class="nav-link">సంప్రదించండి</a>
                </li>
              <!--  <li class="nav-item">
                    <a href="{{url('/velchala')}}" class="nav-link">Faq's</a>
                </li>-->
            </ul>
            <!--/ nav -->
            <p class="text-center"><i>© 2020 Velchala Kondal Rao. All rights reserved. </i></p>
            <ul class="nav justify-content-center termsnav aos-item">
                <!--<li class="nav-item">
                    <a href="{{url('/velchala')}}" class="nav-link">Terms of Services</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}"  class="nav-link">Return Policy</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/velchala')}}"  class="nav-link">Privacy Policy</a>
                </li>-->
            </ul>
            <ul class="nav justify-content-center socialnav aos-item">
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-facebook icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-twitter icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-linkedin icomoon"></span></a>
                </li>
            </ul>            
        </div>
        <!--/container -->
        <a id="movetop" href="#" class="movetop"><span class="icon-arrow-up icomoon"></span></a>
    </footer>
     @endif
    <!--/ footer -->
    <!--/ footer -->

  <!-- scripts for jquery,  bootstrap and custom script files -->
  <!-- scripts for jquery,  bootstrap and custom script files -->
<script src="{{ url('theme/js/jquery-3.2.1.min.js')}}"></script>   
<script src="{{ url('theme/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ url('theme/js/popper.js')}}"></script>     
<!--  navigation -->     
<script src="{{ url('theme/js/bsnav.js')}}"></script>   

<!-- animate wowjs-->
<script src="{{ url('theme/js/wow.min.js')}}"></script>

<!-- file upload  -->
<script src="{{ url('theme/js/yt-video-background.min.js')}}" charset="utf-8"></script>

<!-- typing effect -->
<script src="{{ url('theme/js/typingEffect.js')}}"></script>
<!--[if lt IE 9]>
<script src="js/html5-shiv.js"></script>
<![end if ]-->
<script src="{{ url('theme/js/swiper.min.js')}}"></script>

<!-- script files for grid gallery -->
<script src="{{ url('theme/js/baguetteBox.js')}}"></script>    

<!-- responsive tab -->
<script src="{{ url('theme/js/easyResponsiveTabs.js')}}"></script>

<!-- custom script -->
<script src="{{ url('theme/js/custom.js')}}"></script>  

<!-- accordion -->
<script src="{{ url('theme/js/accordion.js')}}"></script>

<!--/ stepper -->
<script src="{{ url('theme/js/stepper.js')}}"></script>

<!-- file upload  -->
<script src="{{ url('theme/js/imageuploadify.min.js')}}"></script>

<!-- fade loading  -->
<script src="{{ url('theme/js/animsition.js')}}"></script>
<script>
  $(document).ready(function() {
    $('.animsition').animsition();
  });
  </script>
    <!-- hero slider -->
    <script src="{{ url('theme/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{ url('theme/js/owl.carousel.min.js')}}"></script> 
    <script src="{{ url('theme/js/jquery.nicescroll.min.js')}}"></script> 
    <script src="{{ url('theme/js/main.js')}}" ></script>
       

    <!-- book popup --> 
    <div class="modal fade" id="book-more" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Viswanatha A Literary legend</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- modal content -->
                <div class="modal-body">
                <!-- row -->
                <div class="row justify-content-around">
                    <!-- col -->
                    <div class="col-lg-5 text-center">
                        <img src="theme/img/coverpages/cover01.jpg" class="img-fluid" alt="">
                    </div>
                    <!--/col-->
                        <!-- col -->
                        <div class="col-lg-7">
                            <article class="book-pop-article">
                                <h6 class="h6 border-bottom d-flex justify-content-between">
                                    <span>Description</span>
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Add to Wishlist" >
                                        <span class="icon-heart"></span>
                                    </a>
                                </h6>
                                <p class="pb-0 mb-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus asperiores earum suscipit. Dignissimos laudantium quasi eius, unde qui consequatur </p>
                            </article>
                            <dl class="list-dl">
                                <dt>Price:</dt>
                                <dd>
                                    <p class="price">
                                        <span class="offer">275</span>
                                        <span class="oldprice">325</span>
                                    </p>
                                </dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Author:</dt>
                                <dd>Velchala Kondal Rao</dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Publisher:</dt>
                                <dd>Velchala Publications</dd>                           
                            </dl>                      
                            <dl class="list-dl">
                                <dt>No of Pages:</dt>
                                <dd>278</dd>                           
                            </dl>
                            <dl class="list-dl">
                                <dt>Language:</dt>
                                <dd>English</dd>                           
                            </dl>

                            <!--buttons -->
                            <div class="pt-2">
                                <a href="javascript:void(0)" class="orange-btn">Read Online</a>
                                <a href="cart.php" class="orange-btn">Add to Cart</a> 
                            </div>
                            <!--buttons -->
                        </div>
                        <!--/col-->
                </div>
                <!--/ row -->
                </div>    
                <!--/ modal content -->    
            </div>
        </div>
    </div>
    <!--/ book popup-->
 
 <script>
    //add class to header on scroll
    $(window).scroll(function(){
        if($(this).scrollTop() > 50){
            $('.ashokaChakra').addClass('DynamicChakra');
            
        }else{
            $('.ashokaChakra').removeClass('DynamicChakra');
        }
    });
 </script>
 <script type="text/javascript">
    

$( "#autocomplete_search" ).keyup(function(){

    searchValue = $(this).val();
    if(searchValue.length!=0){
        data = 'searchValue='+searchValue+"&_token=<?php echo csrf_token() ?>";
       $("#searchResult").append("Loading ...");
        $.ajax({

            url: "/autocomplete_course",
            type: 'post',
            dataType: "json",
            data: data,
            success: function( response ) {      
             
               var len = response.length; 
               
               $("#searchResult").empty(); 
               $("#searchResult").append("<ul style='list-style: none;padding: 0;margin: 0;text-align: left;border: 1px solid #bbb;border-bottom: none;'>");
          if(response.length>0){
          for( var i = 0; i<len; i++){ 
             
          var url = response[i]['url'];  
          var fname = response[i]['name'];
          
          url="/details/"+url;
          $("#searchResult").append("<li><a href='"+url+"'>"+fname+"</a></li>");    
           
           }
       }else{
           
             $("#searchResult").append("No Books Found");  
          }
      }
    });
   }else{
      
    $("#searchResult").html("No Books Found");

   }
});
</script>