<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
      @extends('includes.user_layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Manage Address</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('user_profile_information')}}">{{ ucfirst(Auth::user()->name) }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Manage Address</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                 @stop
             @section('content2')
                <div class="col-md-8 col-sm-8">
                    <!-- right profile detail -->
                    <div class="user-profile-rt">

                      <div class="text-right">
                         @if(session('success'))
       
         <strong style="color: green;">{{session('success')}}</strong>
        
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
     @endif
                          <a data-toggle="modal" data-target="#newAddress" href="javascript:void(0)" class="orange-btn-border">+ Add Address</a>
                      </div>
                      
                      @if(count($user_address))
                      @foreach($user_address as $value) 
                      <!-- address block -->
                      <div class="p-4 border my-3 address-block wow animate__animated animate__fadeInDown">
                            <p>
                              <span class="fsbold">Name:</span>
                              <span class="flight">{{ ucfirst($value->first_name) }}</span>
                            </p>
                            <p>
                              <span class="fsbold">Address:</span>
                              <span> {{ $value->address1 }},{{ $value->pin_number }}, {{ $value->city }}, {{ $value->state }}.</span>
                            </p>
                             <p>
                               <span class="fsbold">Landmark:</span>
                                <span>{{ ucfirst($value->landmark) }}</span>                             
                            </p>
                            <p>
                               <span class="fsbold">Email:</span>
                                <span>{{ $value->email }}</span>                             
                            </p>
                            <p>
                                <span class="fsbold">Phone:</span>
                                <span>{{ $value->mobile_number }}</span>                             
                            </p>
                            <p class="text-right">
                                <a data-toggle="modal"  data-target="#newAddress" 
                                data-first-name="{{ $value->first_name }}"
                                data-address="{{ $value->address1 }}"
                                data-city-name="{{ $value->city }}"
                                data-pin_number="{{ $value->pin_number }}"
                                data-state="{{ $value->state }}"
                                data-landmark="{{ $value->landmark }}"
                                data-email="{{ $value->email }}"
                                data-mobile_number="{{ $value->mobile_number }}"

                                 href="javascript:void(0)" class="get_data_address"><span  class="icon-edit icomoon mr-1"></span>Edit</a>
                                <a href="{{ url('address-delete',['id'=>$value->id]) }}" class="pl-3 "  onclick="return confirm_delete();"><span class="icon-trash-o icomoon mr-1"></span>Delete</a>
                            </p>
                      </div>
                      <!--/ address block -->
                      @endforeach
                      @else
                              <div class="col-md-6 text-center no-data">
                        <h2 class="h2">No Data Available Now</h2>
                        <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                        <p>Thank you for visit us</p>
                    </div>
                    </div>
                    @endif       
                      
                      

                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
    <div class="modal fade" id="newAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form  action = "{{url('/address-update')}}"  enctype="multipart/form-data" method = "post">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>"> 
      <div class="modal-body">

      <!--form-->
       
          <!-- row -->
          <div class="row">
              <!-- col -->
              <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="firstName">First Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="firstName" name="firstname" aria-describedby="firstName" placeholder="First Name" required>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
              <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="secondName">Second Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="secondName" name="secondname" aria-describedby="firstName" placeholder="Second Name" >
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              
               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="address01">Address Line 01</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="address01" name="address1" aria-describedby="address01" placeholder="Address Line 01" required>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="address02">Address Line 02</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="address02" name="address2" aria-describedby="address02" placeholder="Address Line 02">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="pinNumber">Pin Number</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="pinNumber" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="pin" aria-describedby="pinNumber" placeholder="Write Pin Number" required>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              <!-- col -->
              <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="landmark">Land Mark</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="landmark" name="land_mark" aria-describedby="landmark" placeholder="Ex:Beside Hospital">
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="city">City</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="city" name="city" aria-describedby="city" placeholder="Ex:Hyderabad" required>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              
               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="State">State</label>
                        <div class="input-group">
                            <select id="State" name="state" class="form-control" required>
                                <option selected>Choose...</option>
                                <option>Telangana</option>
                                <option>Andhra Pradesh</option>
                                <option>Tamil Nadu</option>
                                <option>Karnataka</option>
                                <option>Kerala</option>
                                <option>Madhya Pradesh</option>
                            </select>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

              
               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="mNumber">Mobile Number</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="mNumber" onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" name="number" aria-describedby="mNumber" placeholder="Mobile Number" required>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

               <!-- col -->
               <div class="col-lg-6">
                   <!-- form group -->
                   <div class="form-group">
                        <label for="email">Email</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="email"  name="email" aria-describedby="email" placeholder="Enter Valid Email Address" required>
                        </div>
                    </div>
                    <!-- /form group -->
              </div>
              <!--/ col -->

          </div>
          <!--/ row -->
    
       
      </div>
      <div class="modal-footer">       
        <button type="submit" class="orange-btn">Save changes</button>
         <!-- <button type="submit" class="btn btn-primary">Save changes</button> -->
      </div>
        </form>
      <!--/ form -->
    </div>
  </div>
   
</div>
   <script type="text/javascript">
    
         $(".get_data_address").click(function() {
            
             data_first_name = $(this).attr("data-first-name");
             data_state_name = $(this).attr("data-state");
             data_address = $(this).attr("data-address");
             data_email = $(this).attr("data-email");
             data_landmark = $(this).attr("data-landmark");
             alert(data_landmark);
             
          }
   </script>
   @stop

   <!--image upload popup -->
   <!-- Modal -->

   <!--/image upload popup -->
   
    </body>
        <script>
     function confirm_delete(){

       confirmDelete = confirm("R u sure want to delete");

       if(confirmDelete){

          return true;
       }
       else{

        return false;
       }
}
</script>
</html>