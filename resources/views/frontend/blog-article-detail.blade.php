<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ $blog_articles->meta_title}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{{  ucfirst($blog_articles->meta_description)}} "/> 
<meta name="keywords" content="{{  ucfirst($blog_articles->meta_keywords)}}" />
<meta name="robots" content="index, follow"/>
<meta name="copyright" content="Velchala" />
<meta name="author" content="Velchala" />
<meta name="language" content="en_US" />

<meta property="og:url" content="{{ url('blog-article-detail',['catalias'=>$blog_articles->blog_article_url]) }}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{  ucfirst($blog_articles->meta_title)}}" /> 
<meta property="og:description" content="{{  ucfirst($blog_articles->meta_description)}}" /> 


      @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
            
            <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Blog Articles Details</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item"><a href="{{route('blog-article')}}">Blog Articles</a></li>                    
                         <li class="breadcrumb-item active" aria-current="page"><span></span>{{ $blog_articles->blog_article_name }}</li>                    
                        
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
    <!-- container -->
    <div class="container">
        <!-- card -->

        <div class="card blog-detail">
            <!-- card header -->
            <div class="card-header">
                <!-- row -->
                <div class="row justify-content-center wow animate__animated animate__fadeInUp">
                    <div class="col-lg-8">
                        <div class="d-flex justify-content-between pb-3">
                            <a href="{{ url('blog-article') }}" class="fblue"><span class="icon-arrows"></span> Back to Articles</a>
                            <!--<a href="{{ url('theme/uploads/blogs_articles').'/'.$blog_articles->blog_article_pdf }}" download class="">Download <span class="icon-cloud-download icomoon"></span> </a>-->
                        </div>
                        <h1 class="h1 pb-3">{{ $blog_articles->blog_article_name }}</h1>
                        <p class="pb-0 mb-0">Posted on <span class="fsbold">{{ date("d-m-Y",strtotime($blog_articles->created_at)) }}</span></p>

                         <!-- social share -->
                        <div class="social-share py-3 d-flex">
                            <span class="pt-1 span-share">Share this Article</span>
                            
                                    <a target="__blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url('blog-article-detail',['catalias'=>$blog_articles->blog_article_url]) }}" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                                    <a target="__blank" href="https://twitter.com/home?status={{ url('blog-article-detail',['catalias'=>$blog_articles->blog_article_url]) }}" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                                    
                                    <a target="__blank" href="https://api.whatsapp.com/send?text={{ url('blog-article-detail',['catalias'=>$blog_articles->blog_article_url]) }}" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>
                                    
                        </div>
                        <!--/ social share -->
                        <p></p>
                    </div>
                    
                </div>
                <!--/ row -->
            </div>
            <!-- card header -->

             <!-- card body -->
             <div class="card-body">              

                <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <!-- responsive tab -->
                    <div class="">
                        <!--<h4>Me unpleasing impossible</h4>-->
                        <p>{!! $blog_articles->blog_article_desc !!}</p>
                    </div>
                    <!--/ responsive tab -->
                       
                    </div>
                </div>
                <!--/ row -->
             </div>
            <!-- card body -->


        </div>
        <!--/card -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    @stop
