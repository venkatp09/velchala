<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
     @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Poems</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Gallery</a></li>                   
                        <li class="breadcrumb-item active" aria-current="page"><span>Poems</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">
           <!-- container -->
           <div class="container gallery-block grid-gallery">
                <!-- row -->
                <div class="row">
                  @if(count($poems))
                  @foreach($poems as $value) 
                  <div class="col-6 col-sm-6 col-md-3 item wow animate__animated animate__fadeInDown">
                        <a class="lightbox" href="theme/uploads/gallery/{{ $value->poem_image }}">
                            <img class="img-fluid image scale-on-hover" src="theme/uploads/gallery/{{ $value->poem_image }}">
                        </a>
                  </div>
                  @endforeach
                  @else
                  <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p class="text-center">Currently We dont have any data you are looking, We will update you Soon, </p>
                <p class="text-center">Thank you for visit us</p>
            </div>
            </div>
            @endif     

               </div>
               <!--/ row -->
           </div>
           <!--/ container -->
       </div>
       <!--/ sub page body -->
    </main> 
    <!--/ main-->
   @stop
