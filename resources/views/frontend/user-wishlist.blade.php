<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    
      @extends('frontend.includes.user_layout')

  @section('content')

     <!-- alert wishlist-->
     <div id="alertAddrtoCart" class="alert alert-success alert-dismissible" role="alert">
        <strong><span class="icon-check-circle"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your Cart List.  <a href="javascript:void(0)" class="d-block"><strong>View Cart Items</strong></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <!--/ alert wishlist -->
    <!--main-->   
    <main class="subpage-main">
       <!-- header sub page -->
       <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Wishlist Items</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('user_profile_information')}}">{{ ucfirst(Auth::user()->name) }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Wishlist Items</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
       <!--/ hedaer sub page -->

       <!-- sub page body -->
       <div class="subpage-body">

       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row py-3 userprofile-row">
                  @stop
             @section('content2')
                <div class="col-md-8 col-sm-8">
                 
                    <!-- right profile detail -->
                   <div class="user-profile-rt">
                     @if(session('flash_message'))
                                    
                  <strong style="color: green;">{{session('flash_message')}}</strong>
                                  
                @endif
 @if(count($wishlist))
 @foreach($wishlist as $value) 
                    <!-- orders list item -->
                    <div class="myorder-list-item wishlistItem py-3 mb-0">                     

                        <!-- secondary details row -->
                        <div class="row wow animate__animated animate__fadeInDown">
                            <!-- col -->
                            <div class="col-md-2">
                                <a href="{{ url('details',['catalias'=>$value->getProduct->publication_slug]) }}">
                                    <img src="{{ url('theme/uploads/publications').'/'.$value->getProduct->img }}" alt="" class="img-fluid">
                                </a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-10">
                                <p class="fsbold">
                                    <a class="fblack" href="{{ url('details',['catalias'=>$value->getProduct->publication_slug]) }}">{{ ucfirst($value->getProduct->publication_name) }}</a>
                                </p>
                                @if($value->getProduct->stock='in_stock')
                                <p class="text-success">Instock</p>
                                @else
                                <p class="text-success">Out Of stock</p>
                                @endif
                               <!-- <p class="price">
                                    <span class="offer">{{ $value->getProduct->dis_price }}</span>                                    
                                </p>-->
                                <p class="text-right">
                                   <!-- <a  href="javascript:void(0)" data-publ-id="{{ $value->p_id }}" class="addtoCart-icon fblack"><span   class="icon-shopping-cart icomoon mr-1 "></span>Add to Cart</a>-->
                                    <a onclick="return confirm(&quot;Confirm Delete?&quot;)" href="{{route('wishList',['catalias'=>$value->id])}}" class="pl-3  fblack"><span class="icon-trash-o icomoon mr-1"></span>Delete</a>
                                </p>                                
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ secondary details row -->                        
                    </div>
                    
                   @endforeach
                   @else
                   <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                <p>Thank you for visit us</p>
            </div>
            
            @endif       
                    </div>  
                    </div>
                    <!--/ right profile detail -->
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
       </div>
       <!-- /container -->   
                

       </div>
       <!--/ sub page body -->
    </main> 
    <div id="alertAddtocart" class="alert alert-success alert-dismissible wow animate__animated" role="alert">
<strong><span class="icon-check"></span> Success!</strong> Your Product <strong>Viswanatha A Literary Legand </strong> Added Successfully to your cartlist.  <a class="d-block" href="{{url('cart')}}"><strong>View Now</strong></a>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
    <!--/ main-->
     
    <script>
         $(".addtoCart-icon").click(function() {
            
             publicationId = $(this).attr("data-publ-id");
             
             cart_quanity = 1;
             data = 'publicationId='+publicationId+'&cart_quanity='+cart_quanity+"&_token=<?php echo csrf_token() ?>";
            
            $.ajax({
              
               type:'POST',
               url:'/add_to_cart_data_ajax',
               data:data,
               success:function(results) {
                  
                  
                   $('#alertAddtocart').show();
                  
               }
            });
         });

      </script>
    @stop
   <script>
    $(document).ready(function(){        
        $('.addtoCart-icon').click(function(){
            $('#alertAddrtoCart').show();
        }) 
    });
   </script>    
