<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
     
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    @extends('frontend.includes.layout')

  @section('content')
    <!--main-->   
    <main class="subpage-main">
      <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <h1>Articles</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>   
                        <li class="breadcrumb-item active" aria-current="page">Articles</li>                    
                        
                    </ol>
                </nav>
            </div>
            <!--/ container -->
       </div>
    <!-- container -->
    <div class="container">
       <!-- row -->
       <div class="row justify-content-center">
           <!-- card -->
          @if(count($blog_articles)>0)
          @foreach($blog_articles as $value)
            <div class="col-lg-4">
                <div class="card blogcard articlecard">
                    <div class="card-header">	
                       {{ $value->blog_article_name }}
                        @if($loop->iteration<5)
                        <span class="badge badge-success">New</span>
                        @endif
                    </div>
                    <div class="card-body">
                    <p> {!! substr($value->blog_article_desc, 0,100) !!}..</p>
                    <a href="{{ url('blog-article-detail',['url'=>$value->blog_article_url]) }}" class="btn orange-btn">Read More</a>
                    </div>
                </div>              
            </div>
          @endforeach
          @else 
           <div class="col-md-6 text-center no-data">
                <h2 class="h2">No Data Available Now</h2>
                <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                <p>Thank you for visit us</p>
            </div>
            
            @endif   
          </div> 
            <!--/ card -->           

       </div>
       <!--/ row -->
    </div>
    <!--/ container -->
    </main> 
    <!--/ main-->   

    @stop
 