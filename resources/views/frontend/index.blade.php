<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <title>Velchala Kondal Rao</title>
  @extends('frontend.includes.layout')

  @section('content')


    <!--main-->    
	<!-- Hero section -->
	<section class="home-slider">   
        <!-- <div class="ashokaChakra">
            <img src="theme/img/ashokaChakra.svg" alt=>
        </div>   -->

         <!-- video -->
        <div class="video-section">
            <div class="text-video">
                <h1 class="mask">Welcome to velchala.com</h1>
            </div>
            <!-- <video autoplay muted loop>
                <source src="img/Night-Traffic.mp4" type="video/mp4">
                Your browser does not support HTML5 video.
            </video> -->
            <div class="video-background"></div>
          
        </div>

        <!--/ video -->		
	
	</section>
    <div class="great-people">
        <!-- Swiper -->
       
        <div class="swiper-container greatpeople-in">
            <div class="swiper-wrapper">
               
                    <div class="swiper-slide"><img src="theme/img/great/great01.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great02.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great03.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great04.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great05.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great06.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great07.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great08.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great09.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great10.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great11.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great12.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great13.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great14.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great15.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great16.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great17.png" alt="great01.png"></div>       
                    <div class="swiper-slide"><img src="theme/img/great/great18.png" alt="great01.png"></div>       
                         
                    
            </div>
        <!-- Add Pagination -->
        <!-- <div class="swiper-pagination"></div> -->
        </div>
    </div>
    

    <!-- vsp and velchala -->
    <div class="vspsection">
        <!-- container fluid -->
        <div class="container-fluid px-0">
            <!-- row -->
            <div class="row no-gutters">
                <!-- col -->
                <div class="col-12 col-sm-12 col-md-6 vspcol vspleft">
                    <div class="article-div align-self-center">
                        <h6 class="h6">Dr. Velchala Kondal rao</h6>
                        <h3>Personal Profile</h3>
                        <p class="pb-3">Dr. Rao is basically an educationist, known for his pioneering mind and promotinal drive and initiative. He has been the architect of many educational institutions in Andhra Pradesh. Left an indelible mark wherever he worked and whatever positions he held. The last and the last but one positions he held in the State of Andhra Pradesh were Director, telugu Academy and Joint Director, Higher Education.</p>
                        <a href="{{url('velchala')}}" class="orange-btn">Read More</a>
                    </div>
                    <div class="img-div align-self-center">
                        <img src="theme/img/velchalapng.png" alt="" class="img-fluid">
                    </div>
                </div>
                <!--/ col -->
               <!-- col -->
               <div class="col-12 col-sm-12 col-md-6 vspcol vsprt">
                <div class="article-div align-self-center">
                    <h6 class="h6">Vishwanatha Sahitya Peetham</h6>
                    <h3>Founded by Velchala</h3>
                    <p>A literary cum cultual organization namely <b>"Vishwanatha Shitya Peetham"</b> is established by "<b>Sister Nivedita Foundation</b>"  at Red Hills, Hyderabad to function with effect from the month of july 2003. 

</p>

                  
                    <a href="{{url('vsp')}}" class="orange-btn">Read More</a>
                </div>
                <div class="img-div">
                    <img src="theme/img/vsp-png.png" alt="" class="img-fluid">
                </div>
            </div>
            <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!-- /vsp and velchala -->

   
    <!--/ publications -->
    <div class="publications">
        <!-- custom container -->
        <div class="cust-container">
             <!-- title -->
             <div class="title-section wow animate_animated animate_fadeIn">
                <h4 class="h4">Publications</h4>
                <p>Written by Dr. Velchala Kondal Rao</p>               
            </div>
            <!--/ title -->
           
            <!-- books publications -->           
            <div class="swiper-container home-publications">
                <div class="swiper-wrapper ">
                    <!-- slide -->
                    @if(count($publications)>0)
                    @foreach($publications as $value)
                    <div class="swiper-slide">
                        <div class="img-box">
                            <img src="theme/uploads/publications/{{$value->img}}" alt="{{$value->publication_name}}" class="img-fluid">
                            <!--hover -->
                            <div class="hover-section">                               
                                <a  href="{{ url('details',['publications_name_url'=>$value->publication_slug]) }}" ><span  data-publ-id="{{ $value->publi_id }}" class="icon-search icomoon"></span></a>
                            </div>
                            <!--/ hover-->
                        </div>                        
                    </div>
                    @endforeach
                    @else
                    <div class="row justify-content-center no-data">
                    <div class="col-md-8 text-center">
                    <h2 class="h2">No Data Available Now</h2>
                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                    <p>Thank you for visit us</p>
                    </div>
                    </div>
                    @endif
                    
                    <!--/ slide -->  
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
            <!-- /books publications -->
        </div>
        <!--/ custom container -->
        <p class="text-center pt-5">
            <a href="{{ url('/publications')}}" class="orange-btn mx-auto wow animate_animated animate_slideInUp">View More</a>
       </p>
    </div>

    <!-- gallery -->
    <div class="home-gallery">
        <!-- custom container -->
        <div class="cust-container gallery-block grid-gallery">
            <!-- title -->
            <div class="title-section  wow animate_animated animate_slideInUp">
                <h4 class="h4">Gallery</h4>
                <p>Images gallery by VElchala</p>               
            </div>
            <!--/ title -->

            <!-- gallery images -->
           <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- item -->
                    @if(count($photos)>0)
                    @foreach($photos as $val)
                    <div class="col-6 col-sm-6 col-md-3 item  wow animate_animated animate_fadeInUp">
                        <a class="lightbox" href="theme/uploads/photos/{{$val->img}}">
                            <img class="img-fluid image scale-on-hover" src="theme/uploads/photos/{{$val->img}}">
                        </a>
                    </div>
                    
                    @endforeach
                    @else
                    <div class="row justify-content-center no-data">
                    <div class="col-md-8 text-center">
                    <h2 class="h2">No Data Available Now</h2>
                    <p>Currently We dont have any data you are looking, We will update you Soon, </p>
                    <p>Thank you for visit us</p>
                    </div>
                    </div>
                    @endif
                     
                    <!-- item -->                    
                </div>
                <!--/row -->               
           </div>
            <!--/ gallery images -->
           <p class="text-center pt-3">
                <a href="{{url('photo-albums')}}" class="orange-btn mx-auto  wow animate_animated animate_fadeIn">View all Photo Gallery</a>
           </p>
        </div>
        <!--/ customcontainer -->        
    </div>
    <!--/ gallery -->
    <!--/ gallery -->

    <!-- poems -->
       <!-- poems -->
    <div class="home-poems">
        <!-- custome container -->
        <div class="cust-container">
            <h5 class="h5 wow animate_animated animate_slideInDown">Poems Written by Velchala</h5>

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center wow animate_animated animate_slideInUp">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <!-- swiper -->
                        <!-- Swiper -->
                            <div class="swiper-container poems-container">
                                <div class="swiper-wrapper">
                                    
                                    <?php 
                                        for($i=0;$i<count($homeTestimonials);$i++) { ?>
                                    <div class="swiper-slide">
                                        <article>
                                            <p>" <?php echo $homeTestimonials[$i][0]?>"</p>
                                        </article>
                                    </div>   
                                        <?php } ?> 
                                                                                   
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        <!--/ swiper -->
                    </div>
                    <!-- col -->
                </div>
                <!--/row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ custom container -->
    </div>
   

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
     <script>
         $("#add_fav").change(function() {
            
             language_filter = $("#language_filter").val();
             data = 'language_filter='+language_filter+"&_token=<?php echo csrf_token() ?>";

            $.ajax({
               type:'POST',
               url:'/get_langauage_ajax_data',
               data:data,
               success:function(results) {
                  
                  $("#result_publications_ajax").html(results);
               }
            });
         });
      </script>
      <script>
         $(".get_popup").click(function() {
            
             popupId = $(this).attr("data-publ-id");
             data = 'popupId='+popupId+"&_token=<?php echo csrf_token() ?>";
              
            $.ajax({
               type:'POST',
               url:'/ajax_get_popup',
               data:data,
               success:function(results) {
                  
                  
                  $("#result_publications_ajax_popup").html(results);
               }
            });
         });
      </script>
      

    @stop 
