<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    
<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
<!-- style sheets bootstrap and common styles -->
<link rel="stylesheet" href="{{ url('theme/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/style.css')}}">
<link rel="stylesheet" href="{{ url('theme/css/icomoon.css')}}">

<!-- file upload -->
<link rel="stylesheet" href="{{ url('theme/css/imageuploadify.min.css')}}">

<!-- bootstrap nav -->
<link rel="stylesheet" href="{{ url('theme/css/bsnav.css')}}">

<!-- style sheets grid gallery -->
<link rel="stylesheet" href="{{ url('theme/css/grid-gallery.css')}}>
<link rel="stylesheet" href="{{ url('theme/css/baguetteBox.css')}}">

<!-- style sheets for swipe and carousels -->
<link rel="stylesheet" href="{{ url('theme/css/swiper.min.css')}}">   

<!-- responsive tab -->
<link rel="stylesheet" href="{{ url('theme/css/easy-responsive-tabs.css')}}">

<!-- background video -->
<link rel="stylesheet" href="{{ url('theme/css/yt-video-background.css')}}">
    
</head>
<body>

  
    <!--main-->   
    <main class="subpage-main pt-0 bookView">       
       <!-- sub page body -->
       <div class="subpage-body ">
            <!-- container -->
            <div class="container">
                <!-- <embed id="fraDisabled" src="img/readbook.pdf" type="application/pdf" width="100%" height="800px"> -->
                <object width="100%" height="800px" type="application/pdf" 
                    data="{{ url('theme/uploads/publications').'/'.$publications->pdf_file }}" id="pdf_content"><p>Document load was not 
                    successful.</p>
                </object>
            </div>
            <!--/ container -->

       </div>
       <!--/ sub page body -->



    </main> 
    <!--/ main-->
    
    <!-- scripts for jquery,  bootstrap and custom script files -->
<script src="{{ url('theme/js/jquery-3.2.1.min.js')}}"></script>   
<script src="{{ url('theme/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ url('theme/js/popper.js')}}"></script>     
<!--  navigation -->     
<script src="{{ url('theme/js/bsnav.js')}}"></script>   

<!-- file upload  -->
<script src="{{ url('theme/js/yt-video-background.min.js')}}" charset="utf-8"></script>

<!-- typing effect -->
<script src="{{ url('theme/js/typingEffect.js')}}"></script>
<!--[if lt IE 9]>
<script src="js/html5-shiv.js"></script>
<![end if ]-->
<script src="{{ url('theme/js/swiper.min.js')}}"></script>

<!-- script files for grid gallery -->
<script src="{{ url('theme/js/baguetteBox.js')}}"></script>    

<!-- responsive tab -->
<script src="{{ url('theme/js/easyResponsiveTabs.js')}}"></script>

<!-- custom script -->
<script src="{{ url('theme/js/custom.js')}}"></script>  

<!-- accordion -->
<script src="{{ url('theme/js/accordion.js')}}"></script>

<!--/ stepper -->
<script src="{{ url('theme/js/stepper.js')}}"></script>

<!-- file upload  -->
<script src="{{ url('theme/js/imageuploadify.min.js')}}"></script>
    
    
    </body>
</html>