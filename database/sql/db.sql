

INSERT INTO `categories` (`category_id`, `category_name`, `category_alias`, `category_parent_id`, `category_options`, `category_image`, `category_order`, `category_status`, `created_at`, `updated_at`) VALUES
(1, 'Cakes', 'cakes', '0', NULL, '1553670530cakes-navigation-image.jpg', '1', 'active', '2019-03-27 08:38:50', '2019-03-27 08:38:50'),
(2, 'Flower Bouquets', 'flower-bouquets', '0', NULL, '1553670575flowers-boutique-navigation-img.jpg', '2', 'active', '2019-03-27 08:39:35', '2019-03-27 08:39:35'),
(3, 'Gifts', 'gifts', '0', NULL, '1553670602gifts-navigation-image.jpg', '3', 'active', '2019-03-27 08:40:02', '2019-03-27 08:40:02'),
(4, 'Chocolates', 'chocolates', '0', NULL, '1553670620chocklates-navigation-image.jpg', '4', 'active', '2019-03-27 08:40:21', '2019-03-27 08:40:21'),
(5, 'Jewellary', 'jewellary', '0', NULL, '1553670644jewellery-navigation-image.jpg', '5', 'active', '2019-03-27 08:40:44', '2019-03-27 08:40:44'),
(6, 'Millets', 'millets', '0', NULL, '1553670670millets-navigation-img.jpg', '6', 'active', '2019-03-27 08:41:10', '2019-03-27 08:41:10'),
(7, 'Delicious Cakes', 'delicious-cakes', '1', 'types', NULL, '0', 'active', '2019-03-27 08:42:32', '2019-03-27 08:42:32'),
(8, 'Designer Cakes', 'designer-cakes', '1', 'types', NULL, '0', 'active', '2019-03-27 08:43:06', '2019-03-27 08:43:06'),
(9, 'Photo Cakes', 'photo-cakes', '1', 'types', NULL, '0', 'active', '2019-03-27 08:43:19', '2019-03-27 08:43:19'),
(10, 'Eggless Cakes', 'eggless-cakes', '1', 'types', NULL, '0', 'active', '2019-03-27 08:43:53', '2019-03-27 08:43:53'),
(11, 'Heart Shaped Cakes', 'heart-shaped-cakes', '1', 'types', NULL, '0', 'active', '2019-03-27 08:44:04', '2019-03-27 08:44:04'),
(12, 'Black Forest Cakes', 'black-forest-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:44:20', '2019-03-27 08:44:20'),
(13, 'Butterscotch Cakes', 'butterscotch-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:44:38', '2019-03-27 08:44:38'),
(14, 'Chocklate Cakes', 'chocklate-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:44:51', '2019-03-27 08:44:51'),
(15, 'Fruit Cakes', 'fruit-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:45:06', '2019-03-27 08:45:06'),
(16, 'Pineapple Cakes', 'pineapple-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:45:28', '2019-03-27 08:45:28'),
(17, 'Red Velvet Cakes', 'red-velvet-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:45:39', '2019-03-27 08:45:39'),
(18, 'Coffee Cakes', 'coffee-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:45:57', '2019-03-27 08:45:57'),
(19, 'Strawberry Cakes', 'strawberry-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:46:10', '2019-03-27 08:46:10'),
(20, 'Vanilla Cakes', 'vanilla-cakes', '1', 'others', NULL, '0', 'active', '2019-03-27 08:46:21', '2019-03-27 08:46:21'),
(21, 'Roses', 'roses', '2', 'types', NULL, '0', 'active', '2019-04-17 21:49:09', '2019-04-17 21:49:09'),
(22, 'Carnations', 'carnations', '2', 'types', NULL, '0', 'active', '2019-04-17 21:50:07', '2019-04-17 21:50:07'),
(23, 'Gerberas', 'gerberas', '2', 'types', NULL, '0', 'active', '2019-04-17 21:50:43', '2019-04-17 21:50:43'),
(24, 'Lillies', 'lillies', '2', 'types', NULL, '0', 'active', '2019-04-17 21:51:13', '2019-04-17 21:51:13'),
(25, 'Orchids', 'orchids', '2', 'types', NULL, '0', 'active', '2019-04-17 21:51:32', '2019-04-17 21:51:32'),
(26, 'Mixed Flowers', 'mixed-flowers', '2', 'types', NULL, '0', 'active', '2019-04-17 21:52:04', '2019-04-17 21:52:04'),
(27, 'Custom Flowers', 'custom-flowers', '2', 'types', NULL, '0', 'active', '2019-04-17 21:52:27', '2019-04-17 21:52:27'),
(28, 'Anniversary', 'anniversary', '2', 'others', NULL, '0', 'active', '2019-04-17 21:52:59', '2019-04-17 21:52:59'),
(29, 'Personalized Gifts', 'personalized-gifts', '3', 'types', NULL, '0', 'active', '2019-04-17 21:55:31', '2019-04-17 21:55:31'),
(30, 'Soft Toys', 'soft-toys', '3', 'types', NULL, '0', 'active', '2019-04-17 21:55:52', '2019-04-17 21:55:52'),
(31, 'Mugs', 'mugs', '3', 'types', NULL, '0', 'active', '2019-04-17 21:56:07', '2019-04-17 21:56:07'),
(32, 'Idols', 'idols', '3', 'types', NULL, '0', 'active', '2019-04-17 21:56:21', '2019-04-17 21:56:21'),
(33, 'Plants', 'plants', '3', 'types', NULL, '0', 'active', '2019-04-17 21:56:58', '2019-04-17 21:56:58'),
(34, 'Her', 'her', '3', 'others', NULL, '0', 'active', '2019-04-17 21:57:22', '2019-04-17 21:57:22'),
(35, 'Him', 'him', '3', 'others', NULL, '0', 'active', '2019-04-17 21:57:40', '2019-04-17 21:57:40'),
(36, 'Friend', 'friend', '3', 'others', NULL, '0', 'active', '2019-04-17 21:58:00', '2019-04-17 21:58:00'),
(37, 'Boy Friend', 'boy-friend', '3', 'others', NULL, '0', 'active', '2019-04-17 21:58:19', '2019-04-17 21:58:19'),
(38, 'Mother', 'mother', '3', 'others', NULL, '0', 'active', '2019-04-17 21:58:38', '2019-04-17 21:58:38'),
(39, 'Chocklate Bouquet', 'chocklate-bouquet', '4', 'types', NULL, '0', 'active', '2019-04-17 21:59:12', '2019-04-17 21:59:12'),
(40, 'Cadburry Chocklates', 'cadburry-chocklates', '4', 'types', NULL, '0', 'active', '2019-04-17 21:59:48', '2019-04-17 21:59:48'),
(41, 'Personalized Chocklates', 'personalized-chocklates', '4', 'types', NULL, '0', 'active', '2019-04-17 22:00:12', '2019-04-17 22:00:12'),
(42, 'Handmade Chocklates', 'handmade-chocklates', '4', 'types', NULL, '0', 'active', '2019-04-17 22:00:29', '2019-04-17 22:00:29'),
(43, 'All Jewellery', 'all-jewellery', '5', 'types', NULL, '0', 'active', '2019-04-17 22:01:05', '2019-04-17 22:01:05'),
(44, 'Fashion Jewellery', 'fashion-jewellery', '5', 'types', NULL, '0', 'active', '2019-04-17 22:01:27', '2019-04-17 22:01:27'),
(45, 'Traditional imitations', 'traditional-imitations', '5', 'types', NULL, '0', 'active', '2019-04-17 22:01:54', '2019-04-17 22:01:54'),
(46, 'Gold and Diamond', 'gold-and-diamond', '5', 'types', NULL, '0', 'active', '2019-04-17 22:02:27', '2019-04-17 22:02:27'),
(47, 'Gold coins', 'gold-coins', '4', 'types', NULL, '0', 'active', '2019-04-17 22:02:51', '2019-04-17 22:02:51'),
(48, 'Ear Rings', 'ear-rings', '5', 'types', NULL, '0', 'active', '2019-04-17 22:03:40', '2019-04-17 22:03:40'),
(49, 'Millets All', 'millets-all', '6', 'types', NULL, '0', 'active', '2019-04-17 22:04:56', '2019-04-17 22:04:56'),
(50, 'Sorghum', 'sorghum', '6', 'types', NULL, '0', 'active', '2019-04-17 22:05:46', '2019-04-17 22:05:46'),
(51, 'Barnyard millet (Oodhalu)', 'barnyard-millet-oodhalu-', '6', 'types', NULL, '0', 'active', '2019-04-17 22:06:19', '2019-04-17 22:06:19'),
(52, 'Foxtail millet (Navane)', 'foxtail-millet-navane-', '6', 'types', NULL, '0', 'active', '2019-04-17 22:06:48', '2019-04-17 22:06:48'),
(53, 'Little Millets', 'little-millets', '6', 'types', NULL, '0', 'active', '2019-04-17 22:07:04', '2019-04-17 22:07:04'),
(54, 'Sweets', 'sweets', '0', NULL, NULL, '0', 'active', '2019-04-17 22:08:09', '2019-04-17 22:08:09'),
(55, 'Dry Fruit Sweets', 'dry-fruit-sweets', '54', 'types', NULL, '0', 'active', '2019-04-17 22:13:08', '2019-04-17 22:13:08'),
(56, 'Candies', 'candies', '54', 'types', NULL, '0', 'active', '2019-04-17 22:13:33', '2019-04-17 22:13:33'),
(57, 'Snacks', 'snacks', '54', 'types', NULL, '0', 'active', '2019-04-17 22:13:54', '2019-04-17 22:13:54'),
(58, 'Namkeens', 'namkeens', '54', 'types', NULL, '0', 'active', '2019-04-17 22:14:05', '2019-04-17 22:14:05');


INSERT INTO `countries` (`country_id`, `country_name`, `country_code`, `country_phone_prefix`, `country_currency`, `country_language`, `country_status`, `created_at`, `updated_at`) VALUES
(1, 'India', 'IN', '+91', 'Rs.', 'IN', 'active', '2019-02-19 21:03:54', '2019-02-19 21:03:54');

INSERT INTO `users` (`id`, `name`, `email`, `role`, `mobile`, `gender`, `dob`, `image`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', 404, NULL, NULL, NULL, NULL, NULL, '$2y$10$Unhax.X4m8eLYd2fDS3/rOVgulRQr0B3DlsAEROvtBCl6C4v9y2LW', 'active', 'vGhkP4JmKFh4RpuKNA0pHq7jOXOrWeTj3MGPi5rAV5MIm4i5tMIordxbUENm', '2019-02-18 14:57:49', '2019-02-19 19:56:20');


ALTER TABLE `banners` ADD `banner_bg_image` VARCHAR(191) NULL DEFAULT NULL AFTER `banner_image`;

ALTER TABLE `products` ADD `p_meta_keywords` LONGTEXT NULL AFTER `p_status`, ADD `p_meta_description` LONGTEXT NULL AFTER `p_meta_keywords`;

ALTER TABLE `categories` ADD `category_meta_keywords` LONGTEXT NULL AFTER `category_order`, ADD `category_meta_description` LONGTEXT NULL AFTER `category_meta_keywords`;


ALTER TABLE `categories` ADD `category_meta_title` LONGTEXT NULL AFTER `category_order`;