ALTER TABLE `order_items`
ADD `oitem_product_sku_id` INT NULL AFTER `oitem_product_id`,
ADD `oitem_product_sku_vendor_price` INT NULL AFTER `oitem_product_sku_id`,
ADD `oitem_product_sku_site_price` INT NULL AFTER `oitem_product_sku_vendor_price`,
ADD `oitem_product_sku_sale_price` INT NULL AFTER `oitem_product_sku_site_price`;