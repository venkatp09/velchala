<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('coupon_id');
            $table->string('coupon_code')->nullable();
            $table->float('coupon_discount_value')->nullable();
            $table->string('coupon_discount_type')->nullable();
            $table->integer('coupon_quantity')->nullable();
            $table->integer('coupon_country')->nullable()->comment('From country table');
            $table->string('coupon_usage_type')->nullable();
            $table->date('coupon_expiry_date')->nullable();
            $table->string('coupon_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
