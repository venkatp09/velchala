<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->integer('order_user_id');
            $table->string('order_currency');
            $table->float('order_total_weight')->nullable();
            $table->float('order_delivery_charge')->nullable();

            $table->float('order_sub_total_price')->nullable();
            $table->float('order_shipping_price')->nullable();

            $table->string('order_coupon_code')->nullable();
            $table->float('order_coupon_discount')->nullable();
            $table->string('order_coupon_discount_type')->nullable();
            $table->float('order_coupon_discount_amount')->nullable();


            $table->float('order_total_price');

            $table->longText('order_delivery_address');
            $table->string('order_delivery_country')->nullable();
            $table->string('order_delivery_state')->nullable();
            $table->longText('order_billing_address')->nullable();

            $table->string('order_reference_number');
            $table->string('order_status')->default('pending');
            $table->string('order_note')->nullable();

            $table->string('order_payment_invoice_num')->nullable();
            $table->string('order_payment_transaction_id')->nullable();
            $table->string('order_payment_status')->nullable();
            $table->string('order_payment_mode')->nullable();
            $table->dateTime('order_payment_date')->nullable();
            $table->longText('order_payment_response_dump')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
