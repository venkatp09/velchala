<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earnings', function (Blueprint $table) {
            $table->bigIncrements('e_id');
            $table->integer('e_vendor_id')->nullable();
            $table->integer('e_orders_id')->nullable();
            $table->integer('e_product_id')->nullable();
            $table->integer('e_user_id')->nullable();
            $table->float('e_product_price')->nullable();
            $table->date('e_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earnings');
    }
}
