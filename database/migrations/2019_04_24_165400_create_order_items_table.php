<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('oitem_id');
            $table->integer('oitem_order_id');
            $table->string('oitem_product_id');
            $table->string('oitem_product_name');
            $table->text('oitem_product_options')->nullable();
            $table->integer('oitem_qty');
            $table->float('oitem_product_price');
            $table->float('oitem_delivery_charge')->nullable();
            $table->date('oitem_delivery_date')->nullable();
            $table->string('oitem_delivery_time')->nullable();
            $table->string('oitem_message')->nullable();
            $table->string('oitem_nameongift')->nullable();
            $table->float('oitem_discount_price')->nullable();
            $table->string('oitem_discount_type')->nullable()->comment('% or fixed price');
            $table->float('oitem_sub_total');
            $table->text('oitem_note')->nullable();
            $table->string('oitem_status')->default('pending');
            $table->string('oitem_payment_status')->default('pending');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
