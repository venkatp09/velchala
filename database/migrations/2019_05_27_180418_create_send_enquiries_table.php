<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_enquiries', function (Blueprint $table) {
            $table->bigIncrements('se_id');
            $table->string('se_name');
            $table->string('se_email');
            $table->string('se_phone');
            $table->text('se_message');
            $table->integer('se_product_id');
            $table->integer('se_user_id')->nullable();
            $table->integer('se_otp')->nullable();
            $table->string('se_ip_address')->nullable();
            $table->integer('se_status')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('send_enquiries');
    }




}
