<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('p_id');
            $table->string('p_unique_id')->nullable();
            $table->string('p_vendor_id');
            $table->string('p_name');
            $table->string('p_alias');
            $table->string('p_cat_id');
            $table->string('p_sub_cat_id')->nullable();
            $table->string('p_item_spl')->nullable();
            $table->string('p_main_image')->nullable();

            $table->string('p_dishtype')->nullable();

            $table->string('p_availability');



            $table->longText('p_overview')->nullable();
            $table->longText('p_specifications')->nullable();
            $table->longText('p_quality_care_info')->nullable();


            $table->longText('p_meta_title')->nullable();
            $table->longText('p_meta_keywords')->nullable();
            $table->longText('p_meta_description')->nullable();


            $table->integer('p_delivery_charges')->nullable();

            $table->integer('p_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
