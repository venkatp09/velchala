<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmmachethivantaOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ammachethivanta_orders', function (Blueprint $table) {
            $table->bigIncrements('ac_order_id');
            $table->integer('ac_order_user_id');
            $table->longText('ac_order_delivery_address');
            $table->date('ac_order_delivery_date')->nullable();
            $table->string('ac_order_delivery_time')->nullable();
            $table->string('ac_order_reference_number')->nullable();
            $table->float('ac_order_shipping_price')->nullable();
            $table->float('ac_order_price')->nullable();
            $table->float('ac_order_sub_total_price');
            $table->float('ac_order_total_price');
            $table->float('ac_order_total_weight');
            $table->string('ac_order_status')->default('PendingPaymnet');
            $table->text('ac_order_note')->nullable();
            $table->string('ac_order_payment_invoice_num')->nullable();
            $table->string('ac_order_payment_transaction_id')->nullable();
            $table->string('ac_order_payment_status')->nullable();
            $table->string('ac_order_payment_mode')->nullable();
            $table->date('ac_order_payment_date')->nullable();
            $table->longText('ac_order_payment_response_dump')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ammachethivanta_orders');
    }
}
